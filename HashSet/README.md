# Set (Abstract Data Type)

A set is an abstract data type and represents a collection of unique items, just like a mathematical set (e.g. { 1, 2, 3 }). A set cannot contain duplicates and the order of items is not relevant. So, both { 1, 2, 3 } and { 3, 2, 1 } are equal sets.

Use sets when you need super fast lookups against a unique list of items. Rather than retrieving elements from a set, a more typical scenario is to test whether elements **belong** to a set.

You can think about a set as a classroom with students where the arrangement (order) in which students sit during a lecture does not matter.

![gif](images/classroom.png)

### Characteristics of Sets:

- No order
- No indexation (get an element by index as in arrays)
- No duplicate values

Sets have four basic methods:

Name       | Functionality
-----------|----------
 `bool Add(item)` | Adds an **item** to the set and returns a value to indicate if the item was successfully added.
 `bool Contains(item)` | Returns a value to indicate if the **item** exists in the set.
 `bool Remove(item)` | Removes an **item** from the set and returns a value to indicate if the item was successfully removed. This method also returns `false` if the item has not been found in the set.
 `int Count` | Gets the number of elements contained in the set.

# HashSet\<T>

When an item is added through the `Add(item)` method, the `HashSet\<T>` stores it in some internal collection (e.g. array, list etc.). In order to know the position where the new item will be put an `index` is calculated.
Calculating the index can be done by going through the steps below:
1. The `hash code` of the new item is calculated. **Note: The `hash code` is just a large number.**
2. The `index` at which the new item will be added is calculated by using the following formula:
   > `index = newItem.GetHashCode() % internalCollection.Count`

For example:

![](images/hashing.png)

Unfortunately hash collisions can occur. A hash collision is when the hash code of two different objects is the same. One way of dealing with hash collisions is for buckets to store a list of items instead of a single item. The most common implementation uses linked list.

![](images/hash-collision.png)

The length of **buckets** doesn't limit the number of items that can be stored and this is due to the fact that we are combining an array with a linked list.

There's a metric that should be tracked while adding items - the **load factor**.

The load factor is **the number of items divided by the length of *buckets***. If the load factor goes beyond a predefined *threshold*, the **buckets** array is resized, **recalculating the hash for each value**.

> *It is up to the developer who is implementing the hash table to decide what the load factor should be.*  
>
> Keep in mind that as the load factor increases, collisions are more likely to occur which would degrade the overall performance.  
>
> In the absolute worst case, a hash table with only 1 bucket, the hash table behaves like a linked list with O(n) search, insertion, and deletion times.

### Asymptotic Complexity

Operation | Average | Worst
----------|----------|---------
Search | O(1)     | O(n)
Insert | O(1)     | O(n)
Delete | O(1)     | O(n)

#### Your Task

1. Implement all methods from the `IHashSet<T>` interface.
3. Congratulations!

#### Additional Practice

- [Jewels and Stones](https://leetcode.com/problems/jewels-and-stones/)
- [Set Mismatch](https://leetcode.com/problems/set-mismatch/)
- [Unique Number of Occurrences](https://leetcode.com/problems/unique-number-of-occurrences/)
