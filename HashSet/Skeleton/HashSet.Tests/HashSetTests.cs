using HashSet.Skeleton;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HashSet.Tests
{
	[TestClass]
	public class SetTests
	{
		[TestMethod]
		public void ShouldNotAdd_IfItemAlreadyInSet()
		{
			//Arrange
			IHashSet<string> set = new MyHashSet<string>();
			string item = "test";

			//Act
			set.Add(item);
			set.Add(item);

			//Assert
			Assert.AreEqual(1, set.Count);
			Assert.IsTrue(set.Contains(item));
		}
		
		[TestMethod]
		public void ShouldAdd_IfItemtNotInSet()
		{
			//Arrange
			IHashSet<string> set = new MyHashSet<string>();
			string item1 = "test1";
			string item2 = "test2";

			//Act
			set.Add(item1);
			set.Add(item2);

			//Assert
			Assert.AreEqual(2, set.Count);
			Assert.IsTrue(set.Contains(item1));
			Assert.IsTrue(set.Contains(item2));
		}

		[TestMethod]
		public void ShouldIncreaseCount_IfItemNotInSet()
		{
			//Arrange
			IHashSet<string> set = new MyHashSet<string>();
			int initialCount = set.Count;
			string item = "test";

			//Act
			set.Add(item);

			//Assert
			Assert.AreEqual(initialCount + 1, set.Count);
		}
		
		[TestMethod]
		public void ShouldNotChangeCount_IfAddingItemAlreadyInSet()
		{
			//Arrange
			IHashSet<string> set = new MyHashSet<string>();
			string item = "test";
			set.Add(item);
			int initialCount = set.Count;

			//Act
			set.Add(item);

			//Assert
			Assert.AreEqual(initialCount, set.Count);
		}
		
		[TestMethod]
		public void ShouldRemoveItem_IfItemExists()
		{
			//Arrange
			IHashSet<string> set = new MyHashSet<string>();
			string item = "test";
			set.Add(item);

			//Act
			set.Remove(item);

			//Assert
			Assert.IsFalse(set.Contains(item));
		}

		[TestMethod]
		public void ShouldDecreaseCount_IfItemRemoved()
		{
			//Arrange
			IHashSet<string> set = new MyHashSet<string>();
			string item = "test";
			set.Add(item);
			int initialValue = set.Count;

			//Act
			set.Remove(item);

			//Assert
			Assert.AreEqual(initialValue - 1, set.Count);
		}

		[TestMethod]
		public void ShouldNotChangeCount_IfItemNotInSet()
		{
			//Arrange
			IHashSet<string> set = new MyHashSet<string>();
			int initialValue = set.Count;

			//Act
			set.Remove("test");

			//Assert
			Assert.AreEqual(initialValue, set.Count);
		}

		[TestMethod]
		public void ShouldReturnTrue_IfItemExists()
		{
			//Arrange
			IHashSet<string> set = new MyHashSet<string>();
			string item = "test";
			set.Add(item);

			//Act & Assert
			Assert.IsTrue(set.Contains(item));
		}

		[TestMethod]
		public void ShouldReturnFalse_IfItemIsNotInSet()
		{
			//Arrange
			IHashSet<string> set = new MyHashSet<string>();

			//Act & Assert
			Assert.IsFalse(set.Contains("test"));
		}
	}

}
