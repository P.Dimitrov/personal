﻿namespace HashSet.Skeleton
{
    public interface IMyNode<T>
    {
        T Value { get; }

        MyNode<T> Next { get; }
    }
}