﻿using System.Collections.Generic;

namespace HashSet.Skeleton
{
    public interface IMyLinkedList<T> : IEnumerable<T>
    {
        MyNode<T> First { get; }
        MyNode<T> Last { get; }
        int Count { get; }
        void AddLast(MyNode<T> node);
        void AddFirst(MyNode<T> node);
        MyNode<T> Find(T value);
    }
}
