﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace HashSet.Skeleton
{
    public class MyLinkedList<T> : IMyLinkedList<T>
    {
        public MyNode<T> First { get; private set; }

        public MyNode<T> Last { get; private set; }

        public int Count { get; private set; }

        public void AddLast(MyNode<T> node)
        {
            if (First == null)
            {
                SetFirst(node);
            }
            else
            {
                SetLast(node);
            }
            Count++;
        }
        public void AddFirst(MyNode<T> node)
        {
            SetFirst(node);
            Count++;
        }
        public bool Remove(MyNode<T> node)
        {
            MyNode<T> current = this.First;
            if (current == node)
            {
                this.First = current.Next;
                Count--;
                return true;
            }
            while (current != null)
            {
                if (current.Next == node)
                {
                    current.Next = current.Next.Next;
                    Count--;
                    return true;
                }
            }
            return false;
        }
        public MyNode<T> Find(T value)
        {

            MyNode<T> current = this.First;

            while (current != null)
            {

                if (current.Value.Equals(value))
                {
                    return current;
                }

                current = current.Next;

            }

            return current;
        }

        public IEnumerator<MyNode<T>> GetEnumerator()
        {
            MyNode<T> current = First;

            while (current != null)
            {
                yield return current;
                current = current.Next;
            }
        }

        public IEnumerator<T> GetEnumeratorValue()
        {
            MyNode<T> current = First;

            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }
        public void SetLast(MyNode<T> node)
        {
            if (Count != 0)
                this.Last.Next = node;
            this.Last = node;
        }
        private void SetFirst(MyNode<T> newNode)
        {
            this.First = newNode;
            this.Last = newNode;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumeratorValue();
        }
    }
}

