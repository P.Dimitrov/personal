﻿namespace HashSet.Skeleton
{
    public class MyNode<T> : IMyNode<T>
    {
        public MyNode(T value)
        {
            this.Value = value;
        }

        public T Value { get; set; }

        public MyNode<T> Next { get; set; }

        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
