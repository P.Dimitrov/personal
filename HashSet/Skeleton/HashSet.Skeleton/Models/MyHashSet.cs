﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace HashSet.Skeleton
{
    public class MyHashSet<T> : IHashSet<T>
    {
        private MyLinkedList<T>[] buckets;
        private int bucketsLength = 100;

        public MyHashSet()
        {
            this.buckets = new MyLinkedList<T>[100];
        }

        public MyHashSet(int initialBucketsLength)
        {
            this.buckets = new MyLinkedList<T>[initialBucketsLength];
            bucketsLength = initialBucketsLength;
        }

        public int Count { get; private set; }

        public int Capacity { get => this.bucketsLength; }

        public bool Add(T item)
        {
            if (this.Contains(item)) // Does item exist in the hashset
            {
                return false;
            }

            int index = Math.Abs(item.GetHashCode() % this.buckets.Length); // gets index

            if (buckets[index] == null)
            {
                buckets[index] = new MyLinkedList<T>();
            }

            MyNode<T> newNode = new MyNode<T>(item);
            buckets[index].AddLast(newNode);
            Count++;

            if (Count == bucketsLength / 2) // Load Factor
            {
                MyLinkedList<T>[] tempHashSet = new MyLinkedList<T>[bucketsLength * 2];
                foreach (var hashLinkedList in this.buckets)
                {
                    if (hashLinkedList != null)
                    {
                        foreach (var node in hashLinkedList)
                        {
                            int newIndex = Math.Abs(node.GetHashCode() % tempHashSet.Length);

                            if (tempHashSet[newIndex] == null)
                            {
                                tempHashSet[newIndex] = new MyLinkedList<T>();
                                tempHashSet[newIndex].AddFirst(node);
                            }

                            else
                            {
                                tempHashSet[newIndex].AddLast(node);
                            }
                        }
                    }
                }
                bucketsLength *= 2;
                this.buckets = tempHashSet;
            }
            return true;
        }

        public bool Contains(T item)
        {
            int index = Math.Abs(item.GetHashCode() % this.buckets.Length);
            if (this.buckets[index] != null)
            {
                foreach (var node in this.buckets[index])
                {
                    if (node.Value.Equals(item))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool Remove(T item)
        {
            int index = Math.Abs(item.GetHashCode() % this.buckets.Length);

            if (this.buckets[index] != null)
            {
                foreach (var node in this.buckets[index])
                {
                    if (node.Value.Equals(item))
                    {
                        this.buckets[index].Remove(node);
                        Count--;
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
