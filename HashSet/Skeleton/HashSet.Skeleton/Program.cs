﻿using System.Diagnostics;

namespace HashSet.Skeleton
{
	class Program
	{
		static void Main()
		{
			IHashSet<string> hashSet = new MyHashSet<string>();

			Debug.Assert(hashSet.Add("Joe") == true, "Adding Joe is not working correctly");
			Debug.Assert(hashSet.Add("Amy") == true, "Adding Amy is not working correctly");
			Debug.Assert(hashSet.Count == 2, "Count must be equal to 2 at this point");
			Debug.Assert(hashSet.Add("Joe") == false, "Joe already exists");
			Debug.Assert(hashSet.Count == 2, "Count should be 2 at this point");
			hashSet = new MyHashSet<string>(initialBucketsLength: 5);
			hashSet.Add("THE END");
		}
	}
}
