﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of Tag entity.
    /// </summary>
    public class TagConfiguration : IEntityTypeConfiguration<Tag>
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder.HasKey(t => t.Id);

            builder.Property(t => t.Name)
                .IsRequired();

            builder.HasMany(r => r.ReportTags)
                .WithOne(rt => rt.Tag)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
