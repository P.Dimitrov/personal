﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of User entity.
    /// </summary>
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);

            builder.Property(u => u.FirstName)
                .IsRequired();

            builder.Property(u => u.LastName)
                .IsRequired();

            builder.Property(u => u.Email)
                .IsRequired();

            builder.HasMany(u => u.AuthoredReports)
                .WithOne(au => au.Author)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(u => u.DownloadedReports)
                .WithOne(dr => dr.User)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(u => u.Subscriptions)
                .WithOne(dr => dr.User)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
