﻿using InsightHub.Blob.Service.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.CustomExceptions.ExceptionMessages;
using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services
{
    /// <summary>
    /// A class that implements functionalities for Report model - 
    ///  - GetReport
    ///   - GetAllReports
    ///    - FeatureReport
    ///     - UnfeatureReport
    ///      - ApproveReport
    ///       - GetAllAuthoredReports
    ///        - GetAllApprovedReports
    ///         - GetAllPendingReports
    ///          - GetFiveNewestReports
    ///           - GetFeaturedReports
    ///            - GetFiveMostDownloadedReports
    ///             - CreateReport
    ///              - DownloadReport
    ///               - DeleteReport
    ///                - UpdateReport
    ///                 - GetAllUserDownloadedReports
    /// </summary>
    public class ReportService : IReportService
    {
        private readonly InsightHubContext context;
        private readonly ITagService tagService;
        private readonly IBlobService blobService;
        private readonly IEmailSender mailSender;
        private readonly IIndustryService industryService;
        private InsightHubContext assertContext;

        public ReportService(InsightHubContext context, ITagService tagService, IBlobService blobService, 
            IEmailSender mailSender, IIndustryService industryService)
        {
            this.context = context;
            this.tagService = tagService;
            this.blobService = blobService;
            this.mailSender = mailSender;
            this.industryService = industryService;
        }

        public ReportService(InsightHubContext assertContext)
        {
            this.assertContext = assertContext;
        }

        public async Task<ReportDTO> GetReportAsync(int? id)
        {
            var report = await this.context.Reports
                .Include(r => r.Industry)
                .Include(r => r.Author)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .FirstOrDefaultAsync(r => r.Id == id);

            if (report == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ReportNull);
            }

            var reportDTO = new ReportDTO(report);

            return reportDTO;
        }

        public async Task<ICollection<ReportDTO>> GetAllReportsAsync()
        {
            var reports = await this.context.Reports
                .Include(r => r.Industry)
                .Include(r => r.Author)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Select(r => new ReportDTO(r)).ToListAsync();

            return reports;
        }
        public async Task<ReportDTO> FeatureReportAsync(int id)
        {
            var report = await this.context.Reports
                .Include(r => r.Industry)
                .Include(r => r.Author)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .FirstOrDefaultAsync(r => r.Id == id);

            if (report == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ReportNull);
            }

            report.IsFeatured = true;
            await this.context.SaveChangesAsync();

            var reportDTO = new ReportDTO(report);

            return reportDTO;
        }

        public async Task<ReportDTO> UnfeatureReportAsync(int id)
        {
            var report = await this.context.Reports
                .Include(r => r.Industry)
                .Include(r => r.Author)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .FirstOrDefaultAsync(r => r.Id == id);

            if (report == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ReportNull);
            }

            report.IsFeatured = false;
            await this.context.SaveChangesAsync();

            var reportDTO = new ReportDTO(report);

            return reportDTO;
        }

        public async Task<ReportDTO> ApproveReportAsync(int id)
        {
            var report = await this.context.Reports
                .Include(r => r.Industry)
                .ThenInclude(i => i.Subscribers)
                .ThenInclude(ui => ui.User)
                .Include(r => r.Author)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .FirstOrDefaultAsync(r => r.Id == id);

            if (report == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ReportNull);
            }

            report.IsApproved = true;
            await this.context.SaveChangesAsync();

            var subscribers = string.Join(", ", report.Industry.Subscribers.Select(ui => ui.User.UserName));
            if (subscribers != "")
            {
                await mailSender.SendEmailAsync(subscribers, report.Industry.Name, report.Name);
            }

            var reportDTO = new ReportDTO(report);

            return reportDTO;
        }
        public async Task<ICollection<ReportDTO>> GetAllAuthoredReportsAsync(string userName)
        {
            var reports = await this.context.Reports
                .Include(ur => ur.Author)
                .Include(r => r.Industry)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Where(r => r.Author.UserName == userName)
                .Where(r => r.IsApproved)
                .Select(r => new ReportDTO(r)).ToListAsync();

            return reports;
        }

        public async Task<ICollection<ReportDTO>> GetAllApprovedReportsAsync()
        {
            var reports = await this.context.Reports
                .Include(r => r.Industry)
                .Include(r => r.Author)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Where(r => r.IsApproved == true)
                .Select(r => new ReportDTO(r)).ToListAsync();

            return reports;
        }

        public async Task<ICollection<ReportDTO>> GetAllPendingReportsAsync()
        {
            var reports = await this.context.Reports
                .Include(r => r.Industry)
                .Include(r => r.Author)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Where(r => r.IsApproved == false)
                .Select(r => new ReportDTO(r)).ToListAsync();

            return reports;
        }
        public async Task<ICollection<ReportDTO>> GetFiveNewestReportsAsync()
        {
            var reports = await this.context.Reports
                .Include(r => r.Industry)
                .Include(r => r.Author)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .OrderByDescending(r => r.CreatedOn)
                .Take(5)
                .Select(r => new ReportDTO(r))
                .ToListAsync();

            return reports;
        }

        public async Task<ICollection<ReportDTO>> GetFeaturedReportsAsync()
        {
            var reports = await this.context.Reports
                .Include(r => r.Industry)
                .Include(r => r.Author)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Where(r => r.IsFeatured == true)
                .Take(5)
                .Select(r => new ReportDTO(r))
                .ToListAsync();

            return reports;
        }

        public async Task<ICollection<ReportDTO>> GetFiveMostDownloadedReportsAsync()
        {
            var reports = await this.context.Reports
                .Include(r => r.Industry)
                .Include(r => r.Author)
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .OrderByDescending(r => r.TimesDownloaded)
                .Take(5)
                .Select(r => new ReportDTO(r))
                .ToListAsync();

            return reports;
        }

        public async Task<ReportDTO> CreateReportAsync(ReportDTO reportDTO)
        {
            if (reportDTO == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ReportNull);
            }

            if (!reportDTO.Tags.Any())
            {
                throw new BusinessLogicException(ExceptionMessages.ReportNull);
            }

            var industry = await this.context.Industries
                .Include(i => i.Subscribers)
                .ThenInclude(ui => ui.User)
                .FirstOrDefaultAsync(i => i.Name == reportDTO.IndustryName);

            if (industry == null)
            {
                throw new BusinessLogicException(ExceptionMessages.IndustryNull);
            }

            var author = await this.context.Users.
                FirstOrDefaultAsync(u => u.Id == reportDTO.AuthorID);

            if (author == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var report = new Report
            {
                Name = reportDTO.Name,
                Description = reportDTO.Description,
                Content = reportDTO.FileName,
                IndustryID = industry.Id,
                Industry = industry,
                Author = author,
                AuthorID = author.Id
            };

            foreach (var tagDTO in reportDTO.Tags)
            {
                var tag = await this.context.Tags.FirstOrDefaultAsync(tag => tag.Name == tagDTO.Name);

                if (!reportDTO.Tags.Any())
                {
                    throw new BusinessLogicException(ExceptionMessages.ReportNull);
                }

                var reportTag = new ReportTag
                {
                    TagId = tag.Id,
                    Tag = tag,
                    Report = report
                };
                    report.ReportTags.Add(reportTag);

                //TODO: initialize list of authored reports in model
                if (report.Author.AuthoredReports == null)
                {
                    report.Author.AuthoredReports = new List<Report>();
                    report.Author.AuthoredReports.Add(report);
                }
                else
                {
                    report.Author.AuthoredReports.Add(report);
                }

            }
            await this.context.Reports.AddAsync(report);
            await this.context.SaveChangesAsync();

            await this.blobService.UploadFileAsync(reportDTO.File, report.Id.ToString());
            
            var newReportDTO = new ReportDTO(report);
            return newReportDTO;
        }

        public async Task<MemoryStream> DownloadReportAsync(int id, int userID)
        {
            var report = await this.context.Reports
                    .FirstOrDefaultAsync(r => r.Id == id);

            if (report == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ReportNull);
            }

            var user = await this.context.Users
                    .FirstOrDefaultAsync(u => u.Id == userID);

            var memory = await this.blobService.DownloadFileAsync(report.Id.ToString(), report.Content);

            if (!await this.context.UserReports.AnyAsync(r => r.ReportId == id & r.UserId == userID))
            {
                var userDownload = new UserReport
                {
                    Report = report,
                    ReportId = report.Id,
                    User = user,
                    UserId = user.Id
                };
                await context.UserReports.AddAsync(userDownload);
            }

            report.TimesDownloaded++;
            await context.SaveChangesAsync();

            return memory;
        }
        public async Task<bool> DeleteReportAsync(int id)
        {
            var report = await this.context.Reports
                .Include(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(r => r.Author)
                .ThenInclude(a => a.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .FirstOrDefaultAsync(u => u.Id == id);

            if (report == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ReportNull);
            }

            if (report.ReportTags != null)
            {
                this.context.ReportTags.RemoveRange(report.ReportTags);
            }

            await this.blobService.DeleteFileAsync(report.Id.ToString(), report.Content);
            this.context.Reports.Remove(report);
            await this.context.SaveChangesAsync();

            return true;
        }
        public async Task<ReportDTO> UpdateReportAsync(int id, ReportDTO reportDTO)
        {
            var report = await this.context.Reports
                .Include(a => a.Author)
                .Include(i => i.Industry)
                .Include(rt => rt.ReportTags)
                .FirstOrDefaultAsync(r => r.Id == id);

            if (report == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ReportNull);
            }

            var industry = await this.context.Industries.
                FirstOrDefaultAsync(i => i.Name == reportDTO.IndustryName);

            if (industry == null)
            {
                throw new BusinessLogicException(ExceptionMessages.IndustryNull);
            }

            var author = await this.context.Users.
                FirstOrDefaultAsync(u => u.Id == reportDTO.AuthorID);

            if (author == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            try
            {
                report.Name = reportDTO.Name;
                report.Description = reportDTO.Description;
                report.IndustryID = industry.Id;
                report.Industry = industry;
                report.Author = author;
                report.AuthorID = author.Id;

                report.ReportTags.Clear();

                foreach (var tagDTO in reportDTO.Tags)
                {
                    var tag = await this.context.Tags.FirstOrDefaultAsync(tag => tag.Name == tagDTO.Name);

                    var reportTag = new ReportTag
                    {
                        TagId = tag.Id,
                        Tag = tag,
                        Report = report
                    };
                    report.ReportTags.Add(reportTag);
                }

                if (reportDTO.File != null)
                {
                    await this.blobService.DeleteFileAsync(report.Id.ToString(), report.Content);

                    await this.blobService.UploadFileAsync(reportDTO.File, report.Id.ToString());

                    report.Content = reportDTO.File.FileName;
                }
                var newReportDTO = new ReportDTO(report);
                    
                await this.context.SaveChangesAsync();
                return newReportDTO;
            }
            catch (Exception)
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<ICollection<ReportDTO>> GetAllUserDownloadedReportsAsync(string userName)
        {
            var reports = await this.context.UserReports
                .Include(ur => ur.User)
                .Include(ur => ur.Report)
                .ThenInclude(r => r.Industry)
                .Include(ur => ur.Report)
                .ThenInclude(r => r.Author)
                .Include(ur => ur.Report)
                .ThenInclude(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Where(r => r.User.UserName == userName)
                .Select(ur => new ReportDTO(ur.Report)).ToListAsync();

             return reports;
        }
    }
}
