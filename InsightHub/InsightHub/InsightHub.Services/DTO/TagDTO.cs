﻿using InsightHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InsightHub.Services.DTO
{
    /// <summary>
    /// A class that maps the Tag model into Tag DTO.
    /// </summary>
    public class TagDTO
    {
        public TagDTO()
        {

        }

        public TagDTO(Tag tag)
        {
            Id = tag.Id;
            Name = tag.Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
