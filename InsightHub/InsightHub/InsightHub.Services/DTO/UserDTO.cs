﻿using InsightHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InsightHub.Services.DTO
{
    /// <summary>
    /// A class that maps the User model into User DTO.
    /// </summary>
    public class UserDTO
    {
        public UserDTO()
        {

        }

        public UserDTO(User user)
        {
            Id = user.Id;
            UserName = user.UserName;
            FirstName = user.FirstName;
            LastName = user.LastName;
            AuthoredReports = user.AuthoredReports.Select(ar => new ReportDTO(ar)).ToList();
            DownloadedReports = user.DownloadedReports.Select(dr => new ReportDTO(dr.Report)).ToList();
            Subscriptions = user.Subscriptions.Select(ui => ui.Industry.Name).ToList();
            IsApproved = user.IsApproved;
        }

        public int Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public ICollection<ReportDTO> AuthoredReports { get; set; }

        public ICollection<ReportDTO> DownloadedReports { get; set; }

        public ICollection<string> Subscriptions { get; set; }

        public bool IsApproved { get; set; }
    }
}
