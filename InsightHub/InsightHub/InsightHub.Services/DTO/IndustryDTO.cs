﻿using InsightHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace InsightHub.Services.DTO
{
    /// <summary>
    /// A class that maps the Industry model into Industry DTO.
    /// </summary>
    public class IndustryDTO
    {
        public IndustryDTO()
        {

        }
        
        public IndustryDTO(Industry industry)
        {
            Id = industry.Id;
            Name = industry.Name;
        }

        public int Id { get; set; }

        public string Name { get; set; }

    }
}
