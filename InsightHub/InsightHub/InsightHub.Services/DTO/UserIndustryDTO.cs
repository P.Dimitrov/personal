﻿using InsightHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InsightHub.Services.DTO
{
    /// <summary>
    /// A class that maps the UserIndustry model into UserIndustry DTO.
    /// </summary>
    public class UserIndustryDTO
    {
        public UserIndustryDTO(Industry industry)
        {
            Id = industry.Id;
            Name = industry.Name;
            Subscribers = industry.Subscribers.Select(ui => ui.User.UserName);
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<string> Subscribers { get; set; }
    }
}
