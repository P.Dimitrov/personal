﻿using Microsoft.AspNetCore.Identity.UI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using InsightHub.Models;
using InsightHub.Services.Contracts;

namespace InsightHub.Services
{
    public class EmailSender : IEmailSender, IUserActivationMailSender, IUserRegistrationMailSender
    {
        public Task SendEmailAsync(string emails, string industry, string reportTitle)
        {

            MailAddress to = new MailAddress("noreplyinsighthub@gmail.com");
            MailAddress from = new MailAddress("noreplyinsighthub@gmail.com");

            MailMessage message = new MailMessage(from, to);
            message.Subject = $"New {industry} report uploaded";
            message.Body = 
                "Dear Sir/Ma'am,"
                + Environment.NewLine + Environment.NewLine + 
                $"A new {industry} report titled {reportTitle} has been uploaded"
                + Environment.NewLine + Environment.NewLine + 
                "Kind Regards,"
                + Environment.NewLine + 
                "InsightHub Team";

            message.Bcc.Add(new MailAddress(emails));

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("noreplyinsighthub@gmail.com", "insighthub123*"),
                EnableSsl = true
            };
            // code in brackets above needed if authentication required 

            try
            {
                client.Send(message);
            }
            catch (SmtpException ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return Task.CompletedTask;
        }

        public Task SendUserRegistrationEmailAsync(string email)
        {

            MailAddress to = new MailAddress(email);
            MailAddress from = new MailAddress("noreplyinsighthub@gmail.com");

            MailMessage message = new MailMessage(from, to);
            message.Subject = $"InsightHub user account created";
            message.Body = 
                "Dear Sir/Ma'am,"
                + Environment.NewLine + Environment.NewLine + 
                $"Thank you for registering on InsightHub. Please note that your account must be approved by an administrator, before you can use it"
                + Environment.NewLine + Environment.NewLine + 
                "Kind Regards," 
                + Environment.NewLine + 
                "InsightHub Team";

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("noreplyinsighthub@gmail.com", "insighthub123*"),
                EnableSsl = true
            };
            // code in brackets above needed if authentication required 

            try
            {
                client.Send(message);
            }
            catch (SmtpException ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return Task.CompletedTask;
        }

        public Task SendUserActivationEmailAsync(string email)
        {
            MailAddress to = new MailAddress(email);
            MailAddress from = new MailAddress("noreplyinsighthub@gmail.com");

            MailMessage message = new MailMessage(from, to);
            message.Subject = $"InsightHub user account activated";
            message.Body = "Dear Sir/Ma'am,"
                + Environment.NewLine + Environment.NewLine + 
                $"Your InsightHub account has been activated"
                + Environment.NewLine + Environment.NewLine + 
                "Kind Regards,"
                + Environment.NewLine + 
                "InsightHub Team";

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("noreplyinsighthub@gmail.com", "insighthub123*"),
                EnableSsl = true
            };
            // code in brackets above needed if authentication required 

            try
            {
                client.Send(message);
            }
            catch (SmtpException ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return Task.CompletedTask;
        }
    }
}