﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Services.CustomExceptions
{
    /// <summary>
    /// A class for custom exception messages that implements the Exception class.
    /// </summary>
    public class BusinessLogicException : Exception
    {
        public BusinessLogicException()
        {

        }

        public BusinessLogicException(string message)
            : base(message)
        {

        }

        public BusinessLogicException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
