﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that contains the properties of Industry entity.
    /// </summary>
    public class Industry
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Report> Reports { get; set; }
        public ICollection<UserIndustry> Subscribers { get; set; }
    }
}
