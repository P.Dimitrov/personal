﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that contains the properties of Report entity.
    /// </summary>
    public class Report
    {
        public Report()
        {
            this.ReportTags = new List<ReportTag>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int IndustryID { get; set; }
        public Industry Industry { get; set; }
        public ICollection<ReportTag> ReportTags { get; set; }
        public string Content { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime? ChangedOn { get; set; }
        public bool IsApproved { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public bool IsFeatured { get; set; }
        public int AuthorID { get; set; }
        public User Author { get; set; }
        public ICollection<UserReport> Customers { get; set; }
        public int TimesDownloaded { get; set; }
    }
}
