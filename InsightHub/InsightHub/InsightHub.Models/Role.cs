﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that implements IdentityRole.
    /// </summary>
    public class Role : IdentityRole<int>
    {
    }
}
