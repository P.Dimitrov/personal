﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Blob.Service.Contracts
{
    /// <summary>
    /// Interface for the Blob Service to access properties of the class - DownloadFile, UploadFIle and DeleteFile.
    /// </summary>
    public interface IBlobService
    {
        Task<MemoryStream> DownloadFileAsync(string name, string oldName);
        Task UploadFileAsync(IFormFile file, string newFolder);
        Task DeleteFileAsync(string id, string fileTitle);
    }
}
