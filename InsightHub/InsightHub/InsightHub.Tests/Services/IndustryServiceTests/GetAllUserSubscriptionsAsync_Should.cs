﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.IndustryServiceTests
{
    [TestClass]
    public class GetAllUserSubscriptionsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectSubscriptions_When_ValidParamsAreGiven()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ReturnCorrectSubscriptions_When_ValidParamsAreGiven));

            var user1 = new User
            {
                UserName = "John",
            };
            var industry1 = new Industry
            {
                Name = "Financial Services"
            };
            var industry2 = new Industry
            {
                Name = "Retail"
            };
            var industry3 = new Industry
            {
                Name = "Healthcare"
            };
            var subscription1 = new UserIndustry
            { 
                IndustryId = 1,
                UserId = 1
            };
            var subscription2 = new UserIndustry
            {
                IndustryId = 2,
                UserId = 1
            };
            var subscription3 = new UserIndustry
            {
                IndustryId = 3,
                UserId = 1
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddRangeAsync(industry1, industry2, industry3);
                await arrangeContext.Users.AddAsync(user1);
                await arrangeContext.UserIndustries.AddRangeAsync(subscription1, subscription2, subscription3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.GetAllUserSubscriptionsAsync("John");
                var resultList = result.ToList();

                Assert.AreEqual(industry1.Name, resultList[0].Name);
                Assert.AreEqual(industry2.Name, resultList[1].Name);
                Assert.AreEqual(industry3.Name, resultList[2].Name);
            }
        }
    }
}
