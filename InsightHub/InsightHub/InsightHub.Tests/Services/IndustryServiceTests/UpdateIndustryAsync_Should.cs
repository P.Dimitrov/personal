﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.IndustryServiceTests
{
    [TestClass]
    public class UpdateIndustryAsync_Should
    {
        [TestMethod]
        public async Task UpdateIndustry_When_ValidParamsGiven()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(UpdateIndustry_When_ValidParamsGiven));

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);

                var industry = new IndustryDTO
                {
                    Name = "Consumer Electronics",
                    Id = 1
                };

                var updated = await sut.UpdateIndustryAsync(industry);

                Assert.AreEqual(industry.Name, updated.Name);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowException_When_ParamsAreNotValid()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowException_When_ParamsAreNotValid));

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);

                var updated = await sut.UpdateIndustryAsync(null);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowException_When_IndustryNotFound()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowException_When_ParamsAreNotValid));

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);

                var industry = new IndustryDTO
                {
                    Name = "Consumer Electronics",
                    Id = 2
                };

                var updated = await sut.UpdateIndustryAsync(industry);

            }
        }
    }
}
