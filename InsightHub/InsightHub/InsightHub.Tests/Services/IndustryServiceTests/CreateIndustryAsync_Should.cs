﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.IndustryServiceTests
{
    [TestClass]
    public class CreateIndustryAsync_Should
    {
        [TestMethod]
        public async Task CreateNewIndustry_When_ParamsAreCorrect()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(CreateNewIndustry_When_ParamsAreCorrect));

            var industry = new IndustryDTO
            {
                Name = "Financial Services"
            };

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.CreateIndustryAsync(industry);

                Assert.AreEqual(industry.Name, result.Name);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowException_When_ParamsAreNull()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowException_When_ParamsAreNull));

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.CreateIndustryAsync(null);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowException_When_ParamNameIsNull()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowException_When_ParamNameIsNull));

            var industry = new IndustryDTO();

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.CreateIndustryAsync(industry);
            }
        }
    }
}
