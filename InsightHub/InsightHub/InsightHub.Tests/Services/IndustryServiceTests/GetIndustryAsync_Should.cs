﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.IndustryServiceTests
{
    [TestClass]
    public class GetIndustryAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectIndustry_When_ValidParamsAreGiven()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ReturnCorrectIndustry_When_ValidParamsAreGiven));

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };
            var industry2 = new Industry
            {
                Name = "Retail"
            };
            var industry3 = new Industry
            {
                Name = "Healthcare"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.Industries.AddAsync(industry2);
                await arrangeContext.Industries.AddAsync(industry3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.GetIndustryAsync(2);

                Assert.AreEqual(industry2.Id, result.Id);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowException_When_IndustryParamIsNotValid()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowException_When_IndustryParamIsNotValid));

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };
            var industry2 = new Industry
            {
                Name = "Retail"
            };
            var industry3 = new Industry
            {
                Name = "Healthcare"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.Industries.AddAsync(industry2);
                await arrangeContext.Industries.AddAsync(industry3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.GetIndustryAsync(5);
            }
        }
    }
}
