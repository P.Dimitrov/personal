﻿using InsightHub.Blob.Service.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.ReportServiceTests
{
    [TestClass]
    public class DeleteReportAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_ReportIsDeleted()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnTrue_When_ReportIsDeleted));
            var mockTagService = new Mock<ITagService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "Finance"
            };
            var industryDTO = new IndustryDTO
            {
                Id = 1,
                Name = "Finance"
            };
            var author = new User
            {
                UserName = "John Doe",
                AuthoredReports = null
            };
            var authorDTO = new UserDTO
            {
                Id = 1,
                UserName = "John Doe"
            };
            var tag = new Tag
            {
                Name = "ÏT Services"
            };
            var tagDTO = new TagDTO
            {
                Id = 1,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 1 } };

            var report = new Report
            {
                Name = "Rech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
            };
            var reportDTO = new ReportDTO
            {
                Id = 1,
                Name = "Rech Report",
                Description = "Reports' description",
                Industry = industryDTO,
                IndustryID = industryDTO.Id,
                FileName = "FileName",
                Author = "John Doe",
                AuthorID = 1,
                Tags = new List<TagDTO> { new TagDTO { Name = tagDTO.Name, Id = tagDTO.Id } }
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.Users.AddAsync(author);
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();

            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);
                var result = await sut.DeleteReportAsync(1);

                Assert.IsTrue(result);
            }
        }


        [TestMethod]
        public async Task ThrowWhen_DeletedReportNotFoundById()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_DeletedReportNotFoundById));
            var mockTagService = new Mock<ITagService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "Finance"
            };
            var author = new User
            {
                UserName = "John Doe"
            };
            var tag = new Tag
            {
                Id = 3,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 3 } };

            var report = new Report
            {
                Name = "Tech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                //Act & Assert
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);
                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.DeleteReportAsync(10));
            }
        }
    }
}
