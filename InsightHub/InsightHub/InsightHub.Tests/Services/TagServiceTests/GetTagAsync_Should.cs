﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.TagServiceTests
{
    [TestClass]
    public class GetTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTag_When_ValidParamsAreGiven()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectTag_When_ValidParamsAreGiven));
            
            var tag = new Tag
            {
                Name = "Media"
            };
            var tag2 = new Tag
            {
                Name = "IT Services"
            };
            var tag3 = new Tag
            {
                Name = "Finance"
            };
            var tagDTO = new TagDTO
            {
                Name = "Media"
            };
            var tagDTO2 = new TagDTO
            {
                Name = "IT Services"
            };
            var tagDTO3 = new TagDTO
            {
                Name = "Finance"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Tags.AddAsync(tag2);
                await arrangeContext.Tags.AddAsync(tag3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                var result = await sut.GetTagAsync(2);

                Assert.AreEqual(tag2.Id, result.Id);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_TagNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_TagNotFound));

            var tag2 = new Tag
            {
                Name = "IT Services"
            };

            var tagDTO2 = new TagDTO
            {
                Name = "IT Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag2);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                //Act & Assert
                var sut = new TagService(assertContext);
                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.GetTagAsync(10));
            }
        }
    }
}
