﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTO;
using InsightHub.Web.ApiControllers.Helpers;
using InsightHub.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.ApiControllers
{
    /// <summary>
    /// API Controller that:
    ///  - provides information for a list of all tags
    ///   - provides information for a single tag by Id
    ///    - creates a new tag
    ///     - updates a current tag
    ///      - deletes a tag
    /// </summary>
    [Route("api/tags")]
    [ApiController]
    [Authorize(AuthenticationSchemes = InsightHubJWTTokens.AuthSchemes)]
    public class TagsApiController : ControllerBase
    {
        private readonly ITagService tagService;

        public TagsApiController(ITagService tagService)
        {
            this.tagService = tagService;
        }

        // GET: api/TagsApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TagViewModel>>> GetTags()
        {
            try
            {
                var tags = await this.tagService.GetAllTagsAsync();
                var tagViewModels = tags.Select(i => new TagViewModel(i)).ToList();

                return Ok(tagViewModels);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // GET: api/TagsApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TagViewModel>> GetTag(int id)
        {
            try
            {
                var tag = await this.tagService.GetTagAsync(id);
                var tagViewModel = new TagViewModel(tag);

                return Ok(tagViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: api/TagsApi/5

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> PutTag(int id, [FromBody]TagViewModel tag)
        {
            try
            {
                var tagDto = new TagDTO
                {
                    Name = tag.Name,
                    Id = id
                };

                var tagDtoToReturn = await this.tagService.UpdateTagAsync(id, tagDto);

                if (tagDtoToReturn == null)
                {
                    return NotFound();
                }

                var tagViewModel = new TagViewModel(tagDtoToReturn);

                return Ok(tagViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // POST: api/TagsApi
        [HttpPost]
        public async Task<IActionResult> PostTag(TagViewModel tag)
        {
            try
            {
                var tagDto = new TagDTO
                {
                    Name = tag.Name
                };

                tagDto = await this.tagService.CreateTagAsync(tagDto);

                var tagViewModel = new TagViewModel(tagDto);

                return CreatedAtAction("GetTag", new { id = tagViewModel.Id });
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: api/TagsApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TagViewModel>> DeleteTag(int id)
        {
            var tag = await this.tagService.GetTagAsync(id);

            if (tag == null)
            {
                return NotFound();
            }

            var tagViewModel = new TagViewModel(tag);

            await this.tagService.DeleteTagAsync(id);

            return tagViewModel;
        }

    }
}