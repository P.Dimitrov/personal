﻿using InsightHub.Models;
using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Areas.Author.Models
{
    /// <summary>
    /// A class that maps ReportDTO into Report View Model for Author area.
    /// </summary>
    public class AuthorReportViewModel
    {
        public AuthorReportViewModel()
        {
            TagName = new List<string>();
        }
        public AuthorReportViewModel(ReportDTO reportDTO)
        {

            Id = reportDTO.Id;
            Name = reportDTO.Name;
            Description = reportDTO.Description;
            IndustryID = reportDTO.Industry.Id;
            Industry = reportDTO.Industry;
            Author = reportDTO.Author;
            FileName = reportDTO.FileName;
            TagName = reportDTO.Tags.Select(tagId => tagId.Name).ToList();
            TimesDownloaded = reportDTO.TimesDownloaded;
            File = reportDTO.File;
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 10)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        [Display(Name = "Description")]
        public string Description { get; set; }


        public int IndustryID { get; set; }

        [Required]
        [Display(Name = "Industry")]
        public string IndustryName { get; set; }

        public IndustryDTO Industry { get; set; }

        public int AuthorID { get; set; }

        public string Author { get; set; }

        [Required]
        [Display(Name = "Tags")]
        public ICollection<string> TagName { get; set; }
        public string FileName { get; set; }

        public int TimesDownloaded { get; set; }

        public IFormFile File { get; set; }
    }
}
