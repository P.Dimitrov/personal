﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Areas.Admin.Models
{
    /// <summary>
    /// A class that maps UserDTO into User View Model for Admin area.
    /// </summary>
    public class UserViewModel
    {
        public UserViewModel(UserDTO user)
        {
            UserName = user.UserName;
            FirstName = user.FirstName;
            LastName = user.LastName;
            IsApproved = user.IsApproved;
            Id = user.Id;
        }

        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsApproved { get; set; }
        public int Id { get; set; }
    }
}
