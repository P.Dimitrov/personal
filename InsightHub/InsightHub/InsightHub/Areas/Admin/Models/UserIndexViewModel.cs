﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Areas.Admin.Models
{
    /// <summary>
    /// A class that maps UserDTO into User Index View Model for Admin area.
    /// </summary>
    public class UserIndexViewModel
    {
        public UserIndexViewModel(UserDTO user)
        {
            UserName = user.UserName;
            Id = user.Id;
        }

        public string UserName { get; set; }

        public int Id { get; set; }
    }
}
