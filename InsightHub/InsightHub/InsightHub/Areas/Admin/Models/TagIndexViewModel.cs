﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Areas.Admin.Models
{
    public class TagIndexViewModel
    {
        public TagIndexViewModel()
        {

        }

        public TagIndexViewModel(TagDTO tagDTO)
        {
            Id = tagDTO.Id;
            Name = tagDTO.Name;
        }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}
