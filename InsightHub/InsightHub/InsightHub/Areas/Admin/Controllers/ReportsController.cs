﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Models;
using InsightHub.Web.Models;
using Microsoft.AspNetCore.Authorization;

namespace InsightHub.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Provides functionality to the /Report/ route in the Admin area:
    ///   -  Displays all approved reports index page
    ///    - Displays all pending reports index page
    ///     - Displays action to approve a report
    ///      - Displays action to feature a report
    ///       - Displays action to unfeature a report
    ///        - Displays action to get a report by ID
    ///         - Displays action to delete a report
    /// </summary>
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ReportsController : Controller
    {
        private readonly IReportService reportService;

        public ReportsController(IReportService reportService)
        {
            this.reportService = reportService;
        }

        // GET: Admin/Reports
        public async Task<IActionResult> Index()
        {
            var approvedReports = await this.reportService.GetAllApprovedReportsAsync();
            var reportsViewModels = approvedReports.Select(r => new ReportIndexViewModel(r));

            return View(reportsViewModels);
        }

        // GET: Admin/Reports/Pending
        public async Task<IActionResult> Pending()
        {

            try
            {
                var approvedReports = await this.reportService.GetAllPendingReportsAsync();
                var reportsViewModels = approvedReports.Select(r => new ReportIndexViewModel(r));

                return View(reportsViewModels);
            }
            catch (Exception)
            {

                return View(new List<ReportIndexViewModel>());
            }

        }

        // Post: Approve Report
        public async Task<IActionResult> Approve(int id)
        {

            var report = await this.reportService.ApproveReportAsync(id);

            return RedirectToAction(nameof(Index));

        }

        // Post: Feature Report
        public async Task<IActionResult> Feature(int id)
        {

            var report = await this.reportService.FeatureReportAsync(id);

            return RedirectToAction(nameof(Index));

        }

        // Post: Unfeature Report
        public async Task<IActionResult> Unfeature(int id)
        {

            var report = await this.reportService.UnfeatureReportAsync(id);

            return RedirectToAction(nameof(Index));

        }

        // GET: Admin/Reports/Details/5
        public async Task<IActionResult> Details(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var report = await this.reportService.GetReportAsync(id);

            if (report == null)
            {
                return NotFound();
            }

            var reportViewModel = new ReportAdminViewModel(report);

            return View(reportViewModel);

        }

        // GET: Admin/Reports/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var report = await this.reportService.GetReportAsync(id);

            if (report == null)
            {
                return NotFound();
            }

            var reportViewModel = new ReportAdminViewModel(report);

            return View(reportViewModel);

        }

        // POST: Admin/Reports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {

            var report = await this.reportService.DeleteReportAsync(id);

            return RedirectToAction(nameof(Index));
        }
    }
}
