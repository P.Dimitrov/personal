﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Areas.Client.Models
{
    /// <summary>
    /// A class that maps UserDTO and UserIndustryDTO into Industry View Model for Client area.
    /// </summary>
    public class IndustryIndexViewModel
    {
        public IndustryIndexViewModel()
        {

        }

        public IndustryIndexViewModel(UserIndustryDTO industryDTO)
        {
            Id = industryDTO.Id;
            Name = industryDTO.Name;
            Subscribers = industryDTO.Subscribers;
        }

        public IndustryIndexViewModel(IndustryDTO industryDTO)
        {
            Id = industryDTO.Id;
            Name = industryDTO.Name;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<string> Subscribers { get; set; }
    }
}
