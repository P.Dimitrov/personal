﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Web.Models;
using InsightHub.Web.Models.ReportMVCModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.Areas.Client.Controllers
{
    /// <summary>
    /// Provides functionality to the /Report/ route in the Client area:
    ///   -  Displays all downloaded reports index page by username
    /// </summary>
    [Area("Client")]
    [Authorize(Roles = "Client,Author")]
    public class ReportsController : Controller
    {
        private IReportService reportService;

        public ReportsController(IReportService reportService)
        {
            this.reportService = reportService;
        }
        public async Task<IActionResult> Index()
        {

            var reports = await this.reportService.GetAllUserDownloadedReportsAsync(User.Identity.Name);

            var reportsViewModel = reports.Select(r => new IndexReportViewModel
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Description,
                Industry = new IndustryViewModel { Id = r.Industry.Id, Name = r.Industry.Name },
                Author = r.Author,
                Tags = r.Tags.Select(tag => new TagViewModel { Id = tag.Id, Name = tag.Name }).ToList(),
                TimesDownloaded = r.TimesDownloaded
            });

            if (reports == null)
            {
                return NotFound();
            }

            return View(reportsViewModel);
        }
    }
}