﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Client.Models;
using InsightHub.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.Areas.Client.Controllers
{
    /// <summary>
    /// Provides functionality to the /Industry/ route in the Client area:
    ///   -  Displays all industries index page
    ///    - Displays all subscriptions index page by username
    ///     - Displays action to subscribe
    ///      - Displays action to unsubscribe
    /// </summary>
    [Area("Client")]
    [Authorize(Roles = "Client,Author")]
    public class IndustriesController : Controller
    {
        private readonly IIndustryService industryService;

        public IndustriesController(IIndustryService industryService)
        {
            this.industryService = industryService;
        }

        public async Task<IActionResult> Index()
        {
            var industries = await this.industryService.GetAllIndustriesAsync();

            var industryViewModels = industries.Select(i => new IndustryIndexViewModel(i));

            return View(industryViewModels);
        }

        public async Task<IActionResult> Subscriptions()
        {
            var industries = await this.industryService.GetAllUserSubscriptionsAsync(User.Identity.Name);

            var industryViewModels = industries.Select(i => new IndustryIndexViewModel(i));

            return View(industryViewModels);
        }

        public async Task<IActionResult> Subscribe(int id)
        {
            var industry = await this.industryService.SubscribeAsync(User.Identity.Name, id);

            var industryViewModel = new IndustryIndexViewModel(industry);

            return RedirectToAction(nameof(Subscriptions), new { userName = User.Identity.Name });
        }

        public async Task<IActionResult> Unsubscribe(int id)
        {
            var industry = await this.industryService.UnsubscribeAsync(User.Identity.Name, id);

            var industryViewModel = new IndustryIndexViewModel(industry);

            return RedirectToAction(nameof(Subscriptions), new { userName = User.Identity.Name });
        }
    }
}