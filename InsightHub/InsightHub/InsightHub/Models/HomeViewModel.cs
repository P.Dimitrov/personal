﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models
{
    public class HomeViewModel
    {
        public List<ReportViewModel> Newest { get; set; }

        public List<ReportViewModel> MostDownloaded { get; set; }

        public List<ReportViewModel> Featured { get; set; }
    }
}
