﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models.ReportMVCModels
{
    /// <summary>
    /// A class that contains properties for index Report ViewModel.
    /// </summary>
    public class IndexReportViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public IndustryViewModel Industry { get; set; }

        public string Author { get; set; }

        public ICollection<TagViewModel> Tags { get; set; }
        [Display(Name = "Times Downloaded")]
        public int TimesDownloaded { get; set; }
        [Display(Name = "Created On")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreatedOn { get; set; }

    }
}
