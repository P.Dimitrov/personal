﻿using BeerOverflow.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTO
{
    public class BreweryDTO
    {
        public BreweryDTO()
        {
            Beers = new List<BeerDTO>();
        }
        public int BreweryDTOId { get; set; }
        public string Name { get; set; }
        public CountryDTO Country { get; set; }
        public ICollection<BeerDTO> Beers { get; set; }
    }
}
