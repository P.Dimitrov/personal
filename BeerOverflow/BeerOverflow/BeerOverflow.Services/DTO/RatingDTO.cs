﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTO
{
    public class RatingDTO
    {
        public string UserName { get; set; }
        public int RatingValue { get; set; }
        public int BeerId { get; set; }
        public int UserId { get; set; }
    }
}
