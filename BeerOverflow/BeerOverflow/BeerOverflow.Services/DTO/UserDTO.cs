﻿using BeerOverflow.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTO
{
    public class UserDTO
    {
        public UserDTO()
        {
            DrankBeers = new List<BeerDTO>();
            WishListBeers = new List<BeerDTO>();
            Reviews = new List<ReviewDTO>();
        }
        public int UserDTOId { get; set; }
        public string Name { get; set; }
        public ICollection<BeerDTO> DrankBeers { get; set; }
        public ICollection<BeerDTO> WishListBeers { get; set; }
        public ICollection<ReviewDTO> Reviews { get; set; }
    }
}
