﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTO
{
    public class CountryDTO
    {
        public int CountryDTOId { get; set; }
        public string Name { get; set; }
    }
}
