﻿using BeerOverflow.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTO
{
    public class ReviewDTO
    {
        public string ReviewText { get; set; }
        public string Reviewer { get; set; }
        public string Beer { get; set; }
        public int BeerId { get; set; }
        public int UserId { get; set; }
        public bool IsFlagged { get; set; }
        public int? Likes { get; set; }
        public int? Dislikes { get; set; }
    }
}
