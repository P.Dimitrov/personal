﻿using BeerOverflow.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTO
{
    public class BeerDTO
    {
        public BeerDTO()
        {
            Ratings = new List<RatingDTO>();
            Reviews = new List<ReviewDTO>();
        }
        public int BeerDTOId { get; set; }
        public string Name { get; set; }
        public BeerTypeDTO Type { get; set; }
        public BreweryDTO Brewery { get; set; }
        public CountryDTO Country { get; set; }
        public double AlcoholPercentage { get; set; }
        public ICollection<RatingDTO> Ratings { get; set; }
        public ICollection<ReviewDTO> Reviews { get; set; }
    }
}
