﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTO
{
    public class BeerTypeDTO
    {
        public BeerTypeDTO()
        {
            Beers = new List<BeerDTO>();
        }
        public int BeerTypeDTOId { get; set; }
        public string Name { get; set; }
        public ICollection<BeerDTO> Beers { get; set; }
    }
}
