﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class BeerTypeService : IBeerTypeService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerOverflowContext context;

        public BeerTypeService(IDateTimeProvider dateTimeProvider, BeerOverflowContext context)
        {
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.context = context;
        }
        //public BeerTypeDTO GetBeerType(int id)
        //{
        //    var beerType = context.BeerTypes
        //        .Where(beerType => beerType.IsDeleted == false)
        //        .FirstOrDefault(beerType => beerType.Id == id);

        //    if (beerType == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    var beerTypeDTO = new BeerTypeDTO
        //    {
        //        BeerTypeDTOId = beerType.Id,
        //        Name = beerType.Name,
        //        Beers = beerType.Beers.Select(b => new BeerDTO
        //        {
        //            BeerDTOId = beerType.Id,
        //            Name = beerType.Name,
        //        }).ToList(),
        //    };

        //    return beerTypeDTO;
        //}

        /// <summary>
        /// An asynced method that searches a beer type by its ID and shows it.
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>BeerTypeDTO</returns>
        public async Task<BeerTypeDTO> GetBeerTypeAsync(int id)
        {
            var beerType = await context.BeerTypes
                .Where(beerType => beerType.IsDeleted == false)
                .FirstOrDefaultAsync(beerType => beerType.Id == id);

            if (beerType == null)
            {
                throw new ArgumentNullException();
            }

            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = beerType.Id,
                Name = beerType.Name,
                Beers = beerType.Beers.Select(b => new BeerDTO
                {
                    BeerDTOId = beerType.Id,
                    Name = beerType.Name,
                }).ToList(),
            };

            return beerTypeDTO;
        }

        //public BeerTypeDTO GetBeerTypeByName(string beerTypeName)
        //{
        //    var beerType = context.BeerTypes
        //        .Where(beerType => beerType.IsDeleted == false)
        //        .FirstOrDefault(beerType => beerType.Name == beerTypeName);

        //    if (beerType == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    var beerTypeDTO = new BeerTypeDTO
        //    {
        //        BeerTypeDTOId = beerType.Id,
        //        Name = beerType.Name,
        //        Beers = beerType.Beers.Select(b => new BeerDTO
        //        {
        //            BeerDTOId = beerType.Id,
        //            Name = beerType.Name,
        //        }).ToList(),
        //    };

        //    return beerTypeDTO;
        //}

        /// <summary>
        /// An asynced method that searches a beer type by its name and shows it.
        /// </summary>
        /// <param name="beerTypeName">string</param>
        /// <returns>BeerTypeDTO</returns>
        public async Task<BeerTypeDTO> GetBeerTypeByNameAsync(string beerTypeName)
        {
            var beerType = await context.BeerTypes
                .Where(beerType => beerType.IsDeleted == false)
                .FirstOrDefaultAsync(beerType => beerType.Name == beerTypeName);

            if (beerType == null)
            {
                throw new ArgumentNullException();
            }

            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = beerType.Id,
                Name = beerType.Name,
                Beers = beerType.Beers.Select(b => new BeerDTO
                {
                    BeerDTOId = beerType.Id,
                    Name = beerType.Name,
                }).ToList(),
            };

            return beerTypeDTO;
        }

        //public IEnumerable<BeerTypeDTO> GetAllBeerTypes()
        //{
        //    var beerType = context.BeerTypes
        //        .Where(beerType => beerType.IsDeleted == false)
        //        .Select(x => new BeerTypeDTO
        //        {
        //            BeerTypeDTOId = x.Id,
        //            Name = x.Name,
        //            Beers = x.Beers.Select(b => new BeerDTO
        //            {
        //                BeerDTOId = b.Id,
        //                Name = b.Name,
        //            }).ToList(),
        //        });

        //    return beerType;
        //}

        /// <summary>
        /// An asynced method that shows all beer types.
        /// </summary>
        /// <returns>ICollection<BeerTypeDTO></returns>
        public async Task<ICollection<BeerTypeDTO>> GetAllBeerTypesAsync()
        {
            var beerTypes = await context.BeerTypes
                .Where(beerType => beerType.IsDeleted == false)
                .Select(x => new BeerTypeDTO
                {
                    BeerTypeDTOId = x.Id,
                    Name = x.Name,
                    Beers = x.Beers.Select(b => new BeerDTO
                    {
                        BeerDTOId = b.Id,
                        Name = b.Name,
                    }).ToList(),
                }).ToListAsync();

            if (!beerTypes.Any())
            {
                throw new ArgumentNullException();
            }

            return beerTypes;
        }

        //public BeerTypeDTO CreateBeerType(BeerTypeDTO beerTypeDTO)
        //{
        //    var beerType = new BeerType
        //    {
        //        Name = beerTypeDTO.Name
        //    };

        //    context.BeerTypes.Add(beerType);
        //    context.SaveChanges();

        //    return beerTypeDTO;
        //}

        /// <summary>
        /// An asynced method that creates a beer type by given BeerTypeDTO.
        /// </summary>
        /// <param name="beerTypeDTO">BeerTypeDTO</param>
        /// <returns>BeerTypeDTO</returns>
        public async Task<BeerTypeDTO> CreateBeerTypeAsync(BeerTypeDTO beerTypeDTO)
        {
            var beerType = new BeerType
            {
                Name = beerTypeDTO.Name
            };

            await context.BeerTypes.AddAsync(beerType);
            await context.SaveChangesAsync();

            return beerTypeDTO;
        }

        //public BeerTypeDTO UpdateBeerType(BeerTypeDTO beerTypeDTO)
        //{
        //    var beerType = context.BeerTypes
        //        .Where(beerType => beerType.IsDeleted == false)
        //        .FirstOrDefault(beerType => beerType.Id == beerTypeDTO.BeerTypeDTOId);

        //    beerType.Name = beerTypeDTO.Name;

        //    context.SaveChanges();

        //    return beerTypeDTO;
        //}

        /// <summary>
        /// An asynced method that updates a beer type by given BeerTypeDTO.
        /// </summary>
        /// <param name="beerTypeDTO"></param>
        /// <returns></returns>
        public async Task<BeerTypeDTO> UpdateBeerTypeAsync(BeerTypeDTO beerTypeDTO)
        {
            var beerType = await context.BeerTypes
                .Where(beerType => beerType.IsDeleted == false)
                .FirstOrDefaultAsync(beerType => beerType.Id == beerTypeDTO.BeerTypeDTOId);

            if (beerType == null)
            {
                throw new ArgumentNullException();
            }

            beerType.Name = beerTypeDTO.Name;

            await context.SaveChangesAsync();

            return beerTypeDTO;
        }

        //public bool DeleteBeerType(int id)
        //{
        //    try
        //    {
        //        var beerType = context.BeerTypes
        //            .Where(beerType => beerType.IsDeleted == false)
        //            .FirstOrDefault(beerType => beerType.Id == id);

        //        beerType.IsDeleted = true;

        //        context.SaveChanges();

        //        return true;
        //    }

        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// An asynced method that deletes a certain beer type by given ID.
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>bool</returns>
        public async Task<bool> DeleteBeerTypeAsync(int id)
        {
            try
            {
                var beerType = await context.BeerTypes
                    .Where(beerType => beerType.IsDeleted == false)
                    .FirstOrDefaultAsync(beerType => beerType.Id == id);

                beerType.IsDeleted = true;

                await context.SaveChangesAsync();

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }
    }
}
