﻿using BeerOverflow.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IReviewService
    {
         Task <ReviewDTO> GetReviewAsync(int beerId, int userId);
         Task<IEnumerable<ReviewDTO>> GetAllReviewsAsync();
         Task<ReviewDTO> CreateReviewAsync(ReviewDTO reviewDTO);
         Task<ReviewDTO> UpdateReviewAsync(int beerId, int userId, string newName);
         Task<bool> FlagReviewAsync(int beerId, int userId);
         Task<bool> LikeReviewAsync(int beerId, int userId);
         Task<bool> DislikeReviewAsync(int beerId, int userId);
         Task<bool> DeleteReviewAsync(int beerId, int userId);
    }
}

