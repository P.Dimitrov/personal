﻿using BeerOverflow.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IUserService
    {
        public UserDTO GetUser(int id);
        public ICollection<UserDTO> GetAllUsers();
        public UserDTO CreateUser(UserDTO userDTO);
        public UserDTO UpdateUser(UserDTO userDTO);
        public Task<ICollection<BeerDTO>> GetUserDrankListAsync(string userName);
        public Task<ICollection<BeerDTO>> GetUserWishListAsync(string userName);
        public bool DeleteUser(int id);
    }
}
