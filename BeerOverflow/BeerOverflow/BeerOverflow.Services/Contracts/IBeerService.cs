﻿using BeerOverflow.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IBeerService
    {
        Task<BeerDTO> GetBeerAsync(int id);
         Task<ICollection<BeerDTO>> GetAllBeersAsync();
         Task<ICollection<BeerDTO>> GetAllBeersOfTheMonthAsync();
         Task<ICollection<BeerDTO>> GetAllBeersByCountryAsync(int countryId);
         Task<ICollection<BeerDTO>> GetAllBeersByStyleAsync(int styleId);
         Task<ICollection<BeerDTO>> GetAllBeersByBreweryAsync(int breweryId);
         Task<ICollection<BeerDTO>> GetAllBeersByABVAsync();
         Task<ICollection<BeerDTO>> GetAllBeersByNameAsync();
         Task<ICollection<BeerDTO>> GetAllBeersByRatingAsync();
         Task<ICollection<BeerDTO>> SearchBeersByCountryAsync(string country);
         Task<ICollection<BeerDTO>> SearchBeersByBreweryAsync(string brewery);
         Task<ICollection<BeerDTO>> SearchBeersByNameAsync(string name);
         Task<ICollection<BeerDTO>> SearchBeersAsync(string searchParams);
         Task<BeerDTO> CreateBeerAsync(BeerDTO beerDTO);
         Task<BeerDTO> UpdateBeerAsync(int id, BeerDTO beerDTO);
         Task<BeerDTO> DeleteBeerAsync(int id);
        public Task<BeerDTO> AddBeerToUserDrankListAsync(int userId, string userName);
        public Task<BeerDTO> AddBeerToUserWishListAsync(int userId, string userName);
        public Task<bool> AddRatingToBeerAsync(int id, string userName, int rating);
    }
}
