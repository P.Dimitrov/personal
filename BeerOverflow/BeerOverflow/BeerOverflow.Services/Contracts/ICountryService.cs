﻿using BeerOverflow.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface ICountryService
    {
         Task <CountryDTO> GetCountryAsync(int id);
         Task <IEnumerable<CountryDTO>> GetAllCountriesAsync();
         Task <CountryDTO> CreateCountryAsync(CountryDTO countryDTO);
         Task <CountryDTO> GetCountryByNameAsync(string countryName);
         Task <CountryDTO> UpdateCountryAsync(int id, CountryDTO countryDTO);
         Task <bool> DeleteCountryAsync(int id);
    }
}
