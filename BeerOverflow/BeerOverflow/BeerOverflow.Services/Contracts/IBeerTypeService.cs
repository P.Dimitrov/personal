﻿using BeerOverflow.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IBeerTypeService
    {
         Task<BeerTypeDTO> GetBeerTypeAsync(int id);
         Task<BeerTypeDTO> GetBeerTypeByNameAsync(string beerTypeName);
         Task<ICollection<BeerTypeDTO>> GetAllBeerTypesAsync();
         Task<BeerTypeDTO> CreateBeerTypeAsync(BeerTypeDTO beerTypeDTO);
         Task<BeerTypeDTO> UpdateBeerTypeAsync(BeerTypeDTO beerTypeDTO);
         Task<bool> DeleteBeerTypeAsync(int id);
    }
}
