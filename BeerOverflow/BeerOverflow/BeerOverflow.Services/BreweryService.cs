﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class BreweryService : IBreweryService
    {
        private readonly ICountryService countryService;
        private readonly BeerOverflowContext context;

        public BreweryService(ICountryService countryService, BeerOverflowContext context)
        {
            this.countryService = countryService;
            this.context = context;
        }
        //public BreweryDTO GetBrewery(int id)
        //{
        //    var brewery = context.Breweries
        //        .Where(brewery => brewery.IsDeleted == false)
        //        .Include(brewery => brewery.Country)
        //        .FirstOrDefault(brewery => brewery.Id == id);

        //    if (brewery == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    var countryDTO = countryService.GetCountryAsync(brewery.Country.Id);

        //    var breweryDTO = new BreweryDTO
        //    {
        //        BreweryDTOId = brewery.Id,
        //        Name = brewery.Name,

        //        Country = new CountryDTO
        //        {
        //            Name = brewery.Country.Name
        //        }
        //    };

        //    return breweryDTO;
        //}

            /// <summary>
            /// An async method that shows a certain brewery searched by its ID.
            /// </summary>
            /// <param name="id"int</param>
            /// <returns>BreweryDTO</returns>
        public async Task<BreweryDTO> GetBreweryAsync(int id)
        {
            var brewery = await context.Breweries
                .Where(brewery => brewery.IsDeleted == false)
                .Include(brewery => brewery.Country)
                .ThenInclude(brewery => brewery.Beers)
                .FirstOrDefaultAsync(brewery => brewery.Id == id);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            var countryDTO = countryService.GetCountryAsync(brewery.Country.Id);

            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = brewery.Id,
                Name = brewery.Name,

                Country = new CountryDTO
                {
                    Name = brewery.Country.Name
                },
                Beers = brewery.Beers.Select(b => new BeerDTO
                {
                    Name = b.Name
                }).ToList()
            };

            return breweryDTO;
        }
        //public BreweryDTO GetBreweryByName(string breweryName)
        //{
        //    var brewery = context.Breweries
        //        .Where(brewery => brewery.IsDeleted == false)
        //        .Include(brewery => brewery.Country)
        //        .FirstOrDefault(brewery => brewery.Name == breweryName);

        //    if (brewery == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    var countryDTO = countryService.GetCountryAsync(brewery.CountryId);

        //    var breweryDTO = new BreweryDTO
        //    {
        //        BreweryDTOId = brewery.Id,
        //        Name = brewery.Name,

        //        Country = new CountryDTO
        //        {
        //            Name = brewery.Country.Name
        //        }
        //    };

        //    return breweryDTO;
        //}

        /// <summary>
        /// An async method that shows a certain brewery searched by its name.
        /// </summary>
        /// <param name="breweryName">string</param>
        /// <returns>BreweryDTO</returns>
        public async Task<BreweryDTO> GetBreweryByNameAsync(string breweryName)
        {
            var brewery = await context.Breweries
                .Where(brewery => brewery.IsDeleted == false)
                .Include(brewery => brewery.Country)
                .FirstOrDefaultAsync(brewery => brewery.Name == breweryName);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            var countryDTO = await countryService.GetCountryAsync(brewery.CountryId);

            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = brewery.Id,
                Name = brewery.Name,

                Country = new CountryDTO
                {
                    Name = brewery.Country.Name
                }
            };

            return breweryDTO;
        }
        //public ICollection<BreweryDTO> GetAllBreweries()
        //{
        //    var breweries = context.Breweries
        //        .Where(brewery => brewery.IsDeleted == false)
        //        .Select(brewery => new BreweryDTO
        //        {
        //            BreweryDTOId = brewery.Id,
        //            Name = brewery.Name,
        //            Country = new CountryDTO
        //            {
        //                Name = brewery.Country.Name
        //            },
        //        }).ToList();

        //    if (!breweries.Any())
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return breweries;
        //}

        /// <summary>
        /// An async method that shows all breweries in the database.
        /// </summary>
        /// <returns>ICollection<BreweryDTO></returns>
        public async Task<ICollection<BreweryDTO>> GetAllBreweriesAsync()
        {
            var breweries = await context.Breweries
                .Where(brewery => brewery.IsDeleted == false)
                .Select(brewery => new BreweryDTO
                {
                    BreweryDTOId = brewery.Id,
                    Name = brewery.Name,
                    Country = new CountryDTO
                    {
                        Name = brewery.Country.Name
                    },
                }).ToListAsync();

            if (!breweries.Any())
            {
                throw new ArgumentNullException();
            }

            return breweries;
        }

        //public BreweryDTO CreateBrewery(BreweryDTO breweryDTO)
        //{
        //    var brewery = new Brewery
        //    {
        //        Name = breweryDTO.Name,
        //        CountryId = context.Countries.FirstOrDefault(x => x.Name == breweryDTO.Country.Name).Id
        //    };

        //    context.Breweries.Add(brewery);
        //    context.SaveChanges();

        //    return breweryDTO;
        //}

        /// <summary>
        /// An async method that creates a brewery by given breweryDTO.
        /// </summary>
        /// <param name="breweryDTO">BreweryDTO</param>
        /// <returns>BreweryDTO</returns>
        public async Task<BreweryDTO> CreateBreweryAsync(BreweryDTO breweryDTO)
        {
            var country = await context.Countries
                .Where(c => c.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Name == breweryDTO.Country.Name);
            var brewery = new Brewery
            {
                Name = breweryDTO.Name,
                CountryId = country.Id
            };

            await context.Breweries.AddAsync(brewery);
            await context.SaveChangesAsync();

            return breweryDTO;
        }

        //public BreweryDTO UpdateBrewery(int id, BreweryDTO breweryDTO)
        //{
        //    var brewery = context.Breweries
        //        .Where(brewery => brewery.IsDeleted == false)
        //        .FirstOrDefault(brewery => brewery.Id == id);

        //    if (brewery == null)
        //    {
        //        return null;
        //    }

        //    brewery.CountryId = this.context.Countries
        //        .FirstOrDefault(c => c.Name == breweryDTO.Country.Name).Id;
        //    brewery.Name = breweryDTO.Name;

        //    context.SaveChanges();

        //    return breweryDTO;
        //}

        /// <summary>
        /// An async method that searches a certain brewery by its ID and updates it by given new breweryDTO.
        /// </summary>
        /// <param name="id">int</param>
        /// <param name="breweryDTO">BreweryDTO</param>
        /// <returns>BreweryDTO</returns>
        public async Task<BreweryDTO> UpdateBreweryAsync(int id, BreweryDTO breweryDTO)
        {
            var brewery = await context.Breweries
                .Where(brewery => brewery.IsDeleted == false)
                .FirstOrDefaultAsync(brewery => brewery.Id == id);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            brewery.CountryId = this.context.Countries
                .FirstOrDefault(c => c.Name == breweryDTO.Country.Name).Id;
            brewery.Name = breweryDTO.Name;

            await context.SaveChangesAsync();

            return breweryDTO;
        }

        //public bool DeleteBrewery(int id)
        //{
        //    try
        //    {
        //        var brewery = context.Breweries
        //            .Where(brewery => brewery.IsDeleted == false)
        //            .FirstOrDefault(brewery => brewery.Id == id);

        //        brewery.IsDeleted = true;

        //        context.SaveChanges();

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// An async method that a certain brewery by its ID and deletes it.
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>bool</returns>
        public async Task<bool> DeleteBreweryAsync(int id)
        {
            try
            {
                var brewery = await context.Breweries
                    .Where(brewery => brewery.IsDeleted == false)
                    .FirstOrDefaultAsync(brewery => brewery.Id == id);

                brewery.IsDeleted = true;

                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
