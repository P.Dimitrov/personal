﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.Provider.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
