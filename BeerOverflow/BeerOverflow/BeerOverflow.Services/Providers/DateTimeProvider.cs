﻿using BeerOverflow.Services.Provider.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.Provider
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.UtcNow;
    }
}
