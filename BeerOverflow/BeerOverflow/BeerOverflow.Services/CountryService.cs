﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class CountryService : ICountryService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerOverflowContext context;

        public CountryService(IDateTimeProvider dateTimeProvider, BeerOverflowContext context)
        {
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.context = context;
        }

        /// <summary>
        /// A method that finds and returns a certain country by its ID.
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>CountryDTO</returns>
        //public CountryDTO GetCountry(int id)
        //{
        //    var country = context.Countries
        //        .Where(country => country.IsDeleted == false)
        //        .FirstOrDefault(country => country.Id == id);

        //    if (country == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    var countryDTO = new CountryDTO
        //    {
        //        CountryDTOId = country.Id,
        //        Name = country.Name,
        //    };

        //    return countryDTO;
        //}

        /// <summary>
        /// An asynced method that finds and returns a certain country by its ID.
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>CountryDTO</returns>
        public async Task<CountryDTO> GetCountryAsync(int id)
        {
            var country = await this.context.Countries
                .Where(country => country.IsDeleted == false)
                .FirstOrDefaultAsync(country => country.Id == id);

            if (country == null)
            {
                throw new ArgumentNullException();
            }

            var countryDTO = new CountryDTO
            {
                CountryDTOId = country.Id,
                Name = country.Name,
            };

            return countryDTO;
        }

        /// <summary>
        /// A method that finds and returns a certain country by its Name.
        /// </summary>
        /// <param name="countryName">string</param>
        /// <returns>CountryDTO</returns>
        //public CountryDTO GetCountryByName(string countryName)
        //{
        //    var country = context.Countries
        //        .Where(country => country.IsDeleted == false)
        //        .FirstOrDefault(country => country.Name == countryName);

        //    if (country == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    var countryDTO = new CountryDTO
        //    {
        //        CountryDTOId = country.Id,
        //        Name = country.Name,
        //    };

        //    return countryDTO;
        //}

        /// <summary>
        /// An asynced method that finds and returns a certain country by its Name.
        /// </summary>
        /// <param name="countryName">string</param>
        /// <returns>CountryDTO</returns>
        public async Task<CountryDTO> GetCountryByNameAsync(string countryName)
        {
            var country = await context.Countries
                .Where(country => country.IsDeleted == false)
                .FirstOrDefaultAsync(country => country.Name == countryName);

            if (country == null)
            {
                throw new ArgumentNullException();
            }

            var countryDTO = new CountryDTO
            {
                CountryDTOId = country.Id,
                Name = country.Name,
            };

            return countryDTO;
        }

        /// <summary>
        /// A method that finds and returns all countries in the database.
        /// </summary>
        /// <returns>IEnumerable<CountryDTO></returns>
        //public IEnumerable<CountryDTO> GetAllCountries()
        //{
        //    var countries = context.Countries
        //        .Where(country => country.IsDeleted == false)
        //        .Select(country => new CountryDTO
        //        {
        //            CountryDTOId = country.Id,
        //            Name = country.Name,
                    
        //        }).ToList();

        //    if (!countries.Any())
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return countries;
        //}

        /// <summary>
        /// An asynced method that finds and returns all countries in the database.
        /// </summary>
        /// <returns>IEnumerable<CountryDTO></returns>
        public async Task<IEnumerable<CountryDTO>> GetAllCountriesAsync()
        {
            var countries = await context.Countries
                .Where(country => country.IsDeleted == false)
                .Select(country => new CountryDTO
                {
                    CountryDTOId = country.Id,
                    Name = country.Name,

                }).ToListAsync();

            if (!countries.Any())
            {
                throw new ArgumentNullException();
            }

            return countries;
        }

        /// <summary>
        /// A method that creates a country by given name.
        /// </summary>
        /// <param name="countryDTO">CountryDTO</param>
        /// <returns>CountryDTO</returns>
        //public CountryDTO CreateCountry(CountryDTO countryDTO)
        //{
        //    var country = new Country
        //    {
        //        Name = countryDTO.Name
        //    };

        //    context.Countries.Add(country);
        //    context.SaveChanges();

        //    return countryDTO;
        //}

        /// <summary>
        /// An asynced method that creates a country by given name.
        /// </summary>
        /// <param name="countryDTO">CountryDTO</param>
        /// <returns>CountryDTO</returns>
        public async Task<CountryDTO> CreateCountryAsync(CountryDTO countryDTO)
        {
            var country = new Country
            {
                Name = countryDTO.Name
            };

            await context.Countries.AddAsync(country);
            await context.SaveChangesAsync();

            return countryDTO;
        }

        /// <summary>
        /// A method that updates a certain country by given ID and new Name.
        /// </summary>
        /// <param name="id">int</param>
        /// <param name="countryDTO">CountryDTO</param>
        /// <returns>CountryDTO</returns>
        //public CountryDTO UpdateCountry(int id, CountryDTO countryDTO)
        //{
        //    var country = context.Countries
        //        .Where(country => country.IsDeleted == false)
        //        .FirstOrDefault(country => country.Id == id);

        //    if (country == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    country.Name = countryDTO.Name;

        //    context.SaveChanges();

        //    return countryDTO;
        //}

        /// <summary>
        /// An asynced method that updates a certain country by given ID and new Name.
        /// </summary>
        /// <param name="id">int</param>
        /// <param name="countryDTO">CountryDTO</param>
        /// <returns>CountryDTO</returns>
        public async Task<CountryDTO> UpdateCountryAsync(int id, CountryDTO countryDTO)
        {
            var country = await context.Countries
                .Where(country => country.IsDeleted == false)
                .FirstOrDefaultAsync(country => country.Id == id);

            if (country == null)
            {
                throw new ArgumentNullException();
            }

            country.Name = countryDTO.Name;

            await context.SaveChangesAsync();

            return countryDTO;
        }

        /// <summary>
        /// A method that deletes a certain country by given ID.
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>bool</returns>
        //public bool DeleteCountry(int id)
        //{
        //    try
        //    {
        //        var country = context.Countries
        //            .Where(country => country.IsDeleted == false)
        //            .FirstOrDefault(country => country.Id == id);

        //        country.IsDeleted = true;

        //        context.SaveChanges();

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// An asynced method that deletes a certain country by given ID.
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>bool</returns>
        public async Task<bool> DeleteCountryAsync(int id)
        {
            try
            {
                var country = await context.Countries
                    .Where(country => country.IsDeleted == false)
                    .FirstOrDefaultAsync(country => country.Id == id);

                country.IsDeleted = true;

                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
