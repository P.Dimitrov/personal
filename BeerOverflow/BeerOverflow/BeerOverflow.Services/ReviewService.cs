﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider;
using BeerOverflow.Services.Provider.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class ReviewService : IReviewService
    {
        private readonly BeerOverflowContext context;
        private readonly IDateTimeProvider dateTimeProvider;

        public ReviewService(IDateTimeProvider dateTimeProvider, BeerOverflowContext context)
        {
            this.context = context;
            this.dateTimeProvider = dateTimeProvider;
        }
        //public ReviewDTO GetReview(int beerId, int userId)
        //{
        //    var review = context.Reviews
        //        .Where(review => review.IsDeleted == false)
        //        .FirstOrDefault(review => review.BeerId == beerId && review.UserId == userId);

        //    if (review == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    var reviewDTO = new ReviewDTO
        //    {
        //        ReviewText = review.ReviewText,
        //        UserId = review.UserId,
        //        BeerId = review.BeerId
        //    };

        //    return reviewDTO;
        //}

        /// <summary>
        /// An async method that searches for a review by given beer ID and given user ID.
        /// </summary>
        /// <param name="beerId">int</param>
        /// <param name="userId">int</param>
        /// <returns>ReviewDTO</returns>
        public async Task<ReviewDTO> GetReviewAsync(int beerId, int userId)
        {
            var review = await context.Reviews
                .Where(review => review.IsDeleted == false)
                .FirstOrDefaultAsync(review => review.BeerId == beerId && review.UserId == userId);

            if (review == null)
            {
                throw new ArgumentNullException();
            }

            var reviewDTO = new ReviewDTO
            {
                ReviewText = review.ReviewText,
                UserId = review.UserId,
                BeerId = review.BeerId
            };

            return reviewDTO;
        }

        //public IEnumerable<ReviewDTO> GetAllReviews()
        //{
        //    var review = context.Reviews
        //        .Where(review => review.IsDeleted == false)
        //        .Select(r => new ReviewDTO
        //        {
        //            ReviewText = r.ReviewText
        //        });

        //    return review;
        //}

        /// <summary>
        /// An async method that shows all reviews in the database.
        /// </summary>
        /// <returns>IEnumerable<ReviewDTO></returns>
        public async Task<IEnumerable<ReviewDTO>> GetAllReviewsAsync()
        {
            var review = await context.Reviews
                .Where(review => review.IsDeleted == false)
                .Select(r => new ReviewDTO
                {
                    ReviewText = r.ReviewText
                }).ToListAsync();

            if (!review.Any())
            {
                throw new ArgumentNullException();
            }

            return review;
        }

        //public ReviewDTO CreateReview(ReviewDTO reviewDTO)
        //{
        //    var review = new Review
        //    {
        //        BeerId = reviewDTO.BeerId,
        //        UserId = reviewDTO.UserId,
        //        ReviewText = reviewDTO.ReviewText,
        //    };

        //    context.Reviews.Add(review);

        //    return reviewDTO;
        //}

        /// <summary>
        /// An async method that creates a review by given ReviewDTO.
        /// </summary>
        /// <param name="reviewDTO">ReviewDTO</param>
        /// <returns>ReviewDTO</returns>
        public async Task<ReviewDTO> CreateReviewAsync(ReviewDTO reviewDTO)
        {
            var user = await this.context.Users
                .Where(user => user.IsDeleted == false)
                .FirstOrDefaultAsync(user => user.UserName == reviewDTO.Reviewer);

            var review = new Review
            {
                BeerId = reviewDTO.BeerId,
                UserId = user.Id,
                ReviewText = reviewDTO.ReviewText,
            };

            await this.context.Reviews.AddAsync(review);
            await this.context.SaveChangesAsync();

            var reviewDTOToReturn = new ReviewDTO
            {
                ReviewText = review.ReviewText,
                UserId = review.UserId,
                BeerId = review.BeerId
            };

            return reviewDTOToReturn;
        }

        //public ReviewDTO UpdateReview(int beerId, int userId, string newText)
        //{
        //    var review = context.Reviews
        //        .Where(review => review.IsDeleted == false)
        //        .FirstOrDefault(review => review.BeerId == beerId && review.UserId == userId);

        //    review.ReviewText = newText;
        //    context.SaveChanges();

        //    var reviewDTO = new ReviewDTO
        //    {
        //        ReviewText = review.ReviewText,
        //        UserId = review.UserId,
        //        BeerId = review.BeerId
        //    };

        //    return reviewDTO;
        //}

        /// <summary>
        /// An async method that updates a certain review by given beer ID and user ID with new string text.
        /// </summary>
        /// <param name="beerId">int</param>
        /// <param name="userId">int</param>
        /// <param name="newText">string</param>
        /// <returns>ReviewDTO</returns>
        public async Task<ReviewDTO> UpdateReviewAsync(int beerId, int userId, string newText)
        {
            var review = await context.Reviews
                .Where(review => review.IsDeleted == false)
                .FirstOrDefaultAsync(review => review.BeerId == beerId && review.UserId == userId);

            if (review == null)
            {
                throw new ArgumentNullException();
            }

            review.ReviewText = newText;
            await context.SaveChangesAsync();

            var reviewDTO = new ReviewDTO
            {
                ReviewText = review.ReviewText,
                UserId = review.UserId,
                BeerId = review.BeerId
            };

            return reviewDTO;
        }

        //public bool FlagReview(int beerId, int userId)
        //{
        //    try
        //    {
        //        var review = context.Reviews
        //            .Where(review => review.IsDeleted == false)
        //            .FirstOrDefault(review => review.BeerId == beerId && review.UserId == userId);

        //        review.IsFlagged = true;
        //        context.SaveChanges();

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// An async method that flags a certain review by given beer ID and user ID.
        /// </summary>
        /// <param name="beerId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<bool> FlagReviewAsync(int beerId, int userId)
        {
            try
            {
                var review = await this.context.Reviews
                    .Where(review => review.IsDeleted == false)
                    .FirstOrDefaultAsync(review => review.BeerId == beerId && review.UserId == userId);

                review.IsFlagged = true;
                await this.context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //public bool LikeReview(int beerId, int userId)
        //{
        //    try
        //    {
        //        var review = context.Reviews
        //            .Where(review => review.IsDeleted == false)
        //            .FirstOrDefault(review => review.BeerId == beerId && review.UserId == userId);

        //        review.Likes++;
        //        context.SaveChanges();

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// An async method that gives likes to a certain review searched by given beer ID and User ID.
        /// </summary>
        /// <param name="beerId">int</param>
        /// <param name="userId">int</param>
        /// <returns>bool</returns>
        public async Task<bool> LikeReviewAsync(int beerId, int userId)
        {
            try
            {
                var review = await this.context.Reviews
                    .Where(review => review.IsDeleted == false)
                    .FirstOrDefaultAsync(review => review.BeerId == beerId && review.UserId == userId);

                review.Likes++;
                await this.context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //public bool DislikeReview(int beerId, int userId)
        //{
        //    try
        //    {
        //        var review = context.Reviews
        //            .Where(review => review.IsDeleted == false)
        //            .FirstOrDefault(review => review.BeerId == beerId && review.UserId == userId);

        //        review.Dislikes++;
        //        context.SaveChanges();

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// An async method that gives dislikes to a certain review searched by given beer ID and User ID.
        /// </summary>
        /// <param name="beerId">int</param>
        /// <param name="userId">int</param>
        /// <returns>bool</returns>
        public async Task<bool> DislikeReviewAsync(int beerId, int userId)
        {
            try
            {
                var review = await context.Reviews
                    .Where(review => review.IsDeleted == false)
                    .FirstOrDefaultAsync(review => review.BeerId == beerId && review.UserId == userId);

                review.Dislikes++;
                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //public bool DeleteReview(int beerId, int userId)
        //{
        //    try
        //    {
        //        var review = context.Reviews
        //            .Where(review => review.IsDeleted == false)
        //            .FirstOrDefault(review => review.BeerId == beerId && review.UserId == userId);

        //        review.IsDeleted = true;
        //        context.SaveChanges();

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

            /// <summary>
            /// An asynced method that deletes a certain review by given beer ID and user ID=
            /// </summary>
            /// <param name="beerId">int</param>
            /// <param name="userId">int</param>
            /// <returns>bool</returns>
        public async Task<bool> DeleteReviewAsync(int beerId, int userId)
        {
            try
            {
                var review = await context.Reviews
                    .Where(review => review.IsDeleted == false)
                    .FirstOrDefaultAsync(review => review.BeerId == beerId && review.UserId == userId);

                review.IsDeleted = true;
                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
