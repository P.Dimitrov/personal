﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class UserService : IUserService
    {
        private readonly BeerOverflowContext context;

        public UserService(BeerOverflowContext context)
        {
            this.context = context;
        }

        public UserDTO CreateUser(UserDTO userDTO)
        {
            var user = new User
            {
                UserName = userDTO.Name,
            };

            context.Users.Add(user);
            context.SaveChanges();

            return userDTO;
        }

        public bool DeleteUser(int id)
        {
            try
            {
                var user = context.Users
                    .Where(user => user.IsDeleted == false)
                    .FirstOrDefault(user => user.Id == id);

                user.IsDeleted = true;
                context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public ICollection<UserDTO> GetAllUsers()
        {
            var users = context.Users
                .Where(user => user.IsDeleted == false)
                .Select(user => new UserDTO
                {
                    UserDTOId = user.Id,
                    Name = user.UserName
                }).ToList();

            if (users == null)
            {
                throw new ArgumentNullException();
            }

            return users;
        }

        public UserDTO GetUser(int id)
        {
            var user = context.Users
                .Where(user => user.IsDeleted == false)
                .FirstOrDefault(user => user.Id == id);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var userDTO = new UserDTO
            {
                UserDTOId = user.Id,
                Name = user.UserName
            };

            return userDTO;
        }

        public async Task<ICollection<BeerDTO>> GetUserDrankListAsync(string userName)
        {
            var user = await this.context.Users
                .Include(user => user.DrankList)
                .ThenInclude(wb => wb.Beer)
                .ThenInclude(b => b.Type)
                .Include(user => user.DrankList)
                .ThenInclude(wb => wb.Beer)
                .ThenInclude(b => b.Country)
                .Include(user => user.DrankList)
                .ThenInclude(wb => wb.Beer)
                .ThenInclude(b => b.Brewery)
                .Include(user => user.DrankList)
                .ThenInclude(wb => wb.Beer)
                .ThenInclude(b => b.Ratings)
                .ThenInclude(r => r.Rater)
                .Include(user => user.DrankList)
                .ThenInclude(wb => wb.Beer)
                .ThenInclude(b => b.Reviews)
                .ThenInclude(re => re.Reviewer)
                .Where(user => user.IsDeleted == false)
                .FirstOrDefaultAsync(user => user.UserName == userName);

            var drankList = user.DrankList
                .Select(x => x.Beer).Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).ToList();

            return drankList;
        }

        public async Task<ICollection<BeerDTO>> GetUserWishListAsync(string userName)
        {
            var user = await context.Users
                .Include(user => user.WishList)
                .ThenInclude(wb => wb.Beer)
                .ThenInclude(b => b.Type)
                .Include(user => user.WishList)
                .ThenInclude(wb => wb.Beer)
                .ThenInclude(b => b.Country)
                .Include(user => user.WishList)
                .ThenInclude(wb => wb.Beer)
                .ThenInclude(b => b.Brewery)
                .Include(user => user.WishList)
                .ThenInclude(wb => wb.Beer)
                .ThenInclude(b => b.Ratings)
                .ThenInclude(r => r.Rater)
                .Include(user => user.WishList)
                .ThenInclude(wb => wb.Beer)
                .ThenInclude(b => b.Reviews)
                .ThenInclude(re => re.Reviewer)
                .Where(user => user.IsDeleted == false)
                .FirstOrDefaultAsync(user => user.UserName == userName);

            var wishList = user.WishList
                .Select(x => x.Beer).Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).ToList();

            return wishList;
        }

        public UserDTO UpdateUser(UserDTO userDTO)
        {
            var user = context.Users
                .Where(user => user.IsDeleted == false)
                .FirstOrDefault(user => user.Id == userDTO.UserDTOId);

            user.UserName = userDTO.Name;

            var UserDTO = new UserDTO
            {
                UserDTOId = user.Id,
                Name = user.UserName
            };

            return UserDTO;
        }
    }
}
