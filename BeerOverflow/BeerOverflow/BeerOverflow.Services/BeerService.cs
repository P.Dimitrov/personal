using BeerOverflow.Models;
using BeerOverflow.Data;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class BeerService : IBeerService
    {
        private readonly BeerOverflowContext context;

        public BeerService(BeerOverflowContext context)
        {
            this.context = context;
        }
        //public BeerDTO GetBeer(int id)
        //{
        //    var beer = context.Beers
        //        .Include(b => b.Country)
        //        .Include(b => b.Brewery)
        //        .Include(b => b.Ratings)
        //        .ThenInclude(r => r.Rater)
        //        .Include(b => b.Reviews)
        //        .ThenInclude(r => r.Reviewer)
        //        .Include(b => b.Type)
        //        .Where(beer => beer.IsDeleted == false)
        //        .FirstOrDefault(beer => beer.Id == id);

        //    if (beer == null)
        //    {
        //        return null;
        //    }

        //    var beerDTO = new BeerDTO
        //    {
        //        BeerDTOId = beer.Id,
        //        Name = beer.Name,
        //        Type = new BeerTypeDTO
        //        {
        //            Name = beer.Type.Name
        //        },
        //        Brewery = new BreweryDTO
        //        {
        //            Name = beer.Brewery.Name
        //        },
        //        Country = new CountryDTO
        //        {
        //            Name = beer.Country.Name
        //        },
        //        AlcoholPercentage = beer.AlcoholPercentage,
        //        Ratings = beer.Ratings.Select(r => new RatingDTO
        //        {
        //            BeerId = r.BeerId,
        //            RatingValue = r.RatingValue,
        //            UserId = r.UserId,
        //            UserName = r.Rater.UserName
        //        }).ToList(),
        //        Reviews = beer.Reviews.Select(r => new ReviewDTO
        //        {
        //            BeerId = r.BeerId,
        //            Dislikes = r.Dislikes,
        //            IsFlagged = r.IsFlagged,
        //            Likes = r.Likes,
        //            ReviewText = r.ReviewText,
        //            Reviewer = r.Reviewer.UserName,
        //            UserId = r.UserId
        //        }).ToList()
        //    };

        //    return beerDTO;
        //}

        /// <summary>
        /// An async method that shows a beer by a given ID.
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>BeerDTO</returns>
        public async Task<BeerDTO> GetBeerAsync(int id)
        {
            var beer = await context.Beers
                .Include(b => b.Country)
                .Include(b => b.Brewery)
                .Include(b => b.Ratings)
                .ThenInclude(r => r.Rater)
                .Include(b => b.Reviews)
                .ThenInclude(r => r.Reviewer)
                .Include(b => b.Type)
                .Where(beer => beer.IsDeleted == false)
                .FirstOrDefaultAsync(beer => beer.Id == id);

            if (beer == null)
            {
                throw new ArgumentNullException();
            }

            var beerDTO = new BeerDTO
            {
                BeerDTOId = beer.Id,
                Name = beer.Name,
                Type = new BeerTypeDTO
                {
                    Name = beer.Type.Name
                },
                Brewery = new BreweryDTO
                {
                    Name = beer.Brewery.Name
                },
                Country = new CountryDTO
                {
                    Name = beer.Country.Name
                },
                AlcoholPercentage = beer.AlcoholPercentage,
                Ratings = beer.Ratings.Select(r => new RatingDTO
                {
                    BeerId = r.BeerId,
                    RatingValue = r.RatingValue,
                    UserId = r.UserId,
                    UserName = r.Rater.UserName
                }).ToList(),
                Reviews = beer.Reviews.Select(r => new ReviewDTO
                {
                    BeerId = r.BeerId,
                    Dislikes = r.Dislikes,
                    IsFlagged = r.IsFlagged,
                    Likes = r.Likes,
                    ReviewText = r.ReviewText,
                    Reviewer = r.Reviewer.UserName,
                    UserId = r.UserId
                }).ToList()
            };

            return beerDTO;
        }

        //public ICollection<BeerDTO> GetAllBeers()
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        }).ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that shows all beers in the database.
        /// </summary>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> GetAllBeersAsync()
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }

        //public ICollection<BeerDTO> GetAllBeersOfTheMonth()
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false && beer.IsBeerOfTheMonth == true)
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                Reviewer = r.Reviewer.UserName,
        //                UserId = r.UserId
        //            }).ToList()
        //        }).ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that shows all beers of the month.
        /// </summary>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> GetAllBeersOfTheMonthAsync()
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false && beer.IsBeerOfTheMonth == true)
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        Reviewer = r.Reviewer.UserName,
                        UserId = r.UserId
                    }).ToList()
                }).ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }

        //public ICollection<BeerDTO> GetAllBeersByCountry(int countryId)
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Where(b => b.CountryId == countryId)
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        }).ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that shows all beers by given country ID.
        /// </summary>
        /// <param name="countryId">int</param>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> GetAllBeersByCountryAsync(int countryId)
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Where(b => b.CountryId == countryId)
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }

        //public ICollection<BeerDTO> SearchBeersByCountry(string country)
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Where(b => b.Country.Name.Contains(country))
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        }).ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that shows all beers by given country name.
        /// </summary>
        /// <param name="country">string</param>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> SearchBeersByCountryAsync(string country)
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Where(b => b.Country.Name.Contains(country))
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }

        //public ICollection<BeerDTO> GetAllBeersByStyle(int styleId)
        //{
        //    var beerViewModels = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Where(b => b.TypeId == styleId)
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        }).ToList();

        //    if (beerViewModels == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beerViewModels;
        //}

        /// <summary>
        /// An async method that shows all beers by given style ID.
        /// </summary>
        /// <param name="styleId">int</param>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> GetAllBeersByStyleAsync(int styleId)
        {
            var beerViewModels = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Where(b => b.TypeId == styleId)
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).ToListAsync();

            if (beerViewModels == null)
            {
                throw new ArgumentNullException();
            }

            return beerViewModels;
        }

        //public ICollection<BeerDTO> GetAllBeersByBrewery(int breweryId)
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Where(x => x.BreweryId == breweryId)
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        })
        //        .ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that shows all beers by given brewery ID.
        /// </summary>
        /// <param name="breweryId">int</param>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> GetAllBeersByBreweryAsync(int breweryId)
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Where(x => x.BreweryId == breweryId)
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                })
                .ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }

        //public ICollection<BeerDTO> SearchBeersByBrewery(string brewery)
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Where(beer => beer.Brewery.Name.Contains(brewery))
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        })
        //        .ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that shows all beers by given brewery name.
        /// </summary>
        /// <param name="brewery">string</param>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> SearchBeersByBreweryAsync(string brewery)
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Where(beer => beer.Brewery.Name.Contains(brewery))
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                })
                .ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }

        //public ICollection<BeerDTO> GetAllBeersByABV()
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        }).OrderBy(beer => beer.AlcoholPercentage).ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that shows all beers by given ABV.
        /// </summary>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> GetAllBeersByABVAsync()
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).OrderBy(beer => beer.AlcoholPercentage).ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }

        //public ICollection<BeerDTO> GetAllBeersByName()
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        }).OrderBy(beer => beer.Name).ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that shows all beers by beer name.
        /// </summary>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> GetAllBeersByNameAsync()
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).OrderBy(beer => beer.Name).ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }

        //public ICollection<BeerDTO> SearchBeersByName(string name)
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Where(beer => beer.Name.Contains(name))
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        }).OrderBy(beer => beer.Name).ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that searches beers by given name.
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> SearchBeersByNameAsync(string name)
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Where(beer => beer.Name.Contains(name))
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).OrderBy(beer => beer.Name).ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }

        //public ICollection<BeerDTO> GetAllBeersByRating()
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        }).ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that searches beers by rating.
        /// </summary>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> GetAllBeersByRatingAsync()
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }
        //public ICollection<BeerDTO> SearchBeers(string searchParams)
        //{
        //    var beers = context.Beers
        //        .Where(beer => beer.IsDeleted == false)
        //        .Where(beer => beer.Name.Contains(searchParams) || beer.Country.Name.Contains(searchParams) ||
        //                beer.Brewery.Name.Contains(searchParams) || beer.Type.Name.Contains(searchParams))
        //        .Select(beer => new BeerDTO
        //        {
        //            BeerDTOId = beer.Id,
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //            Ratings = beer.Ratings.Select(r => new RatingDTO
        //            {
        //                BeerId = r.BeerId,
        //                RatingValue = r.RatingValue,
        //                UserId = r.UserId,
        //                UserName = r.Rater.UserName
        //            }).ToList(),
        //            Reviews = beer.Reviews.Select(r => new ReviewDTO
        //            {
        //                BeerId = r.BeerId,
        //                Dislikes = r.Dislikes,
        //                IsFlagged = r.IsFlagged,
        //                Likes = r.Likes,
        //                ReviewText = r.ReviewText,
        //                UserId = r.Reviewer.Id
        //            }).ToList()
        //        }).OrderBy(beer => beer.Name).ToList();

        //    if (beers == null)
        //    {
        //        throw new ArgumentNullException();
        //    }

        //    return beers;
        //}

        /// <summary>
        /// An async method that searches beers by given parameters.
        /// </summary>
        /// <param name="searchParams">string</param>
        /// <returns>ICollection<BeerDTO></returns>
        public async Task<ICollection<BeerDTO>> SearchBeersAsync(string searchParams)
        {
            var beers = await context.Beers
                .Where(beer => beer.IsDeleted == false)
                .Where(beer => beer.Name.Contains(searchParams) || beer.Country.Name.Contains(searchParams) ||
                        beer.Brewery.Name.Contains(searchParams) || beer.Type.Name.Contains(searchParams))
                .Select(beer => new BeerDTO
                {
                    BeerDTOId = beer.Id,
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                    Ratings = beer.Ratings.Select(r => new RatingDTO
                    {
                        BeerId = r.BeerId,
                        RatingValue = r.RatingValue,
                        UserId = r.UserId,
                        UserName = r.Rater.UserName
                    }).ToList(),
                    Reviews = beer.Reviews.Select(r => new ReviewDTO
                    {
                        BeerId = r.BeerId,
                        Dislikes = r.Dislikes,
                        IsFlagged = r.IsFlagged,
                        Likes = r.Likes,
                        ReviewText = r.ReviewText,
                        UserId = r.Reviewer.Id
                    }).ToList()
                }).OrderBy(beer => beer.Name).ToListAsync();

            if (beers == null)
            {
                throw new ArgumentNullException();
            }

            return beers;
        }

        //public BeerDTO CreateBeer(BeerDTO beerDTO)
        //{
        //    var beer = new Beer
        //    {
        //        Name = beerDTO.Name,
        //        AlcoholPercentage = beerDTO.AlcoholPercentage,
        //        BreweryId = this.context.Breweries.
        //        FirstOrDefault(br => br.Name == beerDTO.Brewery.Name).Id,
        //        CountryId = this.context.Countries.
        //        FirstOrDefault(c => c.Name == beerDTO.Country.Name).Id,
        //        TypeId = this.context.BeerTypes
        //            .FirstOrDefault(t => t.Name == beerDTO.Type.Name).Id
        //    };

        //    context.Beers.Add(beer);
        //    context.SaveChanges();

        //    return beerDTO;
        //}

        /// <summary>
        /// An async method that creates a beer by given BeerDTO.
        /// </summary>
        /// <param name="beerDTO">BeerDTO</param>
        /// <returns>BeerDTO</returns>
        public async Task<BeerDTO> CreateBeerAsync(BeerDTO beerDTO)
        {
            var beer = new Beer
            {
                Name = beerDTO.Name,
                AlcoholPercentage = beerDTO.AlcoholPercentage,
                BreweryId = this.context.Breweries.
                FirstOrDefault(br => br.Name == beerDTO.Brewery.Name).Id,
                CountryId = this.context.Countries.
                FirstOrDefault(c => c.Name == beerDTO.Country.Name).Id,
                TypeId = this.context.BeerTypes
                    .FirstOrDefault(t => t.Name == beerDTO.Type.Name).Id
            };

            await context.Beers.AddAsync(beer);
            await context.SaveChangesAsync();

            return beerDTO;
        }

        public async Task<bool> AddRatingToBeerAsync(int id, string userName, int rating)
        {
            var beer = await this.context.Beers
                .Where(beer => beer.IsDeleted == false)
                .FirstOrDefaultAsync(beer => beer.Id == id);

            var user = await this.context.Users
                .Where(user => user.IsDeleted == false)
                .FirstOrDefaultAsync(user => user.UserName == userName);

            if (beer != null && user != null)
            {
                var ratingToAdd = new Rating
                {
                    BeerId = beer.Id,
                    UserId = user.Id,
                    RatingValue = rating
                };

                beer.Ratings.Add(ratingToAdd);
                user.Ratings.Add(ratingToAdd);

                await this.context.SaveChangesAsync();
                return true;
            }

            return false;
        }
        public async Task<BeerDTO> AddBeerToUserDrankListAsync(int beerId, string userName)
        {
            var user = await context.Users
                .Where(user => user.IsDeleted == false)
                .FirstOrDefaultAsync(user => user.UserName == userName);

            var beer = await context.Beers
                .Include(b => b.Country)
                .Include(b => b.Brewery)
                .Include(b => b.Ratings)
                .ThenInclude(r => r.Rater)
                .Include(b => b.Reviews)
                .ThenInclude(r => r.Reviewer)
                .Include(b => b.Type)
                .Where(beer => beer.IsDeleted == false)
                .FirstOrDefaultAsync(beer => beer.Id == beerId);

            var drankBeer = new DrankBeer
            {
                BeerId = beer.Id,
                UserId = user.Id
            };

            this.context.DrankBeers.Add(drankBeer);
            user.DrankList.Add(drankBeer);
            beer.DrankUsers.Add(drankBeer);

            await context.SaveChangesAsync();

            var beerDTO = new BeerDTO
            {
                BeerDTOId = beer.Id,
                Name = beer.Name,
                Type = new BeerTypeDTO
                {
                    Name = beer.Type.Name
                },
                Brewery = new BreweryDTO
                {
                    Name = beer.Brewery.Name
                },
                Country = new CountryDTO
                {
                    Name = beer.Country.Name
                },
                AlcoholPercentage = beer.AlcoholPercentage,
                Ratings = beer.Ratings.Select(r => new RatingDTO
                {
                    BeerId = r.BeerId,
                    RatingValue = r.RatingValue,
                    UserId = r.UserId,
                    UserName = r.Rater.UserName
                }).ToList(),
                Reviews = beer.Reviews.Select(r => new ReviewDTO
                {
                    BeerId = r.BeerId,
                    Dislikes = r.Dislikes,
                    IsFlagged = r.IsFlagged,
                    Likes = r.Likes,
                    ReviewText = r.ReviewText,
                    UserId = r.Reviewer.Id
                }).ToList()
            };

            return beerDTO;
        }

        public async Task<BeerDTO> AddBeerToUserWishListAsync(int beerId, string userName)
        {
            var user = await context.Users
                .Where(user => user.IsDeleted == false)
                .FirstOrDefaultAsync(user => user.UserName == userName);

            var beer = await context.Beers
                .Include(b => b.Country)
                .Include(b => b.Brewery)
                .Include(b => b.Ratings)
                .ThenInclude(r => r.Rater)
                .Include(b => b.Reviews)
                .ThenInclude(r => r.Reviewer)
                .Include(b => b.Type)
                .Where(beer => beer.IsDeleted == false)
                .Include(beer => beer.Brewery)
                .FirstOrDefaultAsync(beer => beer.Id == beerId);

            var wishBeer = new WishBeer
            {
                BeerId = beer.Id,
                UserId = user.Id
            };

            this.context.WishBeers.Add(wishBeer);
            user.WishList.Add(wishBeer);
            beer.WishUsers.Add(wishBeer);

            await context.SaveChangesAsync();

            var beerDTO = new BeerDTO
            {
                BeerDTOId = beer.Id,
                Name = beer.Name,
                Type = new BeerTypeDTO
                {
                    Name = beer.Type.Name
                },
                Brewery = new BreweryDTO
                {
                    Name = beer.Brewery.Name
                },
                Country = new CountryDTO
                {
                    Name = beer.Country.Name
                },
                AlcoholPercentage = beer.AlcoholPercentage,
                Ratings = beer.Ratings.Select(r => new RatingDTO
                {
                    BeerId = r.BeerId,
                    RatingValue = r.RatingValue,
                    UserId = r.UserId,
                    UserName = r.Rater.UserName
                }).ToList(),
                Reviews = beer.Reviews.Select(r => new ReviewDTO
                {
                    BeerId = r.BeerId,
                    Dislikes = r.Dislikes,
                    IsFlagged = r.IsFlagged,
                    Likes = r.Likes,
                    ReviewText = r.ReviewText,
                    UserId = r.Reviewer.Id
                }).ToList()
            };

            return beerDTO;
        }

        public async Task<BeerDTO> UpdateBeerAsync(int id, BeerDTO beerDTO)
        {
            var beer = await context.Beers
                .Include(b => b.Country)
                .Include(b => b.Brewery)
                .Include(b => b.Type)
                .Where(beer => beer.IsDeleted == false)
                .FirstOrDefaultAsync(beer => beer.Id == id);

            if (beer == null)
            {
                return null;
            }

            beer.AlcoholPercentage = beerDTO.AlcoholPercentage;
            var brewery = await this.context.Breweries
                .Where(b => b.Name == beerDTO.Brewery.Name)
                .FirstOrDefaultAsync(b => b.Id == beerDTO.BeerDTOId);
            beer.BreweryId = brewery.Id;
            //beer.BreweryId = this.context.Breweries
            //    .FirstOrDefault(br => br.Name == beerDTO.Brewery.Name).Id;
            //beer.CountryId = this.context.Countries
            //    .FirstOrDefault(c => c.Name == beerDTO.Country.Name).Id;
            var country = await this.context.Countries
                .Where(c => c.Name == beerDTO.Country.Name)
                .FirstOrDefaultAsync(c => c.Id == beerDTO.Country.CountryDTOId);
            beer.CountryId = country.Id;
            beer.Name = beerDTO.Name;

            await context.SaveChangesAsync();

            return beerDTO;
        }

        //public BeerDTO DeleteBeer(int id)
        //{
        //    BeerDTO beerDto;
        //    try
        //    {
        //        var beer = context.Beers
        //            .Include(b => b.Country)
        //            .Include(b => b.Brewery)
        //            .Include(b => b.Type)
        //            .Where(beer => beer.IsDeleted == false)
        //            .FirstOrDefault(beer => beer.Id == id);

        //        beer.IsDeleted = true;

        //        context.SaveChanges();

        //        beerDto = new BeerDTO
        //        {
        //            Name = beer.Name,
        //            Type = new BeerTypeDTO
        //            {
        //                Name = beer.Type.Name
        //            },
        //            Brewery = new BreweryDTO
        //            {
        //                Name = beer.Brewery.Name
        //            },
        //            Country = new CountryDTO
        //            {
        //                Name = beer.Country.Name
        //            },
        //            AlcoholPercentage = beer.AlcoholPercentage,
        //        };

        //    }

        //    catch (Exception)
        //    {
        //        return null;
        //    }

        //    return beerDto;
        //}

        /// <summary>
        /// An async method that deletes a certain beer by given id.
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>BeerDTO</returns>
        public async Task<BeerDTO> DeleteBeerAsync(int id)
        {
            BeerDTO beerDto;
            try
            {
                var beer = await context.Beers
                    .Include(b => b.Country)
                    .Include(b => b.Brewery)
                    .Include(b => b.Type)
                    .Where(beer => beer.IsDeleted == false)
                    .FirstOrDefaultAsync(beer => beer.Id == id);

                beer.IsDeleted = true;

                await context.SaveChangesAsync();

                beerDto = new BeerDTO
                {
                    Name = beer.Name,
                    Type = new BeerTypeDTO
                    {
                        Name = beer.Type.Name
                    },
                    Brewery = new BreweryDTO
                    {
                        Name = beer.Brewery.Name
                    },
                    Country = new CountryDTO
                    {
                        Name = beer.Country.Name
                    },
                    AlcoholPercentage = beer.AlcoholPercentage,
                };

            }

            catch (Exception)
            {
                return null;
            }

            return beerDto;
        }
    }
}
