﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class BeerTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<string> Beers { get; set; }
    }
}
