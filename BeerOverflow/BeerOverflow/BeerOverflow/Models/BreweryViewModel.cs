﻿using BeerOverflow.Services.DTO;
using System.Collections.Generic;

namespace BeerOverflow.Web.Models
{
    public class BreweryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public ICollection<BeerViewModel> Beers { get; set; }
    }
}
