﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class ReviewViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Reviewer { get; set; }
        public int UserId { get; set; }
        public int BeerId { get; set; }
        public int? Likes { get; set; }
        public int? Dislikes { get; set; }
    }
}
