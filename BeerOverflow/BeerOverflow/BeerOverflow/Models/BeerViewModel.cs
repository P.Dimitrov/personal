﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class BeerViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double AlcoholPercentage { get; set; }
        public string Type { get; set; }
        public string Brewery { get; set; }
        public string Country { get; set; }
        public string NewReview { get; set; }
        public ICollection<string> RatingUserNames { get; set; }
        public double? Rating { get; set; }
        public int? RatingCount { get; set; }
        public ICollection<ReviewViewModel> Reviews { get; set; }

    }
}
