﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Web.Models;
using BeerOverflow.Services.DTO;

namespace BeerOverflow.Web.ApiControllers
{
    [Route("api/beers")]
    [ApiController]
    public class BeersApiController : ControllerBase
    {
        private readonly BeerOverflowContext context;
        private readonly IBeerService beerService;

        public BeersApiController(BeerOverflowContext context, IBeerService beerService)
        {
            this.context = context;
            this.beerService = beerService;
        }


        //public ActionResult<IEnumerable<BeerViewModel>> GetAllBeers()
        //{
        //    IEnumerable<BeerViewModel> beers = null;

        //    beers = this.beerService
        //    .GetAllBeers()
        //    .Select(b => new BeerViewModel
        //    {
        //        Id = b.BeerDTOId,
        //        AlcoholPercentage = b.AlcoholPercentage,
        //        Brewery = b.Brewery.Name.ToString(),
        //        Country = b.Country.Name.ToString(),
        //        Name = b.Name,
        //        Type = b.Type.Name
        //    });

        //    if (beers == null)
        //    {
        //        return BadRequest();
        //    }

        //    return Ok(beers);
        //}

        [HttpGet]
        public async Task<ActionResult<IEnumerable<BeerViewModel>>> GetAllBeersAsync()
        {
            // IEnumerable<BeerViewModel> beers = null;

            var beers = await this.beerService.GetAllBeersAsync();
            IEnumerable<BeerViewModel> beerList = beers.Select(b => new BeerViewModel
            {
                Id = b.BeerDTOId,
                AlcoholPercentage = b.AlcoholPercentage,
                Brewery = b.Brewery.Name.ToString(),
                Country = b.Country.Name.ToString(),
                Name = b.Name,
                Type = b.Type.Name
            });

            if (beers == null)
            {
                return BadRequest();
            }

            return Ok(beers);
            
        }


        //public ActionResult<BeerViewModel> GetBeer(int id)
        //{
        //    var beerDTO = this.beerService.GetBeer(id);

        //    if (beerDTO == null)
        //    {
        //        return NotFound();
        //    }

        //    var beerViewModel = new BeerViewModel
        //    {
        //        Id = beerDTO.BeerDTOId,
        //        AlcoholPercentage = beerDTO.AlcoholPercentage,
        //        Brewery = beerDTO.Brewery.Name.ToString(),
        //        Country = beerDTO.Country.Name.ToString(),
        //        Name = beerDTO.Name,
        //        Rating = beerDTO.Ratings.Count == 0 ? 0 : beerDTO.Ratings.Select(r => r.RatingValue).Average(),
        //        RatingCount = beerDTO.Ratings.Count(),
        //        Reviews = beerDTO.Reviews.Select(re => new ReviewViewModel
        //        {
        //            Text = re.ReviewText,
        //            Reviewer = re.Reviewer,
        //            Dislikes = re.Dislikes,
        //            Likes = re.Likes
        //        }).ToList(),
        //        Type = beerDTO.Type.Name
        //    };

        //    return Ok(beerViewModel);
        //}

        // GET: api/BeersApi/5
        [HttpGet("beer/{id}")]
        public async Task<ActionResult<BeerViewModel>> GetBeerAsync(int id)
        {
            var beerDTO = await this.beerService.GetBeerAsync(id);

            if (beerDTO == null)
            {
                return NotFound();
            }

            var beerViewModel = new BeerViewModel
            {
                Id = beerDTO.BeerDTOId,
                AlcoholPercentage = beerDTO.AlcoholPercentage,
                Brewery = beerDTO.Brewery.Name.ToString(),
                Country = beerDTO.Country.Name.ToString(),
                Name = beerDTO.Name,
                Rating = beerDTO.Ratings.Count == 0 ? 0 : beerDTO.Ratings.Select(r => r.RatingValue).Average(),
                RatingCount = beerDTO.Ratings.Count(),
                Reviews = beerDTO.Reviews.Select(re => new ReviewViewModel
                {
                    Text = re.ReviewText,
                    Reviewer = re.Reviewer,
                    Dislikes = re.Dislikes,
                    Likes = re.Likes
                }).ToList(),
                Type = beerDTO.Type.Name
            };

            return Ok(beerViewModel);
        }


        //public ActionResult<IEnumerable<BeerViewModel>> GetBeersByCountry(int id)
        //{
        //    var beerViewModels = this.beerService
        //        .GetAllBeersByCountry(id)
        //        .Select(b => new BeerViewModel
        //        {
        //            Id = b.BeerDTOId,
        //            AlcoholPercentage = b.AlcoholPercentage,
        //            Brewery = b.Brewery.Name.ToString(),
        //            Country = b.Country.Name.ToString(),
        //            Name = b.Name,
        //            Type = b.Type.Name
        //        });

        //    if (beerViewModels == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(beerViewModels);
        //}

        [HttpGet("country/{id}")]
        public async Task<ActionResult<IEnumerable<BeerViewModel>>> GetBeersByCountryAsync(int id)
        {
            var beers = await this.beerService.GetAllBeersByCountryAsync(id);
            var beerViewModels = beers.Select(b => new BeerViewModel
                {
                    Id = b.BeerDTOId,
                    AlcoholPercentage = b.AlcoholPercentage,
                    Brewery = b.Brewery.Name.ToString(),
                    Country = b.Country.Name.ToString(),
                    Name = b.Name,
                    Type = b.Type.Name
                });

            if (beers == null)
            {
                return NotFound();
            }

            return Ok(beerViewModels);
        }


        //public ActionResult<IEnumerable<BeerViewModel>> SearchBeersByCountry([FromQuery] string country)
        //{
        //    var beerViewModels = this.beerService
        //        .SearchBeersByCountry(country)
        //        .Select(b => new BeerViewModel
        //        {
        //            Id = b.BeerDTOId,
        //            AlcoholPercentage = b.AlcoholPercentage,
        //            Brewery = b.Brewery.Name.ToString(),
        //            Country = b.Country.Name.ToString(),
        //            Name = b.Name,
        //            Type = b.Type.Name
        //        });

        //    if (beerViewModels == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(beerViewModels);
        //}

        [HttpGet("search/country")]
        public async Task<ActionResult<IEnumerable<BeerViewModel>>> SearchBeersByCountryAsync([FromQuery] string country)
        {
            var beers = await this.beerService.SearchBeersByCountryAsync(country);
            var beerViewModels = beers.Select(b => new BeerViewModel
                {
                    Id = b.BeerDTOId,
                    AlcoholPercentage = b.AlcoholPercentage,
                    Brewery = b.Brewery.Name.ToString(),
                    Country = b.Country.Name.ToString(),
                    Name = b.Name,
                    Type = b.Type.Name
                });

            if (beers == null)
            {
                return NotFound();
            }

            return Ok(beerViewModels);
        }


        //public ActionResult<IEnumerable<BeerViewModel>> SearchBeersByBrewery([FromQuery] string brewery)
        //{
        //    var beerViewModels = this.beerService
        //        .SearchBeersByBrewery(brewery)
        //        .Select(b => new BeerViewModel
        //        {
        //            Id = b.BeerDTOId,
        //            AlcoholPercentage = b.AlcoholPercentage,
        //            Brewery = b.Brewery.Name.ToString(),
        //            Country = b.Country.Name.ToString(),
        //            Name = b.Name,
        //            Type = b.Type.Name
        //        });

        //    if (beerViewModels == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(beerViewModels);
        //}

        [HttpGet("search/brewery")]
        public async Task<ActionResult<IEnumerable<BeerViewModel>>> SearchBeersByBreweryAsync([FromQuery] string brewery)
        {
            var beers = await this.beerService
                .SearchBeersByBreweryAsync(brewery);
            var beerViewModels = beers.Select(b => new BeerViewModel
                {
                    Id = b.BeerDTOId,
                    AlcoholPercentage = b.AlcoholPercentage,
                    Brewery = b.Brewery.Name.ToString(),
                    Country = b.Country.Name.ToString(),
                    Name = b.Name,
                    Type = b.Type.Name
                });

            if (beers == null)
            {
                return NotFound();
            }

            return Ok(beerViewModels);
        }


        //public ActionResult<IEnumerable<BeerViewModel>> SearchBeersByName([FromQuery] string name)
        //{
        //    var beerViewModels = this.beerService
        //        .SearchBeersByName(name)
        //        .Select(b => new BeerViewModel
        //        {
        //            Id = b.BeerDTOId,
        //            AlcoholPercentage = b.AlcoholPercentage,
        //            Brewery = b.Brewery.Name.ToString(),
        //            Country = b.Country.Name.ToString(),
        //            Name = b.Name,
        //            Type = b.Type.Name
        //        });

        //    if (beerViewModels == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(beerViewModels);
        //}

        [HttpGet("search/name")]
        public async Task<ActionResult<IEnumerable<BeerViewModel>>> SearchBeersByNameAsync([FromQuery] string name)
        {
            var beers = await this.beerService.SearchBeersByNameAsync(name);
            var beerViewModels = beers.Select(b => new BeerViewModel
                {
                    Id = b.BeerDTOId,
                    AlcoholPercentage = b.AlcoholPercentage,
                    Brewery = b.Brewery.Name.ToString(),
                    Country = b.Country.Name.ToString(),
                    Name = b.Name,
                    Type = b.Type.Name
                });

            if (beers == null)
            {
                return NotFound();
            }

            return Ok(beerViewModels);
        }


        //public ActionResult<IEnumerable<BeerViewModel>> GetBeersByStyle(int id)
        //{
        //    var beers = this.beerService.GetAllBeersByStyleAsync(id);
        //    var beerViewModels = beers.Select(b => new BeerViewModel
        //    {
        //            Id = b.BeerDTOId,
        //            AlcoholPercentage = b.AlcoholPercentage,
        //            Brewery = b.Brewery.Name.ToString(),
        //            Country = b.Country.Name.ToString(),
        //            Name = b.Name,
        //            Type = b.Type.Name
        //        });

        //    if (beers == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(beerViewModels);
        //}

        [HttpGet("style/{id}")]
        public async Task<ActionResult<IEnumerable<BeerViewModel>>> GetBeersByStyleAsync(int id)
        {
            var beers = await this.beerService.GetAllBeersByStyleAsync(id);
            var beerViewModels = beers.Select(b => new BeerViewModel
            {
                Id = b.BeerDTOId,
                AlcoholPercentage = b.AlcoholPercentage,
                Brewery = b.Brewery.Name.ToString(),
                Country = b.Country.Name.ToString(),
                Name = b.Name,
                Type = b.Type.Name
            });

            if (beers == null)
            {
                return NotFound();
            }

            return Ok(beerViewModels);
        }

        //[HttpGet("brewery/{id}")]
        //public ActionResult<IEnumerable<BeerViewModel>> GetBeersByBrewery(int id)
        //{
        //    var beerViewModels = this.beerService
        //        .GetAllBeersByBrewery(id)
        //        .Select(b => new BeerViewModel
        //        {
        //            Id = b.BeerDTOId,
        //            AlcoholPercentage = b.AlcoholPercentage,
        //            Brewery = b.Brewery.Name.ToString(),
        //            Country = b.Country.Name.ToString(),
        //            Name = b.Name,
        //            Type = b.Type.Name
        //        });

        //    if (beerViewModels == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(beerViewModels);
        //}
        [HttpGet("brewery/{id}")]
        public async Task<ActionResult<IEnumerable<BeerViewModel>>> GetBeersByBreweryAsync(int id)
        {
            var beers = await this.beerService.GetAllBeersByBreweryAsync(id);
            var beerViewModels = beers.Select(b => new BeerViewModel
                {
                    Id = b.BeerDTOId,
                    AlcoholPercentage = b.AlcoholPercentage,
                    Brewery = b.Brewery.Name.ToString(),
                    Country = b.Country.Name.ToString(),
                    Name = b.Name,
                    Type = b.Type.Name
                });

            if (beerViewModels == null)
            {
                return NotFound();
            }

            return Ok(beerViewModels);
        }


        //public ActionResult<IEnumerable<BeerViewModel>> GetBeersByName()
        //{
        //    var beerViewModels = this.beerService
        //        .GetAllBeersByName()
        //        .Select(b => new BeerViewModel
        //        {
        //            Id = b.BeerDTOId,
        //            AlcoholPercentage = b.AlcoholPercentage,
        //            Brewery = b.Brewery.Name.ToString(),
        //            Country = b.Country.Name.ToString(),
        //            Name = b.Name,
        //            Type = b.Type.Name
        //        });

        //    if (beerViewModels == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(beerViewModels);
        //}
        [HttpGet("sort/name")]
        public async Task<ActionResult<IEnumerable<BeerViewModel>>> GetBeersByNameAsync()
        {
            var beers = await this.beerService.GetAllBeersByNameAsync();
            var beerViewModels = beers.Select(b => new BeerViewModel
                {
                    Id = b.BeerDTOId,
                    AlcoholPercentage = b.AlcoholPercentage,
                    Brewery = b.Brewery.Name.ToString(),
                    Country = b.Country.Name.ToString(),
                    Name = b.Name,
                    Type = b.Type.Name
                });

            if (beerViewModels == null)
            {
                return NotFound();
            }

            return Ok(beerViewModels);
        }

        //public ActionResult<IEnumerable<BeerViewModel>> GetBeersByABV()
        //{
        //    var beerViewModels = this.beerService
        //        .GetAllBeersByABV()
        //        .Select(b => new BeerViewModel
        //        {
        //            Id = b.BeerDTOId,
        //            AlcoholPercentage = b.AlcoholPercentage,
        //            Brewery = b.Brewery.Name.ToString(),
        //            Country = b.Country.Name.ToString(),
        //            Name = b.Name,
        //            Type = b.Type.Name
        //        });

        //    if (beerViewModels == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(beerViewModels);
        //}

        [HttpGet("sort/abv")]
        public async Task<ActionResult<IEnumerable<BeerViewModel>>> GetBeersByABVAsync()
        {
            var beers = await this.beerService.GetAllBeersByABVAsync();
            var beerViewModels = beers.Select(b => new BeerViewModel
                {
                    Id = b.BeerDTOId,
                    AlcoholPercentage = b.AlcoholPercentage,
                    Brewery = b.Brewery.Name.ToString(),
                    Country = b.Country.Name.ToString(),
                    Name = b.Name,
                    Type = b.Type.Name
                });

            if (beerViewModels == null)
            {
                return NotFound();
            }

            return Ok(beerViewModels);
        }


        //public ActionResult<IEnumerable<BeerViewModel>> GetBeersByRating()
        //{
        //    var beerViewModels = this.beerService
        //        .GetAllBeersByRating()
        //        .Select(b => new BeerViewModel
        //        {
        //            Id = b.BeerDTOId,
        //            AlcoholPercentage = b.AlcoholPercentage,
        //            Brewery = b.Brewery.Name.ToString(),
        //            Country = b.Country.Name.ToString(),
        //            Name = b.Name,
        //            Type = b.Type.Name
        //        }).OrderBy(beer => beer.Rating);

        //    if (beerViewModels == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(beerViewModels);
        //}

        [HttpGet("sort/rating")]
        public async Task<ActionResult<IEnumerable<BeerViewModel>>> GetBeersByRatingAsync()
        {
            var beers = await this.beerService.GetAllBeersByRatingAsync();
            var beerViewModels = beers.Select(b => new BeerViewModel
                {
                    Id = b.BeerDTOId,
                    AlcoholPercentage = b.AlcoholPercentage,
                    Brewery = b.Brewery.Name.ToString(),
                    Country = b.Country.Name.ToString(),
                    Name = b.Name,
                    Type = b.Type.Name
                }).OrderBy(beer => beer.Rating);

            if (beerViewModels == null)
            {
                return NotFound();
            }

            return Ok(beerViewModels);
        }

        //public ActionResult<BeerViewModel> UpdateBeer(int id, [FromBody] BeerViewModel beer)
        //{
        //    var beerDTO = new BeerDTO
        //    {
        //        AlcoholPercentage = beer.AlcoholPercentage,
        //        Country = new CountryDTO
        //        {
        //            Name = beer.Country
        //        },
        //        Brewery = new BreweryDTO
        //        {
        //            Name = beer.Brewery
        //        },
        //        Name = beer.Name,
        //        Type = new BeerTypeDTO
        //        {
        //            Name = beer.Type
        //        }
        //    };

        //    var updatedBeerDTO = this.beerService.UpdateBeer(id, beerDTO);

        //    if (updatedBeerDTO == null)
        //    {
        //        return BadRequest();
        //    }

        //    var beerViewModel = new BeerViewModel
        //    {
        //        Id = updatedBeerDTO.BeerDTOId,
        //        AlcoholPercentage = updatedBeerDTO.AlcoholPercentage,
        //        Brewery = updatedBeerDTO.Brewery.Name,
        //        Country = updatedBeerDTO.Country.Name,
        //        Name = updatedBeerDTO.Name,
        //        Type = updatedBeerDTO.Type.Name
        //    };

        //    return Ok(beerViewModel);
        //}

        // PUT: api/BeersApi/5
        [HttpPut("update/{id}")]
        public async Task<ActionResult<BeerViewModel>> UpdateBeerAsync(int id, [FromBody] BeerViewModel beer)
        {
            var beerDTO = new BeerDTO
            {
                AlcoholPercentage = beer.AlcoholPercentage,
                Country = new CountryDTO
                {
                    Name = beer.Country
                },
                Brewery = new BreweryDTO
                {
                    Name = beer.Brewery
                },
                Name = beer.Name,
                Type = new BeerTypeDTO
                {
                    Name = beer.Type
                }
            };

            var updatedBeerDTO = await this.beerService.UpdateBeerAsync(id, beerDTO);

            if (updatedBeerDTO == null)
            {
                return BadRequest();
            }

            var beerViewModel = new BeerViewModel
            {
                Id = updatedBeerDTO.BeerDTOId,
                AlcoholPercentage = updatedBeerDTO.AlcoholPercentage,
                Brewery = updatedBeerDTO.Brewery.Name,
                Country = updatedBeerDTO.Country.Name,
                Name = updatedBeerDTO.Name,
                Type = updatedBeerDTO.Type.Name
            };

            return Ok(beerViewModel);
        }

        // POST: api/BeersApi


        //public ActionResult<BeerViewModel> CreateBeer([FromBody] BeerViewModel beer)
        //{
        //    var beerDTO = new BeerDTO
        //    {
        //        AlcoholPercentage = beer.AlcoholPercentage,
        //        Country = new CountryDTO
        //        {
        //            Name = beer.Country
        //        },
        //        Brewery = new BreweryDTO
        //        {
        //            Name = beer.Brewery
        //        },
        //        Name = beer.Name,
        //        Type = new BeerTypeDTO
        //        {
        //            Name = beer.Type
        //        }
        //    };

        //    if (beerDTO == null)
        //    {
        //        return BadRequest();
        //    }

        //    var createdBeerDTO = this.beerService.CreateBeer(beerDTO);

        //    var beerViewModel = new BeerViewModel
        //    {
        //        Id = createdBeerDTO.BeerDTOId,
        //        AlcoholPercentage = createdBeerDTO.AlcoholPercentage,
        //        Brewery = createdBeerDTO.Brewery.Name,
        //        Country = createdBeerDTO.Country.Name,
        //        Name = createdBeerDTO.Name,
        //        Type = createdBeerDTO.Type.Name
        //    };

        //    return Ok(beerViewModel);
        //}

        [HttpPost("create")]
        public async Task<ActionResult<BeerViewModel>> CreateBeerAsync([FromBody] BeerViewModel beer)
        {
            var beerDTO = new BeerDTO
            {
                AlcoholPercentage = beer.AlcoholPercentage,
                Country = new CountryDTO
                {
                    Name = beer.Country
                },
                Brewery = new BreweryDTO
                {
                    Name = beer.Brewery
                },
                Name = beer.Name,
                Type = new BeerTypeDTO
                {
                    Name = beer.Type
                }
            };

            if (beerDTO == null)
            {
                return BadRequest();
            }

            var createdBeerDTO = await this.beerService.CreateBeerAsync(beerDTO);

            var beerViewModel = new BeerViewModel
            {
                Id = createdBeerDTO.BeerDTOId,
                AlcoholPercentage = createdBeerDTO.AlcoholPercentage,
                Brewery = createdBeerDTO.Brewery.Name,
                Country = createdBeerDTO.Country.Name,
                Name = createdBeerDTO.Name,
                Type = createdBeerDTO.Type.Name
            };

            return Ok(beerViewModel);
        }


        //public IActionResult DeleteBeer(int id)
        //{
        //    var isDeleted = this.beerService.DeleteBeer(id);

        //    if (isDeleted == null)
        //    {
        //        return BadRequest();
        //    }

        //    return Ok();
        //}

        // DELETE: api/BeersApi/5
        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeleteBeerAsync(int id)
        {
            var isDeleted = await this.beerService.DeleteBeerAsync(id);

            if (isDeleted == null)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}
