﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.ApiControllers
{
    [Route("api/users")]
    [ApiController]
    public class UsersApiController : Controller
    {
        private readonly IUserService userService;

        public UsersApiController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetUser(int id)
        {
            try
            {
                var user = this.userService.GetUser(id);

                return Ok(user);
            }
            catch (Exception)
            {
                return NotFound($"This user is not created.");
            }
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetAllUsers()
        {
            try
            {
                var users = this.userService.GetAllUsers();
                return Ok(users);
            }
            catch (Exception)
            {
                return BadRequest($"No users created.");
            }
        }

        [HttpPost]
        [Route("")]
        public IActionResult Post([FromBody] UserViewModel userModel)
        {
            if (userModel == null)
            {
                return BadRequest();
            }

            var userDTO = new UserDTO
            {
                UserDTOId = userModel.Id,
                Name = userModel.Name,
            };

            var newUser = this.userService.CreateUser(userDTO);

            return Created("Post", newUser);
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Put(int id, [FromBody]UserViewModel userModel)
        {
            if (id == 0 || userModel == null)
            {
                return BadRequest();
            }

            var userDTO = new UserDTO
            {
                UserDTOId = userModel.Id,
                Name = userModel.Name
            };
            var user = this.userService.UpdateUser(userDTO);

            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id, [FromBody]UserViewModel userModel)
        {
            if (id == 0 || userModel == null)
            {
                return BadRequest();
            }

            var user = this.userService.DeleteUser(userModel.Id);

            return Ok();
        }
    }
}
