﻿using System;
using System.Threading.Tasks;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Web.Controllers
{
    [Route("api/countries")]
    [ApiController]
    public class CountriesApiController : Controller
    {
        private readonly ICountryService countryService;

        public CountriesApiController(ICountryService countryService)
        {
            this.countryService = countryService;
        }


        //public IActionResult GetCountry(int id)
        //{
        //    try
        //    {
        //        var country = this.countryService.GetCountry(id);

        //        return Ok(country);
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound($"This country is not created.");
        //    }
        //}
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetCountryAsync(int id)
        {
            try
            {
                var country = await this.countryService.GetCountryAsync(id);

                return Ok(country);
            }
            catch (Exception)
            {
                return NotFound($"This country is not created.");
            }
        }


        //public IActionResult GetAllCountries()
        //{
        //    try
        //    {
        //        var countries = this.countryService.GetAllCountries();
        //        return Ok(countries);
        //    }
        //    catch(Exception)
        //    {
        //        return BadRequest($"No countries created.");
        //    }
        //}

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllCountriesAsync()
        {
            try
            {
                var countries = await this.countryService.GetAllCountriesAsync();
                return Ok(countries);
            }
            catch (Exception)
            {
                return BadRequest($"No countries created.");
            }
        }


        //public IActionResult Post([FromBody] CountryViewModel countryModel)
        //{
        //    if (countryModel == null)
        //    {
        //        return BadRequest();
        //    }

        //    var countryDTO = new CountryDTO
        //    {
        //        CountryDTOId = countryModel.Id,
        //        Name = countryModel.Name,
        //    };

        //    var newCountry = this.countryService.CreateCountry(countryDTO);

        //    return Created("Post", newCountry);
        //}

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> PostAsync([FromBody] CountryViewModel countryModel)
        {
            if (countryModel == null)
            {
                return BadRequest();
            }

            var countryDTO = new CountryDTO
            {
                CountryDTOId = countryModel.Id,
                Name = countryModel.Name,
            };

            var newCountry = await this.countryService.CreateCountryAsync(countryDTO);

            return Created("Post", newCountry);
        }


        //public IActionResult Put(int id, [FromBody]CountryViewModel countryModel)
        //{
        //    if (id == 0 || countryModel == null)
        //    {
        //        return BadRequest();
        //    }

        //    var countryDTO = new CountryDTO
        //    {
        //        CountryDTOId = countryModel.Id,
        //        Name = countryModel.Name
        //    };
        //    var country = this.countryService.UpdateCountry(id, countryDTO);

        //    return Ok();
        //}

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody]CountryViewModel countryModel)
        {
            if (id == 0 || countryModel == null)
            {
                return BadRequest();
            }

            var countryDTO = new CountryDTO
            {
                CountryDTOId = countryModel.Id,
                Name = countryModel.Name
            };
            var country = await this.countryService.UpdateCountryAsync(id, countryDTO);

            return Ok();
        }


        //public IActionResult Delete(int id, [FromBody]CountryViewModel countryModel)
        //{
        //    if (id == 0 || countryModel == null)
        //    {
        //        return BadRequest();
        //    }

        //    var country = this.countryService.DeleteCountry(countryModel.Id);

        //    return Ok();
        //}

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteAsync(int id, [FromBody]CountryViewModel countryModel)
        {
            if (id == 0 || countryModel == null)
            {
                return BadRequest();
            }

            var country = await this.countryService.DeleteCountryAsync(countryModel.Id);

            return Ok();
        }
    }
}