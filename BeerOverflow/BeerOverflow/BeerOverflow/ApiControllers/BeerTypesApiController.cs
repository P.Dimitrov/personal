﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.ApiControllers
{
    [Route("api/styles")]
    [ApiController]
    public class BeerTypesApiController : Controller
    {
        private readonly IBeerTypeService beerTypeService;

        public BeerTypesApiController(IBeerTypeService beerTypeService)
        {
            this.beerTypeService = beerTypeService;
        }


        //public IActionResult GetBeerType(int id)
        //{
        //    try
        //    {
        //        var beerType = this.beerTypeService.GetBeerType(id);

        //        return Ok(beerType);
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound($"This type of beer is not created.");
        //    }
        //}

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetBeerType(int id)
        {
            try
            {
                var beerType = await this.beerTypeService.GetBeerTypeAsync(id);

                return Ok(beerType);
            }
            catch (Exception)
            {
                return NotFound($"This type of beer is not created.");
            }
        }


        //public IActionResult GetAllBeerTypes()
        //{
        //    try
        //    {
        //        var beerTypes = this.beerTypeService.GetAllBeerTypes();
        //        return Ok(beerTypes);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest($"No types of beer are created.");
        //    }
        //}

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllBeerTypes()
        {
            try
            {
                var beerTypes = await this.beerTypeService.GetAllBeerTypesAsync();
                return Ok(beerTypes);
            }
            catch (Exception)
            {
                return BadRequest($"No types of beer are created.");
            }
        }


        //public IActionResult Post([FromBody] BeerTypeViewModel beerTypeModel)
        //{
        //    if (beerTypeModel == null)
        //    {
        //        return BadRequest();
        //    }

        //    var beerTypeDTO = new BeerTypeDTO
        //    {
        //        BeerTypeDTOId = beerTypeModel.Id,
        //        Name = beerTypeModel.Name,
        //    };

        //    var newBeerType = this.beerTypeService.CreateBeerType(beerTypeDTO);

        //    return Created("Post", newBeerType);
        //}

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Post([FromBody] BeerTypeViewModel beerTypeModel)
        {
            if (beerTypeModel == null)
            {
                return BadRequest();
            }

            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = beerTypeModel.Id,
                Name = beerTypeModel.Name,
            };

            var newBeerType = await this.beerTypeService.CreateBeerTypeAsync(beerTypeDTO);

            return Created("Post", newBeerType);
        }


        //public IActionResult Put(int id, [FromBody]BeerTypeViewModel beerTypeModel)
        //{
        //    if (id == 0 || beerTypeModel == null)
        //    {
        //        return BadRequest();
        //    }

        //    var beerTypeDTO = new BeerTypeDTO
        //    {
        //        BeerTypeDTOId = beerTypeModel.Id,
        //        Name = beerTypeModel.Name
        //    };
        //    var beerType = this.beerTypeService.UpdateBeerType(beerTypeDTO);

        //    return Ok();
        //}

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]BeerTypeViewModel beerTypeModel)
        {
            if (id == 0 || beerTypeModel == null)
            {
                return BadRequest();
            }

            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = beerTypeModel.Id,
                Name = beerTypeModel.Name
            };
            var beerType = await this.beerTypeService.UpdateBeerTypeAsync(beerTypeDTO);

            return Ok();
        }

        
        //public IActionResult Delete(int id, [FromBody]CountryViewModel beerTypeModel)
        //{
        //    if (id == 0 || beerTypeModel == null)
        //    {
        //        return BadRequest();
        //    }

        //    var beerType = this.beerTypeService.DeleteBeerType(beerTypeModel.Id);

        //    return Ok();
        //}

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(int id, [FromBody]CountryViewModel beerTypeModel)
        {
            if (id == 0 || beerTypeModel == null)
            {
                return BadRequest();
            }

            var beerType = await this.beerTypeService.DeleteBeerTypeAsync(beerTypeModel.Id);

            return Ok();
        }

    }
}
