﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.ApiControllers
{
    [Route("api/breweries")]
    [ApiController]
    public class BreweriesApiController : Controller
    {
        private readonly IBreweryService breweryService;

        public BreweriesApiController(IBreweryService breweryService)
        {
            this.breweryService = breweryService;
        }


        //public IActionResult GetBrewery(int id)
        //{
        //    try
        //    {
        //        var brewery = this.breweryService.GetBrewery(id);

        //        return Ok(brewery);
        //    }
        //    catch (Exception)
        //    {
        //        return NotFound($"This brewery is not created.");
        //    }

        //}
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetBreweryAsync(int id)
        {
            try
            {
                var brewery = await this.breweryService.GetBreweryAsync(id);

                return Ok(brewery);
            }
            catch (Exception)
            {
                return NotFound($"This brewery is not created.");
            }

        }

        //public IActionResult GetAllBreweries()
        //{
        //    try
        //    {
        //        var breweries = this.breweryService.GetAllBreweries();
        //        return Ok(breweries);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest($"No breweries created.");
        //    }
        //}

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllBreweriesAsync()
        {
            try
            {
                var breweries = await this.breweryService.GetAllBreweriesAsync();
                return Ok(breweries);
            }
            catch (Exception)
            {
                return BadRequest($"No breweries created.");
            }
        }


        //public IActionResult Post([FromBody] BreweryViewModel breweryModel)
        //{
        //    if (breweryModel == null)
        //    {
        //        return BadRequest();
        //    }

        //    var breweryDTO = new BreweryDTO
        //    {
        //        BreweryDTOId = breweryModel.Id,
        //        Name = breweryModel.Name,
        //    };

        //    var newBrewery = this.breweryService.CreateBrewery(breweryDTO);

        //    return Created("Post", newBrewery);
        //}

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> PostAsync([FromBody] BreweryViewModel breweryModel)
        {
            if (breweryModel == null)
            {
                return BadRequest();
            }

            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = breweryModel.Id,
                Name = breweryModel.Name,
            };

            var newBrewery = await this.breweryService.CreateBreweryAsync(breweryDTO);

            return Created("Post", newBrewery);
        }

        //public IActionResult Put(int id, [FromBody]BreweryViewModel breweryModel)
        //{
        //    var breweryDTO = new BreweryDTO
        //    {
        //        Name = breweryModel.Name,
        //        Country = new CountryDTO
        //        {
        //            Name = breweryModel.Country
        //        },
        //    };

        //    var updatedBreweryDTO = this.breweryService.UpdateBrewery(id, breweryDTO);

        //    if (updatedBreweryDTO == null)
        //    {
        //        return BadRequest();
        //    }

        //    var beerViewModel = new BeerViewModel
        //    {
        //        Name = updatedBreweryDTO.Name,
        //        Country = updatedBreweryDTO.Country.Name,  
        //    };

        //    return Ok(breweryModel);
        //}

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody]BreweryViewModel breweryModel)
        {
            var breweryDTO = new BreweryDTO
            {
                Name = breweryModel.Name,
                Country = new CountryDTO
                {
                    Name = breweryModel.Country
                },
            };

            var updatedBreweryDTO = await this.breweryService.UpdateBreweryAsync(id, breweryDTO);

            if (updatedBreweryDTO == null)
            {
                return BadRequest();
            }

            var beerViewModel = new BeerViewModel
            {
                Name = updatedBreweryDTO.Name,
                Country = updatedBreweryDTO.Country.Name,
            };

            return Ok(breweryModel);
        }


        //public IActionResult Delete(int id, [FromBody]CountryViewModel breweryModel)
        //{
        //    if (id == 0 || breweryModel == null)
        //    {
        //        return BadRequest();
        //    }

        //    var brewery = this.breweryService.DeleteBrewery(breweryModel.Id);

        //    return Ok();
        //}

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteAsync(int id, [FromBody]CountryViewModel breweryModel)
        {
            if (id == 0 || breweryModel == null)
            {
                return BadRequest();
            }

            var brewery = await this.breweryService.DeleteBreweryAsync(breweryModel.Id);

            return Ok();
        }
    }
}
