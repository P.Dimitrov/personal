﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Web.Models;
using BeerOverflow.Services.DTO;
using Microsoft.AspNetCore.Authorization;

namespace BeerOverflow.Web.Controllers
{
    public class BeersController : Controller
    {
        private readonly BeerOverflowContext context;
        private readonly IBeerService beerService;
        private readonly IBreweryService breweryService;
        private readonly ICountryService countryService;
        private readonly IBeerTypeService beerTypeService;
        private readonly IReviewService reviewService;

        public BeersController(BeerOverflowContext context, IBeerService beerService,
            IBreweryService breweryService, ICountryService countryService, IBeerTypeService beerTypeService
            , IReviewService reviewService)
        {
            this.context = context;
            this.beerService = beerService;
            this.breweryService = breweryService;
            this.countryService = countryService;
            this.beerTypeService = beerTypeService;
            this.reviewService = reviewService;
        }

        // GET: Beers
        //public IActionResult Index(string sortOrder, string searchParams)
        //{
        //    ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
        //    ViewBag.CountrySortParm = sortOrder == "Country" ? "country_desc" : "Country";
        //    ViewBag.BrewerySortParm = sortOrder == "Brewery" ? "brewery_dsec" : "Brewery";
        //    ViewBag.TypeSortParm = sortOrder == "Type" ? "type_dsec" : "Type";

        //    var beers = this.beerService.GetAllBeers()
        //        .Select(b => new BeerViewModel
        //        {
        //            Id = b.BeerDTOId,
        //            AlcoholPercentage = b.AlcoholPercentage,
        //            Brewery = b.Brewery.Name.ToString(),
        //            Country = b.Country.Name.ToString(),
        //            Rating = b.Ratings.Count == 0 ? 0 : b.Ratings.Select(r => r.RatingValue).Average(),
        //            RatingCount = b.Ratings.Count(),
        //            Name = b.Name,
        //            Type = b.Type.Name
        //        });

        //    if (!String.IsNullOrEmpty(searchParams))
        //    {
        //        beers = beers.Where(b => b.Brewery.ToLower().Contains(searchParams.ToLower()) || b.Country.ToLower().Contains(searchParams.ToLower())
        //                || b.Name.ToLower().Contains(searchParams.ToLower()) || b.Type.ToLower().Contains(searchParams.ToLower()));
        //    }

        //    if (beers == null)
        //    {
        //        return BadRequest();
        //    }

        //    switch (sortOrder)
        //    {
        //        case "name_desc":
        //            beers = beers.OrderByDescending(b => b.Name);
        //            break;
        //        case "Country":
        //            beers = beers.OrderBy(b => b.Country);
        //            break;
        //        case "country_desc":
        //            beers = beers.OrderByDescending(b => b.Country);
        //            break;
        //        case "Brewery":
        //            beers = beers.OrderBy(b => b.Brewery);
        //            break;
        //        case "brewery_desc":
        //            beers = beers.OrderByDescending(b => b.Brewery);
        //            break;
        //        case "Type":
        //            beers = beers.OrderBy(b => b.Type);
        //            break;
        //        case "type_desc":
        //            beers = beers.OrderByDescending(b => b.Type);
        //            break;
        //        default:
        //            beers = beers.OrderBy(s => s.Name);
        //            break;
        //    }

        //    return View(beers);
        //}

        // GET: Beers
        public async Task<IActionResult> Index(string sortOrder, string searchParams)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.CountrySortParm = sortOrder == "Country" ? "country_desc" : "Country";
            ViewBag.BrewerySortParm = sortOrder == "Brewery" ? "brewery_dsec" : "Brewery";
            ViewBag.TypeSortParm = sortOrder == "Type" ? "type_dsec" : "Type";

            var allBeers = await this.beerService.GetAllBeersAsync();
            var beers = allBeers
                .Select(b => new BeerViewModel
                {
                    Id = b.BeerDTOId,
                    AlcoholPercentage = b.AlcoholPercentage,
                    Brewery = b.Brewery.Name.ToString(),
                    Country = b.Country.Name.ToString(),
                    Rating = b.Ratings.Count == 0 ? 0 : b.Ratings.Select(r => r.RatingValue).Average(),
                    RatingCount = b.Ratings.Count(),
                    Name = b.Name,
                    Type = b.Type.Name
                });

            if (!String.IsNullOrEmpty(searchParams))
            {
                beers = beers.Where(b => b.Brewery.ToLower().Contains(searchParams.ToLower()) || b.Country.ToLower().Contains(searchParams.ToLower())
                        || b.Name.ToLower().Contains(searchParams.ToLower()) || b.Type.ToLower().Contains(searchParams.ToLower()));
            }

            if (beers == null)
            {
                return BadRequest();
            }

            beers = sortOrder switch
            {
                "name_desc" => beers.OrderByDescending(b => b.Name),
                "Country" => beers.OrderBy(b => b.Country),
                "country_desc" => beers.OrderByDescending(b => b.Country),
                "Brewery" => beers.OrderBy(b => b.Brewery),
                "brewery_desc" => beers.OrderByDescending(b => b.Brewery),
                "Type" => beers.OrderBy(b => b.Type),
                "type_desc" => beers.OrderByDescending(b => b.Type),
                _ => beers.OrderBy(s => s.Name),
            };
            return View(beers);
        }

        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> LikeReviewAsync(int beerId, int userId)
        {
            await this.reviewService.LikeReviewAsync(beerId, userId);

            return RedirectToAction(nameof(Details), new { id = beerId });
        }

        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> DislikeReviewAsync(int beerId, int userId)
        {
            await this.reviewService.DislikeReviewAsync(beerId, userId);

            return RedirectToAction(nameof(Details), new { id = beerId });
        }

        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> AddToWishListAsync(int beerId, string userName)
        {
            await this.beerService.AddBeerToUserWishListAsync(beerId, userName);

            return RedirectToAction(nameof(Details), new { id = beerId });
        }

        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> AddToDrankListAsync(int beerId, string userName)
        {
            await this.beerService.AddBeerToUserDrankListAsync(beerId, userName);

            return RedirectToAction(nameof(Details), new { id = beerId });
        }

        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> AddReviewAsync(BeerViewModel beer)
        {
            var reviewDTO = new ReviewDTO
            {
                BeerId = beer.Id,
                ReviewText = beer.NewReview,
                Reviewer = User.Identity.Name
            };
            await this.reviewService.CreateReviewAsync(reviewDTO);

            return RedirectToAction(nameof(Details), new { id = beer.Id });
        }

        // GET: Beers/Details/5s
        //public IActionResult Details(int id)
        //{
        //    var beer = this.beerService.GetBeer(id);

        //    if (beer == null)
        //    {
        //        return NotFound();
        //    }

        //    var beerViewModel = new BeerViewModel
        //    {
        //        Id = beer.BeerDTOId,
        //        AlcoholPercentage = beer.AlcoholPercentage,
        //        Brewery = beer.Brewery.Name.ToString(),
        //        Country = beer.Country.Name.ToString(),
        //        Name = beer.Name,
        //        Rating = beer.Ratings.Count == 0 ? 0 : beer.Ratings.Select(r => r.RatingValue).Average(),
        //        RatingCount = beer.Ratings.Count(),
        //        Reviews = beer.Reviews.Select(re => new ReviewViewModel
        //        {
        //            Text = re.ReviewText,
        //            Reviewer = re.Reviewer,
        //            Dislikes = re.Dislikes,
        //            Likes = re.Likes,
        //            BeerId = re.BeerId,
        //            UserId = re.UserId
        //        }).ToList() ?? null,
        //        Type = beer.Type.Name
        //    };

        //    return View(beerViewModel);
        //}

        public async Task<IActionResult> Details(int id)
        {
            var beer = await this.beerService.GetBeerAsync(id);

            if (beer == null)
            {
                return NotFound();
            }

            var beerViewModel = new BeerViewModel
            {
                Id = beer.BeerDTOId,
                AlcoholPercentage = beer.AlcoholPercentage,
                Brewery = beer.Brewery.Name.ToString(),
                Country = beer.Country.Name.ToString(),
                Name = beer.Name,
                RatingUserNames = beer.Ratings.Select(r => r.UserName).ToList(),
                Rating = beer.Ratings.Count == 0 ? 0 : beer.Ratings.Select(r => r.RatingValue).Average(),
                RatingCount = beer.Ratings.Count(),
                Reviews = beer.Reviews.Select(re => new ReviewViewModel
                {
                    Text = re.ReviewText,
                    Reviewer = re.Reviewer,
                    Dislikes = re.Dislikes,
                    Likes = re.Likes,
                    BeerId = re.BeerId,
                    UserId = re.UserId
                }).ToList() ?? null,
                Type = beer.Type.Name
            };

            return View(beerViewModel);
        }

        // GET: Beers/Create
        //public IActionResult Create()
        //{
        //    var breweries = this.breweryService.GetAllBreweries()
        //        .Select(br => new BreweryViewModel
        //        {
        //            Id = br.BreweryDTOId,
        //            Name = br.Name
        //        });
        //    var countries = this.countryService.GetAllCountries()
        //        .Select(c => new CountryViewModel
        //        {
        //            Id = c.CountryDTOId,
        //            Name = c.Name
        //        });
        //    var styles = this.beerTypeService.GetAllBeerTypes()
        //        .Select(bt => new BeerTypeViewModel
        //        {
        //            Id = bt.BeerTypeDTOId,
        //            Name = bt.Name
        //        });

        //    ViewData["Brewery"] = new SelectList(breweries, "Name", "Name");
        //    ViewData["Country"] = new SelectList(countries, "Name", "Name");
        //    ViewData["Type"] = new SelectList(styles, "Name", "Name");

        //    return View();
        //}
        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> Create()
        {
            var breweries = await this.breweryService.GetAllBreweriesAsync();
            var breweriesViewModel = breweries.Select(b => new BreweryViewModel
            {
                Id = b.BreweryDTOId,
                Name = b.Name,
                Country = b.Country.Name.ToString(),
            });
            var countries = await this.countryService.GetAllCountriesAsync();
            var countriesViewModel = countries.Select(c => new CountryViewModel
            {
                Id = c.CountryDTOId,
                Name = c.Name
            });
            var styles = await this.beerTypeService.GetAllBeerTypesAsync();
            var stylesViewModel = styles.Select(s => new BeerTypeViewModel
            {
                Id = s.BeerTypeDTOId,
                Name = s.Name
            });

            ViewData["Brewery"] = new SelectList(breweriesViewModel, "Name", "Name");
            ViewData["Country"] = new SelectList(countriesViewModel, "Name", "Name");
            ViewData["Type"] = new SelectList(stylesViewModel, "Name", "Name");

            return View();
        }

        // POST: Beers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.

        //public IActionResult Create(BeerViewModel beerViewModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var beerDto = new BeerDTO
        //        {
        //            Name = beerViewModel.Name,
        //            Type = this.beerTypeService.GetBeerTypeByName(beerViewModel.Type),
        //            Brewery = this.breweryService.GetBreweryByName(beerViewModel.Brewery),
        //            Country = this.countryService.GetCountryByName(beerViewModel.Country),
        //            AlcoholPercentage = beerViewModel.AlcoholPercentage
        //        };

        //        this.beerService.CreateBeer(beerDto);
        //    }
        //    var breweries = this.breweryService.GetAllBreweries()
        //        .Select(br => new BreweryViewModel
        //        {
        //            Id = br.BreweryDTOId,
        //            Name = br.Name
        //        });
        //    var countries = this.countryService.GetAllCountries()
        //        .Select(c => new CountryViewModel
        //        {
        //            Id = c.CountryDTOId,
        //            Name = c.Name
        //        });
        //    var styles = this.beerTypeService.GetAllBeerTypes()
        //        .Select(bt => new BeerTypeViewModel
        //        {
        //            Id = bt.BeerTypeDTOId,
        //            Name = bt.Name
        //        });

        //    ViewData["BreweryId"] = new SelectList(breweries, "Name", "Name", beerViewModel.Brewery);
        //    ViewData["CountryId"] = new SelectList(countries, "Name", "Name", beerViewModel.Country);
        //    ViewData["TypeId"] = new SelectList(styles, "Name", "Name", beerViewModel.Type);

        //    return View(beerViewModel);
        //}
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User, Admin")]
        public async Task <IActionResult> Create(BeerViewModel beerViewModel)
        {
            if (ModelState.IsValid)
            {
                var beerDto = new BeerDTO
                {
                    Name = beerViewModel.Name,
                    Type = await this.beerTypeService.GetBeerTypeByNameAsync(beerViewModel.Type),
                    Brewery = await this.breweryService.GetBreweryByNameAsync(beerViewModel.Brewery),
                    Country = await this.countryService.GetCountryByNameAsync(beerViewModel.Country),
                    AlcoholPercentage = beerViewModel.AlcoholPercentage
                };

                await this.beerService.CreateBeerAsync(beerDto);
            }
            var breweries = await this.breweryService.GetAllBreweriesAsync();
            var breweriesViewModel = breweries.Select(b => new BreweryViewModel
            {
                Id = b.BreweryDTOId,
                Name = b.Name,
                Country = b.Country.Name.ToString(),
            });
            var countries = await this.countryService.GetAllCountriesAsync();
            var countriesViewModel = countries.Select(c => new CountryViewModel
            {
                Id = c.CountryDTOId,
                Name = c.Name
            });
            var styles = await this.beerTypeService.GetAllBeerTypesAsync();
            var stylesViewModel = styles.Select(s => new BeerTypeViewModel
            {
                Id = s.BeerTypeDTOId,
                Name = s.Name
            });


            ViewData["BreweryId"] = new SelectList(breweriesViewModel, "Name", "Name", beerViewModel.Brewery);
            ViewData["CountryId"] = new SelectList(countriesViewModel, "Name", "Name", beerViewModel.Country);
            ViewData["TypeId"] = new SelectList(stylesViewModel, "Name", "Name", beerViewModel.Type);

            return View(beerViewModel);
        }

        // GET: Beers/Edit/5
        //public IActionResult Edit(int id)
        //{
        //    var beerDTO = this.beerService.GetBeer(id);

        //    if (beerDTO == null)
        //    {
        //        return NotFound();
        //    }

        //    var beerViewModel = new BeerViewModel
        //    {
        //        AlcoholPercentage = beerDTO.AlcoholPercentage,
        //        Brewery = beerDTO.Brewery.Name,
        //        Country = beerDTO.Country.Name,
        //        Id = beerDTO.BeerDTOId,
        //        Name = beerDTO.Name,
        //        Type = beerDTO.Type.Name
        //    };

        //    var breweries = this.breweryService.GetAllBreweries()
        //        .Select(br => new BreweryViewModel
        //        {
        //            Id = br.BreweryDTOId,
        //            Name = br.Name
        //        });
        //    var countries = this.countryService.GetAllCountries()
        //        .Select(c => new CountryViewModel
        //        {
        //            Id = c.CountryDTOId,
        //            Name = c.Name
        //        });
        //    var styles = this.beerTypeService.GetAllBeerTypes()
        //        .Select(bt => new BeerTypeViewModel
        //        {
        //            Id = bt.BeerTypeDTOId,
        //            Name = bt.Name
        //        });

        //    ViewData["Brewery"] = new SelectList(breweries, "Name", "Name");
        //    ViewData["Country"] = new SelectList(countries, "Name", "Name");
        //    ViewData["Type"] = new SelectList(styles, "Name", "Name");

        //    return View(beerViewModel);
        //}
        [Authorize(Roles = "User, Admin")]
        public async Task <IActionResult> Edit(int id)
        {
            var beerDTO = await this.beerService.GetBeerAsync(id);

            if (beerDTO == null)
            {
                return NotFound();
            }

            var beerViewModel = new BeerViewModel
            {
                AlcoholPercentage = beerDTO.AlcoholPercentage,
                Brewery = beerDTO.Brewery.Name,
                Country = beerDTO.Country.Name,
                Id = beerDTO.BeerDTOId,
                Name = beerDTO.Name,
                Type = beerDTO.Type.Name
            };
            var breweries = await this.breweryService.GetAllBreweriesAsync();
            var breweriesViewModel = breweries.Select(b => new BreweryViewModel
            {
                Id = b.BreweryDTOId,
                Name = b.Name,
                Country = b.Country.Name.ToString(),
            });
            var countries = await this.countryService.GetAllCountriesAsync();
            var countriesViewModel = countries.Select(c => new CountryViewModel
            {
                Id = c.CountryDTOId,
                Name = c.Name
            });
            var styles = await this.beerTypeService.GetAllBeerTypesAsync();
            var stylesViewModel = styles.Select(s => new BeerTypeViewModel
            {
                Id = s.BeerTypeDTOId,
                Name = s.Name
            });


            ViewData["Brewery"] = new SelectList(breweriesViewModel, "Name", "Name");
            ViewData["Country"] = new SelectList(countriesViewModel, "Name", "Name");
            ViewData["Type"] = new SelectList(stylesViewModel, "Name", "Name");

            return View(beerViewModel);
        }

        // POST: Beers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.

        //public IActionResult Edit(int id, BeerViewModel beerViewModel)
        //{
        //    if (id != beerViewModel.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        var beerDto = new BeerDTO
        //        {
        //            Name = beerViewModel.Name,
        //            Type = this.beerTypeService.GetBeerTypeByName(beerViewModel.Type),
        //            Brewery = this.breweryService.GetBreweryByName(beerViewModel.Brewery),
        //            Country = this.countryService.GetCountryByName(beerViewModel.Country),
        //            AlcoholPercentage = beerViewModel.AlcoholPercentage
        //        };

        //        this.beerService.UpdateBeer(id, beerDto);
        //    }

        //    ViewData["BreweryId"] = new SelectList(context.Breweries, "Name", "Name", beerViewModel.Brewery);
        //    ViewData["CountryId"] = new SelectList(context.Countries, "Name", "Name", beerViewModel.Country);
        //    ViewData["TypeId"] = new SelectList(context.BeerTypes, "Name", "Name", beerViewModel.Type);
        //    return View(beerViewModel);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> Edit(int id, BeerViewModel beerViewModel)
        {
            if (id != beerViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var beerDto = new BeerDTO
                {
                    Name = beerViewModel.Name,
                    Type = await this.beerTypeService.GetBeerTypeByNameAsync(beerViewModel.Type),
                    Brewery = await this.breweryService.GetBreweryByNameAsync(beerViewModel.Brewery),
                    Country = await this.countryService.GetCountryByNameAsync(beerViewModel.Country),
                    AlcoholPercentage = beerViewModel.AlcoholPercentage
                };

                await this.beerService.UpdateBeerAsync(id, beerDto);
            }

            ViewData["BreweryId"] = new SelectList(context.Breweries, "Name", "Name", beerViewModel.Brewery);
            ViewData["CountryId"] = new SelectList(context.Countries, "Name", "Name", beerViewModel.Country);
            ViewData["TypeId"] = new SelectList(context.BeerTypes, "Name", "Name", beerViewModel.Type);
            return View(beerViewModel);
        }

        [Authorize(Roles = "User, Admin")]
        // GET: Beers/Delete/5
        //public IActionResult Delete(int id)
        //{
        //    var beer = await this.beerService.GetBeerAsync(id);

        //    if (beer == null)
        //    {
        //        return NotFound();
        //    }

        //    var beerViewModel = new BeerViewModel
        //    {
        //        Name = beer.Name,
        //        Brewery = beer.Brewery.Name,
        //        Country = beer.Country.Name,
        //        AlcoholPercentage = beer.AlcoholPercentage,
        //        Type = beer.Type.Name
        //    };

        //    return View(beerViewModel);
        //}

        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var beer = await this.beerService.GetBeerAsync(id);

            if (beer == null)
            {
                return NotFound();
            }

            var beerViewModel = new BeerViewModel
            {
                Name = beer.Name,
                Brewery = beer.Brewery.Name,
                Country = beer.Country.Name,
                AlcoholPercentage = beer.AlcoholPercentage,
                Type = beer.Type.Name
            };

            return View(beerViewModel);
        }


        //public IActionResult DeleteConfirmed(int id)
        //{
        //    this.beerService.DeleteBeer(id);
        //    return RedirectToAction(nameof(Index));
        //}

        // POST: Beers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> DeleteConfirmedAsync(int id)
        {
            await this.beerService.DeleteBeerAsync(id);
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> AddRating(int Id, string userName, int rating)
        {
            await Task.Run(() => this.beerService.AddRatingToBeerAsync(Id, userName, rating));

            return RedirectToAction(nameof(Details), new { id = Id });
        }

        private async Task<bool> BeerExistsAsync(int id)
        {
            return await context.Beers.AnyAsync(e => e.Id == id);
        }
    }
}
