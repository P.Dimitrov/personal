﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Web.Models;

namespace BeerOverflow.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;
        private readonly IBeerService beerService;
        private readonly IBreweryService breweryService;

        public HomeController(ILogger<HomeController> logger, 
            IBeerService beerService, IBreweryService breweryService)
        {
            this.logger = logger;
            this.beerService = beerService;
            this.breweryService = breweryService;
        }

        public async Task<IActionResult> Index()
        {
            var beers = await this.beerService.GetAllBeersOfTheMonthAsync();
            var beersOfTheMonthViewModel = beers.Select(b => new BeerViewModel
            {
                    AlcoholPercentage = b.AlcoholPercentage,
                    Brewery = b.Brewery.Name,
                    Country = b.Country.Name,
                    Name = b.Name,
                    Type = b.Type.Name
                }).ToList();

            return View(beersOfTheMonthViewModel);
        }

        public IActionResult PageNotFound()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
