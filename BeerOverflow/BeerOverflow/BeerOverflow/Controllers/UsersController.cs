﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Web.Models;

namespace BeerOverflow.Web.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        public async Task<IActionResult> GetWishListAsync(string userName)
        {
            var beers = await this.userService.GetUserWishListAsync(userName);

            var beerViewModels = beers.Select(beer => new BeerViewModel
            {
                Name = beer.Name,
                AlcoholPercentage = beer.AlcoholPercentage,
                Brewery = beer.Brewery.Name,
                Country = beer.Country.Name
            }).ToList();

            return View(beerViewModels);
        }
        public async Task<IActionResult> GetDrankListAsync(string userName)
        {
            var beers = await this.userService.GetUserDrankListAsync(userName);

            var beerViewModels = beers.Select(beer => new BeerViewModel
            {
                Name = beer.Name,
                AlcoholPercentage = beer.AlcoholPercentage,
                Brewery = beer.Brewery.Name,
                Country = beer.Country.Name
            }).ToList();

            return View(beerViewModels);
        }
    }
}
