﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Web.Models;
using BeerOverflow.Services.DTO;

namespace BeerOverflow.Web.Controllers
{
    public class CountriesController : Controller
    {
        private readonly BeerOverflowContext context;
        private readonly ICountryService countryService;

        public CountriesController(BeerOverflowContext context, ICountryService countryService)
        {
            this.context = context;
            this.countryService = countryService;
        }

        // GET: Countries

        //public IActionResult Index()
        //{
        //   var countries = this.countryService.GetAllCountries()
        //            .Select(c => new CountryViewModel
        //            {
        //                Id = c.CountryDTOId,
        //                Name = c.Name
        //            });

        //        if (countries == null)
        //        {
        //            return BadRequest();
        //        }

        //        return View(countries);

        //}

        // GET: Countries
        public async Task<IActionResult> Index()
        {
            var countries = await this.countryService.GetAllCountriesAsync();
            var countriesViewModel = countries.Select(c => new CountryViewModel
            {
                Id = c.CountryDTOId,
                Name = c.Name
            });

            if (countries == null)
            {
                return BadRequest();
            }

            return View(countriesViewModel);

        }

        // GET: Countries/Details/5
        //public IActionResult Details(int id)
        //{
        //    var country = this.countryService.GetCountry(id);

        //    if (country == null)
        //    {
        //        return NotFound();
        //    }

        //    var countryViewModel = new CountryViewModel
        //    {
        //        Id = country.CountryDTOId,
        //        Name = country.Name,

        //    };

        //    return View(countryViewModel);
        //}

        public async Task<IActionResult> DetailsAsync(int id)
        {
            var country = await this.countryService.GetCountryAsync(id);

            if (country == null)
            {
                return NotFound();
            }

            var countryViewModel = new CountryViewModel
            {
                Id = country.CountryDTOId,
                Name = country.Name,

            };

            return View(countryViewModel);
        }

        // GET: Countries/Create
        public IActionResult Create()
        {
            return View();
        }


        //public IActionResult Create([Bind("Id,Name,IsDeleted")] Country country)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var countryDTO = new CountryDTO
        //        {
        //            CountryDTOId = country.Id,
        //            Name = country.Name
        //        };

        //        this.countryService.CreateCountry(countryDTO);
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(country);
        //}

        // POST: Countries/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAsync([Bind("Id,Name,IsDeleted")] Country country)
        {
            if (ModelState.IsValid)
            {
                var countryDTO = new CountryDTO
                {
                    CountryDTOId = country.Id,
                    Name = country.Name
                };

                await this.countryService.CreateCountryAsync(countryDTO);
                return RedirectToAction(nameof(Index));
            }
            return View(country);
        }

        // GET: Countries/Edit/5
        //public IActionResult Edit(int id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var country = this.countryService.GetCountry(id);

        //    if (country == null)
        //    {
        //        return NotFound();
        //    }

        //    var countryViewModel = new CountryViewModel
        //    {
        //        Id = country.CountryDTOId,
        //        Name = country.Name
        //    };

        //    ViewData["CountryId"] = new SelectList(context.Countries, "Name", "Name", countryViewModel.Id);
        //    return View(countryViewModel);
        //}

        public async Task<IActionResult> EditAsync(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var country = await this.countryService.GetCountryAsync(id);

            if (country == null)
            {
                return NotFound();
            }

            var countryViewModel = new CountryViewModel
            {
                Id = country.CountryDTOId,
                Name = country.Name
            };

            ViewData["CountryId"] = new SelectList(context.Countries, "Name", "Name", countryViewModel.Id);
            return View(countryViewModel);
        }


        //public IActionResult Edit(int id, CountryViewModel countryViewModel)
        //{
        //    if (id != countryViewModel.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        var countryDTO = new CountryDTO
        //        {
        //            Name = countryViewModel.Name,
        //        };

        //        this.countryService.UpdateCountry(id, countryDTO);
        //    }

        //    ViewData["CountryId"] = new SelectList(context.Countries, "Name", "Name", countryViewModel.Name);

        //    return View(countryViewModel);
        //}

        // POST: Countries/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAsync(int id, CountryViewModel countryViewModel)
        {
            if (id != countryViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var countryDTO = new CountryDTO
                {
                    Name = countryViewModel.Name,
                };

                await this.countryService.UpdateCountryAsync(id, countryDTO);
            }

            ViewData["CountryId"] = new SelectList(context.Countries, "Name", "Name", countryViewModel.Name);

            return View(countryViewModel);
        }

        // GET: Countries/Delete/5
        //public IActionResult Delete(int id)
        //{
        //    var countryDTO = this.countryService.GetCountry(id);

        //    if (countryDTO == null)
        //    {
        //        return NotFound();
        //    }

        //    var countryViewModel = new CountryViewModel
        //    {
        //        Id = countryDTO.CountryDTOId,
        //        Name = countryDTO.Name
        //    };

        //    return View(countryViewModel);
        //}

        public async Task<IActionResult> DeleteAsync(int id)
        {
            var countryDTO = await this.countryService.GetCountryAsync(id);

            if (countryDTO == null)
            {
                return NotFound();
            }

            var countryViewModel = new CountryViewModel
            {
                Id = countryDTO.CountryDTOId,
                Name = countryDTO.Name
            };

            return View(countryViewModel);
        }


        //public IActionResult DeleteConfirmed(int id)
        //{
        //    this.countryService.DeleteCountry(id);
        //    return RedirectToAction(nameof(Index));
        //}

        // POST: Countries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmedAsync(int id)
        {
            await this.countryService.DeleteCountryAsync(id);
            return RedirectToAction(nameof(Index));
        }


        //private bool CountryExists(int id)
        //{
        //    return context.Countries.Any(e => e.Id == id);
        //}
        private async Task<bool> CountryExistsAsync(int id)
        {
            return await context.Countries.AnyAsync(e => e.Id == id);
        }

    }
}
