﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BeerOverflow.Data;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Web.Models;
using BeerOverflow.Services.DTO;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BeerOverflow.Web.Controllers
{
    public class BreweriesController : Controller
    {
        private readonly BeerOverflowContext context;
        private readonly IBreweryService breweryService;
        private readonly ICountryService countryService;

        public BreweriesController(BeerOverflowContext context, IBreweryService breweryService, ICountryService countryService)
        {
            this.context = context;
            this.breweryService = breweryService;
            this.countryService = countryService;
        }

        // GET: Breweries
        //public IActionResult Index()
        //{
        //    var breweries = this.breweryService.GetAllBreweries()
        //        .Select(b => new BreweryViewModel
        //        {
        //            Id = b.BreweryDTOId,
        //            Name = b.Name,
        //            Country = b.Country.Name.ToString(),
        //        });

        //    if (breweries == null)
        //    {
        //        return BadRequest();
        //    }

        //    return View(breweries);
        //}

        public async Task<IActionResult> Index()
        {
            var breweries = await this.breweryService.GetAllBreweriesAsync();
            var breweriesViewModel = breweries.Select(b => new BreweryViewModel
            {
                Id = b.BreweryDTOId,
                Name = b.Name,
                Country = b.Country.Name.ToString(),
            });

            if (breweries == null)
            {
                return BadRequest();
            }

            return View(breweriesViewModel);
        }

        // GET: Breweries/Details/5
        //public IActionResult Details(int id)
        //{
        //    var brewery = this.breweryService.GetBrewery(id);

        //    if (brewery == null)
        //    {
        //        return NotFound();
        //    }

        //    var breweryViewModel = new BreweryViewModel
        //    {
        //        Id = brewery.BreweryDTOId,
        //        Name = brewery.Name,
        //        Country = brewery.Country.Name
        //    };

        //    return View(breweryViewModel);
        //}

        public async Task<IActionResult> DetailsAsync(int id)
        {
            var brewery = await this.breweryService.GetBreweryAsync(id);

            if (brewery == null)
            {
                return NotFound();
            }

            var breweryViewModel = new BreweryViewModel
            {
                Id = brewery.BreweryDTOId,
                Name = brewery.Name,
                Country = brewery.Country.Name.ToString(),
                Beers = brewery.Beers.Select(b => new BeerViewModel
                {
                    Name = b.Name
                }).ToList() ?? null
                
            };
            return View(breweryViewModel);
        }

        // GET: Breweries/Create
        //public IActionResult Create()
        //{
        //        var countries = this.countryService.GetAllCountriesAsync()
        //        .Select(c => new CountryViewModel
        //        {
        //            Id = c.CountryDTOId,
        //            Name = c.Name
        //        });

        //    ViewData["Country"] = new SelectList(countries, "Name", "Name");

        //    return View();
        //}

        public async Task<IActionResult> CreateAsync()
        {
            var countries = await this.countryService.GetAllCountriesAsync();
            var countriesViewModel = countries.Select(c => new CountryViewModel
            {
                Id = c.CountryDTOId,
                Name = c.Name
            });

            ViewData["Country"] = new SelectList(countriesViewModel, "Name", "Name");

            return View();
        }

        // POST: Breweries/Create


        //public IActionResult Create(BreweryViewModel breweryViewModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var breweryDTO = new BreweryDTO
        //        {
        //            Name = breweryViewModel.Name,
        //            Country = this.countryService.GetCountryByNameAsync(breweryViewModel.Country)
        //        };

        //        this.breweryService.CreateBrewery(breweryDTO);
        //    }

        //    var countries = this.countryService.GetAllCountries()
        //        .Select(c => new CountryViewModel
        //        {
        //            Id = c.CountryDTOId,
        //            Name = c.Name
        //        });

        //    ViewData["CountryId"] = new SelectList(countries, "Name", "Name", breweryViewModel.Country);

        //    return View(breweryViewModel);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAsync(BreweryViewModel breweryViewModel)
        {
            if (ModelState.IsValid)
            {
                var breweryDTO = new BreweryDTO
                {
                    Name = breweryViewModel.Name,
                    Country = await this.countryService.GetCountryByNameAsync(breweryViewModel.Country)
                };

                 await this.breweryService.CreateBreweryAsync(breweryDTO);
            }

            var countries = await this.countryService.GetAllCountriesAsync();

            var countriesViewModel = countries.Select(c => new CountryViewModel
            {
                Id = c.CountryDTOId,
                Name = c.Name
            });

            ViewData["CountryId"] = new SelectList(countriesViewModel, "Name", "Name", breweryViewModel.Country);

            return View(breweryViewModel);
        }

        // GET: Breweries/Edit/5
        //public IActionResult Edit(int id)
        //{
        //    var breweryDTO = this.breweryService.GetBrewery(id);

        //    if (breweryDTO == null)
        //    {
        //        return NotFound();
        //    }

        //    var breweryViewModel = new BreweryViewModel
        //    {
        //        Id = breweryDTO.BreweryDTOId,
        //        Name = breweryDTO.Name,
        //        Country = breweryDTO.Country.Name
        //    };

        //    var countries = this.countryService.GetAllCountries()
        //        .Select(c => new CountryViewModel
        //        {
        //            Id = c.CountryDTOId,
        //            Name = c.Name
        //        });

        //    ViewData["Country"] = new SelectList(countries, "Name", "Name");

        //    return View(breweryViewModel);
        //}
        public async Task<IActionResult> Edit(int id)
        {
            var breweryDTO = await this.breweryService.GetBreweryAsync(id);

            if (breweryDTO == null)
            {
                return NotFound();
            }

            var breweryViewModel = new BreweryViewModel
            {
                Id = breweryDTO.BreweryDTOId,
                Name = breweryDTO.Name,
                Country = breweryDTO.Country.Name
            };

            var countries = await this.countryService.GetAllCountriesAsync();

            var countriesViewModel = countries.Select(c => new CountryViewModel
            {
                Id = c.CountryDTOId,
                Name = c.Name
            });

            ViewData["Country"] = new SelectList(countriesViewModel, "Name", "Name");

            return View(breweryViewModel);
        }


        // POST: Breweries/Edit/5


        //public IActionResult Edit(int id, BreweryViewModel breweryViewModel)
        //{
        //    if (id != breweryViewModel.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        var breweryDTO = new BreweryDTO
        //        {
        //            Name = breweryViewModel.Name,
        //            Country = this.countryService.GetCountryByName(breweryViewModel.Country)
        //        };

        //        this.breweryService.UpdateBrewery(id, breweryDTO);
        //    }

        //    ViewData["CountryId"] = new SelectList(context.Countries, "Name", "Name", breweryViewModel.Country);

        //    return View(breweryViewModel);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, BreweryViewModel breweryViewModel)
        {
            if (id != breweryViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var breweryDTO = new BreweryDTO
                {
                    Name = breweryViewModel.Name,
                    Country = await this.countryService.GetCountryByNameAsync(breweryViewModel.Country)
                };

                await this.breweryService.UpdateBreweryAsync(id, breweryDTO);
            }

            ViewData["CountryId"] = new SelectList(context.Countries, "Name", "Name", breweryViewModel.Country);

            return View(breweryViewModel);
        }

        // GET: Breweries/Delete/5
        //public IActionResult Delete(int id)
        //{
        //    var breweryDTO = this.breweryService.GetBrewery(id);

        //    if (breweryDTO == null)
        //    {
        //        return NotFound();
        //    }

        //    var breweryViewModel = new BreweryViewModel
        //    {
        //        Id = breweryDTO.BreweryDTOId,
        //        Name = breweryDTO.Name
        //    };

        //    return View(breweryViewModel);
        //}

        public async Task<IActionResult> DeleteAsync(int id)
        {
            var breweryDTO = await this.breweryService.GetBreweryAsync(id);

            if (breweryDTO == null)
            {
                return NotFound();
            }

            var breweryViewModel = new BreweryViewModel
            {
                Id = breweryDTO.BreweryDTOId,
                Name = breweryDTO.Name
            };

            return View(breweryViewModel);
        }

        // POST: Breweries/Delete/5

        //public IActionResult DeleteConfirmed(int id)
        //{
        //    this.breweryService.DeleteBrewery(id);
        //    return RedirectToAction(nameof(Index));
        //}

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmedAsync(int id)
        {
            await this.breweryService.DeleteBreweryAsync(id);
            return RedirectToAction(nameof(Index));
        }

        //private bool BreweryExists(int id)
        //{
        //    return context.Breweries.Any(e => e.Id == id);
        //}

        private async Task<bool> BreweryExistsAsync(int id)
        {
            return await context.Breweries.AnyAsync(e => e.Id == id);
        }
    }
}
