﻿using BeerOverflow.Models.Contracts;
using System.Collections.Generic;

namespace BeerOverflow.Models
{
    public class Country : ICountry
    {
        public Country()
        {
            Breweries = new List<Brewery>();
            Beers = new List<Beer>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Brewery> Breweries { get; set; }
        public ICollection<Beer> Beers { get; set; }
    }
}