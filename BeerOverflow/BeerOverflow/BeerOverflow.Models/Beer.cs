﻿using System.Linq;
using System.Collections.Generic;
using BeerOverflow.Models.Contracts;

namespace BeerOverflow.Models
{
    public class Beer : IBeer
    {
        public Beer()
        {
            Ratings = new List<Rating>();
            Reviews = new List<Review>();
            WishUsers = new List<WishBeer>();
            DrankUsers = new List<DrankBeer>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int TypeId { get; set; }
        public BeerType Type { get; set; }
        public int BreweryId { get; set; }
        public Brewery Brewery { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public double AlcoholPercentage { get; set; }
        public ICollection<Rating> Ratings { get; set; }
        public ICollection<Review> Reviews { get; set; }
        public ICollection<WishBeer> WishUsers { get; set; }
        public ICollection<DrankBeer> DrankUsers { get; set; }
        public bool IsBeerOfTheMonth { get; set; }
        public bool IsDeleted { get; set; }
    }
}
