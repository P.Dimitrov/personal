﻿using BeerOverflow.Models.Contracts;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace BeerOverflow.Models
{
    public class User : IdentityUser<int>
    {
        public User()
        {
            WishList = new List<WishBeer>();
            DrankList = new List<DrankBeer>();
            Ratings = new List<Rating>();
            Reviews = new List<Review>();
        }
        public ICollection<WishBeer> WishList { get; set; }
        public ICollection<DrankBeer> DrankList { get; set; }
        public ICollection<Review> Reviews { get; set; }
        public ICollection<Rating> Ratings { get; set; }
        public bool IsDeleted { get; set; }
    }
}
