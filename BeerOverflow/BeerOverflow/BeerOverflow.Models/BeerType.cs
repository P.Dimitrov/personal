﻿using BeerOverflow.Models.Contracts;
using System.Collections.Generic;

namespace BeerOverflow.Models
{
    public class BeerType : IBeerType
    {
        public BeerType()
        {
            Beers = new List<Beer>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Beer> Beers { get; set; }
    }
}
