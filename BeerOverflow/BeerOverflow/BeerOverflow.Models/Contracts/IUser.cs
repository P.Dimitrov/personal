﻿using System;
using System.Collections.Generic;
namespace BeerOverflow.Models.Contracts
{
    public interface IUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<WishBeer> WishList { get; set; }
        public ICollection<DrankBeer> DrankList { get; set; }
        public ICollection<Review> Reviews { get; set; }
        public bool IsDeleted { get; set; }
    }
}
