﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Models.Contracts
{
    public interface IBeerUser
    {

        public int BeerId { get; set; }
        public Beer Beer { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
