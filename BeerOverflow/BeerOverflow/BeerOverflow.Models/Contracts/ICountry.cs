﻿using System.Collections.Generic;

namespace BeerOverflow.Models.Contracts
{
    public interface ICountry
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Brewery> Breweries { get; set; }
        public ICollection<Beer> Beers { get; set; }
    }
}
