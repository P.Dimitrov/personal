﻿using System.Collections.Generic;

namespace BeerOverflow.Models.Contracts
{
    public interface IBrewery
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public string Name { get; set; }
        public ICollection<Beer> Beers { get; set; }
        public bool IsDeleted { get; set; }
    }
}