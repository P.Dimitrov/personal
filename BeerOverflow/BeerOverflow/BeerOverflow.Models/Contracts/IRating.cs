﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Models.Contracts
{
    public interface IRating
    {
        public int RatingValue { get; set; }
        public Beer Beer { get; set; }
        public int BeerId { get; set; }
    }
}
