﻿using BeerOverflow.Models.Contracts;
using System.Collections.Generic;

namespace BeerOverflow.Models
{
    public class Brewery : IBrewery
    {
        public Brewery()
        {
            Beers = new List<Beer>();
        }
        public int Id { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public string Name { get; set; }
        public ICollection<Beer> Beers { get; set; }
        public bool IsDeleted { get; set; }
    }
}