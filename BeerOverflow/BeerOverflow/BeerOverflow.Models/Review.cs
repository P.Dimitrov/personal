﻿using BeerOverflow.Models.Contracts;
using System.Collections.Generic;

namespace BeerOverflow.Models
{
    public class Review : IReview
    {
        public string ReviewText { get; set; }
        public int BeerId { get; set; }
        public Beer Beer { get; set; }
        public int UserId { get; set; }
        public User Reviewer { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsFlagged { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
    }
}