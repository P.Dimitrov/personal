﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Configurations
{
    public class BreweryConfiguration : IEntityTypeConfiguration<Brewery>
    {

        public void Configure(EntityTypeBuilder<Brewery> builder)
        {
            builder.HasKey(br => br.Id);

            builder.Property(br => br.Name)
                .HasMaxLength(25)
                .IsRequired();

            builder.Property(br => br.IsDeleted)
                .IsRequired();

            builder.HasOne(br => br.Country)
                .WithMany(c => c.Breweries)
                .HasForeignKey(br => br.CountryId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasMany(br => br.Beers)
                .WithOne(b => b.Brewery)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(br => br.CountryId);
        }
    }
}
