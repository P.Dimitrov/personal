﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Configurations
{
    public class WishBeerConfiguration : IEntityTypeConfiguration<WishBeer>
    {
        public void Configure(EntityTypeBuilder<WishBeer> builder)
        {
            builder.HasKey(db => new
            {
                db.BeerId,
                db.UserId
            });

            builder.HasOne(wb => wb.Beer)
                .WithMany(b => b.WishUsers)
                .HasForeignKey(wb => wb.BeerId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(wb => wb.User)
                .WithMany(u => u.WishList)
                .HasForeignKey(wb => wb.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
