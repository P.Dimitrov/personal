﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Configurations
{
    public class ReviewConfiguration : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.HasKey(db => new
            {
                db.BeerId,
                db.UserId
            });

            builder.Property(rev => rev.ReviewText)
                .IsRequired();

            builder.HasOne(rev => rev.Reviewer)
                .WithMany(u => u.Reviews)
                .HasForeignKey(rev => rev.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(rev => rev.Beer)
                .WithMany(b => b.Reviews)
                .HasForeignKey(rev => rev.BeerId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.Property(rev => rev.IsDeleted)
                .IsRequired();

            builder.Property(rev => rev.IsFlagged)
                .IsRequired();

            builder.Property(rev => rev.Likes);

            builder.Property(rev => rev.Dislikes);

            builder.HasIndex(rev => rev.BeerId);

            builder.HasIndex(rev => rev.UserId);
        }
    }
}