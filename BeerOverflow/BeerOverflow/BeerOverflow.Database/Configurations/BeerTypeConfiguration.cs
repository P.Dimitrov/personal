﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Configurations
{
    public class BeerTypeConfiguration : IEntityTypeConfiguration<BeerType>
    {
        
        public void Configure(EntityTypeBuilder<BeerType> builder)
        {
            builder.HasKey(bt => bt.Id);

            builder.Property(bt => bt.Name)
                .HasMaxLength(25)
                .IsRequired();

            builder.Property(bt => bt.IsDeleted)
                .IsRequired();

            builder.HasMany(bt => bt.Beers)
                .WithOne(b => b.Type)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
