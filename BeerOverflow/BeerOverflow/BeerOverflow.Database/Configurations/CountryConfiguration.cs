﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Configurations
{
    public class CountryConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Name)
                .HasMaxLength(25)
                .IsRequired();

            builder.Property(c => c.IsDeleted)
                .IsRequired();

            builder.HasMany(br => br.Breweries)
                .WithOne(b => b.Country)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(br => br.Beers)
                .WithOne(b => b.Country)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}
