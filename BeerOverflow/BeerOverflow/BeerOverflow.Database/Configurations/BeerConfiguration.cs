﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Configurations
{
    public class BeerConfiguration : IEntityTypeConfiguration<Beer>
    {
        public void Configure(EntityTypeBuilder<Beer> builder)
        {
            builder.HasKey(b => b.Id);

            builder.Property(b => b.AlcoholPercentage)
                .IsRequired();

            builder.Property(b => b.Name)
                .HasMaxLength(40)
                .IsRequired();

            builder.HasOne(b => b.Type)
                .WithMany(t => t.Beers)
                .HasForeignKey(b => b.TypeId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(b => b.Brewery)
                .WithMany(bb => bb.Beers)
                .HasForeignKey(b => b.BreweryId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(b => b.Country)
                .WithMany(c => c.Beers)
                .HasForeignKey(b => b.CountryId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasMany(b => b.Ratings)
                .WithOne(r => r.Beer)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(b => b.Reviews)
                .WithOne(r => r.Beer)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(b => b.WishUsers)
                .WithOne(r => r.Beer)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(b => b.DrankUsers)
                .WithOne(r => r.Beer)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(b => b.IsBeerOfTheMonth)
                .IsRequired();

            builder.Property(b => b.IsDeleted)
                .IsRequired();

            builder.HasIndex(b => b.CountryId);

            builder.HasIndex(b => b.TypeId);
        }
    }
}
