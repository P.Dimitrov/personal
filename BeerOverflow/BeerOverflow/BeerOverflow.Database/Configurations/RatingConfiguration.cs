﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Configurations
{
    public class RatingConfiguration : IEntityTypeConfiguration<Rating>
    {
        public void Configure(EntityTypeBuilder<Rating> builder)
        {
            builder.HasKey(db => new
            {
                db.BeerId,
                db.UserId
            });

            builder.Property(r => r.RatingValue)
                .IsRequired();

            builder.HasOne(r => r.Beer)
                .WithMany(b => b.Ratings)
                .HasForeignKey(r => r.BeerId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(r => r.Rater)
                .WithMany(u => u.Ratings)
                .HasForeignKey(r => r.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasIndex(b => b.UserId);

            builder.HasIndex(b => b.BeerId);
        }
    }
}
