﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);

            builder.Property(u => u.IsDeleted)
                .IsRequired();

            builder.HasMany(u => u.WishList)
                .WithOne(wb => wb.User)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(u => u.DrankList)
                .WithOne(db => db.User)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(u => u.Reviews)
                .WithOne(rev => rev.Reviewer)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(u => u.Ratings)
                .WithOne(r => r.Rater)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
