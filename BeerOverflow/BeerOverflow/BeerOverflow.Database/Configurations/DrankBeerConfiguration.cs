﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Configurations
{
    public class DrankBeerConfiguration : IEntityTypeConfiguration<DrankBeer>
    {
        public void Configure(EntityTypeBuilder<DrankBeer> builder)
        {
            builder.HasKey(db => new
            {
                db.BeerId,
                db.UserId
            });

            builder.HasOne(db => db.Beer)
                .WithMany(b => b.DrankUsers)
                .HasForeignKey(db => db.BeerId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(db => db.User)
                .WithMany(u => u.DrankList)
                .HasForeignKey(db => db.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
