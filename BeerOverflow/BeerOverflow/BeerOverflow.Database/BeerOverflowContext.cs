﻿//using BeerOverflow.Data.Configurations;
using BeerOverflow.Data.Configurations;
using BeerOverflow.Data.Seeder;
using BeerOverflow.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data
{
    public class BeerOverflowContext : IdentityDbContext<User, Role, int>
    {
        public BeerOverflowContext(DbContextOptions<BeerOverflowContext> options)
            :base(options)
        {

        }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<Beer> Beers { get; set; }
        public DbSet<WishBeer> WishBeers { get; set; }
        public DbSet<DrankBeer> DrankBeers { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<BeerType> BeerTypes { get; set; }


        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.EnableSensitiveDataLogging();
        //}
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new BeerConfiguration());
            builder.ApplyConfiguration(new BeerTypeConfiguration());
            builder.ApplyConfiguration(new BreweryConfiguration());
            builder.ApplyConfiguration(new CountryConfiguration());
            builder.ApplyConfiguration(new DrankBeerConfiguration());
            builder.ApplyConfiguration(new RatingConfiguration());
            builder.ApplyConfiguration(new ReviewConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());
            builder.ApplyConfiguration(new WishBeerConfiguration());

            builder.InitialSeed();

            base.OnModelCreating(builder);
        }
    }
}
