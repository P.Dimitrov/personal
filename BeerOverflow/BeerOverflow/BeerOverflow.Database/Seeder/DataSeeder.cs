﻿using BeerOverflow.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Seeder
{
    public static class DataSeeder
    {
        public static void InitialSeed(this ModelBuilder builder)
        {

            builder.Entity<Role>().HasData(
                new Role 
                { 
                    Id = 1, 
                    Name = "Admin", 
                    NormalizedName = "ADMIN" 
                },
                new Role 
                { 
                    Id = 2, 
                    Name = "User", 
                    NormalizedName = "User" 
                }
            );

            var hasher = new PasswordHasher<User>();

            User adminUser = new User
            {
                Id = 1,
                UserName = "admin@admin.com",
                NormalizedUserName = "ADMIN@ADMIN.COM",
                Email = "admin@admin.com",
                NormalizedEmail = "ADMIN@ADMIN.COM",
                LockoutEnabled = true,
                SecurityStamp = "JK82L91TB50IX67PO92302JSD30NGF",
            };

            adminUser.PasswordHash = hasher.HashPassword(adminUser, "admin");

            User iva = new User
            {
                Id = 2,
                UserName = "iva@iva.com",
                NormalizedUserName = "IVA@IVA.COM",
                Email = "iva@iva.com",
                NormalizedEmail = "IVA@IVA.COM",
                LockoutEnabled = true,
                SecurityStamp = "K3NV7340BJD72PJCDHSI37GZR409JL",
            };

            iva.PasswordHash = hasher.HashPassword(iva, "user");

            User pesho = new User
            {
                Id = 3,
                UserName = "pesho@pesho.com",
                NormalizedUserName = "PESHO@PESHO.COM",
                Email = "pesho@pesho.com",
                NormalizedEmail = "PESHO@PESHO.COM",
                LockoutEnabled = true,
                SecurityStamp = "SK83NVJDI39TDOW028FJSDKOAO280",
            };

            pesho.PasswordHash = hasher.HashPassword(pesho, "user");

            User stamat = new User
            {
                Id = 4,
                UserName = "stamat@stamat.com",
                NormalizedUserName = "STAMAT@STAMAT.COM",
                Email = "stamat@stamat.com",
                NormalizedEmail = "STAMAT@STAMAT.COM",
                LockoutEnabled = true,
                SecurityStamp = "KZMVH47390KBNF9392KIFKOFI299",
            };

            stamat.PasswordHash = hasher.HashPassword(stamat, "user");

            builder.Entity<User>().HasData(adminUser, iva, pesho, stamat);

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = adminUser.Id
                },
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = iva.Id
                },
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = pesho.Id
                },
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = stamat.Id
                });

            var bulgaria = new Country
            {
                Id = 1,
                Name = "Bulgaria",
            };
            var germany = new Country
            {
                Id = 2,
                Name = "Germany"
            };
            var belgium = new Country
            {
                Id = 3,
                Name = "Belgium"
            };
            var ireland = new Country
            {
                Id = 4,
                Name = "Ireland"
            };

            builder.Entity<Country>().HasData(bulgaria, germany, belgium, ireland);


            var bolyarkaBrewery = new Brewery
            {
                Id = 1,
                Name = "Bolyarka",
              //  Country = bulgaria,
                CountryId = bulgaria.Id
            };
            var hofbrauBrewery = new Brewery
            {
                Id = 2,
                Name = "HofBrau",
             //   Country = germany,
                CountryId = germany.Id
            };
            var bourgogneBrewery = new Brewery
            {
                Id = 3,
                Name = "Bourgogne des Flandres",
             //   Country = germany,
                CountryId = germany.Id
            };

            builder.Entity<Brewery>().HasData(bolyarkaBrewery, hofbrauBrewery, bourgogneBrewery);


            var stout = new BeerType
            {
                Id = 1,
                Name = "Stout"
            };
            var lager = new BeerType
            {
                Id = 2,
                Name = "Lager"
            };
            var porter = new BeerType
            {
                Id = 3,
                Name = "Porter"
            };

            builder.Entity<BeerType>().HasData(stout, porter, lager);

            var bolyarkaBeer = new Beer
            {
                Id = 1,
                Name = "Bolyarka",
                CountryId = 1,
                TypeId = 1,
                AlcoholPercentage = 5.5,
                BreweryId = bolyarkaBrewery.Id,
                IsBeerOfTheMonth = true
            };
            var hofbrauBeer = new Beer
            {
                Id = 2,
                Name = "Hofbrau",
                CountryId = 3,
                TypeId = 2,
                AlcoholPercentage = 8,
                BreweryId = hofbrauBrewery.Id
            };
            var bourgogneBeer = new Beer
            {
                Id = 3,
                Name = "Bourgogne des Flandres",
                CountryId = 3,
                TypeId = 3,
                AlcoholPercentage = 11,
                BreweryId = hofbrauBrewery.Id
            };
            var guinessBeer = new Beer
            {
                Id = 4,
                Name = "Guiness",
                CountryId = 4,
                TypeId = 1,
                AlcoholPercentage = 11,
                BreweryId = hofbrauBrewery.Id,
                IsBeerOfTheMonth = true
            };

            builder.Entity<Beer>().HasData(bourgogneBeer, bolyarkaBeer, hofbrauBeer, guinessBeer);

            var wishBeer1 = new WishBeer
            {
                BeerId = bolyarkaBeer.Id,
                UserId = iva.Id
            };
            var wishBeer2 = new WishBeer
            {
                BeerId = hofbrauBeer.Id,
                UserId = iva.Id
            };
            var wishBeer3 = new WishBeer
            {
                BeerId = bourgogneBeer.Id,
                UserId = iva.Id
            };
            var wishBeer4 = new WishBeer
            {
                BeerId = hofbrauBeer.Id,
                UserId = stamat.Id
            };
            var wishBeer5 = new WishBeer
            {
                BeerId = bourgogneBeer.Id,
                UserId = stamat.Id
            };

            builder.Entity<WishBeer>().HasData(wishBeer1, wishBeer2, wishBeer3, wishBeer4, wishBeer5);

            var drankBeer1 = new DrankBeer
            {
                BeerId = bolyarkaBeer.Id,
                UserId = iva.Id
            };
            var drankBeer2 = new DrankBeer
            {
                BeerId = 2,
                UserId = iva.Id
            };
            var drankBeer3 = new DrankBeer
            {
                BeerId = 3,
                UserId = iva.Id
            };
            var drankBeer4 = new DrankBeer
            {
                BeerId = bolyarkaBeer.Id,
                UserId = pesho.Id
            };
            var drankBeer5 = new DrankBeer
            {
                BeerId = 3,
                UserId = pesho.Id
            };

            builder.Entity<DrankBeer>().HasData(drankBeer1, drankBeer2, drankBeer3, drankBeer4, drankBeer5);

            var review1 = new Review
            {
                BeerId = bolyarkaBeer.Id,
                ReviewText = "Great Taste",
                UserId = iva.Id,
                Likes = 2,
                Dislikes = 1
            };
            var review2 = new Review
            {
                BeerId = hofbrauBeer.Id,
                ReviewText = "Great",
                UserId = iva.Id,
                Likes = 0,
                Dislikes = 1
            };
            var review3 = new Review
            {
                BeerId = bourgogneBeer.Id,
                ReviewText = "Amazing",
                UserId = iva.Id,
                Likes = 1,
                Dislikes = 0
            };
            var review4 = new Review
            {
                BeerId = bolyarkaBeer.Id,
                ReviewText = "Extraordinary",
                UserId = pesho.Id,
            };

            builder.Entity<Review>().HasData(review1, review2, review3, review4);

            var rating1 = new Rating
            {
                BeerId = bolyarkaBeer.Id,
                UserId = iva.Id,
                RatingValue = 5
            };
            var rating2 = new Rating
            {
                BeerId = hofbrauBeer.Id,
                UserId = iva.Id,
                RatingValue = 4
            };
            var rating3 = new Rating
            {
                BeerId = bourgogneBeer.Id,
                UserId = stamat.Id,
                RatingValue = 3
            };

            builder.Entity<Rating>().HasData(rating1, rating2, rating3);

        }
    }
}
