﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.CountryServiceTests
{
    [TestClass]
    public class DeleteCountryAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_CountryIsDeleted()
        {
            var options = Utilities.GetOptions(nameof(ReturnTrue_When_CountryIsDeleted));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            
            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.DeleteCountryAsync(1);

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_CountryIsNotDeleted()
        {
            var options = Utilities.GetOptions(nameof(ReturnFalse_When_CountryIsNotDeleted));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.DeleteCountryAsync(2);

                Assert.IsFalse(result);
            }
        }
    }
}
