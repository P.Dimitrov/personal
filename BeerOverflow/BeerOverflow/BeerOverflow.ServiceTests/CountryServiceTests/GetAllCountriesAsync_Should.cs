﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.CountryServiceTests
{
    [TestClass]
    public class GetAllCountriesAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCountries_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectCountries_When_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var country2 = new Country
            {
                Name = "Belgium"
            };
            var country3 = new Country
            {
                Name = "Germany"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var countryDTO2 = new CountryDTO
            {
                CountryDTOId = 2,
                Name = "Belgium"
            };
            var countryDTO3 = new CountryDTO
            {
                CountryDTOId = 3,
                Name = "Germany"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Countries.AddAsync(country2);
                await arrangeContext.Countries.AddAsync(country3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.GetAllCountriesAsync();
                List<CountryDTO> resultList = result.Select(c => c).ToList();

                Assert.AreEqual(country.Name, resultList[0].Name);
                Assert.AreEqual(country2.Name, resultList[1].Name);
                Assert.AreEqual(country3.Name, resultList[2].Name);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_NoCountryIsFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_NoCountryIsFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            ///Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut =  new CountryService(mockDateTimeProvider.Object, assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() =>  sut.GetAllCountriesAsync());
            }
        }
    }
}
