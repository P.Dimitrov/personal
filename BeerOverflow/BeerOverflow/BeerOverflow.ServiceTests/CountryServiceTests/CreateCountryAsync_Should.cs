﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.CountryServiceTests
{
    [TestClass]
    public class CreateCountryAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectType_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectType_When_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(mockDateTimeProvider.Object, assertContext);
                var expectedType = new CountryDTO();
                var result = await sut.CreateCountryAsync(countryDTO);

                Assert.IsInstanceOfType(result, typeof(CountryDTO));
            }
        }

        [TestMethod]
        public async Task ReturnCorrectCountry_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectCountry_When_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Name = "Germany"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Germany"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(mockDateTimeProvider.Object, assertContext);
                var expected = new CountryDTO
                {
                    CountryDTOId = 1,
                    Name = "Germany"
                };
                var result = await sut.CreateCountryAsync(countryDTO);

                Assert.AreEqual(expected.CountryDTOId, result.CountryDTOId);
                Assert.AreEqual(expected.Name, result.Name);
            }
        }
    }
}
