﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.CountryServiceTests
{
    [TestClass]
    public class UpdateCountryAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCountry_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectCountry_When_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var newCountryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Belgium"
            };
            

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.UpdateCountryAsync(1, newCountryDTO);

                Assert.AreEqual(newCountryDTO.CountryDTOId, result.CountryDTOId);
                Assert.AreEqual(newCountryDTO.Name, result.Name);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_UpdatedCountryNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_UpdatedCountryNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Name = "Bulgaria"
            };

            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var newCountryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Belgium"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act & Assert
                var sut = new CountryService(mockDateTimeProvider.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateCountryAsync(2, newCountryDTO));
            }
        }
    }
}
