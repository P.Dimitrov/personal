﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.CountryServiceTests
{
    [TestClass]
    public class GetCountryByNameAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCountry_When_GivenValidName()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectCountry_When_GivenValidName));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var country2 = new Country
            {
                Name = "Belgium"
            };
            var country3 = new Country
            {
                Name = "Germany"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var countryDTO2 = new CountryDTO
            {
                CountryDTOId = 2,
                Name = "Belgium"
            };
            var countryDTO3 = new CountryDTO
            {
                CountryDTOId = 3,
                Name = "Germany"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Countries.AddAsync(country2);
                await arrangeContext.Countries.AddAsync(country3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.GetCountryByNameAsync("Germany");

                Assert.AreEqual(country3.Name, result.Name);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_CountryNotFoundByName()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_CountryNotFoundByName));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Name = "Bulgaria"
            };

            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act & Assert
                var sut = new CountryService(mockDateTimeProvider.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetCountryByNameAsync("UK"));
            }
        }
    }
}
