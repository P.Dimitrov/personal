﻿//using BeerOverflow.Data;
//using BeerOverflow.Models;
//using BeerOverflow.Services.DTO;
//using BeerOverflow.ServiceTests;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace BeerOverflow.Services.Tests.BeerServiceTests
//{
//    [TestClass]
//    public class GetAllBeersAsync_Should
//    {
//        [TestMethod]
//        public async Task ReturnCorrectAllBeers_When_ParamsAreValid()
//        {
//            var options = Utilities.GetOptions(nameof(ReturnCorrectAllBeers_When_ParamsAreValid));

//            var beer = new Beer
//            {
//                Name = "Bolyarka"
//            };
//            var beer2 = new Beer
//            {
//                Name = "Ariana"
//            };
//            var beer3 = new Beer
//            {
//                Name = "Zagorka"
//            };
//            var beerDTO = new BeerDTO
//            {
//                BeerDTOId = 1,
//                Name = "Bolyarka"
//            };
//            var beerDTO2 = new BeerDTO
//            {
//                BeerDTOId = 2,
//                Name = "Ariana"
//            };
//            var beerDTO3 = new BeerDTO
//            {
//                BeerDTOId = 3,
//                Name = "Zagorka"
//            };

//            using (var arrangeContext = new BeerOverflowContext(options))
//            {
//                await arrangeContext.Beers.AddAsync(beer);
//                await arrangeContext.Beers.AddAsync(beer2);
//                await arrangeContext.Beers.AddAsync(beer3);
//                await arrangeContext.SaveChangesAsync();
//            }

//            //Act & Assert
//            using (var assertContext = new BeerOverflowContext(options))
//            {
//                var sut = new BeerService(assertContext);
//                var result = await sut.GetAllBeersAsync();
//                List<BeerDTO> resultList = result.Select(c => c).ToList();

//                Assert.AreEqual(beer.Name, resultList[0].Name);
//                Assert.AreEqual(beer2.Name, resultList[1].Name);
//                Assert.AreEqual(beer3.Name, resultList[2].Name);
//            }
//        }
//    }
//}
