﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BreweryServiceTests
{
    [TestClass]
    public class GetAllBreweriesAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBreweries_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBreweries_When_ParamsAreValid));
            var mockCountryService = new Mock<ICountryService>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var brewery = new Brewery
            {
                Name = "Kamenitza",
                Country = country
            };
            var brewery2 = new Brewery
            {
                Name = "Zagorka",
                Country = country
            };
            var brewery3 = new Brewery
            {
                Name = "Pirinsko",
                Country = country
            };
            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Kamenitza",
                Country = countryDTO
            };
            var breweryDTO2 = new BreweryDTO
            {
                BreweryDTOId = 2,
                Name = "Zagorka",
                Country = countryDTO
            };
            var breweryDTO3 = new BreweryDTO
            {
                BreweryDTOId = 3,
                Name = "Pirinsko",
                Country = countryDTO
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.Breweries.AddAsync(brewery2);
                await arrangeContext.Breweries.AddAsync(brewery3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                var result = await sut.GetAllBreweriesAsync();
                List<BreweryDTO> resultList = result.Select(c => c).ToList();

                Assert.AreEqual(brewery.Name, resultList[0].Name);
                Assert.AreEqual(brewery2.Name, resultList[1].Name);
                Assert.AreEqual(brewery3.Name, resultList[2].Name);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_NoBreweryIsFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_NoBreweryIsFound));
            var mockCountryService = new Mock<ICountryService>();

            ///Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(mockCountryService.Object, assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAllBreweriesAsync());
            }
        }
    }
}
