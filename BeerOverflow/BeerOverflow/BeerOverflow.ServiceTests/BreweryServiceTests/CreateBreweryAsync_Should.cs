﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BreweryServiceTests
{
    [TestClass]
    public class CreateBreweryAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTypeBrewery_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectTypeBrewery_When_ParamsAreValid));
            var mockCountryService = new Mock<ICountryService>();

            var country = new Country
            {
                Name = "Germany"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Germany"
            };
            var brewery = new Brewery
            {
                Name = "Deutschland",
                Country = country
            };

            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Deutschland",
                Country = countryDTO
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                var expectedType = new BreweryDTO();
                var result = await sut.CreateBreweryAsync(breweryDTO);

                Assert.IsInstanceOfType(result, typeof(BreweryDTO));
            }
        }

        [TestMethod]
        public async Task ReturnCorrectBrewery_When_CreatedWithValidParams()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBrewery_When_CreatedWithValidParams));
            var mockCountryService = new Mock<ICountryService>();

            var country = new Country
            {
                Name = "Czech Republic"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Czech Republic"
            };
            var brewery = new Brewery
            {
                Name = "Pilsner",
                Country = country
            };

            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Pilsner",
                Country = countryDTO
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                var expected = new BreweryDTO
                {
                    BreweryDTOId = 1,
                    Name = "Pilsner",
                    Country = countryDTO
                };
                var result = await sut.CreateBreweryAsync(breweryDTO);

                Assert.AreEqual(expected.BreweryDTOId, result.BreweryDTOId);
                Assert.AreEqual(expected.Name, result.Name);
                Assert.AreEqual(expected.Country.Name, result.Country.Name);
            }
        }
    }
}
