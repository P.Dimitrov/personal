﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BreweryServiceTests
{
    [TestClass]
    public class GetBreweryByNameAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBrewery_When_ValidNameIsGiven()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBrewery_When_ValidNameIsGiven));
            var mockCountryService = new Mock<ICountryService>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var brewery = new Brewery
            {
                Name = "Kamenitza",
                Country = country
            };
            var brewery2 = new Brewery
            {
                Name = "Zagorka",
                Country = country
            };
            var brewery3 = new Brewery
            {
                Name = "Pirinsko",
                Country = country
            };
            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Kamenitza",
                Country = countryDTO
            };
            var breweryDTO2 = new BreweryDTO
            {
                BreweryDTOId = 2,
                Name = "Zagorka",
                Country = countryDTO
            };
            var breweryDTO3 = new BreweryDTO
            {
                BreweryDTOId = 3,
                Name = "Pirinsko",
                Country = countryDTO
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.Breweries.AddAsync(brewery2);
                await arrangeContext.Breweries.AddAsync(brewery3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                var result = await sut.GetBreweryByNameAsync("Pirinsko");

                Assert.AreEqual(brewery3.Id, result.BreweryDTOId);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_BreweryNotFoundByName()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_BreweryNotFoundByName));
            var mockCountryService = new Mock<ICountryService>();

            var brewery = new Brewery
            {
                Name = "Kamenitza"
            };

            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Kamenitza"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act & Assert
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetBreweryByNameAsync("Pirinsko"));
            }
        }
    }
}
