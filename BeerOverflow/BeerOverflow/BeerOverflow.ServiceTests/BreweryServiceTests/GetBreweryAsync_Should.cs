﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BreweryServiceTests
{
    [TestClass]
    public class GetBreweryAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBrewery_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBrewery_When_ParamsAreValid));
            var mockCountryService = new Mock<ICountryService>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var brewery = new Brewery
            {
                Name = "Kamenitza",
                Country = country
            };
            var brewery2 = new Brewery
            {
                Name = "Zagorka",
                Country = country
            };
            var brewery3 = new Brewery
            {
                Name = "Pirinsko",
                Country = country
            };
            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Kamenitza",
                Country = countryDTO
            };
            var breweryDTO2 = new BreweryDTO
            {
                BreweryDTOId = 2,
                Name = "Zagorka",
                Country = countryDTO
            };
            var breweryDTO3 = new BreweryDTO
            {
                BreweryDTOId = 3,
                Name = "Pirinsko",
                Country = countryDTO
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.Breweries.AddAsync(brewery2);
                await arrangeContext.Breweries.AddAsync(brewery3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                var result = await sut.GetBreweryAsync(2);

                //Assert.AreEqual(brewery2.Id, result.BreweryDTOId);
                Assert.AreEqual(brewery2.Name, result.Name);
                Assert.AreEqual(brewery2.Country.Name, result.Country.Name);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_BreweryNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_BreweryNotFound));
            var mockCountryService = new Mock<ICountryService>();

            var brewery = new Brewery
            {
                Name = "Heineken"
            };

            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Heineken"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act & Assert
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetBreweryAsync(10));
            }
        }
    }
}
