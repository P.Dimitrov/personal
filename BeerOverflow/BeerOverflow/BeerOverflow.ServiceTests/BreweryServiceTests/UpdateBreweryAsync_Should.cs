﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BreweryServiceTests
{
    [TestClass]
    public class UpdateBreweryAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBrewery_When_NewNameIsGiven()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBrewery_When_NewNameIsGiven));
            var mockCountryService = new Mock<ICountryService>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var brewery = new Brewery
            {
                Name = "Kamenitza",
                Country = country
            };
            var newBrewery = new Brewery
            {
                Name = "Zagorka",
                Country = country
            };
           
            var BreweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Kamenitza",
                Country = countryDTO
            };
            var newBreweryDTO = new BreweryDTO
            {
                BreweryDTOId = 2,
                Name = "Zagorka",
                Country = countryDTO
            };
            
            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                var result = await sut.UpdateBreweryAsync(1, newBreweryDTO);

                Assert.AreEqual(newBreweryDTO.BreweryDTOId, result.BreweryDTOId);
                Assert.AreEqual(newBreweryDTO.Name, result.Name);
                Assert.AreEqual(newBreweryDTO.Country.Name, result.Country.Name);
            }

        }

        //[TestMethod]
        //public async Task ReturnCorrectBrewery_When_NewCountryIsGiven()
        //{
        //    var options = Utilities.GetOptions(nameof(ReturnCorrectBrewery_When_NewCountryIsGiven));
        //    var mockCountryService = new Mock<ICountryService>();

        //    var country = new Country
        //    {
        //        Name = "Bulgaria"
        //    };
        //    var countryDTO = new CountryDTO
        //    {
        //        CountryDTOId = 1,
        //        Name = "Bulgaria"
        //    };
        //    var brewery = new Brewery
        //    {
        //        Name = "Kamenitza",
        //        Country = country
        //    };

        //    var breweryDTO = new BreweryDTO
        //    {
        //        BreweryDTOId = 1,
        //        Name = "Kamenitza",
        //        Country = countryDTO
        //    };
        //    var newCountry = new Country
        //    {
        //        Name = "Belgium"
        //    };
        //    var newCountryDTO = new CountryDTO
        //    {
        //        CountryDTOId = 1,
        //        Name = "Belgium"
        //    };

        //    var newBrewery = new Brewery
        //    {
        //        Name = "Kamenitza",
        //        Country = newCountry
        //    };
        //    var newBreweryDTO = new BreweryDTO
        //    {
        //        BreweryDTOId = 1,
        //        Name = "Kamenitza",
        //        Country = newCountryDTO
        //    };

        //    using (var arrangeContext = new BeerOverflowContext(options))
        //    {
        //        await arrangeContext.Breweries.AddAsync(brewery);
        //        await arrangeContext.SaveChangesAsync();
        //    }

        //    //Act & Assert
        //    using (var assertContext = new BeerOverflowContext(options))
        //    {
        //        var sut = new BreweryService(mockCountryService.Object, assertContext);
        //        var result = await sut.UpdateBreweryAsync(1, newBreweryDTO);

        //        Assert.AreEqual(newBreweryDTO.BreweryDTOId, result.BreweryDTOId);
        //        Assert.AreEqual(newBreweryDTO.Name, result.Name);
        //        Assert.AreEqual(newBreweryDTO.Country.Name, result.Country.Name);
        //    }

        //}

        [TestMethod]
        public async Task ThrowExceptionWhen_UpdatedBreweryNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowExceptionWhen_UpdatedBreweryNotFound));
            var mockCountryService = new Mock<ICountryService>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var brewery = new Brewery
            {
                Name = "Heineken"
            };

            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Heineken"
            };
            var newBrewery = new Brewery
            {
                Name = "Zagorka",
                Country = country
            };
            var newBreweryDTO = new BreweryDTO
            {
                BreweryDTOId = 2,
                Name = "Zagorka",
                Country = countryDTO
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act & Assert
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateBreweryAsync(3, newBreweryDTO));
            }
        }
    }
}
