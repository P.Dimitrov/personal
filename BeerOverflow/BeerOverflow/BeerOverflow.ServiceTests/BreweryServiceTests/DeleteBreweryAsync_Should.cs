﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BreweryServiceTests
{
    [TestClass]
    public class DeleteBreweryAsync_Should
    {


        [TestMethod]
        public async Task ReturnTrue_When_BreweryIsDeleted()
        {
            var options = Utilities.GetOptions(nameof(ReturnTrue_When_BreweryIsDeleted));
            var mockCountryService = new Mock<ICountryService>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var brewery = new Brewery
            {
                Name = "Kamenitza",
                Country = country
            };
            var brewery2 = new Brewery
            {
                Name = "Zagorka",
                Country = country
            };
            var brewery3 = new Brewery
            {
                Name = "Pirinsko",
                Country = country
            };
            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Kamenitza",
                Country = countryDTO
            };
            var breweryDTO2 = new BreweryDTO
            {
                BreweryDTOId = 2,
                Name = "Zagorka",
                Country = countryDTO
            };
            var breweryDTO3 = new BreweryDTO
            {
                BreweryDTOId = 3,
                Name = "Pirinsko",
                Country = countryDTO
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.Breweries.AddAsync(brewery2);
                await arrangeContext.Breweries.AddAsync(brewery3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                var result = await sut.DeleteBreweryAsync(1);

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_BreweryIsNotDeleted()
        {
            var options = Utilities.GetOptions(nameof(ReturnTrue_When_BreweryIsDeleted));
            var mockCountryService = new Mock<ICountryService>();

            var country = new Country
            {
                Name = "Bulgaria"
            };
            var countryDTO = new CountryDTO
            {
                CountryDTOId = 1,
                Name = "Bulgaria"
            };
            var brewery = new Brewery
            {
                Name = "Kamenitza",
                Country = country
            };
            var brewery2 = new Brewery
            {
                Name = "Zagorka",
                Country = country
            };
            var brewery3 = new Brewery
            {
                Name = "Pirinsko",
                Country = country
            };
            var breweryDTO = new BreweryDTO
            {
                BreweryDTOId = 1,
                Name = "Kamenitza",
                Country = countryDTO
            };
            var breweryDTO2 = new BreweryDTO
            {
                BreweryDTOId = 2,
                Name = "Zagorka",
                Country = countryDTO
            };
            var breweryDTO3 = new BreweryDTO
            {
                BreweryDTOId = 3,
                Name = "Pirinsko",
                Country = countryDTO
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.Breweries.AddAsync(brewery2);
                await arrangeContext.Breweries.AddAsync(brewery3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(mockCountryService.Object, assertContext);
                var result = await sut.DeleteBreweryAsync(7);

                Assert.IsFalse(result);
            }
        }
    }
}
