using BeerOverflow.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerOverflow.ServiceTests
{
    public class Utilities
    {
        public static DbContextOptions<BeerOverflowContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<BeerOverflowContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}
