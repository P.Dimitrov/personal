﻿//using BeerOverflow.Data;
//using BeerOverflow.Models;
//using BeerOverflow.Services.DTO;
//using BeerOverflow.Services.Provider.Contracts;
//using BeerOverflow.ServiceTests;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading.Tasks;

//namespace BeerOverflow.Services.Tests.ReviewServiceTests
//{
//    [TestClass]
//    public class CreateReviewAsync_Should
//    {
//        [TestMethod]
//        public async Task ReturnCorrectTypeReview_When_CreatedWithValidParams()
//        {
//            var options = Utilities.GetOptions(nameof(ReturnCorrectTypeReview_When_CreatedWithValidParams));
//            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

//            var review = new Review
//            {
//                BeerId = 1,
//                UserId = 34,
//                ReviewText = "Great beer!"
//            };
//            var reviewDTO = new ReviewDTO
//            {
//                BeerId = 1,
//                UserId = 34,
//                ReviewText = "Great beer!"
//            };

//            using (var arrangeContext = new BeerOverflowContext(options))
//            {
//                await arrangeContext.Reviews.AddAsync(review);
//                await arrangeContext.SaveChangesAsync();
//            }

//            //Act & Assert
//            using (var assertContext = new BeerOverflowContext(options))
//            {
//                var sut = new ReviewService(mockDateTimeProvider.Object, assertContext);
//                var expectedType = new ReviewDTO();
//                var result = await sut.CreateReviewAsync(reviewDTO);

//                Assert.IsInstanceOfType(result, typeof(ReviewDTO));
//            }
//        }

//        [TestMethod]
//        public async Task ReturnCorrectReview_When_CreatedWithValidParams()
//        {
//            var options = Utilities.GetOptions(nameof(ReturnCorrectReview_When_CreatedWithValidParams));
//            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

//            var review = new Review
//            {
//                BeerId = 1,
//                UserId = 34,
//                ReviewText = "Great beer!"
//            };
//            var reviewDTO = new ReviewDTO
//            {
//                BeerId = 1,
//                UserId = 34,
//                ReviewText = "Great beer!"
//            };

//            using (var arrangeContext = new BeerOverflowContext(options))
//            {
//                await arrangeContext.Reviews.AddAsync(review);
//                await arrangeContext.SaveChangesAsync();
//            }

//            //Act & Assert
//            using (var assertContext = new BeerOverflowContext(options))
//            {
//                var sut = new ReviewService(mockDateTimeProvider.Object, assertContext);
//                var expected = new ReviewDTO
//                {
//                    BeerId = 1,
//                    UserId = 34,
//                    ReviewText = "Great beer!"
//                };
//                var result = await sut.CreateReviewAsync(reviewDTO);

//                Assert.AreEqual(expected.BeerId, result.BeerId);
//                Assert.AreEqual(expected.UserId, result.UserId);
//                Assert.AreEqual(expected.ReviewText, result.ReviewText);
//            }
//        }
//    }
//}
