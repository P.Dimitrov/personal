﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.ReviewServiceTests
{
    [TestClass]
    public class GetReviewAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectReview_When_GivenParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectReview_When_GivenParamsAreValid));
            var mockReviewService = new Mock<IReviewService>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var review = new Review
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };
            var reviewDTO = new ReviewDTO
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };

            var review2 = new Review
            {
                BeerId = 4,
                UserId = 15,
                ReviewText = "I don't like it!!!"
            };
            var reviewDTO2 = new ReviewDTO
            {
                BeerId = 4,
                UserId = 15,
                ReviewText = "I don't like it!!!"
            };
            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Reviews.AddAsync(review);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new ReviewService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.GetReviewAsync(1, 34);

                Assert.AreEqual(review.BeerId, result.BeerId);
                Assert.AreEqual(review.UserId, result.UserId);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_ReviewNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_ReviewNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var review = new Review
            {
                BeerId = 4,
                UserId = 15,
                ReviewText = "I don't like it!!!"
            };
            var reviewDTO = new ReviewDTO
            {
                BeerId = 4,
                UserId = 15,
                ReviewText = "I don't like it!!!"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Reviews.AddAsync(review);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act & Assert
                var sut = new ReviewService(mockDateTimeProvider.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetReviewAsync(1, 14));
            }
        }
    }
}
