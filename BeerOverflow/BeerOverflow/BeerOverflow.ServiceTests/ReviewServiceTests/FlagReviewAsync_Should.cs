﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.ReviewServiceTests
{
    [TestClass]
    public class FlagReviewAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_ReviewIsFlagged()
        {
            var options = Utilities.GetOptions(nameof(ReturnTrue_When_ReviewIsFlagged));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var review = new Review
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };
            var reviewDTO = new ReviewDTO
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Reviews.AddAsync(review);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new ReviewService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.FlagReviewAsync(1, 34);

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_ReviewIsNotFlagged()
        {
            var options = Utilities.GetOptions(nameof(ReturnFalse_When_ReviewIsNotFlagged));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var review = new Review
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };
            var reviewDTO = new ReviewDTO
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Reviews.AddAsync(review);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new ReviewService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.FlagReviewAsync(1, 33);

                Assert.IsFalse(result);
            }
        }
    }
}
