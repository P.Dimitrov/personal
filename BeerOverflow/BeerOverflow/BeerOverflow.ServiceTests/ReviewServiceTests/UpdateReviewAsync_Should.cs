﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.ReviewServiceTests
{
    [TestClass]
    public class UpdateReviewAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectlyUpdatedReview_When_NewTextIsGiven()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectlyUpdatedReview_When_NewTextIsGiven));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var review = new Review
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };
            var reviewDTO = new ReviewDTO
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };

            //var newReview = new Review
            //{
            //    BeerId = 1,
            //    UserId = 34,
            //    ReviewText = "I don't like it!!!"
            //};
            var newReviewDTO = new ReviewDTO
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "I don't like it!!!"
            };

            

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Reviews.AddAsync(review);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new ReviewService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.UpdateReviewAsync(1, 34, newReviewDTO.ReviewText);

                Assert.AreEqual(newReviewDTO.ReviewText, result.ReviewText);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionWhen_UpdatedReviewIsNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowExceptionWhen_UpdatedReviewIsNotFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var review = new Review
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };
            var reviewDTO = new ReviewDTO
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };

            var newReview = new Review
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "I don't like it!!!"
            };
            var newReviewDTO = new ReviewDTO
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "I don't like it!!!"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Reviews.AddAsync(review);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act & Assert
                var sut = new ReviewService(mockDateTimeProvider.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateReviewAsync(1, 33, newReviewDTO.ReviewText));
            }
        }
    }
}
