﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.ReviewServiceTests
{
    [TestClass]
    public class GetAllReviewsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectReview_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectReview_When_ParamsAreValid));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var review = new Review
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };
            var reviewDTO = new ReviewDTO
            {
                BeerId = 1,
                UserId = 34,
                ReviewText = "Great beer!"
            };

            var review2 = new Review
            {
                BeerId = 4,
                UserId = 15,
                ReviewText = "I don't like it!!!"
            };
            var reviewDTO2 = new ReviewDTO
            {
                BeerId = 4,
                UserId = 15,
                ReviewText = "I don't like it!!!"
            };
            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Reviews.AddAsync(review);
                await arrangeContext.Reviews.AddAsync(review2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new ReviewService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.GetAllReviewsAsync();
                List<ReviewDTO> resultList = result.Select(c => c).ToList();

                Assert.AreEqual(review.ReviewText, resultList[0].ReviewText);
                Assert.AreEqual(review2.ReviewText, resultList[1].ReviewText);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_NoReviewIsFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_NoReviewIsFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            ///Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new ReviewService(mockDateTimeProvider.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAllReviewsAsync());
            }
        }
    }
}
