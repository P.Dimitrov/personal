﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerTypeServiceTests
{
    [TestClass]
    public class UpdateBeerTypeAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectlyUpdatedBeerType_When_NewNameIsGiven()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectlyUpdatedBeerType_When_NewNameIsGiven));
            var mockBeerTypeService = new Mock<IBeerTypeService>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var beerType = new BeerType
            {
                Name = "Light",
            };
            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = 1,
                Name = "Light"
            };
            var newBeerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = 1,
                Name = "Dark"
            };
            
            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.BeerTypes.AddAsync(beerType);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerTypeService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.UpdateBeerTypeAsync(newBeerTypeDTO);

                Assert.AreEqual(newBeerTypeDTO.BeerTypeDTOId, result.BeerTypeDTOId);
                Assert.AreEqual(newBeerTypeDTO.Name, result.Name);
            }

        }

        [TestMethod]
        public async Task ThrowExceptionWhen_UpdatedBeerTypeIsNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowExceptionWhen_UpdatedBeerTypeIsNotFound));
            var mockBeerTypeService = new Mock<IBeerTypeService>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var beerType = new BeerType
            {
                Name = "Light",
            };
            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = 1,
                Name = "Light"
            };
            var newBeerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = 3,
                Name = "Dark"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.BeerTypes.AddAsync(beerType);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act & Assert
                var sut = new BeerTypeService(mockDateTimeProvider.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateBeerTypeAsync(newBeerTypeDTO));
            }
        }
    }
}
