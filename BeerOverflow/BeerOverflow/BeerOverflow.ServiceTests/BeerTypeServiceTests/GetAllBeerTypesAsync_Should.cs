﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerTypeServiceTests
{
    [TestClass]
    public class GetAllBeerTypesAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBeerTypes_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBeerTypes_When_ParamsAreValid));
            var mockBeerTypeService = new Mock<IBeerTypeService>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var beerType = new BeerType
            {
                Name = "Light",
            };
            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = 1,
                Name = "Light"
            };
            var beerType2 = new BeerType
            {
                Name = "Dark",
            };
            var beerTypeDTO2 = new BeerTypeDTO
            {
                BeerTypeDTOId = 2,
                Name = "Dark"
            };
            var beerType3 = new BeerType
            {
                Name = "Stout",
            };
            var beerTypeDTO3 = new BeerTypeDTO
            {
                BeerTypeDTOId = 3,
                Name = "Stout"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.BeerTypes.AddAsync(beerType);
                await arrangeContext.BeerTypes.AddAsync(beerType2);
                await arrangeContext.BeerTypes.AddAsync(beerType3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerTypeService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.GetAllBeerTypesAsync();
                List<BeerTypeDTO> resultList = result.Select(c => c).ToList();

                Assert.AreEqual(beerType.Name, resultList[0].Name);
                Assert.AreEqual(beerType2.Name, resultList[1].Name);
                Assert.AreEqual(beerType3.Name, resultList[2].Name);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_NoBeerTypeIsFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_NoBeerTypeIsFound));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            ///Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerTypeService(mockDateTimeProvider.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAllBeerTypesAsync());
            }
        }
    }
}
