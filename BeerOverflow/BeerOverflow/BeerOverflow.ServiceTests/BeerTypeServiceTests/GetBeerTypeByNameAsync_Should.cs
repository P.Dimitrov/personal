﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerTypeServiceTests
{
    [TestClass]
    public class GetBeerTypeByNameAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBeerType_WhenANameIsValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBeerType_WhenANameIsValid));
            var mockBeerTypeService = new Mock<IBeerTypeService>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var beerType = new BeerType
            {
                Name = "Light",
            };
            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = 1,
                Name = "Light"
            };
            var beerType2 = new BeerType
            {
                Name = "Dark",
            };
            var beerTypeDTO2 = new BeerTypeDTO
            {
                BeerTypeDTOId = 2,
                Name = "Dark"
            };
            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.BeerTypes.AddAsync(beerType);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerTypeService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.GetBeerTypeByNameAsync("Light");

                Assert.AreEqual(beerType.Id, result.BeerTypeDTOId);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_BeerTypeNotFoundByName()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_BeerTypeNotFoundByName));
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var beerType = new BeerType
            {
                Name = "Dark",
            };
            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = 1,
                Name = "Dark"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.BeerTypes.AddAsync(beerType);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act & Assert
                var sut = new BeerTypeService(mockDateTimeProvider.Object, assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetBeerTypeByNameAsync("Light"));
            }
        }
    }
}
