﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerTypeServiceTests
{
    [TestClass]
    public class DeleteBeerType_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_BeerTypeIsDeleted()
        {
            var options = Utilities.GetOptions(nameof(ReturnTrue_When_BeerTypeIsDeleted));
            var mockBeerTypeService = new Mock<IBeerTypeService>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var beerType = new BeerType
            {
                Name = "Light",
            };
            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = 1,
                Name = "Light"
            };
            var beerType2 = new BeerType
            {
                Name = "Dark",
            };
            var beerTypeDTO2 = new BeerTypeDTO
            {
                BeerTypeDTOId = 2,
                Name = "Dark"
            };
            var beerType3 = new BeerType
            {
                Name = "Stout",
            };
            var beerTypeDTO3 = new BeerTypeDTO
            {
                BeerTypeDTOId = 3,
                Name = "Stout"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.BeerTypes.AddAsync(beerType);
                await arrangeContext.BeerTypes.AddAsync(beerType2);
                await arrangeContext.BeerTypes.AddAsync(beerType3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerTypeService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.DeleteBeerTypeAsync(2);

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_BeerTypeIsNotDeleted()
        {
            var options = Utilities.GetOptions(nameof(ReturnFalse_When_BeerTypeIsNotDeleted));
            var mockBeerTypeService = new Mock<IBeerTypeService>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var beerType = new BeerType
            {
                Name = "Light",
            };
            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = 1,
                Name = "Light"
            };
            var beerType2 = new BeerType
            {
                Name = "Dark",
            };
            var beerTypeDTO2 = new BeerTypeDTO
            {
                BeerTypeDTOId = 2,
                Name = "Dark"
            };
            var beerType3 = new BeerType
            {
                Name = "Stout",
            };
            var beerTypeDTO3 = new BeerTypeDTO
            {
                BeerTypeDTOId = 3,
                Name = "Stout"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.BeerTypes.AddAsync(beerType);
                await arrangeContext.BeerTypes.AddAsync(beerType2);
                await arrangeContext.BeerTypes.AddAsync(beerType3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerTypeService(mockDateTimeProvider.Object, assertContext);
                var result = await sut.DeleteBeerTypeAsync(7);

                Assert.IsFalse(result);
            }
        }
    }
}
