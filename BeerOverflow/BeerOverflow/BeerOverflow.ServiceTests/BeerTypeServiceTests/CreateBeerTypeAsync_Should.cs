﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Provider.Contracts;
using BeerOverflow.ServiceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerTypeServiceTests
{
    [TestClass]
    public class CreateBeerTypeAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTypeBeerType_When_CreatedWithValidParams()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectTypeBeerType_When_CreatedWithValidParams));
            var mockBeerTypeService = new Mock<IBeerTypeService>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var beerType = new BeerType
            {
                Name = "Light",
            };
            var beerTypeDTO = new BeerTypeDTO
            {
                BeerTypeDTOId = 1,
                Name = "Light"
            };
            
            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.BeerTypes.AddAsync(beerType);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerTypeService(mockDateTimeProvider.Object, assertContext);
                var expectedType = new BeerTypeDTO();
                var result = await sut.CreateBeerTypeAsync(beerTypeDTO);

                Assert.IsInstanceOfType(result, typeof(BeerTypeDTO));
            }
        }

        [TestMethod]
        public async Task ReturnCorrectBeerType_When_CreatedWithValidParams()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBeerType_When_CreatedWithValidParams));
            var mockBeerTypeService = new Mock<IBeerTypeService>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var beerType3 = new BeerType
            {
                Name = "Stout",
            };
            var beerTypeDTO3 = new BeerTypeDTO
            {
                BeerTypeDTOId = 3,
                Name = "Stout"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.BeerTypes.AddAsync(beerType3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerTypeService(mockDateTimeProvider.Object, assertContext);
                var expected = new BeerTypeDTO
                {
                    BeerTypeDTOId = 3,
                    Name = "Stout"
                };
                var result = await sut.CreateBeerTypeAsync(beerTypeDTO3);

                Assert.AreEqual(expected.BeerTypeDTOId, result.BeerTypeDTOId);
                Assert.AreEqual(expected.Name, result.Name);
            }
        }
    }
}
