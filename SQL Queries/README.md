## Queries
```cs

```
1. Write a SQL query to find all information about all departments (use "TelerikAcademy" database).
```cs
SELECT *
FROM Departments
```
2. Write a SQL query to find all department names.
```cs
SELECT e.Name
FROM Departments as e
```
3. Write a SQL query to find the salary of each employee.
```cs
SELECT e.FirstName, e.LastName, e.Salary
FROM Employees as e
```
4. Write a SQL to find the full name of each employee.
```cs
SELECT e.FirstName, e.MiddleName, e.LastName
FROM Employees as e
```
5. Write a SQL query to find the email addresses of each employee (by his first and last name). Consider that the mail domain is telerik.com. Emails should look like “John.Doe@telerik.com". The produced column should be named "Full Email Addresses".
```cs
SELECT e.FirstName + e.LastName + '@telerik.com'
FROM Employees as e
```
6. Write a SQL query to find all different employee salaries.
```cs
SELECT DISTINCT e.Salary
FROM Employees as e
```
7. Write a SQL query to find all information about the employees whose job title is “Sales Representative“.
```cs
SELECT *
FROM Employees as e
WHERE e.JobTitle='Sales Representative'
```
8. Write a SQL query to find the names of all employees whose first name starts with "SA".
```cs
SELECT e.FirstName, e.MiddleName, e.LastName
FROM Employees as e
WHERE e.FirstName LIKE 'Sa%'
```
9. Write a SQL query to find the names of all employees whose last name contains "ei".
```cs
SELECT e.FirstName, e.MiddleName, e.LastName
FROM Employees as e
WHERE e.FirstName LIKE 'Sa%'
```
10. Write a SQL query to find the salary of all employees whose salary is in the range [20000…30000].
```cs
SELECT DISTINCT e.Salary
FROM Employees as e
WHERE 30000 > e.Salary AND e.Salary > 20000  
```
11. Write a SQL query to find the names of all employees whose salary is 25000, 14000, 12500 or 23600.
```cs
SELECT e.FirstName, e.MiddleName, e.LastName, e.Salary
FROM Employees as e
WHERE e.Salary = 25000 OR e.Salary = 23600 OR e.Salary = 14000 OR e.Salary = 12500
```
12. Write a SQL query to find all employees that do not have manager.
```cs
SELECT *
FROM Employees as e
WHERE e.ManagerID IS NULL
```
13. Write a SQL query to find all employees that have salary more than 50000. Order them in decreasing order by salary.
```cs
SELECT * 
FROM Employees as e
WHERE e.Salary > 50000
ORDER BY e.Salary DESC
```
14. Write a SQL query to find the top 5 best paid employees.
```cs
SELECT TOP 5 Salary
FROM Employees as e
ORDER BY e.Salary DESC
```
15. Write a SQL query to find all employees along with their address. Use inner join with ON clause.
```cs
SELECT *
FROM Employees as e
JOIN Addresses as a
ON e.AddressID = a.AddressID
```
16. Write a SQL query to find all employees and their address. Use equijoins (conditions in the WHERE clause).
```cs
SELECT *
FROM Employees as e, Addresses as a
WHERE e.AddressID = a.AddressID
```
17. Write a SQL query to find all employees along with their manager.
```cs
SELECT *
FROM Employees as e, Addresses as a
WHERE e.AddressID = a.AddressID
```
18. Write a SQL query to find all employees, along with their manager and their address. Join the 3 tables: Employees e, Employees m and Addresses a.
```cs
SELECT *
FROM Employees as e, Employees as d
WHERE e.ManagerID = d.EmployeeID
```
19. Write a SQL query to find all departments and all town names as a single list. Use UNION.
```cs
SELECT d.Name
FROM Departments as d
UNION
SELECT e.Name
FROM Towns as e
```
20. Write a SQL query to find all the employees and the manager for each of them along with the employees that do not have manager. Use right outer join. Rewrite the query to use left outer join.
```cs
SELECT *
FROM Employees as e
LEFT JOIN Employees as d
ON e.ManagerID = d.EmployeeID
SELECT *
FROM Employees as e
RIGHT JOIN Employees as d
ON e.ManagerID = d.EmployeeID
```
21. Write a SQL query to find the names of all employees from the departments "Sales" and "Finance" whose hire year is between 1995 and 2005.
```cs
SELECT *
FROM Employees as e, Departments as d
WHERE e.DepartmentID = d.DepartmentID 
AND d.Name = 'Sales' 
OR d.Name = 'Finance' 
AND e.HireDate > '1995-01-01 00:00:00' 
AND e.HireDate < '2005-12-31 23:59:59'
```
22. Write a SQL query to find the names and salaries of the employees that take the minimal salary in the company.
    - Use a nested SELECT statement.
```cs
SELECT e.FirstName, e.MiddleName, e.LastName, e.Salary
FROM Employees AS e
WHERE e.Salary = (SELECT MIN(Salary) FROM Employees)
```
23. Write a SQL query to find the names and salaries of the employees that have a salary that is up to 10% higher than the minimal salary for the company.
```cs
SELECT e.FirstName, e.MiddleName, e.LastName, e.Salary
FROM Employees AS e
WHERE Salary < (SELECT (MIN(Salary)* 1.1) FROM Employees)
ORDER BY Salary
```
24. Write a SQL query to find the full name, salary and department of the employees that take the minimal salary in their department.
    - Use a nested SELECT statement.
```cs
SELECT e.FirstName, e.MiddleName, e.LastName, e.Salary, d.Name
FROM Employees AS e
JOIN Departments AS d
ON e.DepartmentID = d.DepartmentID
WHERE Salary = (SELECT MIN(Salary) 
FROM Employees AS em 
WHERE em.DepartmentID = d.DepartmentID)
ORDER BY Salary
```
