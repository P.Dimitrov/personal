﻿using System;
using DictionaryWorkshop.Implementation.Contracts;
using DictionaryWorkshop.Implementation;
using DictionaryWorkshop.Models;
using System.Collections.Generic;

namespace DictionaryWorkshop.Skeleton
{
    class Program
    {
        static void Main()
        {
            // Your dictionary implementation
             IMyDictionary<Person, ContactInfo> phoneBook = new MyDictionary<Person, ContactInfo>();
            
            // *** Microsoft dictionary implementation.***
            // *** Uncomment the line below if you want to see what would be an expected behavior.***
            // 
           //IDictionary<Person, ContactInfo> phoneBook = new Dictionary<Person, ContactInfo>();
            // 
            // -------------------------------------------------------------------------------------

            Console.WriteLine("-= 0 =-");
            Console.WriteLine(string.Join('\n', phoneBook));
            Console.WriteLine("---------------------------");

            // name:            Gosho
            // date of birth:   18-Oct-1954
            // phone number:    02946973
            var gosho = new Person("Gosho", dateOfBirth: new DateTime(1954, 10, 18));
            phoneBook[gosho] = new ContactInfo(phoneNumber: "02946973");

            Console.WriteLine("-= 1 =-");
            Console.WriteLine(string.Join('\n', phoneBook));
            Console.WriteLine("---------------------------");

            // name:            Mariya
            // date of birth:   27-Jan-1957
            // phone number:    0889156783
            var mariya = new Person("Mariya", dateOfBirth: new DateTime(1957, 1, 27));
            phoneBook[mariya] = new ContactInfo(phoneNumber: "0889156783");

            Console.WriteLine("-= 2 =-");
            Console.WriteLine(string.Join('\n', phoneBook));
            Console.WriteLine("---------------------------");

            // name:            Ina
            // date of birth:   05-Sep-1989
            // phone number:    444970062
            var ina = new Person("Ina", dateOfBirth: new DateTime(1989, 9, 5));
            phoneBook.Add(ina, new ContactInfo("444970062"));

            Console.WriteLine("-= 3 =-");
            Console.WriteLine(string.Join('\n', phoneBook));
            Console.WriteLine("---------------------------");

            // Change the contact info of Ina to be the same as Mariya's
            phoneBook[ina] = phoneBook[mariya];

            Console.WriteLine("-= 4 =-");
            Console.WriteLine(string.Join('\n', phoneBook));
            Console.WriteLine("---------------------------");

            // Remove Mariya from the phone book.
            // Ina's contact info should be available.
            phoneBook.Remove(mariya);

            Console.WriteLine("-= 5 =-");
            Console.WriteLine(string.Join('\n', phoneBook));
            Console.WriteLine("---------------------------");
        }
    }
}
