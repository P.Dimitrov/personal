﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DictionaryWorkshop.Implementation
{
    public class MyLinkedList<TKey, TValue> : IEnumerable<MyKeyValuePair<TKey, TValue>>
    {
        public MyLinkedList(TKey key, TValue value)
        {
            this.Head = new MyKeyValuePair<TKey, TValue>(key, value);
        }
        public MyLinkedList()
        {

        }
        public MyKeyValuePair<TKey, TValue> Head { get; private set; }
        public bool Add(TKey key, TValue value)
        {
            if (this.Contains(key))
            {
                return false;
            }
            if (this.Head == null)
            {
                this.Head = new MyKeyValuePair<TKey, TValue>(key, value);
                return true;
            }
            var current = this.Head;

            while (current.Next != null)
            {
                current = current.Next;
            }
            current.Next = new MyKeyValuePair<TKey, TValue>(key, value);

            return true;
        }
        public bool Remove(TKey key)
        {
            if (this.Head == null)
            {
                return false;
            }

            var current = this.Head;

            if (current.Key.GetHashCode() == key.GetHashCode())
            {
                this.Head = current.Next;
                current.Next = null;
                return true;
            }

            while (current != null)
            {
                if (current.Next.Key.GetHashCode() == key.GetHashCode())
                {
                    current.Next = current.Next.Next;
                    return true;
                }
                current = current.Next;
            }
            return false;
        }
        public bool Contains(TKey key)
        {
            var current = this.Head;

            while (current != null)
            {
                if (current.Key.GetHashCode() == key.GetHashCode())
                {
                    return true;
                }
                current = current.Next;
            }
            return false;
        }
        public IEnumerator<MyKeyValuePair<TKey, TValue>> GetEnumerator()
        {
            MyKeyValuePair<TKey, TValue> current = this.Head;

            while (current != null)
            {
                yield return current;
                current = current.Next;
            }

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            MyKeyValuePair<TKey, TValue> current = this.Head;

            while (current != null)
            {
                yield return current;
                current = current.Next;
            }

        }
        public override string ToString()
        {
            StringBuilder toPrint = new StringBuilder();

            var current = this.Head;
            while (current != null)
            {
                toPrint.Append(current + Environment.NewLine);
                current = current.Next;
            }
            return toPrint.ToString();
        }
    }
}
