﻿using System;

namespace DictionaryWorkshop.Implementation
{
    public class MyKeyValuePair<TKey, TValue>
    {
        public TKey Key { get; private set; }

        public TValue Value { get; private set; }

        public MyKeyValuePair<TKey, TValue> Next { get; set; }

        public MyKeyValuePair(TKey key, TValue value)
        {
            this.Key = key;
            this.Value = value;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var other = obj as MyKeyValuePair<TKey, TValue>;
            if (other == null)
                return false;

            return this.Key.Equals(other.Key) && this.Value.Equals(other.Value);
        }

        public void SetValue(TValue newValue)
        {
            this.Value = newValue;
        }
        public override int GetHashCode()
        {
            int keyHashCode = this.Key.GetHashCode();
            int valueHashCode = this.Value.GetHashCode();
            return keyHashCode ^ valueHashCode;
        }

        public override string ToString()
        {
            return $"{this.Key} ({this.Value})";
        }
    }
}
