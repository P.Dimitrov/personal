﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DictionaryWorkshop.Implementation.Contracts;

namespace DictionaryWorkshop.Implementation
{
    public class MyDictionary<TKey, TValue> : IMyDictionary<TKey, TValue>, IEnumerable<MyKeyValuePair<TKey, TValue>>
    {
        private MyLinkedList<TKey, TValue>[] buckets;

        public MyDictionary()
        {
            buckets = new MyLinkedList<TKey, TValue>[100];
            this.Capacity = 100;
        }

        public MyDictionary(int capacity)
        {
            buckets = new MyLinkedList<TKey, TValue>[capacity];
            this.Capacity = capacity;
        }

        public int Capacity { get; set; }

        public int Count { get; set; }
        public TValue this[TKey key]
        {
            get
            {
                int index = Math.Abs(key.GetHashCode() % this.buckets.Length);
                if (this.buckets[index] != null)
                {
                    foreach (var item in this.buckets[index])
                    {
                        if (item.Key.GetHashCode() == key.GetHashCode())
                        {
                            return item.Value;
                        }
                    }
                }
                throw new Exception("Item with this key does not exist");
            }
            set
            {
                int index = Math.Abs(key.GetHashCode() % this.buckets.Length);
                if (this.buckets[index] == null || !this.ContainsKey(key))
                {
                    this.Add(key, value);
                }
                else if (this.buckets[index] != null)
                {
                    foreach (var item in this.buckets[index])
                    {
                        if (item.Key.GetHashCode() == key.GetHashCode())
                        {
                            item.SetValue(value);
                        }
                    }
                }
            }
        }

        public bool Add(TKey key, TValue value)
        {
            if (this.Count >= buckets.Length / 2)
            {
                MyLinkedList<TKey, TValue>[] temp = new MyLinkedList<TKey, TValue>[this.Capacity * 2];
                
                foreach (var linkedList in this.buckets)
                {
                    if (linkedList != null)
                    {
                        foreach (var keyValuePair in linkedList)
                        {
                            int newIndex = Math.Abs(keyValuePair.Key.GetHashCode() % temp.Length);
                            if (temp[newIndex] == null)
                            {
                                temp[newIndex] = new MyLinkedList<TKey, TValue>(keyValuePair.Key, keyValuePair.Value);
                                continue;
                            }
                            temp[newIndex].Add(keyValuePair.Key, keyValuePair.Value);
                        }
                    }
                }
                this.buckets = temp;
                this.Capacity = temp.Length;
            }
            int index = Math.Abs(key.GetHashCode() % this.buckets.Length);
            if (this.buckets[index] == null)
            {
                this.buckets[index] = new MyLinkedList<TKey, TValue>(key, value);
                Count++;
                return true;
            }
            if (this.buckets[index].Add(key, value))
            {
                Count++;
                return true;
            }
            return false;
        }
        public bool ContainsKey(TKey key)
        {
            int index = Math.Abs(key.GetHashCode() % this.buckets.Length);

            if (this.buckets[index] != null && this.buckets[index].Contains(key))
            {
                return true;
            }
            return false;
        }

        public IEnumerable<TKey> GetKeys()
        {
            List<TKey> keys = new List<TKey>();
            foreach (var linkedList in buckets)
            {
                if (linkedList != null)
                {
                    foreach (var keyValuePair in linkedList)
                    {
                        keys.Add(keyValuePair.Key);
                    }
                }
            }
            return keys;
        }

        public IEnumerable<TValue> GetValues()
        {
            List<TValue> values = new List<TValue>();
            foreach (var linkedList in buckets)
            {
                if (linkedList != null)
                {
                    foreach (var keyValuePair in linkedList)
                    {
                        values.Add(keyValuePair.Value);
                    }
                }
            }
            return values;
        }

        public bool Remove(TKey key)
        {
            int index = Math.Abs(key.GetHashCode() % this.buckets.Length);

            if (this.buckets[index] != null && this.buckets[index].Contains(key))
            {
                this.buckets[index].Remove(key);
                Count--;
                return true;
            }
            return false;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            int index = Math.Abs(key.GetHashCode() % this.buckets.Length);

            foreach (var keyValuePair in this.buckets[index])
            {
                if (keyValuePair.Key.GetHashCode() == key.GetHashCode())
                {
                    value = keyValuePair.Value;
                    return true;
                }
            }
            value = default;
            return false;
        }

        public IEnumerator<MyKeyValuePair<TKey, TValue>> GetEnumerator()
        {
            foreach (var linkedList in buckets)
            {
                if (linkedList != null)
                {
                    MyKeyValuePair<TKey, TValue> current = linkedList.Head;

                    while (current != null)
                    {
                        yield return current;
                        current = current.Next;
                    }
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (var linkedList in buckets)
            {
                if (linkedList != null)
                {
                    MyKeyValuePair<TKey, TValue> current = linkedList.Head;

                    while (current != null)
                    {
                        yield return current;
                        current = current.Next;
                    }
                }
            }
        }
        public override string ToString()
        {
            StringBuilder toPrint = new StringBuilder();
            foreach (var linkedList in this.buckets)
            {
                if (linkedList != null)
                {
                    toPrint.Append(linkedList);
                }
            }
            return toPrint.ToString().Trim();
        }
    }
}
