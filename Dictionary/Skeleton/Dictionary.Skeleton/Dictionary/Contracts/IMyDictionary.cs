﻿using System.Collections.Generic;

namespace DictionaryWorkshop.Implementation.Contracts
{
    //
    // Summary:
    //  Represents a generic collection of key/value pairs.
    //
    // Type parameters:
    //  TKey: 
    //     The type of keys in the dictionary.
    //
    //  TValue:
    //     The type of values in the dictionary.
    public interface IMyDictionary<TKey, TValue>
    {
        //
        // Summary:
        //  Adds an element with the provided key and value to the dictionary
        //
        // Parameters:
        //  key:
        //   The object to use as the key of the element to add.
        //  value:
        //   The object to use as the value of the element to add.
        //
        // Returns:
        //  true if the element is successfully added.
        //  false if an element with the same key exists.
        bool Add(TKey key, TValue value);

        //
        // Summary:
        //  Removes the element with the specified key from the dictionary.
        //
        // Parameters:
        //  key:
        //  The key of the element to remove.
        //
        // Returns:
        //  true if the element is successfully removed; otherwise, false. 
        //  This method also returns false if key was not found in the dictionary.
        //
        // Exceptions:
        //  throws an ArgumentNullException if key is null.
        bool Remove(TKey key);

        //
        // Summary:
        //  Gets or sets the element with the specified key.
        //
        // Parameters:
        //  key: 
        //   The key of the element to retrieve.
        //
        // Returns:
        //  The element with the specified key.
        //
        // Exceptions:
        //  throws an ArgumentNullException when key is null.
        //  throws a KeyNotFoundException when key is not found.
        TValue this[TKey key]
        {
            get;
            set;
        }

        //
        // Summary:
        //  Determines whether the dictionary contains an element with the specified key.
        //
        // Parameters:
        //  key:
        //   The key to locate in the dictionary.
        //
        // Returns:
        //  true if the dictionary contains an element with the key; otherwise, false.
        //
        // Exceptions:
        //  throws an ArgumentNullException if key is null.
        bool ContainsKey(TKey key);

        //
        // Summary:
        //  Returns a collection containing all the keys in the dictionary.
        IEnumerable<TKey> GetKeys();

        //
        // Summary:
        //  Returns a collection containing all the values in the dictionary
        IEnumerable<TValue> GetValues();

        //
        // Summary:
        //  Gets the value associated with the specified key.
        //
        // Parameters:
        //  key:
        //      The key whose value to get.
        //
        //  value:
        //      When this method returns, the value associated with the specified key, if the key is found;
        //      otherwise, the default value for the type of the value parameter.
        //      This parameter is passed uninitialized.
        //
        // Returns:
        //  true if the dictionary contains an element with the specified key; 
        //  otherwise, false.
        //
        // Exceptions:
        //  throws an ArgumentNullException of key is null.
        bool TryGetValue(TKey key, out TValue value);
    }
}
