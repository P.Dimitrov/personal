﻿using System;

namespace DictionaryWorkshop.Models
{
    public class Person
    {
        public string Name { get; private set; }
        public DateTime DateOfBirth { get; set; }

        public Person(string name, DateTime dateOfBirth)
        {
            this.Name = name;
            this.DateOfBirth = dateOfBirth;
        }

        public override bool Equals(object obj)
        {
            var other = obj as Person;
            if (other == null)
                return false;

            bool namesMatch = this.Name.Equals(other.Name);
            bool dobMatch = this.DateOfBirth.Equals(other.DateOfBirth);

            return namesMatch && dobMatch;
        }

        public override int GetHashCode()
        {
            int nameHashCode = this.Name.GetHashCode();
            int dobHashCode = this.DateOfBirth.GetHashCode();
            
            return nameHashCode ^ dobHashCode;
        }

        public override string ToString()
        {
            return $"{this.Name} ({this.DateOfBirth.ToShortDateString()})";
        }
    }
}
