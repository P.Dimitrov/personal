﻿using System;

namespace DictionaryWorkshop.Models
{
    public class ContactInfo
    {
        private DateTime createdOn;
        public ContactInfo(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
                throw new ArgumentNullException("Phone number cannot be null or empty!");
            
            this.createdOn = DateTime.Now;
            this.PhoneNumber = phoneNumber;
        }
        public string PhoneNumber { get; private set; }

        public override bool Equals(object obj)
        {
            var other = (ContactInfo)obj;
            bool phoneNumbersMatch = this.PhoneNumber.Equals(other.PhoneNumber);
            bool creationDatesMatch = this.createdOn.Equals(other.createdOn);
            return phoneNumbersMatch && creationDatesMatch;
        }

        public override int GetHashCode()
        {
            int phoneHashCode = this.PhoneNumber.GetHashCode();
            int creationDateHashCode = this.createdOn.GetHashCode();
            return phoneHashCode ^ creationDateHashCode;
        }

        public override string ToString()
        {
            return this.PhoneNumber;
        }
    }
}
