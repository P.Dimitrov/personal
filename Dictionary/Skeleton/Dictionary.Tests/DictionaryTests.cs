using System;
using System.Collections.Generic;
using System.Linq;
using DictionaryWorkshop.Implementation;
using DictionaryWorkshop.Implementation.Contracts;
using DictionaryWorkshop.Models;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DictionaryWorkshop.Tests
{
    [TestClass]
    public class DictionaryTests
    {
        [TestMethod]
        public void SholdAdd_EmptyDictionary_NoDups_1()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>();

            var expected = new MyKeyValuePair<string, int>[]
            {
                new MyKeyValuePair<string, int>("three", 3),
                new MyKeyValuePair<string, int>("one", 1),
                new MyKeyValuePair<string, int>("four", 4),
            };

            // Act
            foreach (var item in expected)
            {
                myDict.Add(item.Key, item.Value);
            }

            // Assert
            var actual = myDict.ToArray();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [TestMethod]
        public void SholdAdd_EmptyDictionary_NoDups_2()
        {
            // Arrange
            var myDict = new MyDictionary<Person, ContactInfo>();

            var expected = new MyKeyValuePair<Person, ContactInfo>[]
            {
                new MyKeyValuePair<Person, ContactInfo>(new Person("Peter", DateTime.Now),new ContactInfo("0888-123-456")),
                new MyKeyValuePair<Person, ContactInfo>(new Person("Maria", DateTime.Now),new ContactInfo("0888-123-456")),
                new MyKeyValuePair<Person, ContactInfo>(new Person("Georgi", DateTime.Now),new ContactInfo("0888-123-456")),
                new MyKeyValuePair<Person, ContactInfo>(new Person("Kalina", DateTime.Now),new ContactInfo("0888-123-456")),
            };

            // Act
            foreach (var item in expected)
            {
                myDict.Add(item.Key, item.Value);
            }

            // Assert
            var actual = myDict.ToArray();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [TestMethod]
        public void ShouldAdd_LowCapacity()
        {
            // Arrange
            int totalItemsCount = 1000;
            int initialCapacity = 1;
            var myDict = new MyDictionary<string, string>(initialCapacity);
            var expected = new MyKeyValuePair<string, string>[totalItemsCount];

            // Act
            for (int i = 0; i < totalItemsCount; i++)
            {
                expected[i] = new MyKeyValuePair<string, string>($"k:{i}", $"v:{i}");
                myDict.Add($"k:{i}", $"v:{i}");
            }

            // Assert
            var actual = myDict.ToArray();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [TestMethod]
        public void AddShouldThrowException_WhenSameKeyAdded()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>();

            // Act
            myDict.Add("Pesho", 1);

            // Assert
            Assert.IsFalse(myDict.Add("Pesho", 2));
        }

        [TestMethod]
        public void AddShouldResizeCollection_WhenFilledByMoreThanHalf()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);

            // Act
            myDict.Add("Pesho", 1);
            myDict.Add("Gosho", 2);
            myDict.Add("Misho", 3);
            myDict.Add("Tosho", 4);

            // Assert
            Assert.AreEqual(8, myDict.Capacity);
        }

        [TestMethod]
        public void ShouldAddNewKey_WhenAddedByIndex()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);

            // Act
            myDict["Pesho"] = 1;
            var sut = new MyKeyValuePair<string, int>("Pesho", 1);

            // Assert
            Assert.IsTrue(myDict.Contains(sut));
        }

        [TestMethod]
        public void ShouldChangeValue_WhenItemAccessedByKey()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);

            // Act
            myDict["Pesho"] = 1;
            myDict["Pesho"] = 2;
            var sut = new MyKeyValuePair<string, int>("Pesho", 2);

            // Assert
            Assert.IsTrue(myDict.Contains(sut));
        }
        [TestMethod]
        public void ContainsKeyShouldReturnFalse_IfKeyDoesNotExist()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);


            // Assert
            Assert.IsFalse(myDict.ContainsKey("Pesho"));
        }

        [TestMethod]
        public void ContainsKeyShouldReturnTrue_IfKeyExists()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);

            // Act
            myDict["Pesho"] = 1;

            // Assert
            Assert.IsTrue(myDict.ContainsKey("Pesho"));
        }

        [TestMethod]
        public void ShouldReturnListOfKeys_IfKeysExists()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);
            List<string> keys = new List<string>();

            // Act
            myDict["Pesho"] = 1;
            myDict["Gosho"] = 2;
            myDict["Misho"] = 3;

            var keysFromDict = myDict.GetKeys().ToList();

            keys.Add("Pesho");
            keys.Add("Gosho");
            keys.Add("Misho");

            // Assert
            CollectionAssert.AreEquivalent(keys, keysFromDict);
        }

        [TestMethod]

        public void ShouldReturnEmptyList_IfNoKeysPresent()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);
            List<string> keys = new List<string>();

            // Act

            var keysFromDict = myDict.GetKeys().ToList();

            // Assert
            CollectionAssert.AreEquivalent(keys, keysFromDict);
        }
        [TestMethod]
        public void ShouldReturnListOfValues_IfValuesExists()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);
            List<int> keys = new List<int>();

            // Act
            myDict["Pesho"] = 1;
            myDict["Gosho"] = 2;
            myDict["Misho"] = 3;

            var keysFromDict = myDict.GetValues().ToList();

            keys.Add(1);
            keys.Add(2);
            keys.Add(3);

            // Assert
            CollectionAssert.AreEquivalent(keys, keysFromDict);
        }

        [TestMethod]
        public void ShouldReturnEmptyList_IfNoValuesPresent()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);
            List<string> keys = new List<string>();

            // Act

            var keysFromDict = myDict.GetValues().ToList();

            // Assert
            CollectionAssert.AreEquivalent(keys, keysFromDict);
        }

        [TestMethod]
        public void ShouldNotContainItem_IfItemIsRemoved()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);
            var sut = new MyKeyValuePair<string, int>("Pesho", 2);
            myDict.Add("Pesho",2);

            // Act
            myDict.Remove("Pesho");

            // Assert
            Assert.IsFalse(myDict.Contains(sut));
        }

        [TestMethod]
        public void RemoveShouldReturnFalse_IfItemDoesNotExist()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);

            // Act & Assert
            Assert.IsFalse(myDict.Remove("Pesho"));
        }

        [TestMethod]
        public void TryGetValueShouldReturnTrue_IfItemExists()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);
            var sut = new MyKeyValuePair<string, int>("Pesho", 2);

            // Act
            myDict.Add("Pesho", 2);

            // Assert
            Assert.IsTrue(myDict.TryGetValue("Pesho", out int value));
        }

        [TestMethod]
        public void TryGetValueShouldReturnFalse_IfItemDoesNotExist()
        {
            // Arrange
            var myDict = new MyDictionary<string, int>(4);

            // Act & Assert
            Assert.IsFalse(myDict.Remove("Pesho"));
        }
    }
}
