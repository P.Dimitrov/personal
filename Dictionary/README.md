# Dictionary (Abstract Data Type)

The most common objective of computer programs is to store and retrieve data. Much of the effort goes towards finding efficient ways to organize collections of data records so that they can be stored and retrieved quickly. 

The dictionary ADT describes such a collection that provides operations for storing records, finding records, and removing records. This ADT gives us a standard basis for comparing various data structures. Loosly speaking, we can say that any data structure that supports insert, search, and deletion is a "dictionary".

Dictionaries depend on the concepts of a **search key** and **comparable objects**. 

Here's a part of the interface that defines the dictionary as an ADT:

```cs
public interface IDictionary<TKey, TValue>
{
    // Summary:
    //  Adds an element with the provided key and value to the dictionary
    void Add(TKey key, TValue value);

    // Summary:
    //  Gets or sets the element with the specified key.
    //
    // Parameters:
    //  key: 
    //   The key of the element to retrieve.
    //
    // Returns:
    //  The element with the specified key.
    TValue this[TKey key]
    {
        get;
        set;
    }

    // Summary:
    //  Removes the element with the specified key from the dictionary.
    //
    // Returns:
    //  true if the element is successfully removed; otherwise, false. 
    //  This method also returns false if key was not found in the dictionary.
    bool Remove(TKey key);
}
```

The method `Add(TKey key, TValue value)` and the property indexer `TValue this[TKey key]` are the heart of the structure. If there are multiple records in the dictionary whose values match the key, then there is no particular requirement as to which one is returned.

### Characteristics of Dictionaries:

Name       | Functionality
-----------|----------
 `void Add(TKey key, TValue value)` | Adds an element with the provided key and value to the dictionary.
 `TValue this[TKey key] { get; set; }` | Gets or sets the element with the specified key.
 `bool Remove(TKey key)` | Removes the element with the specified key from the dictionary. This method also returns false if key was not found in the dictionary.

### Asymptotic Complexity

Same as `HashSet`

#### Your Task

1. Implement all methods from the `IMyDictionary<T>` interface.

#### Additional Practice

`TODO`
