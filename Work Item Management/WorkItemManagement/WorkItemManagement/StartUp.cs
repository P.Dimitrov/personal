﻿using WorkItemManagement.Core;

namespace WorkItemManagement
{
    public static class StartUp
    {
        public static void Main() => Engine.Instance.Run();
    }
}
