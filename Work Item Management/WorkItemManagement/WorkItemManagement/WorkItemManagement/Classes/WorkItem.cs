﻿using System;
using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.WorkItemManagement.Classes
{
    public class WorkItem : IWorkItem
    {
        private static long id = 1;
        private string title;
        private string description;
        private readonly List<IComment> comments;
        private readonly List<string> history;
        
        public WorkItem(string title, string description)
        {
            Id = id++;
            this.Title = title;
            this.Description = description;
            this.comments = new List<IComment>();
            this.history = new List<string>();            
        }
        public string Title
        {
            get
            {
                return this.title;
            }
            protected set
            {
                if (value.Length < 10 || value.Length > 50)
                {
                    throw new ArgumentOutOfRangeException("Title must be between 10 and 50 symbols");
                }
                this.title = value;
            }
        }
        public string Description
        {
            get
            {
                return this.description;
            }
            protected set
            {
                if (value.Length < 10 || value.Length > 500)
                {
                    throw new ArgumentOutOfRangeException("Description must be between 10 and 500 symbols");
                }
                this.description = value;
            }
        }
        public long Id { get; set; }

        public List<string> History => this.history;

        public List<IComment> Comments => this.comments;

        /// <summary>
        /// A method that adds comment for this workitem.
        /// </summary>
        /// <param name="comment"></param>
        public void CommentAdd(IComment comment)
        {
            this.Comments.Add(comment);
        }

        /// <summary>
        /// A method that adds string commnet to the history of this workitem.
        /// </summary>
        /// <param name="history"></param>
        public void HistoryAdd(string history)
        {
            this.History.Add(history);
        }

        /// <summary>
        /// A virtual method for assigning member to a specific workitem.
        /// </summary>
        /// <param name="member"></param>
        public virtual void Assign(IMember member) { }

        /// <summary>
        /// A virtual method that unassignes member from a specific workitem.
        /// </summary>
        public virtual void Unassign() { }

        /// <summary>
        /// A method that prints the ID and the name of the workitem.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"ID: {WorkItem.id} {this.Title}";
        }
        
    }
}
