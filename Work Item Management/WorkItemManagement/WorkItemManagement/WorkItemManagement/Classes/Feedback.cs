﻿using System;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.WorkItemManagement.Classes
{
    public class Feedback : WorkItem, IFeedback
    {
        public Feedback(string title, string description, int rating, FeedbackStatus feedbackStatus) : base(title, description)
        {
            this.Rating = rating;
            this.Status = feedbackStatus;
        }
        public int Rating { get; set; }
        public FeedbackStatus Status { get; set; }

        /// <summary>
        /// A method that prints the full information about this feedback.
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"Feedback - ID:{this.Id} {this.Title}" + Environment.NewLine +
                $"Description - {this.Description}" + Environment.NewLine +
                $"Rating - {this.Rating}" + Environment.NewLine +
                $"Feedback Status - {this.Status}";
        }
    }
}
