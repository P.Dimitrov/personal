﻿using System;
using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.WorkItemManagement.Classes
{
    public class Team : ITeam
    {
        private string name;
        private IList<IMember> teamMembers = new List<IMember>();
        private IList<IBoard> teamBoards = new List<IBoard>();
        private readonly IList<string> teamActivityList = new List<string>();
        

        public Team(string name)
        {
            this.Name = name;
            this.TeamMembers = teamMembers;
            this.TeamBoards = teamBoards;
            this.TeamActivityList = teamActivityList;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            protected set
            {
                if (value.Length < 5 || value.Length > 10)
                {
                    throw new ArgumentOutOfRangeException("Team name must have between 5 and 10 symbols.");
                }
                this.name = value;
            }
        }

        public IList<IMember> TeamMembers { get; }

        public IList<IBoard> TeamBoards { get; }

        public IList<string> TeamActivityList { get; protected set; }

        /// <summary>
        /// A method that prints the name of the team.
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"{this.Name}";
        }
    }
}
