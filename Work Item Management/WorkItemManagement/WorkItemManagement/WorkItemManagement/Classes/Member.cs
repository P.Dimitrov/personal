﻿using System;
using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.WorkItemManagement.Classes
{
    public class Member : IMember
    {
        private static readonly IList<string> names = new List<string>();
        private readonly IList<IWorkItem> memberAssignedWorkItems = new List<IWorkItem>();
        private readonly IList<string> memberActivityList = new List<string>();
        private string name;

        public Member(string name)
        {
            this.Name = name;
            this.MemberActivityList = memberActivityList;
            this.MemberAssignedWorkItems = memberAssignedWorkItems;
        }
        public string Name
        {
            get
            {
                return this.name;
            }
           protected set
            {
                if (value.Length < 5 || value.Length > 15)
                {
                    throw new ArgumentOutOfRangeException("Member name must be between 5 and 15 symbols");
                }
                foreach (var name in names)
                {
                    if (name == value)
                    {
                        throw new ArgumentException("Member with this name already exists");
                    }
                }
                names.Add(value);
                this.name = value;
            }
        }

        public IList<IWorkItem> MemberAssignedWorkItems { get; protected set; }

        public IList<string> MemberActivityList { get; protected set; }

        /// <summary>
        /// A method that prints the name of the member.
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"{this.Name}";
        }

    }
}
