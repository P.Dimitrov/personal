﻿using System;
using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.WorkItemManagement.Classes
{
    public class Board : IBoard
    {
        private string name;
        private readonly IList<IWorkItem> boardWorkItems;
        private readonly IList<string> boardActivityList = new List<string>();
        public Board(string name)
        {
            this.Name = name;
            this.boardWorkItems = new List<IWorkItem>();
            this.BoardActivityList = boardActivityList;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            protected set
            {
                if (value.Length < 5 || value.Length > 10)
                {
                    throw new ArgumentOutOfRangeException("Board name must have between 5 and 10 symbols.");
                }
                this.name = value;
            }
        }
        public IList<IWorkItem> BoardWorkItems 
        { 
            get => this.boardWorkItems; 
            protected set
            {

            }
        }
        public IList<string> BoardActivityList { get; protected set; }

        /// <summary>
        /// A method that prints the name of the board.
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"{this.Name}";
        }

        /// <summary>
        /// A method that adds workitems to this specific board.
        /// </summary>
        /// <param name="boardWorkItem"></param>
        public void Add(IWorkItem boardWorkItem)
        {
            boardWorkItems.Add(boardWorkItem);
        }
    }
}
