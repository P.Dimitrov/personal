﻿using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.WorkItemManagement.Classes
{
    public class Comment : IComment
    {
        public Comment(IMember commenter, string comment)
        {
            this.Commenter = commenter;
            this.CommentText = comment;
        }
        public IMember Commenter { get; set; }

        public string CommentText { get; set; }
    }
}
