﻿using System;
using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.WorkItemManagement.Classes
{
    public class Bug : WorkItem, IBug, IAssignable
    {
        public Bug(string title, string description, List<string> steps, Priority priorityLevel, Severity severityLevel,
            BugStatus statusType)
            : base(title, description)
        {
            this.Steps = steps;
            this.PriorityLevel = priorityLevel;
            this.SeverityLevel = severityLevel;
            this.Status = statusType;
        }
        public List<string> Steps { get; set; }
        public Priority PriorityLevel { get; set; }
        public Severity SeverityLevel { get; set; }
        public BugStatus Status { get; set; }
        public IMember Assignee { get; set; }

        /// <summary>
        /// A method that assigns a specific member to this bug.
        /// Adds a string comment to this specific member's activity.
        /// Adds a string commnet to this specific bug's history.
        /// </summary>
        /// <param name="member"></param>
        public override void Assign(IMember member)
        {
            this.Assignee = member;
            member.MemberActivityList.Add($"Bug {this.Id}{this.Title} assigned");
            this.HistoryAdd($"Assigned to {member.Name}");
        }

        /// <summary>
        /// A method that removes this specific bug from the member's activity list.
        /// Adds a string commnet to this bug's history.
        /// </summary>
        public override void Unassign()
        {
            this.Assignee.MemberActivityList.Add($"Bug {this.Id}{this.Title} unassigned");
            this.HistoryAdd($"Unassigned from {this.Assignee.Name}");
            this.Assignee = default;
        }

        /// <summary>
        /// A method that prints the full information of the bug.
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"Bug - ID:{this.Id} {this.Title}" + Environment.NewLine +
                $"Description - {this.Description}" + Environment.NewLine +
                $"Steps - {string.Join(", ", this.Steps)}" + Environment.NewLine +
                $"Priority - {this.PriorityLevel}" + Environment.NewLine +
                $"Severity - {this.SeverityLevel}" + Environment.NewLine +
                $"BugStatus - {this.Status}";
        }
    }
}
