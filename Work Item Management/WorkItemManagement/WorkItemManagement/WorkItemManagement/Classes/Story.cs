﻿using System;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.WorkItemManagement.Classes
{
    public class Story : WorkItem, IStory, IAssignable
    {
        public Story(string title, string description, Priority priority, Size size, StoryStatus storyStatus) : base(title,description)
        {
            this.Priority = priority;
            this.Size = size;
            this.Status = storyStatus;
        }

        public Priority Priority { get; set; }
        public Size Size { get; set; }
        public StoryStatus Status { get; set; }
        public IMember Assignee { get; set; }

        /// <summary>
        /// A method that assigns a specific member to this story.
        /// Adds a string comment to this specific member's activity.
        /// Adds a string commnet to this specific story's history.
        /// </summary>
        /// <param name="member"></param>
        public override void Assign(IMember member)
        {
            this.Assignee = member;
            member.MemberActivityList.Add($"Story {this.Id}{this.Title} assigned");
            this.HistoryAdd($"Assigned to {member.Name}");
        }

        /// <summary>
        /// A method that removes this specific bug from the member's activity list.
        /// Adds a string commnet to this bug's history.
        /// </summary>
        public override void Unassign()
        {
            this.HistoryAdd($"Unassigned from {this.Assignee.Name}");
            this.Assignee.MemberActivityList.Add($"Story {this.Id}{this.Title} unassigned");
            this.Assignee = default;
        }

        /// <summary>
        /// A method that prints the full information of the story.
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"Story - ID:{this.Id} {this.Title}" + Environment.NewLine +
                $"Description - {this.Description}" + Environment.NewLine +
                $"Priority - {this.Priority}" + Environment.NewLine +
                $"Size - {this.Size}" + Environment.NewLine +
                $"Story Status - {this.Status}";
        }
    }
}
