﻿namespace WorkItemManagement.WorkItemManagement.Enums
{
    public enum BugStatus
    {
        Active,
        Fixed
    }
}
