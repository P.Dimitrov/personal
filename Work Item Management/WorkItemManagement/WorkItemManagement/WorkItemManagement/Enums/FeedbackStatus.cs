﻿namespace WorkItemManagement.WorkItemManagement.Enums
{
    public enum FeedbackStatus
    {
        New,
        Scheduled,
        Unscheduled,
        Done
    }
}
