﻿namespace WorkItemManagement.WorkItemManagement.Enums
{
    public enum StoryStatus
    {
        NotDone,
        InProgress,
        Done
    }
}
