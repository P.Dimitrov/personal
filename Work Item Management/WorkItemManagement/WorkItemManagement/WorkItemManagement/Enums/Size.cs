﻿namespace WorkItemManagement.WorkItemManagement.Enums
{
    public enum Size
    {
        Large,
        Medium,
        Small
    }
}
