﻿namespace WorkItemManagement.WorkItemManagement.Enums
{
    public enum Severity
    {
        Critical,
        Major,
        Minor
    }
}
