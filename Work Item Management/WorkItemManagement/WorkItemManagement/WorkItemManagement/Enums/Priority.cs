﻿namespace WorkItemManagement.WorkItemManagement.Enums
{
    public enum Priority
    {
        High,
        Medium,
        Low
    }
}
