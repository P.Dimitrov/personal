﻿namespace WorkItemManagement.WorkItemManagement.Contracts
{
    public interface IWorkItem : IAssignable
    {
        long Id { get; }
        string Title { get;  }
        string Description { get; }
        void CommentAdd(IComment comment);
        void HistoryAdd(string history);
        string ToString();
    }
}
