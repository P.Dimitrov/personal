﻿using System.Collections.Generic;

namespace WorkItemManagement.WorkItemManagement.Contracts
{
    public interface IMember
    {
        string Name { get; }
        IList<IWorkItem> MemberAssignedWorkItems { get; }
        IList<string> MemberActivityList { get; }
        string ToString();
    }
}
