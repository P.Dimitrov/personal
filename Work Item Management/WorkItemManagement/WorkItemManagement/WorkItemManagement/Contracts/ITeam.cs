﻿using System.Collections.Generic;

namespace WorkItemManagement.WorkItemManagement.Contracts
{
    public interface ITeam
    {
        string Name { get; }

        IList<IMember> TeamMembers { get; }

        IList<IBoard> TeamBoards { get; }

        IList<string> TeamActivityList { get; }

        string ToString();
     }
       
}
