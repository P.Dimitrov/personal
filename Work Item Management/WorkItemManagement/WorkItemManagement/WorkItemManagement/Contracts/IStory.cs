﻿using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.WorkItemManagement.Contracts
{
    public interface IStory : IWorkItem, IAssignable
    {
        Priority Priority { get; set; }
        Size Size { get; set; }
        StoryStatus Status { get; set; }
        IMember Assignee { get; }
    }
}
