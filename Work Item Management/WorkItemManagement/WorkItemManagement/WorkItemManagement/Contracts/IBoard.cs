﻿using System.Collections.Generic;

namespace WorkItemManagement.WorkItemManagement.Contracts
{
    public interface IBoard
    {
        string Name { get;  }
        IList<IWorkItem> BoardWorkItems { get; }
        IList<string> BoardActivityList { get; }
        string ToString();
        void Add(IWorkItem boardWorkItem);
    }
}
