﻿namespace WorkItemManagement.WorkItemManagement.Contracts
{
    public interface IComment
    {
        IMember Commenter { get; }
        string CommentText { get; }
    }
}
