﻿namespace WorkItemManagement.WorkItemManagement.Contracts
{
    public interface IAssignable
    {
        void Assign(IMember member);
        void Unassign();
    }
}
