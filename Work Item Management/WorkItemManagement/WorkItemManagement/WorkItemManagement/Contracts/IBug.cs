﻿using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.WorkItemManagement.Contracts
{
    public interface IBug : IWorkItem, IAssignable
    {
        List<string> Steps { get; }
        Priority PriorityLevel { get; set; }
        Severity SeverityLevel { get; set; }
        BugStatus Status { get; set; }
        IMember Assignee { get; }
    }
}
