﻿using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.WorkItemManagement.Contracts
{
    public interface IFeedback : IWorkItem
    {
        int Rating { get; set; }
        FeedbackStatus Status { get; set; }
    }
}
