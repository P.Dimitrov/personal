﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.Core.Commands.Creating
{
    public class ChangeFeedbackCommand : Command
    {
        public ChangeFeedbackCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {

        }

        /// <summary>
        /// 
        /// This method finds a Feedback with given ID and changes its Property /Rating, Status/.
        /// 1/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse the program throws ArgumentException message.
        /// 2/ With "FirstOrDefault" LINQ query we search the list of feedbacks if there is a feedback with the given ID.
        /// If there is no feedback with this ID an ArgumentNullException is thrown.
        /// 3/ The second input is checked with Try-Parse statement if it is any of the predefined properties'states.
        /// If none of the predefined properties is parsed successfully then an ArgumentNullException is thrown.
        /// When there is a successful Enum-Parse then the program uses if-else statements to change the property
        /// with it's new state.
        /// 4/ In the Feedback's history the program adds comment about the change.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string parameterChanged;
            string newValue;
            long iD;
            string Input;
            try
            {
                iD = long.Parse(CommandParameters[0]);
                Input = CommandParameters[1].ToLower();
            }
            catch
            {
                throw new ArgumentException("Failed to parse ChangeFeedback command parameters.");
            }

            var currentFeedback = Lists.Feedback.FirstOrDefault(x => x.Id == iD);
            if (currentFeedback == null)
            {
                throw new ArgumentNullException("There is no Feedback with this ID.");
            }
            if (int.TryParse(Input, out int newRating))
            {
                currentFeedback.Rating = newRating;

                parameterChanged = "Rating";
                newValue = Input;
            }
            else if (Input != null)
            {
                Enum.TryParse(typeof(FeedbackStatus), Input, ignoreCase: true, out var parsedEnumStatus);
                currentFeedback.Status = (FeedbackStatus)parsedEnumStatus;

                parameterChanged = "Status";
                newValue = parsedEnumStatus.ToString();
            }
            else
            {
                throw new ArgumentNullException("Invalid change!");
            }
            currentFeedback.HistoryAdd($"{currentFeedback.Title} was changed.");
            return $"Changed Feedback ID: {currentFeedback.Id} Title: {currentFeedback.Title}'s {parameterChanged} to {newValue}";
        }
    }
}
