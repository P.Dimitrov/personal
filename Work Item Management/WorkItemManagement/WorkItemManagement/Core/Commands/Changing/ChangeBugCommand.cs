﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.Core.Commands.Creating
{
    public class ChangeBugCommand : Command
    {
        public ChangeBugCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {

        }

        /// <summary>
        /// 
        /// This method finds a Bug with given ID and changes its Property /Priority, Severity, Status/.
        /// 1/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse the program throws ArgumentException message.
        /// 2/ With "FirstOrDefault" LINQ query we search the list of bugs if there is a bug with the given ID.
        /// If there is no bug with this ID an ArgumentNullException is thrown.
        /// 3/ The second input is checked with Try-Parse statement if it is any of the predefined properties'states.
        /// If none of the predefined properties is parsed successfully then an ArgumentNullException is thrown.
        /// When there is a successful Enum-Parse then the program uses if-else statements to change the property
        /// with it's new state.
        /// 4/ In the Bug's history the program adds comment about the change.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            long iD;
            string enumInput;
            try
            {
                iD = long.Parse(CommandParameters[0]);
                enumInput = CommandParameters[1].ToLower().ToString();
            }
            catch
            {
                throw new ArgumentException("Failed to parse ChangeBug command parameters.");
            }

            var currentBug = Lists.Bugs.FirstOrDefault(x => x.Id == iD);
            if (currentBug == null)
            {
                throw new ArgumentNullException("There is no bug with this ID.");
            }
            Enum.TryParse(typeof(Priority), enumInput, ignoreCase: true, out var parsedEnumPriority);
            Enum.TryParse(typeof(Severity), enumInput, ignoreCase: true, out var parsedEnumSeverity);
            Enum.TryParse(typeof(BugStatus), enumInput, ignoreCase: true, out var parsedEnumStatus);
            string changedParameter;
            if (parsedEnumPriority != null)
            {
                currentBug.PriorityLevel = (Priority)parsedEnumPriority;
                changedParameter = "Priority";
            }
            else if (parsedEnumSeverity != null)
            {
                currentBug.SeverityLevel = (Severity)parsedEnumSeverity;
                changedParameter = "Severity";
            }
            else if (parsedEnumStatus != null)
            {
                currentBug.Status = (BugStatus)parsedEnumStatus;
                changedParameter = "Status";
            }
            else
            {
                throw new ArgumentNullException("Invalid change!");
            }
            currentBug.HistoryAdd($"{currentBug.Title} was changed.");
            return $"Changed Bug ID: {currentBug.Id} Title: {currentBug.Title}'s {changedParameter} to {enumInput}";
        }
         
    }
}
