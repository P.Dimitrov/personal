﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.Core.Commands.Creating
{
    public class ChangeStoryCommand : Command
    {
        public ChangeStoryCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {

        }

        /// <summary>
        /// 
        /// This method finds a Story with given ID and changes its Property /Priority, Size, Status/.
        /// 1/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse the program throws ArgumentException message.
        /// 2/ With "FirstOrDefault" LINQ query we search the list of stories if there is a story with the given ID.
        /// If there is no story with this ID an ArgumentNullException is thrown.
        /// 3/ The second input is checked with Try-Parse statement if it is any of the predefined properties'states.
        /// If none of the predefined properties is parsed successfully then an ArgumentNullException is thrown.
        /// When there is a successful Enum-Parse then the program uses if-else statements to change the property
        /// with it's new state.
        /// 4/ In the Story's history the program adds comment about the change.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            long iD;
            string enumInput;
            try
            {
                iD = long.Parse(CommandParameters[0]);
                enumInput = CommandParameters[1].ToLower().ToString();
            }
            catch
            {
                throw new ArgumentException("Failed to parse ChangeStory command parameters.");
            }

            var currentStory = Lists.Story.FirstOrDefault(x => x.Id == iD);
            if (currentStory == null)
            {
                throw new ArgumentNullException("There is no Story with this ID.");
            }
            Enum.TryParse(typeof(Priority), enumInput, ignoreCase: true, out var parsedEnumPriority);
            Enum.TryParse(typeof(Size), enumInput, ignoreCase: true, out var parsedEnumSize);
            Enum.TryParse(typeof(StoryStatus), enumInput, ignoreCase: true, out var parsedEnumStatus);
            string changedParameter;
            if (parsedEnumPriority != null)
            {
                currentStory.Priority = (Priority)parsedEnumPriority;
                changedParameter = "Priority";
            }
            else if (parsedEnumSize != null)
            {
                currentStory.Size = (Size)parsedEnumSize;
                changedParameter = "Size";
            }
            else if (parsedEnumStatus != null)
            {
                currentStory.Status = (StoryStatus)parsedEnumStatus;
                changedParameter = "Status";
            }
            else
            {
                throw new ArgumentNullException("Invalid change!");
            }
            currentStory.HistoryAdd($"{currentStory.Title} was changed.");
            return $"Changed Story ID: {currentStory.Id} Title: {currentStory.Title}'s {changedParameter} to {enumInput}";
        }
    }
}
