﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.Core.Commands.Creating
{
    public class CreateStoryCommand : Command
    {
        public CreateStoryCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {

        }

        /// <summary>
        /// This method creates a Story with given parameters - title, description, priority, size, 
        /// statusType and is directly created to a specific board with given name, at a specific team.
        /// 1/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 2/ With "FirstOrDefault" LINQ query the program searches the list of teams if there is a team with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 3/ With "FirstOrDefault" LINQ query the program searches the list of boards if there is a board with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 4/ The new Story is created via WorkItem Factory.
        /// 5/ The Story is added to the curent board's list of workitems.
        /// 6/ A string comment is added to the current board's list of activities.
        /// 7/ The current Story is added to the list of stories.
        /// 8/ A comment is added to the current story's history.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string title;
            string description;
            Priority priority;
            Size size;
            StoryStatus storyStatus;
            string team;
            string board;
            try
            {
                title = CommandParameters[0];
                description = CommandParameters[1];
                priority = GetPriority(CommandParameters[2]);
                size = GetSize(CommandParameters[3]);
                storyStatus = GetStatus(CommandParameters[4]);
                team = CommandParameters[5];
                board = CommandParameters[6];
            }
            catch
            {
                throw new ArgumentException("Failed to parse CreateStory command parameters.");
            }

            var currentTeam = Lists.Teams.FirstOrDefault(x => x.Name.ToLower() == team.ToLower());
            if (currentTeam == null)
            {
                throw new ArgumentNullException($"Team with team name {team} does not exist");
            }
            var currentTeamBoard = currentTeam.TeamBoards.FirstOrDefault(x => x.Name.ToLower() == board.ToLower());
            if (currentTeamBoard == null)
            {
                throw new ArgumentNullException($"Board with board name {board} does not exist");
            }

            var newStory = this.Factory.CreateStory(title, description, priority, size, storyStatus);
           
            currentTeamBoard.Add(newStory);
            currentTeamBoard.BoardActivityList.Add($"Story {newStory.Title} added.");
            base.Lists.AddWorkItem(newStory);
            newStory.HistoryAdd($"{newStory.Title} was changed.");

            return $"Created {newStory.ToString()}" + Environment.NewLine +
                $"Added to Team - {currentTeam.Name} in Board - {currentTeamBoard.Name}";
        }

        /// <summary>
        /// A method that sets the priority of a story according to input.
        /// </summary>
        /// <param name="priorityAsString">Input priority level.</param>
        /// <returns>Priority Enum</returns>
        public Priority GetPriority(string priorityAsString)
        {
            switch (priorityAsString.ToLower())
            {
                case "high":
                    return Priority.High;
                case "medium":
                    return Priority.Medium;
                case "low":
                    return Priority.Low;
                default:
                    throw new InvalidOperationException("Invalid Priority Type");
            }
        }

        /// <summary>
        /// A method that sets the size of a story according to input.
        /// </summary>
        /// <param name="sizeAsString">Input size.</param>
        /// <returns>Size Enum</returns>
        public Size GetSize(string sizeAsString)
        {
            switch (sizeAsString.ToLower())
            {
                case "large":
                    return Size.Large;
                case "medium":
                    return Size.Medium;
                case "small":
                    return Size.Small;
                default:
                    throw new InvalidOperationException("Invalid Size Type");
            }
        }

        /// <summary>
        /// A method that sets the status of a story according to input.
        /// </summary>
        /// <param name="statusAsString">Input status.</param>
        /// <returns>StoryStatus Enum</returns>
        public StoryStatus GetStatus(string statusAsString)
        {
            switch (statusAsString.ToLower())
            {
                case "notdone":
                    return StoryStatus.NotDone;
                case "inprogress":
                    return StoryStatus.InProgress;
                case "done":
                    return StoryStatus.Done;
                default:
                    throw new InvalidOperationException("Invalid Status Type");
            }
        }
    }
}
