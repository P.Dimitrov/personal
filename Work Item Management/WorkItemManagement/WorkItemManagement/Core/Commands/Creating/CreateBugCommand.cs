﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.Core.Commands.Creating
{
    public class CreateBugCommand : Command
    {
        public CreateBugCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {

        }

        /// <summary>
        /// This method creates a Bug with given parameters - title, description, steps to reproduce, 
        /// priorityLevel, severityLevel, statusType and is directly created to a specific board with 
        /// given name, at a specific team.
        /// 1/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 2/ With "FirstOrDefault" LINQ query the program searches the list of teams if there is a team with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 3/ With "FirstOrDefault" LINQ query the program searches the list of boards if there is a board with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 4/ The new Bug is created via WorkItem Factory.
        /// 5/ The Bug is added to the curent board's list of workitems.
        /// 6/ A string comment is added to the current board's list of activities.
        /// 7/ The current Bug is added to the list of bugs.
        /// 8/ A comment is added to the current bug's history.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string title;
            string description;
            List<string> steps;
            Priority priorityLevel;
            Severity severityLevel;
            BugStatus statusType;
            string team;
            string board;
            try
            {
                title = CommandParameters[0];
                description = CommandParameters[1];
                steps = CommandParameters[2].Split(" > ").ToList();
                priorityLevel = GetPriority(CommandParameters[3]);
                severityLevel = GetSeverity(CommandParameters[4]);
                statusType = GetStatus(CommandParameters[5]);
                team = CommandParameters[6];
                board = CommandParameters[7];
            }
            catch
            {
                throw new ArgumentException("Failed to parse CreateBug command parameters.");
            }

            var currentTeam = Lists.Teams.FirstOrDefault(x => x.Name.ToLower() == team.ToLower());
            if (currentTeam == null)
            {
                throw new ArgumentNullException($"Team with team name {team} does not exist");
            }
            var currentTeamBoard = currentTeam.TeamBoards.FirstOrDefault(x => x.Name.ToLower() == board.ToLower());
            if (currentTeamBoard == null)
            {
                throw new ArgumentNullException($"Board with board name {board} does not exist");
            }
            
            var newBug = this.Factory.CreateBug(title, description, steps, priorityLevel,
            severityLevel, statusType);

            currentTeamBoard.Add(newBug);
            currentTeamBoard.BoardActivityList.Add($"Bug {newBug.Title} added.");
            base.Lists.AddWorkItem(newBug);
            newBug.HistoryAdd($"{newBug.Title} was changed.");

            return $"Created {newBug.ToString()}" + Environment.NewLine +
                $"Added to Team - {currentTeam.Name} in Board - {currentTeamBoard.Name}";

        }

        /// <summary>
        /// A method that sets the priority of a bug according to input.
        /// </summary>
        /// <param name="priorityAsString">Input priority level.</param>
        /// <returns>Priority Enum</returns>
        public Priority GetPriority(string priorityAsString)
        {
            switch (priorityAsString.ToLower())
            {
                case "high":
                    return Priority.High;
                case "medium":
                    return Priority.Medium;
                case "low":
                    return Priority.Low;
                default:
                    throw new InvalidOperationException("Invalid Priority Type");
            }
        }

        /// <summary>
        /// A method that sets the severity of a bug according to input.
        /// </summary>
        /// <param name="severityAsString">Input severity level.</param>
        /// <returns>Severity Enum</returns>
        public Severity GetSeverity(string severityAsString)
        {
            switch (severityAsString.ToLower())
            {
                case "critical":
                    return Severity.Critical;
                case "major":
                    return Severity.Major;
                case "minor":
                    return Severity.Minor;
                default:
                    throw new InvalidOperationException("Invalid Severity Type");
            }
        }

        /// <summary>
        ///  A method that sets the status of a bug according to input.
        /// </summary>
        /// <param name="statusAsString">Input status.</param>
        /// <returns>BugStatus Enum</returns>
        public BugStatus GetStatus(string statusAsString)
        {
            switch (statusAsString.ToLower())
            {
                case "active":
                    return BugStatus.Active;
                case "fixed":
                    return BugStatus.Fixed;
                default:
                    throw new InvalidOperationException("Invalid Status Type");
            }
        }
    }
}
