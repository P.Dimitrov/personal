﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Commands.Creating
{
    public class CreateBoardCommand : Command
    {
        public CreateBoardCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }

        /// <summary>
        /// This method creates a Board with string Name and is directly created to a specific team with given String Name.
        /// 1/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse the program throws ArgumentException message.
        /// 2/ With "FirstOrDefault" LINQ query the program searches the list of teams if there is a team with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 3/ With "FirstOrDefault" LINQ query the program searches the list of boards if there is a board with the given name.
        /// If there is a board with the same name an ArgumentException is thrown.
        /// 4/ The new Board is created via WorkItem Factory.
        /// 5/ The Board is added to the curent team's list of boards.
        /// 6/ A string comment is added to the current team's list of activities.
        /// 7/ A string comment is added to the current board's list of activities.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string boardName;
            string teamName;
            try
            {
                boardName = CommandParameters[0];
                teamName = CommandParameters[1];
            }
            catch
            {
                throw new ArgumentException("Failed to parse CreateBoard command parameters.");
            }
            
            var currentTeam = Lists.Teams.FirstOrDefault(x => x.Name == teamName);
            if (currentTeam == null)
            {
                throw new ArgumentNullException("Cannot create board in a non-existing team.");
            }
            if (currentTeam.TeamBoards.Any(x => x.Name == boardName))
            {
                throw new ArgumentException("There is already a board with this name.");
            }
            var newBoard = Factory.CreateBoard(boardName);
            currentTeam.TeamBoards.Add(newBoard);
            currentTeam.TeamActivityList.Add($"Board {newBoard.Name} added");
            newBoard.BoardActivityList.Add($"{newBoard.Name} was created and added to team {currentTeam.Name}.");
            return $"Created board {newBoard.Name} in team {currentTeam.Name}";
        }
    }
}
