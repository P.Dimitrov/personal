﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.Core.Commands.Creating
{
    public class CreateFeedbackCommand : Command
    {
        public CreateFeedbackCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {

        }

        /// <summary>
        /// This method creates a Feedback with given parameters - title, description, rating, 
        /// statusType and is directly created to a specific board with given name, at a specific team.
        /// 1/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 2/ With "FirstOrDefault" LINQ query the program searches the list of teams if there is a team with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 3/ With "FirstOrDefault" LINQ query the program searches the list of boards if there is a board with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 4/ The new Feedback is created via WorkItem Factory.
        /// 5/ The Feedback is added to the curent board's list of workitems.
        /// 6/ A string comment is added to the current board's list of activities.
        /// 7/ The current Bug is added to the list of bugs.
        /// 8/ A comment is added to the current feedback's history.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string title;
            string description;
            int rating;
            FeedbackStatus statusType;
            string team;
            string board;
            try
            {
                title = CommandParameters[0];
                description = CommandParameters[1];
                rating = Int32.Parse(CommandParameters[2]);
                statusType = GetStatus(CommandParameters[3]);
                team = CommandParameters[4];
                board = CommandParameters[5];
            }
            catch
            {
                throw new ArgumentException("Failed to parse CreateFeedback command parameters.");
            }
            
            var currentTeam = Lists.Teams.FirstOrDefault(x => x.Name.ToLower() == team.ToLower());
            if (currentTeam == null)
            {
                throw new ArgumentNullException($"Team with team name {team} does not exist");
            }
            var currentTeamBoard = currentTeam.TeamBoards.FirstOrDefault(x => x.Name.ToLower() == board.ToLower());
            if (currentTeamBoard == null)
            {
                throw new ArgumentNullException($"Board with board name {board} does not exist");
            }
            var newFeedback = this.Factory.CreateFeedback(title, description, rating, statusType);
            currentTeamBoard.Add(newFeedback);
            currentTeamBoard.BoardActivityList.Add($"Feedback {newFeedback.Title} added.");
            base.Lists.AddWorkItem(newFeedback);
            newFeedback.HistoryAdd($"{newFeedback.Title} was changed.");

            return $"Created {newFeedback.ToString()}" + Environment.NewLine + 
                $"Added to Team - {currentTeam.Name} in Board - {currentTeamBoard.Name}";

        }

        /// <summary>
        /// A method that sets the status of a feedback according to input.
        /// </summary>
        /// <param name="feedbackAsString">Input status.</param>
        /// <returns>FeedbackStatus Enum</returns>
        public FeedbackStatus GetStatus(string feedbackAsString)
        {
            switch (feedbackAsString.ToLower())
            {
                case "new":
                    return FeedbackStatus.New;
                case "scheduled":
                    return FeedbackStatus.Scheduled;
                case "unscheduled":
                    return FeedbackStatus.Unscheduled;
                case "done":
                    return FeedbackStatus.Done;
                default:
                    throw new InvalidOperationException("Invalid Status Type");
            }
        }
    }
}
