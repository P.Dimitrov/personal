﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Commands.Creating
{
    public class CreateMemberCommand : Command
    {
        public CreateMemberCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }

        /// <summary>
        /// This method creates a Member with string Name.
        /// 1/ The method uses Try-Catch statement that checks if the given parameter is the correct type.
        /// If the parameter can't parse the program throws ArgumentException message.
        /// 2/ With "FirstOrDefault" LINQ query the program searches the list of members if there is a member with the given name.
        /// If there is a member with this name an ArgumentException is thrown.
        /// 4/ The new Member is created via WorkItem Factory.
        /// 5/ The Member is added to the list of members.
        /// 6/ A string comment is added to the current member's list of activities.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string name;
            try
            {
                name = CommandParameters[0];
            }
            catch
            {
                throw new ArgumentException("Failed to parse CreateMember command parameters.");
            }
            if (Lists.Members.Any(x => x.Name == name))
            {
                throw new ArgumentException("There is already a member with this name.");
            }
            var newMember = Factory.CreateMember(name);
            base.Lists.AddMember(newMember);
            newMember.MemberActivityList.Add($"{newMember.Name} was created.");

            return $"Created {newMember.Name}";
        }
    }
}
