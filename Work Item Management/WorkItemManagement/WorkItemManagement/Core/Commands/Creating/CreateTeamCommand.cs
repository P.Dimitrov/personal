﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Commands.Creating
{
    public class CreateTeamCommand : Command
    {
        public CreateTeamCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }

        /// <summary>
        /// This method creates a Team with string Name.
        /// 1/ The method uses Try-Catch statement that checks if the given parameter is the correct type.
        /// If the parameter can't parse the program throws ArgumentException message.
        /// 2/ With "FirstOrDefault" LINQ query the program searches the list of teams if there is a team with the given name.
        /// If there is a team with this name an ArgumentException is thrown.
        /// 4/ The new Team is created via WorkItem Factory.
        /// 5/ The Team is added to the list of teams.
        /// 6/ A string comment is added to the current team's list of activities.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string name;
            try
            {
                name = CommandParameters[0];
            }
            catch
            {
                throw new ArgumentException("Failed to parse CreateTeam command parameters.");
            }
            
            if (Lists.Teams.Any(x => x.Name == name)) 
            {
                throw new ArgumentException("There is already a team with this name.");
            }

            var newTeam = Factory.CreateTeam(CommandParameters[0]);
            base.Lists.AddTeam(newTeam);
            newTeam.TeamActivityList.Add($"{newTeam.Name} was created.");
            
            return $"Created team {newTeam.Name}";
        }
    }
}
