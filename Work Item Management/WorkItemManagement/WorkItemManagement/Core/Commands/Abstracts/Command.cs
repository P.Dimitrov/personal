﻿using System.Collections.Generic;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Factories;

namespace WorkItemManagement.Core.Commands.Abstracts
{
    public abstract class Command : ICommand

    {
        protected Command(IList<string> commandLine, ILists lists)
        {
            this.Factory = new WorkItemFactory();
            this.CommandParameters = new List<string>(commandLine);
            this.Lists = lists;
        }

        protected IList<string> CommandParameters { get; }

        protected IWorkItemFactory Factory { get; }

        protected ILists Lists { get; }

        public abstract string Execute();
    }
}
