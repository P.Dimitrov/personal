﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Commands.Listing
{
    public class ShowBoardsActivityCommand : Command
    {
        public ShowBoardsActivityCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }

        /// <summary>
        /// 1/ This method shows the activity of a given Board.
        /// 2/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 2/ With "FirstOrDefault" LINQ query the program searches the list of teams if there is a team with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 3/ With "FirstOrDefault" LINQ query the program searches the list of boards if there is a board with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 4/ The program makes a list of the current Board's all activity.
        /// 5/ The program uses if-else statement to check if the list of board's activities is null.
        /// If it is a ArgumentNullException is thrown.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string teamName;
            string boardName;
            try
            {
                teamName = CommandParameters[0];
                boardName = CommandParameters[1];
            }
            catch
            {
                throw new ArgumentException("Failed to parse ShowBoardsActivity command parameters.");
            }
            var currentTeam = Lists.Teams.FirstOrDefault(x => x.Name == teamName);
            if (currentTeam == null)
            {
                throw new ArgumentNullException($"Team {teamName} does not exist");
            }
            var currentBoard = currentTeam.TeamBoards.FirstOrDefault(x => x.Name == boardName);
            if (currentBoard == null)
            {
                throw new ArgumentNullException($"Board {boardName} does not exist.");
            }
            var currentActivity = currentBoard.BoardActivityList.ToList();
            if (currentActivity.Count == 0)
            {
                return "There is no activity added to this Board";
            }
            var str = new StringBuilder();
            str.AppendLine($"{currentBoard.Name}'s Activity: ");
            for (int i = 0; i < currentActivity.Count; i++)
            {
                str.AppendLine($"** {currentActivity[i]}");
            }

            return str.ToString().Trim();

        }
    }
}
