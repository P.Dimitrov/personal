﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.Core.Commands.Listing.WorkItems
{
    public class ShowWorkItemsCommand : Command
    {
        public ShowWorkItemsCommand(IList<string> commandLine, ILists lists)
           : base(commandLine, lists)
        {
        }

        /// <summary>
        /// This method sorts and prints the workitems with different filter parameter.
        /// 1/ If-statement checks if the given input parameters are more than 3.
        /// If they are an ArgumentException is thrown.
        /// 2/If-statement checks if the lists with bugs, feedbacks and stories are empty.
        /// If they are an ArgumentException is thrown.
        /// 3/ With a StringBuilder the program prints the wanted list of workitems.
        /// 4/With if-else statemenets the program checks whether the count of the parameters is 0, 1, 2 or 4.
        /// With every different case the filtration is different.
        /// 5/ If the parameters'count is 0 - the program prints all workitems from the lists with bugs,
        /// feedbacks and stories.
        /// 6/ If the parameters'count is 1 - bugs, feedback, story, title, priority, severity, size, rating.
        /// With if-else statements the program shows a list of the workitems, filtered by the given parameters.
        /// 7/If the parameters'count is 2 - status, assignee.
        /// With if-else statements the program shows a list of workitems, filtered by the given parameters.
        /// 8/If the parameters'count is 4 - title, priority, severity, size, rating.
        /// With if-else statements the program shows a list of workitems, filtered by the given parameters.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        public override string Execute()
        {
            if (CommandParameters.Count == 3 || CommandParameters.Count > 4)
            {
                throw new ArgumentException("Failed to parse ShowWorkItemsCommand command parameters.");
            }
            if (Lists.Bugs.Count == 0 && Lists.Feedback.Count == 0 && Lists.Story.Count == 0)
            {
                throw new ArgumentException("No Work Items exist");
            }
            var str = new StringBuilder();
            if (CommandParameters.Count == 0)
            {
                foreach (var bug in Lists.Bugs)
                {
                    str.Append($"{bug.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                }
                foreach (var story in Lists.Story)
                {
                    str.Append($"{story.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                }
                foreach (var feedback in Lists.Feedback)
                {
                    str.Append($"{feedback.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                }
            }
            if (CommandParameters.Count == 1)
            {
                var input = CommandParameters[0];
                if (input == "bugs" || input == "feedback" || input == "story" || 
                    input == "title" || input == "priority" ||
                    input == "severity" || input == "size" || input == "rating")
                {
                    if (input == "bugs")
                    {
                        if (Lists.Bugs.Count == 0)
                        {
                            throw new ArgumentException("No Work Items of type Bug exist");
                        }
                        else
                        {
                            foreach (var bug in Lists.Bugs)
                            {
                                str.Append($"{bug.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                            }
                        }
                    }
                    if (input == "story")
                    {
                        if (Lists.Story.Count == 0)
                        {
                            throw new ArgumentException("No Work Items of type Story exist");
                        }
                        else
                        {
                            foreach (var story in Lists.Story)
                            {
                                str.Append($"{story.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                            }
                        }
                    }
                    if (input == "feedback")
                    {
                        if (Lists.Feedback.Count == 0)
                        {
                            throw new ArgumentException("No Work Items of type Feedback exist");
                        }
                        else
                        {
                            foreach (var story in Lists.Feedback)
                            {
                                str.Append($"{story.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                            }
                        }
                    }
                    if (input == "title")
                    {
                        var sortedByTitle = new List<IWorkItem>();
                        foreach (var bug in Lists.Bugs)
                        {
                            sortedByTitle.Add(bug);
                        }
                        foreach (var feedback in Lists.Feedback)
                        {
                            sortedByTitle.Add(feedback);
                        }
                        foreach (var story in Lists.Story)
                        {
                            sortedByTitle.Add(story);
                        }
                        sortedByTitle = sortedByTitle.OrderBy(x => x.Title).ToList();
                        foreach (var workitem in sortedByTitle)
                        {
                            str.Append($"{workitem.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                        }
                    }
                    if (input == "priority")
                    {
                        if (Lists.Story.Count == 0 && Lists.Bugs.Count == 0)
                        {
                            throw new ArgumentException("No Work Items of type Bug and Story exist");
                        }
                        var sortedByPriority = new List<IWorkItem>();
                        foreach (var bug in Lists.Bugs)
                        {
                            if (bug.PriorityLevel.ToString() == "High")
                            {
                                sortedByPriority.Add(bug);
                            }
                        }
                        foreach (var story in Lists.Story)
                        {
                            if (story.Priority.ToString() == "High")
                            {
                                sortedByPriority.Add(story);
                            }
                        }
                        foreach (var bug in Lists.Bugs)
                        {
                            if (bug.PriorityLevel.ToString() == "Medium")
                            {
                                sortedByPriority.Add(bug);
                            }
                        }
                        foreach (var story in Lists.Story)
                        {
                            if (story.Priority.ToString() == "Medium")
                            {
                                sortedByPriority.Add(story);
                            }
                        }
                        foreach (var bug in Lists.Bugs)
                        {
                            if (bug.PriorityLevel.ToString() == "Low")
                            {
                                sortedByPriority.Add(bug);
                            }
                        }
                        foreach (var story in Lists.Story)
                        {
                            if (story.Priority.ToString() == "Low")
                            {
                                sortedByPriority.Add(story);
                            }
                        }
                        foreach (var workItem in sortedByPriority)
                        {
                            str.Append($"{workItem.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                        }
                    }
                    if (input == "severity")
                    {
                        if (Lists.Bugs.Count == 0)
                        {
                            throw new ArgumentException("No Work Items of type Bug exist");
                        }
                        var sortedBySeverity = new List<IBug>();
                        sortedBySeverity = Lists.Bugs.OrderBy(x => x.SeverityLevel).ToList();
                        foreach (var bug in sortedBySeverity)
                        {
                            str.Append($"{bug.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                        }
                    }
                    if (input == "size")
                    {
                        if (Lists.Story.Count == 0)
                        {
                            throw new ArgumentException("No Work Items of type Story exist");
                        }
                        var sortedBySize = new List<IStory>();
                        sortedBySize = Lists.Story.OrderBy(x => x.Size).ToList();
                        foreach (var story in sortedBySize)
                        {
                            str.Append($"{story.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                        }
                    }
                    if (input == "rating")
                    {
                        if (Lists.Feedback.Count == 0)
                        {
                            throw new ArgumentException("No Work Items of type Feedback exist");
                        }
                        var sortedByRating = new List<IFeedback>();
                        sortedByRating = Lists.Feedback.OrderBy(x => x.Rating).ToList();
                        foreach (var feedback in sortedByRating)
                        {
                            str.Append($"{feedback.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                        }
                    }
                }
            }

            if (CommandParameters.Count == 2)
            {
                if (Lists.Story.Count == 0 && Lists.Bugs.Count == 0)
                {
                    if (CommandParameters[0] == "status" && Lists.Feedback.Count == 0)
                    {
                        throw new ArgumentException("No Work Items of type Bug, Feedback or Story exist");
                    }
                    throw new ArgumentException("No Work Items of type Bug or Story exist");
                }
                var filter = CommandParameters[0];
                var input = CommandParameters[1];
                if (filter == "status" || filter == "assignee")
                {
                    if (filter == "assignee")
                    {
                        foreach (var member in Lists.Members)
                        {
                            if (member.Name.ToLower() == input.ToLower())
                            {
                                if (member.MemberAssignedWorkItems.Count == 0 )
                                {
                                    throw new ArgumentException($"Member {member.Name} does not have any work items assigned");
                                }
                                foreach (var workitem in member.MemberAssignedWorkItems)
                                {
                                    str.Append($"{workitem.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                                }
                            }
                        }
                    }
                    if (filter == "status")
                    {
                        foreach (var bug in Lists.Bugs)
                        {
                            if (bug.Status.ToString().ToLower() == $"{input.ToLower()}")
                            {
                                str.Append($"{bug.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                            }
                        }
                        foreach (var story in Lists.Story)
                        {
                            if (story.Status.ToString().ToLower() == $"{input.ToLower()}")
                            {
                                str.Append($"{story.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                            }
                        }
                        foreach (var feedback in Lists.Feedback)
                        {
                            if (feedback.Status.ToString().ToLower() == $"{input.ToLower()}")
                            {
                                str.Append($"{feedback.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                            }
                        }
                    }
                }
            }
            if (CommandParameters.Count == 4)
            {
                if (Lists.Story.Count == 0 && Lists.Bugs.Count == 0)
                {
                    throw new ArgumentException("No Work Items of type Bug or Story exist");
                }
                var filter = CommandParameters[0];
                var input = CommandParameters[1];
                var filter1 = CommandParameters[2];
                var input1 = CommandParameters[3];
                if ((filter == "status" || filter == "assignee") && (filter1 == "status" || filter1 == "assignee") && (filter != filter1))
                {
                    if (filter == "assignee" && filter1 == "status")
                    {
                        foreach (var member in Lists.Members)
                        {
                            if (member.Name.ToLower() == input.ToLower())
                            {
                                if (member.MemberAssignedWorkItems.Count == 0)
                                {
                                    throw new ArgumentException($"Member {member.Name} does not have any work items assigned");
                                }
                            }
                        }
                        if (input1 != "active" && input1 != "fixed" && input1 != "active"
                            && input1 != "done" && input1 != "new" && input1 != "unscheduled" && input1 != "scheduled"
                            && input1 != "inprogress" && input1 != "notdone")
                        {
                            throw new ArgumentException($"{input1} is not a valid Work Item status");
                        }
                        foreach (var bug in Lists.Bugs)
                        {
                            if (bug.Assignee.Name.ToLower() == $"{input.ToLower()}" && bug.Status.ToString().ToLower() == $"{input1.ToLower()}")
                            {
                                str.Append($"{bug.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                            }
                        }
                        foreach (var story in Lists.Story)
                        {
                            if (story.Assignee.Name.ToLower() == $"{input.ToLower()}" && story.Status.ToString().ToLower() == $"{input1.ToLower()}")
                            {
                                str.Append($"{story.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                            }
                        }
                    }
                    if (filter == "status" && filter1 == "assignee")
                    {
                        foreach (var member in Lists.Members)
                        {
                            if (member.Name.ToLower() == input1.ToLower())
                            {
                                if (member.MemberAssignedWorkItems.Count == 0)
                                {
                                    throw new ArgumentException($"Member {member.Name} does not have any work items assigned");
                                }
                            }
                        }
                        if (input != "active" && input != "fixed" && input != "active"
                            && input != "done" && input != "new" && input != "unscheduled" && input != "scheduled"
                            && input != "inprogress" && input != "notdone")
                        {
                            throw new ArgumentException($"{input1} is not a valid Work Item status");
                        }
                        foreach (var member in Lists.Members)
                        {
                            if (member.Name.ToLower() == input1.ToLower())
                            {
                                if (member.MemberAssignedWorkItems.Count == 0)
                                {
                                    throw new ArgumentException($"Member {member.Name} does not have any work items assigned");
                                }
                            }
                        }
                        foreach (var bug in Lists.Bugs)
                        {
                            if (bug.Assignee.Name.ToLower() == $"{input1.ToLower()}" && bug.Status.ToString().ToLower() == $"{input.ToLower()}")
                            {
                                str.Append($"{bug.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                            }
                        }
                        foreach (var story in Lists.Story)
                        {
                            if (story.Assignee.Name.ToLower() == $"{input1.ToLower()}" && story.Status.ToString().ToLower() == $"{input.ToLower()}")
                            {
                                str.Append($"{story.ToString()} " + Environment.NewLine + "----------------" + Environment.NewLine);
                            }
                        }
                    }
                }
            }
            return str.ToString().Trim();
        }
    }
}
