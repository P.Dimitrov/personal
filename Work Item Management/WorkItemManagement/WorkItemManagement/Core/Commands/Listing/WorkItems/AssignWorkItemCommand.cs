﻿using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.Core.Commands.OtherCommands
{
    public class AssignWorkItemCommand : Command
    {
        public AssignWorkItemCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }

        /// <summary>
        /// 1/ This method assignes a given workitem /bug or story/ to a given member.
        /// 2/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 3/ A foreach loop searches in the list of members if there is a member with the given name.
        /// If there is no member an ArgumentNullException is thrown.
        /// 4/ A foreach loop searches in the list of bugs if there is a bug with the given ID.
        /// If there is no bug an ArgumentNullException is thrown.
        /// 5/ A foreach loop searches in the list of stories if there is a story with the given ID.
        /// If there is no story an ArgumentNullException is thrown.
        /// 4/ If the workitem is found it is assigned to the given member.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string assigneeString;
            IMember assignee = default;
            long workItemID;
            bool workItemExists = false;
            try
            {
                assigneeString = CommandParameters[0];
                workItemID = long.Parse(CommandParameters[1]);
            }
            catch
            {
                throw new ArgumentException("Failed to parse AssignWorkItem command parameters.");
            }
            foreach (var member in Lists.Members)
            {
                if (member.Name.ToLower() == assigneeString.ToLower())
                {
                    assignee = member;
                    break;
                }
            }
            if (assignee == default)
            {
                throw new ArgumentException($"Member {assigneeString} does not exist");
            }
            foreach (var bug in Lists.Bugs)
            {
                if (workItemID == bug.Id)
                {
                    workItemExists = true;
                    bug.Assign(assignee);
                    assignee.MemberAssignedWorkItems.Add(bug);
                    break;
                }
            }
            foreach (var story in Lists.Story)
            {
                if (workItemID == story.Id)
                {
                    workItemExists = true;
                    story.Assign(assignee);
                    assignee.MemberAssignedWorkItems.Add(story);
                    break;
                }
            }
            if (workItemExists == false)
            {
                throw new ArgumentException($"Work Item with ID {workItemID} does not exist or is not assignable");
            }
            return $"WorkItem {workItemID} assigned to {assignee.Name}";
        }
    }
}
