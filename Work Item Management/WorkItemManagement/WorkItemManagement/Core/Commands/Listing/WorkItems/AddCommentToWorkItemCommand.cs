﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.Core.Commands.OtherCommands
{
    public class AddCommentToWorkItemCommand : Command
    {
        public AddCommentToWorkItemCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }

        /// <summary>
        /// 1/ This method adds given member to a given team.
        /// 2/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 3/ With "FirstOrDefault" LINQ query the program searches the list of members if there is a member with the given name.
        /// If there is no member with this name an ArgumentNullException is thrown.
        /// With "FirstOrDefault" LINQ query the program searches the list of members for all the 
        /// members that have assigned workitems and from them if there is a workitem with a specific ID.
        /// If there is no member with this name a message is printed.
        /// 4/ The program adds comment to the member's activity list.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string commenter;
            long id;
            string comment;
            IMember existingMember = default;
            try
            {
                commenter = CommandParameters[0];
                id = long.Parse(CommandParameters[1]);
                comment = CommandParameters[2];
            }
            catch
            {
                throw new ArgumentException("Failed to parse AddCommentToWorkItemCommand command parameters.");
            }
            var currentMember = Lists.Members.FirstOrDefault(x => x.Name == CommandParameters[0]);
            if (currentMember == null)
            {
                throw new ArgumentNullException("There is no such member.");
            }

            string output = $"Work Item with ID {id} does not exist or is not assigned to member {commenter}";

            var result = Lists.Members.SelectMany(x => x.MemberAssignedWorkItems).FirstOrDefault(x => x.Id == id);
            if (result != default)
            {
                var commentToAdd = new Comment(currentMember, comment);
                result.CommentAdd(commentToAdd);
                currentMember.MemberActivityList.Add($"Added comment \"{comment}\" to Work Item ID {id}");
                output = $"Member {commenter} added comment \"{comment}\" to Work Item ID {id}";
            }
            return output;
        }
    }
}
