﻿using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.Core.Commands.OtherCommands
{
    public class UnassignWorkItemCommand : Command
    {
        public UnassignWorkItemCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }

        /// <summary>
        /// 1/ This method unassignes a given workitem /bug or story/ from a given member.
        /// 2/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 3/ A foreach loop searches in the list of members if there is a member with the given name.
        /// If there is no member an ArgumentNullException is thrown.
        /// 4/ A foreach loop searches in the list of members if there is a member with the given name.
        /// If there is no member an ArgumentNullException is thrown.
        /// 5/ If the member is found the current workitem is removed from the member's list of assigned workitems.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string assigneeString;
            IMember assignee = default;
            long workItemID;
            bool workItemExists = false;
            try
            {
                assigneeString = CommandParameters[0];
                workItemID = long.Parse(CommandParameters[1]);
            }
            catch
            {
                throw new ArgumentException("Failed to parse UnassignWorkItem command parameters.");
            }
            foreach (var member in Lists.Members)
            {
                if (member.Name.ToLower() == assigneeString.ToLower())
                {
                    assignee = member;
                    break;
                }
            }
            if (assignee == default)
            {
                throw new ArgumentException($"Member {assigneeString} does not exist");
            }
            foreach (var workItem in assignee.MemberAssignedWorkItems)
            {
                if (workItemID == workItem.Id)
                {
                    workItemExists = true;
                    workItem.Unassign();
                    assignee.MemberAssignedWorkItems.Remove(workItem);
                    break;
                }
            }
            if (workItemExists == false)
            {
                throw new ArgumentException($"Work Item with ID {workItemID} does not exist");
            }
            return $"WorkItem {workItemID} unassigned from {assignee.Name}";
        }
    }
}
