﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Commands.Listing
{
    public class ShowPersonsActivityCommand : Command
    {
        public ShowPersonsActivityCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }
        /// <summary>
        /// 1/ This method shows the activity of a given Person.
        /// 2/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 3/ With "FirstOrDefault" LINQ query the program searches the list of members if there is a member with the given name.
        /// If there is no member with this name an ArgumentNullException is thrown.
        /// 4/ The program makes a list of the current Member's all activity.
        /// 5/ The program uses if-else statement to check if the list of member's activities is null.
        /// If it is a ArgumentNullException is thrown.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string name;
            try
            {
                name = CommandParameters[0];
            }
            catch
            {
                throw new ArgumentException("Failed to parse ShowPersonsActivity command parameters.");
            }
            var currentMember = Lists.Members.FirstOrDefault(x => x.Name == name);
            if (currentMember == null)
            {
                throw new ArgumentNullException($"Member {name} does not exist.");
            }
            var currentActivity = currentMember.MemberAssignedWorkItems.ToList();
            var str = new StringBuilder();

            if (currentActivity.Count == 0)
            {
                return $"Member does not have any activity assigned";
            }

            str.AppendLine($"{currentMember.Name}'s Activity: ");
            for (int i = 0; i < currentActivity.Count; i++)
            {
                str.AppendLine($"** {currentMember.MemberAssignedWorkItems[i]}");
            }

            return str.ToString().Trim();

        }
    }
}
