﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Commands.Creating
{
    public class AddMemberCommand : Command
    {
        public AddMemberCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }

        /// <summary>
        /// 1/ This method adds given member to a given team.
        /// 2/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 3/ With "FirstOrDefault" LINQ query the program searches the list of teams if there is a team with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 4/ With "FirstOrDefault" LINQ query the program searches the list of members if there is a member with the given name.
        /// If there is no member with this name an ArgumentNullException is thrown.
        /// 5/ The program adds the member to the current team's list of members.
        /// 6/ The program adds comment to the team's activity list.
        /// 7/ The program adds comment to the member's activity list.
        /// </summary>
        /// <returns>String confirmation.</returns>
        public override string Execute()
        {
            string name;
            string teamName;
            try
            {
                name = CommandParameters[0];
                teamName = CommandParameters[1];
            }
            catch
            {
                throw new ArgumentException("Failed to parse AddMember command parameters.");
            }
            var currentTeam = Lists.Teams.FirstOrDefault(x => x.Name == CommandParameters[1]);
            if (currentTeam == null)
            {
                throw new ArgumentNullException("There is no such team.");
            }
            var currentMember = Lists.Members.FirstOrDefault(x => x.Name == CommandParameters[0]);
            if (currentMember == null)
            {
                throw new ArgumentNullException("There is no such member.");
            }
            currentTeam.TeamMembers.Add(currentMember);
            currentMember.MemberActivityList.Add($"Added to Team {currentTeam.Name}.");
            currentTeam.TeamActivityList.Add($"Member {currentMember.Name} added to Team.");
            return $"Member {currentMember.Name} added to Team {currentTeam.Name}";
        }
    }
}
