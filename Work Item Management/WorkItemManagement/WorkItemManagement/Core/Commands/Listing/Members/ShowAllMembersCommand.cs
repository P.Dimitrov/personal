﻿using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Commands.Listing
{
    public class ShowAllMembersCommand : Command
    {
        public ShowAllMembersCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }

        /// <summary>
        /// 1/ This method shows all members.
        /// 2/ The program uses if-else statement to check if the list of members is null.
        /// If it is a ArgumentNullException is thrown.
        /// </summary>
        /// <returns>String list of all members.</returns>
        public override string Execute()
        {
             var str = new StringBuilder();

            if (Lists.Members.Count == 0)
            {
                return "No Members exist in the current database.";
            }

            for (int i = 0; i < Lists.Members.Count; i++)
            {
                str.AppendLine($"{Lists.Members[i].ToString()} ");
            }

            return str.ToString().Trim();

        }
    }
}
