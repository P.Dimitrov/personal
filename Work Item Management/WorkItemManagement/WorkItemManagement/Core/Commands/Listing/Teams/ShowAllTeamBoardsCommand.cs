﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;


namespace WorkItemManagement.Core.Commands.Listing
{
    public class ShowAllTeamBoardsCommand : Command
    {
        public ShowAllTeamBoardsCommand(IList<string> commandLine, ILists lists)
           : base(commandLine, lists)
        {
        }

        /// <summary>
        /// 1/ This method shows all boards for a given team.
        /// 2/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 3/ With "FirstOrDefault" LINQ query the program searches the list of teams if there is a team with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 4/ The method uses if-else statement to check if the list of team boards is null.
        /// If it is a message is printed.
        /// </summary>
        /// <returns>String list of all boards from a given team.</returns>
        public override string Execute()
        {
            string teamName;
            try
            {
                teamName = CommandParameters[0];
            }
            catch
            {
                throw new ArgumentException("Failed to parse ShowAllTeamBoards command parameters.");
            }
            var currentTeam = Lists.Teams.FirstOrDefault(x => x.Name == teamName);
            if (currentTeam == null)
            {
                throw new ArgumentNullException("There is no such team.");
            }
            if (currentTeam.TeamBoards.Count == 0)
            {
                return $"No boards have been added to team {teamName}";
            }
            var str = new StringBuilder();
            str.AppendLine($"Boards of Team {currentTeam.ToString()}:");

            for (int i = 0; i < currentTeam.TeamBoards.Count; i++)
            {
                str.AppendLine($"{currentTeam.TeamBoards[i].ToString()}");
            }

            return str.ToString().Trim();

        }
    }
}
