﻿using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Commands.Listing
{
    public class ShowAllTeamsCommand : Command
    {
        public ShowAllTeamsCommand(IList<string> commandLine, ILists lists)
           : base(commandLine, lists)
        {
        }


        /// <summary>
        /// 1/ This method shows all teams.
        /// 2/ The program uses if-else statement to check if the list of teams is null.
        /// If it is a ArgumentNullException is thrown.
        /// </summary>
        /// <returns>String list of all teams.</returns>
        public override string Execute()
        {

            if (Lists.Teams.Count == 0)
            {
                return $"No teams exist in the current database";
            }

            var str = new StringBuilder();

            for (int i = 0; i < Lists.Teams.Count; i++)
            {

                str.AppendLine($"{Lists.Teams[i].ToString()} ");
            }

            return str.ToString().Trim();

        }
    }
}
