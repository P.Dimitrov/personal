﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Commands.Listing
{
    public class ShowTeamsActivityCommand : Command
    {
        public ShowTeamsActivityCommand(IList<string> commandLine, ILists lists)
            : base(commandLine, lists)
        {
        }

        /// <summary>
        /// 1/ This method shows the activity of a given Team.
        /// 2/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 3/ With "FirstOrDefault" LINQ query the program searches the list of teams if there is a team with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 4/ The program makes a list of the current Team's all activity.
        /// 5/ The program uses if-else statement to check if the list of team's activities is null.
        /// If it is a ArgumentNullException is thrown.
        /// </summary>
        /// <returns>String list of the current team's activity.</returns>
        public override string Execute()
        {
            string name;
            try
            {
                name = CommandParameters[0];
            }
            catch
            {
                throw new ArgumentException("Failed to parse ShowTeamsActivity command parameters.");
            }
            var currentTeam = Lists.Teams.FirstOrDefault(x => x.Name == CommandParameters[0]);
            if (currentTeam.TeamBoards.Any(x => x.Name == name))
            {
                throw new ArgumentException("There is no such team.");
            }
            var currentActivity = currentTeam.TeamActivityList.ToList();
            var str = new StringBuilder();

            if (currentActivity.Count == 0)
            {
                return $"There is no activity within this team";
            }

            str.AppendLine($"{currentTeam.Name}'s Activity: ");
            for (int i = 0; i < currentActivity.Count; i++)
            {
                str.AppendLine($"** {currentTeam.TeamActivityList[i]}");
            }

            return str.ToString().Trim();

        }
    }
}
