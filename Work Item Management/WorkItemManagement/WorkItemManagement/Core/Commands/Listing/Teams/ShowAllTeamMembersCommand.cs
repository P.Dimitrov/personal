﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;


namespace WorkItemManagement.Core.Commands.Listing
{
    public class ShowAllTeamMembersCommand : Command
    {
        public ShowAllTeamMembersCommand(IList<string> commandLine, ILists lists)
           : base(commandLine, lists)
        {
        }

        /// <summary>
        /// 1/ This method shows all members for a given team.
        /// 2/ The method uses Try-Catch statement that checks if the given parameters are the correct type.
        /// If the parameters can't parse, the program throws ArgumentException message.
        /// 4/ With "FirstOrDefault" LINQ query the program searches the list of teams if there is a team with the given name.
        /// If there is no team with this name an ArgumentNullException is thrown.
        /// 5/ The method uses if-else statement to check if the list of team members is null.
        /// If it is a message is printed.
        /// </summary>
        /// <returns></returns>
        public override string Execute()
        {
            string teamName;
            try
            {
                teamName = CommandParameters[0];
            }
            catch
            {
                throw new ArgumentException("Failed to parse ShowAllTeamMembers command parameters.");
            }
            var currentTeam = Lists.Teams.FirstOrDefault(x => x.Name == teamName);
            if (currentTeam == null)
            {
                throw new ArgumentNullException($"Team {teamName} does not exist.");
            }
            
            if (currentTeam.TeamMembers.Count == 0)
            {
                return $"There are no members added to team {teamName}";
            }

            var str = new StringBuilder();
            str.AppendLine($"Members of Team {currentTeam.ToString()}:");

            for (int i = 0; i < currentTeam.TeamMembers.Count; i++)
            {
                str.AppendLine($"{currentTeam.TeamMembers[i].ToString()}");
            }

            return str.ToString().Trim();

        }
    }
}
