﻿using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Abstracts;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Commands.Help
{
    class HelpCommand : Command
    {
        public HelpCommand(IList<string> commandLine, ILists lists)
           : base(commandLine, lists)
        {
        }

        /// <summary>
        /// A method that prints the help information of the program by 
        /// giving the correct commands and the restrictions of the parameters given as an input.
        /// </summary>
        /// <returns>string</returns>
        public override string Execute()
        {
            return "createmember - Creates new member" + Environment.NewLine +
                 "Format - createmember, <Name>" + Environment.NewLine +
                 "Restrictions:" + Environment.NewLine +
                 "Name should be unique in the application and between 5 and 15 symbols." + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "createteam - Creates new team" + Environment.NewLine +
                 "Format - createteam, <Name>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "createboard - Creates new board" + Environment.NewLine +
                 "Format - createboard, <Name>" + Environment.NewLine +
                 "Restrictions:" + Environment.NewLine +
                 "Name should be between 5 and 15 symbols." + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "createbug - Creates new Bug work item and adds to a board" + Environment.NewLine +
                 "Format - createbug, <Title>, <Description>, <Steps>, <Priority>, <Severity>, <Status>, <Team>, <Board>" + Environment.NewLine +
                 "Restrictions:" + Environment.NewLine +
                 "Title should be between 5 and 50 symbols." + Environment.NewLine +
                 "Description should be between 10 and 500 symbols." + Environment.NewLine +
                 "Steps must be separated by \" > \" " + Environment.NewLine +
                 "Priority can be - High, Medium or Low" + Environment.NewLine +
                 "Severity can be - Critical, Major or Minor" + Environment.NewLine +
                 "Status can be - Active or Fixed" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "createfeedback - Creates new Feedback work item and adds to a board" + Environment.NewLine +
                 "Format - createfeedback, <Title>, <Description>, <Rating>, <Status>, <Team>, <Board>" + Environment.NewLine +
                 "Restrictions:" + Environment.NewLine +
                 "Title should be between 5 and 50 symbols." + Environment.NewLine +
                 "Description should be between 10 and 500 symbols." + Environment.NewLine +
                 "Rating can be a number between -2,147,483,648 and 2,147,483,647" + Environment.NewLine +
                 "Status can be - New, Scheduled, Unscheduled or Done" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "createstory - Creates new Story work item and adds to a board" + Environment.NewLine +
                 "Format - createstory, <Title>, <Description>, <Priority>, <Size>, <Status>, <Team>, <Board>" + Environment.NewLine +
                 "Restrictions:" + Environment.NewLine +
                 "Title should be between 5 and 50 symbols." + Environment.NewLine +
                 "Description should be between 10 and 500 symbols." + Environment.NewLine +
                 "Priority can be - High, Medium, Low" + Environment.NewLine +
                 "Size can be - Large, Medium, Small" + Environment.NewLine +
                 "Status can be - New, Scheduled, Unscheduled or Done" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "showallmembers - Shows all members" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "showallteams - Shows all teams" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "showallboards - Shows all boards" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "showallteammembers - Shows all members of a given team" + Environment.NewLine +
                 "Format - showallteammembers, <Team Name>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "addmember - Adds a member to a team" + Environment.NewLine +
                 "Format - addmember, <Member Name>, <Team Name>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "showpersonsactivity - Shows a member's work items he has worked/is working on" + Environment.NewLine +
                 "Format - showpersonsactivity, <Member Name>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "showteamsactivity - Shows a team's activity list" + Environment.NewLine +
                 "Format - showteamsactivity, <Team Name>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "showboardsactivity - Shows a board's activity list" + Environment.NewLine +
                 "Format - showboardsactivity, <Board Name>, <Team Name>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "changebug - Changes a bug's Priority, Severity or Status to different valid value" + Environment.NewLine +
                 "Format - changebug, <Bug ID>, <New value>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "changefeedback - Changes a feedback's Rating or Status to different valid value" + Environment.NewLine +
                 "Format - changefeedback, <Feedback ID>, <New value>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "changestory - Changes a story's Priority, Size or Status to different valid value" + Environment.NewLine +
                 "Format - changestory, <Story ID>, <New value>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "assignworkitem - Assigns a work item to a member" + Environment.NewLine +
                 "Format - assignworkitem, <WorkItem ID>, <Member Name>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "unassignworkitem - Unassigns a work item to a member" + Environment.NewLine +
                 "Format - unassignworkitem, <WorkItem ID>, <Member Name>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "addcommenttoworkitem - Adds a comment from a member to a work item" + Environment.NewLine +
                 "Format - addcommenttoworkitem, <Member>, <WorkItem ID>, <Comment>" + Environment.NewLine +
                 "---------------------------------------" + Environment.NewLine +
                 "showworkitems - Filters or sorts all work items" + Environment.NewLine +
                 "Format:" + Environment.NewLine +
                 "showworkitems - Lists all work items" + Environment.NewLine +
                 "showworkitems, <sort> - Shows all work items sorted by the given property" + Environment.NewLine +
                 "List of possibilities for <sort> - title, priority, severity, size, rating" + Environment.NewLine +
                 "showworkitems, <filter by> - Shows all work items with the given property" + Environment.NewLine +
                 "List of possible <filter by> - bugs, story, feedback, title, assignee, status" + Environment.NewLine +
                 "Additional Note - assignee and status require additional input \", <Assignee Name>\" or \", <Valid Status>\" respectively, " + Environment.NewLine +
                 "and can be combined in the following format \"showworkitems, assignee, <Assignee Name>, status, <Valid Status>\"";
        }
    }
}
