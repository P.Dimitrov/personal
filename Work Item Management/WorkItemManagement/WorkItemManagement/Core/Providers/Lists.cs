﻿using System.Collections.Generic;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.Core.Providers
{
    public class Lists : ILists
    {
        private readonly List<IMember> members = new List<IMember>();

        private readonly List<ITeam> teams = new List<ITeam>();

        private readonly List<IBug> bugs = new List<IBug>();

        private readonly List<IStory> stories = new List<IStory>();

        private readonly List<IFeedback> feedbacks = new List<IFeedback>();

        public IList<IMember> Members
        {
            get
            {
                return this.members;
            }
        }

        public IList<ITeam> Teams
        {
            get
            {
                return this.teams;
            }
        }
        public IList<IBug> Bugs
        {
            get
            {
                return this.bugs;
            }
        }
        public IList<IFeedback> Feedback
        {
            get
            {
                return this.feedbacks;
            }
        }
        public IList<IStory> Story
        {
            get
            {
                return this.stories;
            }
        }

        /// <summary>
        /// A method that adds every created member to a list of members.
        /// </summary>
        /// <param name="member">Every created member.</param>
        public void AddMember(IMember member)
        {
            members.Add(member);
        }

        /// <summary>
        /// A method that adds every created team to a list of teams.
        /// </summary>
        /// <param name="team">Every created team.</param>
        public void AddTeam(ITeam team)
        {
            teams.Add(team);
        }

        /// <summary>
        /// A method that adds every created bug to a list of bugs.
        /// </summary>
        /// <param name="bug">Every created bug.</param>
        public void AddWorkItem(IBug bug)
        {
            bugs.Add(bug);
        }

        /// <summary>
        /// A method that adds every created feedback to a list of feedbacks.
        /// </summary>
        /// <param name="feedback">Every created feedback.</param>
        public void AddWorkItem(IFeedback feedback)
        {
            feedbacks.Add(feedback);
        }

        /// <summary>
        /// A method that adds every created story to a list of stories.
        /// </summary>
        /// <param name="story">Every created story.</param>
        public void AddWorkItem(IStory story)
        {
            stories.Add(story);
        }
    }
}
