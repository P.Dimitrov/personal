﻿using System;
using System.Linq;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Commands.Help;
using WorkItemManagement.Core.Commands.Listing;
using WorkItemManagement.Core.Commands.Listing.WorkItems;
using WorkItemManagement.Core.Commands.OtherCommands;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Providers
{
    class CommandParser : ICommandParser
    {
        private static readonly ILists lists = new Lists();
        public ICommand ParseCommand(string commandLine)
        {
            var lineParameters = commandLine
                .Trim()
                .Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);

            var commandName = lineParameters[0];
            var parameters = lineParameters.Skip(1).ToList();
            return commandName switch
            {
                "createmember" => new CreateMemberCommand(parameters, lists),
                "createteam" => new CreateTeamCommand(parameters, lists),
                "createboard" => new CreateBoardCommand(parameters, lists),
                "createbug" => new CreateBugCommand(parameters, lists),
                "createfeedback" => new CreateFeedbackCommand(parameters, lists),
                "createstory" => new CreateStoryCommand(parameters, lists),
                "showallmembers" => new ShowAllMembersCommand(parameters, lists),
                "showallteams" => new ShowAllTeamsCommand(parameters, lists),
                "showallboards" => new ShowAllTeamBoardsCommand(parameters, lists),
                "showallteammembers" => new ShowAllTeamMembersCommand(parameters, lists),
                "addmember" => new AddMemberCommand(parameters, lists),
                "showpersonsactivity" => new ShowPersonsActivityCommand(parameters, lists),
                "showteamsactivity" => new ShowTeamsActivityCommand(parameters, lists),
                "showboardsactivity" => new ShowBoardsActivityCommand(parameters, lists),
                "showworkitems" => new ShowWorkItemsCommand(parameters, lists),
                "changebug" => new ChangeBugCommand(parameters, lists),
                "changefeedback" => new ChangeFeedbackCommand(parameters, lists),
                "changestory" => new ChangeStoryCommand(parameters, lists),
                "assignworkitem" => new AssignWorkItemCommand(parameters, lists),
                "unassignworkitem" => new UnassignWorkItemCommand(parameters, lists),
                "addcommenttoworkitem" => new AddCommentToWorkItemCommand(parameters, lists),
                "help" => new HelpCommand(parameters, lists),
                _ => throw new InvalidOperationException("Command does not exist")
            };
        }
    }
}
