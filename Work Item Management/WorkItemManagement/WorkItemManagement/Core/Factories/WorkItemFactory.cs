﻿using System.Collections.Generic;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.Core.Factories
{
    public class WorkItemFactory : IWorkItemFactory
    {
        /// <summary>
        /// A method that creates a new team with given name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>ITeam</returns>
        public ITeam CreateTeam(string name)
        {
            return new Team(name);
        }

        /// <summary>
        /// A method that creates a new member with given name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>IMember</returns>
        public IMember CreateMember(string name)
        {
            return new Member(name);
        }

        /// <summary>
        /// A method that creates a new board with given name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>IBoard</returns>
        public IBoard CreateBoard(string name)
        {
            return new Board(name);
        }

        /// <summary>
        /// A method that creates a new bug with given parameters.
        /// </summary>
        /// <param name="title">Title of the bug.</param>
        /// <param name="description">Description of the bug.</param>
        /// <param name="steps">Steps to reproduce the bug.</param>
        /// <param name="priorityLevel">Priority level of the bug.</param>
        /// <param name="severityLevel">Severity level of the bug.</param>
        /// <param name="statusType">Status type of the bug.</param>
        /// <returns>IBug</returns>
        public IBug CreateBug(string title, string description, List<string> steps, Priority priorityLevel,
            Severity severityLevel, BugStatus statusType)
        {
            return new Bug( title,  description,  steps,  priorityLevel, severityLevel,  statusType);
        }

        /// <summary>
        /// A method that creates a new story with given parameters.
        /// </summary>
        /// <param name="title">Title of the story.</param>
        /// <param name="description">Description of the story.</param>
        /// <param name="priority">Priority level of the story.</param>
        /// <param name="size">Size of the story.</param>
        /// <param name="storyStatus">Status type of the story.</param>
        /// <returns>IStory</returns>
        public IStory CreateStory(string title, string description, Priority priority, Size size, 
            StoryStatus storyStatus)
        {
            return new Story( title,  description,  priority,  size,  storyStatus);
        }

        /// <summary>
        /// A method that creates a new feedback with given parameters.
        /// </summary>
        /// <param name="title">Title of the feedback.</param>
        /// <param name="description">Description of the feedback.</param>
        /// <param name="rating">Rating of the feedback.</param>
        /// <param name="feedbackStatus">Status type of the feedback.</param>
        /// <returns></returns>
        public IFeedback CreateFeedback(string title, string description, int rating, FeedbackStatus feedbackStatus)
        {
            return new Feedback( title,  description,  rating,  feedbackStatus);
        }
    }
}
