﻿using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace WorkItemManagement.Core.Contracts
{
    public interface IWorkItemFactory
    {
         ITeam CreateTeam(string name);

         IMember CreateMember(string name);

         IBoard CreateBoard(string name);

         IBug CreateBug(string title, string description, List<string> steps, Priority priorityLevel,
            Severity severityLevel, BugStatus statusType);

         IStory CreateStory(string title, string description, Priority priority, Size size,
            StoryStatus storyStatus);

         IFeedback CreateFeedback(string title, string description, int rating, FeedbackStatus feedbackStatus);
        
    }
}
