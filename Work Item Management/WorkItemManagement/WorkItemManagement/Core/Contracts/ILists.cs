﻿using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement.Core.Contracts
{
    public interface ILists
    {
        IList<IMember> Members { get; }

        IList<ITeam> Teams { get; }
        IList<IBug> Bugs { get; }
        IList<IStory> Story { get; }
        IList<IFeedback> Feedback { get; }

        void AddMember(IMember members);

        void AddTeam(ITeam teams);
        public void AddWorkItem(IBug bug);
        public void AddWorkItem(IFeedback feedback);
        public void AddWorkItem(IStory story);
    }
}