﻿namespace WorkItemManagement.Core.Contracts
{
    public interface ICommand
    {
        string Execute();
    }
}
