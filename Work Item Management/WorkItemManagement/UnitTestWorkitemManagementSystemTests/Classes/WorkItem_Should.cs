﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace WorkItemManagement_Tests
{
    [TestClass]
    public class WorkItem_Should
    {
        [TestMethod]
        public void WorkItem_Sets_Correct_Title()
        {
            //Arrange
            var title = "A Computer Bug";
            var description = "The worst thing that could happen";
            //Act
            var sut = new WorkItem(title, description);
            //Assert
            Assert.AreEqual(title, sut.Title);
        }

        [TestMethod]
        public void WorkItem_Sets_Correct_Description()
        {
            //Arrange
            var title = "A Computer Bug";
            var description = "The worst thing that could happen";
            //Act
            var sut = new WorkItem(title, description);
            //Assert
            Assert.AreEqual(description, sut.Description);
        }

        [TestMethod]
        public void WorkItem_Sets_Correct_ID()
        {
            //Act
            var sut = new FakeWorkItem();
            var sut2 = new FakeWorkItem();
            //Assert
            Assert.AreEqual(2, sut.Id);
            Assert.AreEqual(3, sut2.Id);
        }

        [TestMethod]
        public void WorkItem_ThrowsException_When_TitleLengthLessThan10()
        {
            //Arrange
            var title = "A Bug";
            var description = "The worst thing that could happen";

            //Act & Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new WorkItem(title, description));
        }

        [TestMethod]
        public void WorkItem_Throws_Exception_When_TitleLengthMoreThan50()
        {
            //Arrange
            var title = "A Bug which really is not cool, because it compltely ruins the website and cannot do anything and makes me want to kill myself";
            var description = "The worst thing that could happen";

            //Act & Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new WorkItem(title, description));
        }

        [TestMethod]
        public void WorkItem_Throws_Exception_When_DescriptionLengthLessThan10()
        {
            //Arrange
            var title = "A Bug";
            var description = "The worst";

            //Act & Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new WorkItem(title, description));
        }

        [TestMethod]
        public void WorkItem_Throws_Exception_When_DescriptionLengthMoreThan50()
        {
            //Arrange
            var title = "A Bug";
            var description = "The worst thing that could happen in the whole wide world";

            //Act & Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new WorkItem(title, description));
        }

        [TestMethod]
        public void WorkItem_CommentAdd_Adds_To_Comment_List()
        {
            //Arrange
            var member = new FakeMember()
            {
                Name = "Pesho"
            };
            var comment = new Comment(member, "My Comment");
            var title = "A Computer Bug";
            var description = "The worst thing that could happen";
            var sut = new WorkItem(title, description);
            //Act
            sut.CommentAdd(comment);
            //Assert
            Assert.AreEqual(sut.Comments[0], comment);
        }

        [TestMethod]
        public void WorkItem_HistoryAdd_Adds_To_History_List()
        {
            //Arrange
            var history = "Ancient";
            var title = "A Computer Bug";
            var description = "The worst thing that could happen";
            var sut = new WorkItem(title, description);
            //Act
            sut.HistoryAdd(history);
            //Assert
            Assert.AreEqual(history, sut.History[0]);
        }

        [TestMethod]
        public void WorkItem_Print_Prints_Correct_Info()
        {
            var sut = new FakeWorkItem
            {
                Title = "Bug Title"
            };
            //Act & Assert
            Assert.AreEqual($"ID: 1 Bug Title", sut.ToString());
        }
    }
    class FakeWorkItem : IWorkItem
    {
        private static long id = 1;
        public FakeWorkItem()
        {
            this.Id = id++;
        }
        public long Id { get; set; }

        public string Title { get; set; }

        public string Description => throw new NotImplementedException();

        public void Assign(IMember member)
        {
            throw new NotImplementedException();
        }

        public void CommentAdd(IComment comment)
        {
            throw new NotImplementedException();
        }

        public void HistoryAdd(string history)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return $"ID: {this.Id} {this.Title}";
        }

        public void Unassign()
        {
            throw new NotImplementedException();
        }
    }
    class FakeMember : IMember
    {
        public string Name {get;set;}

        public IList<IWorkItem> MemberAssignedWorkItems => throw new NotImplementedException();

        public IList<string> MemberActivityList => throw new NotImplementedException();

        public string Print()
        {
            throw new NotImplementedException();
        }
    }
}