﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WorkItemManagement.WorkItemManagement.Classes;

namespace UnitTestWorkitemManagementSystemTests.Classes
{
    [TestClass]
    public class Team_Should
    {
        [TestMethod]

        public void SetCorrectTeamName()
        {
            //Arrange
            var teamName = "TeamName1";

            //Act
            var newTeam = new Team(teamName);

            //Assert

            Assert.AreEqual(teamName, newTeam.Name);
        }
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]

        public void SetInvalidTeamName()
        {
            //Arrange
            var teamName = "Team";

            //Act
            var newTeam = new Team(teamName);

            //Assert

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => newTeam.Name);
        }
        [TestMethod]

        public void PrintCorrectTeamName()
        {
            //Arrange
            var teamName = "TeamName1";

            //Act
            var newTeam = new Team(teamName);
            var result = newTeam.ToString();

            //Assert
            Assert.AreEqual(teamName, newTeam.ToString());
        }
    }
}
