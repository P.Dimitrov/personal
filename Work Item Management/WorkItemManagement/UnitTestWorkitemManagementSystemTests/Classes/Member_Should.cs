﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WorkItemManagement.WorkItemManagement.Classes;

namespace UnitTestWorkitemManagementSystemTests.Classes
{
    [TestClass]
    public class Member_Should
    {
        [TestMethod]

        public void SetCorrectMemberName()
        {
            //Arrange
            var memberName = "RandomName";

            //Act
            var newMember = new Member(memberName);

            //Assert

            Assert.AreEqual(memberName, newMember.Name);
        }
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]

        public void SetInvalidMemberName()
        {
            //Arrange
            var memberName = "Bob";

            //Act
            var newMember = new Member(memberName);

            //Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => newMember.Name);
        }

        [TestMethod]

        public void PrintCorrectMemberName()
        {
            //Arrange
            var memberName = "MemberName1";


            //Act
            var newMember = new Member(memberName);
            var result = newMember.ToString();

            //Assert

            Assert.AreEqual(memberName, newMember.ToString());
        }
    }
}
