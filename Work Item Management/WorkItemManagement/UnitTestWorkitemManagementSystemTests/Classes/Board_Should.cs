﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WorkItemManagement.WorkItemManagement.Classes;

namespace UnitTestWorkitemManagementSystemTests.Classes
{
    [TestClass]
    public class Board_Should
    {
        [TestMethod]

        public void SetCorrectBoardName()
        {
            //Arrange
            var boardName = "BoardName1";

            //Act
            var newBoard = new Board(boardName);

            //Assert

            Assert.AreEqual(boardName, newBoard.Name);
        }
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]

        public void SetInvalidBoardName()
        {
            //Arrange
            var boardName = "one";

            //Act
            var newBoard = new Board(boardName);

            //Assert

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => newBoard.Name);
        }
        [TestMethod]

        public void PrintCorrectBoardName()
        {
            //Arrange
            var boardName = "BoardName1";


            //Act
            var newBoard = new Board(boardName);
            var result = newBoard.ToString();

            //Assert

            Assert.AreEqual(boardName, result);
        }
    }
}
