﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Classes
{
    [TestClass]
    public class Story_Should
    {
        [TestMethod]
        public void Story_Assigns_Assignee_Correctly()
        {
            //Arrange
            var title = "A Cool Story";
            var description = "Cool Story, Bro!";
            var priority = Priority.Medium;
            var size = Size.Medium;
            var status = StoryStatus.InProgress;
            var member = new Member("Gosho");
            //Act
            var sut = new Story(title, description, priority, size, status);
            sut.Assign(member);
            //Assert
            Assert.AreEqual(member, sut.Assignee);
        }

        [TestMethod]
        public void Story_Unassigns_Assignee_Correctly()
        {
            //Arrange
            var title = "A Cool Story";
            var description = "Cool Story, Bro!";
            var priority = Priority.Medium;
            var size = Size.Medium;
            var status = StoryStatus.InProgress;
            var member = new Member("Tisho");
            //Act
            var sut = new Story(title, description, priority, size, status);
            //Assert
            sut.Assign(member);
            Assert.AreEqual(member, sut.Assignee);
            sut.Unassign();
            Assert.AreEqual(default, sut.Assignee);
        }

        [TestMethod]
        public void Story_Prints_Correct_Info()
        {
            //Arrange
            var title = "A Cool Story";
            var description = "Cool Story, Bro!";
            var priority = Priority.Medium;
            var size = Size.Medium;
            var status = StoryStatus.InProgress;

            //Act
            var sut = new Story(title, description, priority, size, status);
            string print = $"Story - ID:{sut.Id} {sut.Title}" + Environment.NewLine +
                $"Description - {sut.Description}" + Environment.NewLine +
                $"Priority - {sut.Priority}" + Environment.NewLine +
                $"Size - {sut.Size}" + Environment.NewLine +
                $"Story Status - {sut.Status}";

            //Assert
            Assert.AreEqual(print, sut.ToString());
        }
    }
}
