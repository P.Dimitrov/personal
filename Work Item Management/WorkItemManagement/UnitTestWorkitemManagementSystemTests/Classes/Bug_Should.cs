﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Classes
{
    [TestClass]
    public class Bug_Should
    {
        [TestMethod]

        public void SetValidBugTitle()
        {
            //Arrange 
            var steps = new List<string>();

            //Act
            var newBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active);

            //Assert

            Assert.IsInstanceOfType(newBug, typeof(IBug));
        }
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]

        public void SetInvalidBugTitle()
        {
            //Arrange
            var steps = new List<string>();

            //Act
            var newBug = new Bug("Bugs", "This is a very serious bug!", steps, Priority.High,
                 Severity.Critical, BugStatus.Active);

            //Assert

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => newBug.Title);
        }
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]
        public void SetInvalidBugDescription()
        {
            //Arrange
            var steps = new List<string>();

            //Act
            var newBug = new Bug("Bugs Bunny", "Bad!", steps, Priority.High,
                Severity.Critical, BugStatus.Active);

            //Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => newBug.Description);
        }
    }
}
