﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.Core.Commands.Listing;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.Boards
{
    [TestClass]
    public class ShowBoardsActivityCommand_Should
    {
        [TestMethod]
        public void ShowBoardActivityCommand_ThrowsException_When_FewerThanRequiredParametersArePassed()
        {
            //Arrange
            var fakeParams = new List<string> { "Just one Parameter" };
            var fakeList = new Lists();

            //Act
            var sut = new ShowBoardsActivityCommand(fakeParams, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentException>(sut.Execute);
        }
        [TestMethod]
        public void ShowBoardActivityCommand_ThrowsException_When_TeamOrBoardDoesNotExist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeBoard = new Board("Board1");
            var fakeTeam = new Team("TeamName1");
            fakeList.Teams.Add(fakeTeam);
            fakeTeam.TeamBoards.Add(fakeBoard);
            var fakeParams1 = new List<string> { "Board1", "TeamName2" };
            var fakeParams2 = new List<string> { "Board2", "TeamName1" };

            //Act
            var sut1 = new ShowBoardsActivityCommand(fakeParams1, fakeList);
            var sut2 = new ShowBoardsActivityCommand(fakeParams2, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(sut1.Execute);
            Assert.ThrowsException<ArgumentNullException>(sut2.Execute);
        }
        [TestMethod]
        public void ShowBoardActivityCommand_ReturnsExpectedOutput_When_NoActivityPresentInBoard()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeBoard = new Board("Board2");
            var fakeTeam = new Team("TeamName2");
            fakeList.Teams.Add(fakeTeam);
            fakeTeam.TeamBoards.Add(fakeBoard);
            var fakeParams = new List<string> { "TeamName2", "Board2" };

            //Act
            var sut = new ShowBoardsActivityCommand(fakeParams, fakeList);

            //Assert
            Assert.AreEqual("There is no activity added to this Board", sut.Execute());
        }
        [TestMethod]
        public void ShowBoardActivityCommand_ReturnsExpectedOutput_When_ValidParamsArePassedAndThereAreWorkItemsToShow()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeBoard = new Board("Board2");
            var fakeTeam = new Team("TeamName2");
            fakeList.Teams.Add(fakeTeam);
            fakeTeam.TeamBoards.Add(fakeBoard);
            fakeBoard.BoardActivityList.Add("Board2 was created to Team TeamName2.");
            var fakeParams = new List<string> { "TeamName2", "Board2" };

            //Act
            var sut = new ShowBoardsActivityCommand(fakeParams, fakeList);
            var str = new StringBuilder();
            str.AppendLine("Board2's Activity: ");
            str.AppendLine($"** Board2 was created to Team TeamName2.");
            //Assert
            Assert.AreEqual(str.ToString().Trim(), sut.Execute());
        }
    }
}
