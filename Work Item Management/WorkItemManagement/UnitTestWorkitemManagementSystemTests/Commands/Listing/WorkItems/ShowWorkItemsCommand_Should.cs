﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.Core.Commands.Listing.WorkItems;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Enums;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.WorkItems
{
    [TestClass]
    public class ShowWorkItemsCommand_Should
    {
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void ShowWorkItems_ThrowsException_When_IncorrectNumberOfParamsAreGiven()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "bugs", "priority", "Size" };

            //Act & Assert
            var sut = new ShowWorkItemsCommand(fakeParams, fakeList);
            sut.Execute();
        }

        [TestMethod]
        public void ShowWorkItems_ThrowsException_When_WorkItemListsAreEmpty()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "bugs" };
            var fakeParams1 = new List<string> { "feedback" };
            var fakeParams2 = new List<string> { "story" };

            //Act & Assert
            var sut = new ShowWorkItemsCommand(fakeParams, fakeList);
            var sut1 = new ShowWorkItemsCommand(fakeParams1, fakeList);
            var sut2 = new ShowWorkItemsCommand(fakeParams2, fakeList);
            Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.ThrowsException<ArgumentException>(() => sut1.Execute());
            Assert.ThrowsException<ArgumentException>(() => sut2.Execute());
        }
        [TestMethod]
        public void ShowWorkItems_ListsAllItems_When_NoParamsGiven()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string>();
            var steps = new List<string> { "one", "two", "three" };
            var newBug = new Bug("Bugs Bunny", "Bugs are bad", steps, Priority.High,
                Severity.Critical, BugStatus.Active);
            fakeList.Bugs.Add(newBug);
            var newBug2 = new Bug("Daffy Duck", "Bugs are not so bad", steps, Priority.Low,
                Severity.Major, BugStatus.Fixed);
            fakeList.Bugs.Add(newBug2);

            string.Join<IWorkItem>(',', fakeList.Bugs);
            //Act & Assert
            var sut = new ShowWorkItemsCommand(fakeParams, fakeList);
        }
    }
}
