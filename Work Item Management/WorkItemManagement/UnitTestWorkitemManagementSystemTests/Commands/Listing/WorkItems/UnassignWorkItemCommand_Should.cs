﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.OtherCommands;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.WorkItems
{
    [TestClass]
    public class UnassignWorkItemCommand_Should
    {
        [TestMethod]
        public void UnassignWorkItemCommand_ThrowsException_When_ParamsAreNotValid()
        {
            var fakeList = new Lists();
            var fakeParams = new List<string> { "ivan" };
            var fakeParams2 = new List<string> { "ivan", "gosho" };
            var fakeParams3 = new List<string> { "ivan", "2" };
            var fakeParams4 = new List<string> { "todor", "1" };
            var fakeSteps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", fakeSteps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            var fakeMember = new Member("Todor");

            // Act 
            var sut = new UnassignWorkItemCommand(fakeParams, fakeList);
            var sut2 = new UnassignWorkItemCommand(fakeParams2, fakeList);
            var sut3 = new UnassignWorkItemCommand(fakeParams3, fakeList);
            var sut4 = new UnassignWorkItemCommand(fakeParams4, fakeList);

            // Assert
            Assert.ThrowsException<ArgumentException>(sut.Execute);
            Assert.ThrowsException<ArgumentException>(sut2.Execute);
            Assert.ThrowsException<ArgumentException>(sut3.Execute);
            Assert.ThrowsException<ArgumentException>(sut4.Execute);
        }

        [TestMethod]
        public void UnassignWorkItemCommand_Correctly_Removes_WorkItemAssignee()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "haralampi", "1" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("Haralampi");
            fakeList.Members.Add(fakeMember);

            // Act 
            var assign = new AssignWorkItemCommand(fakeParams, fakeList);
            assign.Execute();
            var sut = new UnassignWorkItemCommand(fakeParams, fakeList);
            sut.Execute();

            // Assert
            Assert.AreEqual(default, fakeBug.Assignee);
        }

        [TestMethod]
        public void AssignWorkItem_Correctly_Adds_To_MemberActivityList()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "100qn", "1" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("100qn");
            fakeList.Members.Add(fakeMember);

            // Act 
            var assign = new AssignWorkItemCommand(fakeParams, fakeList);
            assign.Execute();
            var sut = new UnassignWorkItemCommand(fakeParams, fakeList);
            sut.Execute();

            // Assert
            Assert.IsTrue(fakeMember.MemberActivityList.Contains("Bug 1Bugs Bunny unassigned"));
        }
        [TestMethod]
        public void AssignWorkItem_Correctly_Adds_To_WorkItemHistory()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "Stoyan", "1" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("Stoyan");
            fakeList.Members.Add(fakeMember);

            // Act 
            var assign = new AssignWorkItemCommand(fakeParams, fakeList);
            assign.Execute();
            var sut = new UnassignWorkItemCommand(fakeParams, fakeList);
            sut.Execute();

            // Assert
            Assert.IsTrue(fakeBug.History.Contains("Unassigned from Stoyan"));
        }
        [TestMethod]
        public void AssignWorkItem_Returns_ExpectedOutput()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "5ar40", "1" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("5ar40");
            fakeList.Members.Add(fakeMember);

            // Act 
            var assign = new AssignWorkItemCommand(fakeParams, fakeList);
            assign.Execute();
            var sut = new UnassignWorkItemCommand(fakeParams, fakeList);

            // Assert
            Assert.AreEqual("WorkItem 1 unassigned from 5ar40", sut.Execute());
        }
    }
}
