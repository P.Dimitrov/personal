﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.OtherCommands;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.WorkItems
{
    [TestClass]
    public class AssignWorkItemCommand_Should
    {
        [TestMethod]
        public void AssignWorkItem_ThrowsException_When_ParamsAreNotValid()
        {
            var fakeList = new Lists();
            var fakeParams = new List<string> { "misho"};
            var fakeParams2 = new List<string> { "misho", "gosho" };
            var fakeParams3 = new List<string> { "misho", "2" };
            var fakeParams4 = new List<string> { "tisho", "1" };
            var fakeSteps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", fakeSteps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            var fakeMember = new Member("Misho");

            // Act 
            var sut = new AssignWorkItemCommand(fakeParams, fakeList);
            var sut2 = new AssignWorkItemCommand(fakeParams2, fakeList);
            var sut3 = new AssignWorkItemCommand(fakeParams3, fakeList);
            var sut4 = new AssignWorkItemCommand(fakeParams4, fakeList);

            // Assert
            Assert.ThrowsException<ArgumentException>(sut.Execute);
            Assert.ThrowsException<ArgumentException>(sut2.Execute);
            Assert.ThrowsException<ArgumentException>(sut3.Execute);
            Assert.ThrowsException<ArgumentException>(sut4.Execute);
        }

        [TestMethod]
        public void AssignWorkItem_Correctly_Sets_WorkItemAssignee()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "pesho", "1" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("Pesho");
            fakeList.Members.Add(fakeMember);

            // Act 
            var sut = new AssignWorkItemCommand(fakeParams, fakeList);
            sut.Execute();

            // Assert
            Assert.AreEqual(fakeMember, fakeBug.Assignee);
        }

        [TestMethod]
        public void AssignWorkItem_Correctly_Adds_To_MemberActivityList()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "grisho", "1" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("Grisho");
            fakeList.Members.Add(fakeMember);

            // Act 
            var sut = new AssignWorkItemCommand(fakeParams, fakeList);
            sut.Execute();

            // Assert
            Assert.IsTrue(fakeMember.MemberActivityList.Contains("Bug 1Bugs Bunny assigned"));
        }
        [TestMethod]
        public void AssignWorkItem_Correctly_Adds_To_WorkItemHistory()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "grozdan", "1" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("Grozdan");
            fakeList.Members.Add(fakeMember);

            // Act 
            var sut = new AssignWorkItemCommand(fakeParams, fakeList);
            sut.Execute();

            // Assert
            Assert.IsTrue(fakeBug.History.Contains("Assigned to Grozdan"));
        }
        [TestMethod]
        public void AssignWorkItem_Returns_ExpectedOutput()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "mitko", "1" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("Mitko");
            fakeList.Members.Add(fakeMember);

            // Act 
            var sut = new AssignWorkItemCommand(fakeParams, fakeList);
            sut.Execute();

            // Assert
            Assert.AreEqual("WorkItem 1 assigned to Mitko", sut.Execute());
        }
    }
}
