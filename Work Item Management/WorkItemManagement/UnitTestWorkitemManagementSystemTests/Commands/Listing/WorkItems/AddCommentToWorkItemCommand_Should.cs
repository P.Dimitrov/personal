﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core.Commands.OtherCommands;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.WorkItems
{
    [TestClass]
    public class AddCommentToWorkItemCommand_Should
    {
        [TestMethod]
        public void AddCommentToWorkItem_ThrowsException_When_IsGivenInvalidCommands()
        {
            // Arrange
            var fakeList = new Lists();

            // Act
            var fakeParamsForAdding = new List<string> { "1", "blago", "napravih neshto" };
            var fakeParamsForAdding1 = new List<string> { "blago", "gosho", };
            var fakeParamsForAdding2 = new List<string> { "gosho" };

            var sut = new AddCommentToWorkItemCommand(fakeParamsForAdding, fakeList);
            var sut1 = new AddCommentToWorkItemCommand(fakeParamsForAdding1, fakeList);
            var sut2 = new AddCommentToWorkItemCommand(fakeParamsForAdding2, fakeList);

            // Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.ThrowsException<ArgumentException>(() => sut1.Execute());
            Assert.ThrowsException<ArgumentException>(() => sut2.Execute());
        }
        [TestMethod]
        public void AddCommentToWorkItem_ThrowsException_When_MemberDoesNotExist()
        {
            // Arrange
            var fakeList = new Lists();

            // Act 
            var fakeParamsForAdding = new List<string> { "ninja", "1", "napravih neshto" };
            var sut = new AddCommentToWorkItemCommand(fakeParamsForAdding, fakeList);

            // Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.Execute());
        }
        [TestMethod]
        public void AddCommentToWorkItem_ReturnsErrorOutput_If_WorkItemNotAssignedToMember()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "name1", "1", "napravih neshto" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("Name1");
            fakeList.Members.Add(fakeMember);

            // Act 
            var fakeParamsForAdding = new List<string> { "Name1", "1", "napravih neshto" };
            var sut = new AddCommentToWorkItemCommand(fakeParamsForAdding, fakeList);

            // Assert
            Assert.AreEqual("Work Item with ID 1 does not exist or is not assigned to member Name1",
                sut.Execute());
        }
        [TestMethod]
        public void AddCommentToWorkItem_ReturnsCorrectOutput_If_GivenValidInput()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "name2", "1", "napravih neshto" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("Name2");
            fakeList.Members.Add(fakeMember);

            var assignInput = new List<string> { "Name2", "1" };
            var assign = new AssignWorkItemCommand(assignInput, fakeList);
            assign.Execute();

            // Act 
            var fakeParamsForAdding = new List<string> { "Name2", "1", "napravih neshto" };
            var sut = new AddCommentToWorkItemCommand(fakeParamsForAdding, fakeList);

            // Assert
            Assert.AreEqual("Member Name2 added comment \"napravih neshto\" to Work Item ID 1",
                sut.Execute());
        }
        [TestMethod]
        public void AddCommentToWorkItem_AddsCommentToMemberActivityList_If_GivenValidInput()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "name3", "1", "napravih neshto" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("Name3");
            fakeList.Members.Add(fakeMember);

            var assignInput = new List<string> { "Name3", "1" };
            var assign = new AssignWorkItemCommand(assignInput, fakeList);
            assign.Execute();

            // Act 
            var fakeParamsForAdding = new List<string> { "Name3", "1", "napravih neshto" };
            var sut = new AddCommentToWorkItemCommand(fakeParamsForAdding, fakeList);
            sut.Execute();

            // Assert
            Assert.IsTrue(fakeMember.MemberActivityList.Contains("Added comment \"napravih neshto\" to Work Item ID 1"));
        }
        [TestMethod]
        public void AddCommentToWorkItem_AddsCommentToWorkItem_If_GivenValidInput()
        {
            // Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "name4", "1", "napravih neshto" };
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeList.Bugs.Add(fakeBug);
            var fakeMember = new Member("Name4");
            fakeList.Members.Add(fakeMember);

            var assignInput = new List<string> { "Name4", "1" };
            var assign = new AssignWorkItemCommand(assignInput, fakeList);
            assign.Execute();

            // Act 
            var commentToCheck = new Comment(fakeMember, "napravih neshto");
            var fakeParamsForAdding = new List<string> { "Name4", "1", "napravih neshto" };
            var sut = new AddCommentToWorkItemCommand(fakeParamsForAdding, fakeList);
            sut.Execute();

            // Assert
            Assert.IsTrue(fakeBug.Comments.Where(x => x.Commenter == fakeMember)
                .Any(x => x.CommentText == "napravih neshto"));
        }
    }
}
