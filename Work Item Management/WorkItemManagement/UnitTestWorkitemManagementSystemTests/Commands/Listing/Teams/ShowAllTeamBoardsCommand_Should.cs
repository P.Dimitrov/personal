﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Commands.Listing;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.Teams
{
    [TestClass]
    public class ShowAllTeamBoardsCommand_Should
    {
        [TestMethod]
        public void ShowAllTeamBoardsCommand_ThrowsException_When_NoParamsGiven()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { };

            //Act
            var sut = new ShowAllTeamBoardsCommand(fakeParams, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentException>(sut.Execute);
        }

        [TestMethod]
        public void ShowAllTeamBoardsCommand_ThrowsException_When_TeamDoesNotExist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "Team-z" };

            //Act
            var sut = new ShowAllTeamBoardsCommand(fakeParams, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(sut.Execute);
        }

        [TestMethod]
        public void ShowAllTeamBoardsCommand_ReturnsCorrectOutput_When_TeamDoesNotHaveBoards()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeTeam = new Team("Team-x");
            fakeList.Teams.Add(fakeTeam);
            var fakeParams = new List<string> { "Team-x" };

            //Act
            var sut = new ShowAllTeamBoardsCommand(fakeParams, fakeList);

            //Assert
            Assert.AreEqual("No boards have been added to team Team-x", sut.Execute());
        }

        [TestMethod]
        public void ShowAllTeamBoardsCommand_ListsAllTeamsBoards()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeTeam = new Team("Team-y");
            var fakeBoard = new Board("Board-y");
            var fakeBoard2 = new Board("Board-x");
            fakeTeam.TeamBoards.Add(fakeBoard);
            fakeTeam.TeamBoards.Add(fakeBoard2);
            fakeList.Teams.Add(fakeTeam);
            var fakeParams = new List<string> { "Team-y" };

            //Act
            var sut = new ShowAllTeamBoardsCommand(fakeParams, fakeList);
            var expectedOutput = "Boards of Team Team-y:" + Environment.NewLine +
                "Board-y" + Environment.NewLine +
                "Board-x";
            //Assert
            Assert.AreEqual(expectedOutput, sut.Execute());
        }
    }
}
