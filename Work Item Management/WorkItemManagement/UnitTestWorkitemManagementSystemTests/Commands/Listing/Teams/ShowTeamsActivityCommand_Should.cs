﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Listing;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.Teams
{
    [TestClass]
    public class ShowTeamsActivityCommand_Should
    {
        [TestMethod]
        public void ShowTeamsActivity_ThrowsException_WhenNoParamsGiven()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { };

            //Act
            var sut = new ShowTeamsActivityCommand(fakeParams, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentException>(sut.Execute);
        }

        [TestMethod]
        public void ShowTeamsActivity_ThrowsException_WhenTeamDoesNotExist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "Team-p" };

            //Act
            var sut = new ShowTeamsActivityCommand(fakeParams, fakeList);

            //Assert
            Assert.ThrowsException<NullReferenceException>(sut.Execute);
        }

        [TestMethod]
        public void ShowTeamsActivity_ReturnsCorrectOutput_When_TeamDoesNotHaveActivities()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeTeam = new Team("Team-p");
            fakeList.Teams.Add(fakeTeam);
            var fakeParams = new List<string> { "Team-p" };

            //Act
            var sut = new ShowTeamsActivityCommand(fakeParams, fakeList);

            //Assert
            Assert.AreEqual("There is no activity within this team", sut.Execute());
        }

        [TestMethod]
        public void ShowTeamsActivityCommand_ListsAllActivities()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeTeam = new Team("Team-h");
            var fakeActivity = "Team was created";
            var fakeActivity2 = "Team was deleted";
            fakeTeam.TeamActivityList.Add(fakeActivity);
            fakeTeam.TeamActivityList.Add(fakeActivity2);
            fakeList.Teams.Add(fakeTeam);
            var fakeParams = new List<string> { "Team-h" };

            //Act
            var sut = new ShowTeamsActivityCommand(fakeParams, fakeList);
            var expectedOutput = "Team-h's Activity: " + Environment.NewLine +
                "** Team was created" + Environment.NewLine +
                "** Team was deleted";

            //Assert
            Assert.AreEqual(expectedOutput, sut.Execute());
        }
    }
}
