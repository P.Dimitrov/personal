﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Listing;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.Teams
{
    [TestClass]
    public class ShowAllTeamsCommand_Should
    {
        [TestMethod]
        public void ShowAllTeamsCommand_ReturnsCorrectOutput_If_NoTeamsExist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string>();

            //Act
            var sut = new ShowAllTeamsCommand(fakeParams, fakeList);

            //Assert
            Assert.AreEqual("No teams exist in the current database", sut.Execute());
        }

        [TestMethod]
        public void ShowAllTeamsCommand_ShowsAllMembersIfSeveralTeamsExist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string>();
            var team1 = new Team("Team-q");
            var team2 = new Team("Team-t");
            fakeList.Teams.Add(team1);
            fakeList.Teams.Add(team2);

            //Act
            var sut = new ShowAllTeamsCommand(fakeParams, fakeList);

            //Assert
            Assert.AreEqual($"Team-q "+Environment.NewLine+"Team-t", sut.Execute());
        }
    }
}
