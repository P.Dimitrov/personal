﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Commands.Listing;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.Teams
{
    [TestClass]
    public class ShowAllTeamMembersCommand_Should
    {
        [TestMethod]
        public void ShowAllTeamMembers_ThrowsException_WhenNoParamsGiven()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { };

            //Act
            var sut = new ShowAllTeamMembersCommand(fakeParams, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentException>(sut.Execute);
        }

        [TestMethod]
        public void ShowAllTeamMembers_ThrowsException_WhenTeamDoesNotExist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "Team-l" };

            //Act
            var sut = new ShowAllTeamMembersCommand(fakeParams, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(sut.Execute);
        }

        [TestMethod]
        public void ShowAllTeamMembersCommand_ReturnsCorrectOutput_When_TeamDoesNotHaveMembers()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeTeam = new Team("Team-k");
            fakeList.Teams.Add(fakeTeam);
            var fakeParams = new List<string> { "Team-k" };

            //Act
            var sut = new ShowAllTeamMembersCommand(fakeParams, fakeList);

            //Assert
            Assert.AreEqual("There are no members added to team Team-k", sut.Execute());
        }

        [TestMethod]
        public void ShowAllTeamMembersCommand_ListsAllMembers()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeTeam = new Team("Team-k");
            var fakeMember = new Member("R2-D2");
            var fakeMember2 = new Member("C-3PO");
            fakeTeam.TeamMembers.Add(fakeMember);
            fakeTeam.TeamMembers.Add(fakeMember2);
            fakeList.Teams.Add(fakeTeam);
            var fakeParams = new List<string> { "Team-k" };

            //Act
            var sut = new ShowAllTeamMembersCommand(fakeParams, fakeList);
            var expectedOutput = "Members of Team Team-k:" + Environment.NewLine +
                "R2-D2" + Environment.NewLine +
                "C-3PO";

            //Assert
            Assert.AreEqual(expectedOutput, sut.Execute());
        }
    }
}
