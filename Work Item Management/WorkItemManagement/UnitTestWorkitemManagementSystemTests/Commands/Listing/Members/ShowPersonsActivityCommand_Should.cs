﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Listing;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.Members
{
    [TestClass]
    public class ShowPersonsActivityCommand_Should
    {
        [TestMethod]
        public void ShowPersonsActivityCommand_ThrowsException_If_NoNameGiven()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string>();

            //Act
            var sut = new ShowPersonsActivityCommand(fakeParams, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentException>(sut.Execute);
        }

        [TestMethod]
        public void ShowPersonsActivityCommand_ThrowsException_If_MemberDoesNotExist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "Evgeni" };

            //Act
            var sut = new ShowPersonsActivityCommand(fakeParams, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(sut.Execute);
        }
        [TestMethod]
        public void ShowPersonsActivityCommand_ReturnsCorrectOutput_If_MemberDoesNotHaveAssignedWorkItems()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeMember = new Member("Evgeni");
            fakeList.Members.Add(fakeMember);
            var fakeParams = new List<string> { "Evgeni" };

            //Act
            var sut = new ShowPersonsActivityCommand(fakeParams, fakeList);
            
            //Assert
            Assert.AreEqual("Member does not have any activity assigned", sut.Execute());
        }
        [TestMethod]
        public void ShowPersonsActivityCommand_ListsMembersAssignedWorkItems()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeMember = new Member("Blagoi");
            fakeList.Members.Add(fakeMember);
            var fakeParams = new List<string> { "Blagoi" };

            var steps = new List<string>();
            var newBug = new Bug("Bugs Bunny", "Bad and getting worse!", steps, Priority.High,
            Severity.Critical, BugStatus.Active);
            var newBug2 = new Bug("Daffy Duck", "Unsadnesslessly!", steps, Priority.Low,
            Severity.Minor, BugStatus.Fixed);

            fakeMember.MemberAssignedWorkItems.Add(newBug);
            fakeMember.MemberAssignedWorkItems.Add(newBug2);

            var expectedOutput = string.Join(Environment.NewLine, "Blagoi's Activity: ", $"** {newBug.ToString()}", $"** {newBug2.ToString()}");
            var sut = new ShowPersonsActivityCommand(fakeParams, fakeList);

            //Assert
            Assert.AreEqual(expectedOutput, sut.Execute());
        }
    }
}
