﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Listing;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.Members
{
    [TestClass]
    public class ShowAllMembersCommand_Should
    {
        [TestMethod]
        public void ShowAllMembersCommand_ReturnsCorrectOutput_If_NoMembersExist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string>();

            //Act
            var sut = new ShowAllMembersCommand(fakeParams, fakeList);

            //Assert
            Assert.AreEqual("No Members exist in the current database.", sut.Execute());
        }

        [TestMethod]
        public void ShowAllMembersCommand_ShowsAllMembersIfSeveralMembersExist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string>();
            var fakeMember1 = new Member("Member-e");
            var fakeMember2 = new Member("Member-f");
            fakeList.Members.Add(fakeMember1);
            fakeList.Members.Add(fakeMember2);

            //Act
            var sut = new ShowAllMembersCommand(fakeParams, fakeList);

            //Assert
            Assert.AreEqual("Member-e " + Environment.NewLine + "Member-f", sut.Execute());
        }
    }
}
