﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;

namespace UnitTestWorkitemManagementSystemTests.Commands.Listing.Members
{
    [TestClass]
    public class AddMemberCommand_Should
    {
        [TestMethod]
        public void AddMemberCommand_ThrowsException_When_WrongNumberOfParamsGiven()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeParams = new List<string> { "Gosho" };

            //Act
            var sut = new AddMemberCommand(fakeParams, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentException>(sut.Execute);
        }

        [TestMethod]
        public void AddMemberCommand_ThrowsException_When_MemberOrTeamDoNotExist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeMember = new Member("Member-a");
            var fakeTeam = new Team("Team-a");
            fakeList.Members.Add(fakeMember);
            fakeList.Teams.Add(fakeTeam);
            var fakeParams1 = new List<string> { "Member-a", "Team-b" };
            var fakeParams2 = new List<string> { "Member-b", "Team-a" };

            //Act
            var sut1 = new AddMemberCommand(fakeParams1, fakeList);
            var sut2 = new AddMemberCommand(fakeParams2, fakeList);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(sut1.Execute);
            Assert.ThrowsException<ArgumentNullException>(sut2.Execute);
        }

        [TestMethod]
        public void AddMemberCommand_AddsMemberToTeam()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeMember = new Member("Member-b");
            var fakeTeam = new Team("Team-b");
            fakeList.Members.Add(fakeMember);
            fakeList.Teams.Add(fakeTeam);
            var fakeParams = new List<string> { "Member-b", "Team-b" };

            //Act
            var sut = new AddMemberCommand(fakeParams, fakeList);
            sut.Execute();
            //Assert
            Assert.IsTrue(fakeTeam.TeamMembers.Contains(fakeMember));
        }
        
        [TestMethod]
        public void AddMemberCommand_ReturnsCorrectedOutput()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeMember = new Member("Member-c");
            var fakeTeam = new Team("Team-c");
            fakeList.Members.Add(fakeMember);
            fakeList.Teams.Add(fakeTeam);
            var fakeParams = new List<string> { "Member-c", "Team-c" };

            //Act
            var sut = new AddMemberCommand(fakeParams, fakeList);

            //Assert
            Assert.AreEqual("Member Member-c added to Team Team-c", sut.Execute());
        }

        [TestMethod]
        public void AddMemberCommand_AddsCommentToMemberAndTeam()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeMember = new Member("Member-d");
            var fakeTeam = new Team("Team-d");
            fakeList.Members.Add(fakeMember);
            fakeList.Teams.Add(fakeTeam);
            var fakeParams = new List<string> { "Member-d", "Team-d" };

            //Act
            var sut = new AddMemberCommand(fakeParams, fakeList);
            sut.Execute();
            //Assert
            Assert.IsTrue(fakeMember.MemberActivityList.Contains("Added to Team Team-d."));
            Assert.IsTrue(fakeTeam.TeamActivityList.Contains("Member Member-d added to Team."));
        }
    }
}
