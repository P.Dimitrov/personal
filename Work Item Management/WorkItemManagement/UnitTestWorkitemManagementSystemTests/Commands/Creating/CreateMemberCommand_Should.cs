﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkItemManagement.Core.Factories;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Providers;
using System;

namespace UnitTestWorkitemManagementSystemTests.Commands.Creating
{
    [TestClass]
    public class CreateMemberCommand_Should
    {
        [TestMethod]

        public void ParseValidCommand()
        {
            //Arrange
            var member = "RandomMember1";
            var factory = new WorkItemFactory();

            //Act
            var newMember = factory.CreateMember(member);

            //Assert

            Assert.IsInstanceOfType(newMember, typeof(IMember));
        }


        [TestMethod]
        public void ThrowExceptionWhenTheSameMemberNameIsUsed()
        {
            // Arrange

            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var firstMember = factory.CreateMember("RandomMember2");
            testlists.AddMember(firstMember);
            var command = new string[] { "RandomMember2" };

            //Act 
            var sut = new CreateMemberCommand(command, testlists);

            //Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Execute());
        }

        [TestMethod]
        public void AddMemberToMemberLists()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var firstMember = factory.CreateMember("RandomMember");

            //Act 
            testlists.AddMember(firstMember);

            //Assert
            Assert.IsTrue(testlists.Members.Contains(firstMember));
        }

        [TestMethod]
        public void PrintAfterCreatingMember()
        {
            // Arrange
            var testlists = new Lists();
            var command = new string[] { "RandomMember4" };
            var newMember = new CreateMemberCommand(command, testlists);
            string commandPrint = "Created RandomMember4";

            //Act 
            var sut = newMember.Execute();

            //Assert
            Assert.AreEqual(commandPrint, sut);
        }
    }

}
