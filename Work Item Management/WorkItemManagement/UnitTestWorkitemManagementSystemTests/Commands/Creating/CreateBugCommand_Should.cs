﻿using System;
using WorkItemManagement.Core.Factories;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkItemManagement.WorkItemManagement.Classes;
using System.Collections.Generic;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Creating
{
    [TestClass]
    public class CreateBugCommand_Should
    {
        [TestMethod]

        public void ParseValidCommand()
        {
            //Arrange
            var title = "BugTitle123";
            var description = "Very, very bad.";
            List<string> steps = new List<string>();
            Priority priorityLevel = Priority.Medium;
            Severity severityLevel = Severity.Major;
            BugStatus statusType = BugStatus.Active;
            var factory = new WorkItemFactory();

            //Act
            var newBug = factory.CreateBug(title, description, steps, priorityLevel, severityLevel, statusType);

            //Assert
            Assert.IsInstanceOfType(newBug, typeof(IBug));
        }

        [TestMethod]
        public void ThrowExceptionWhenThereIsNoTeamForThisBug()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var command = new string[] { "BugTitle123", "Very, very bad.",
            "I cannot log in the site", "medium", "major", "active", "Board123", "RandomName" };

            //Act 
            var sut = new CreateBugCommand(command, testlists);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.Execute());
        }

        [TestMethod]
        public void ThrowExceptionWhenThereIsNoBoardForThisBug()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var currentTeam = new Team("RandomName");
            testlists.AddTeam(currentTeam);
            var command = new string[] { "BugTitle123", "Very, very bad.", 
            "I cannot log in the site", "medium", "major", "active", "Board123", "RandomName" };

            //Act 
            var sut = new CreateBugCommand(command, testlists);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.Execute());
        }

        [TestMethod]
        public void AddBugToBoardWorkIIemLists()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var currentTeam = new Team("RandomName");
            testlists.AddTeam(currentTeam);
            var currentBoard = new Board("Board123");
            var firstBoard = factory.CreateBoard("Board123");
            currentTeam.TeamBoards.Add(firstBoard);
            var title = "BugTitle123";
            var description = "Very, very bad.";
            List<string> steps = new List<string>();
            Priority priorityLevel = Priority.Medium;
            Severity severityLevel = Severity.Major;
            BugStatus statusType = BugStatus.Active;
            var newBug = factory.CreateBug(title, description, steps, priorityLevel, severityLevel, statusType);

            //Act 
            currentBoard.Add(newBug);

            //Assert
            Assert.IsTrue(currentBoard.BoardWorkItems.Contains(newBug));
        }
        [TestMethod]
        public void PrintAfterCreatingBug()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var currentTeam = factory.CreateTeam("RandomName");
            testlists.AddTeam(currentTeam);
            var currentBoard = factory.CreateBoard("Board345");
            currentTeam.TeamBoards.Add(currentBoard);
            var command = new string[] { "BugTitle123", "Very, very bad.",
            "I cannot log in the site", "medium", "major", "active", "RandomName", "Board345" };
            var newBug = new CreateBugCommand(command, testlists);
            string commandPrint = $"Created Bug - ID:12 BugTitle123" + Environment.NewLine +
                $"Description - Very, very bad." + Environment.NewLine +
                $"Steps - I cannot log in the site" + Environment.NewLine +
                $"Priority - Medium" + Environment.NewLine +
                $"Severity - Major" + Environment.NewLine +
                $"BugStatus - Active" + Environment.NewLine +
                $"Added to Team - RandomName in Board - Board345";

            //Act 
            var sut = newBug.Execute();

            //Assert
            Assert.AreEqual(commandPrint, sut);
        }
    }
}
