﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkItemManagement.Core.Factories;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Creating
{
    [TestClass]
    public class CreateStoryCommand_Should
    {
        [TestMethod]

        public void ParseValidCommand()
        {
            //Arrange
            var title = "StoryName123";
            var description = "There should be a long story here.";
            Priority priorityLevel = Priority.Low;
            Size storySize = Size.Small;
            StoryStatus statusType = StoryStatus.InProgress;
            var factory = new WorkItemFactory();

            //Act
            var newStory = factory.CreateStory(title, description, priorityLevel, storySize, statusType);

            //Assert
            Assert.IsInstanceOfType(newStory, typeof(IStory));
        }

        [TestMethod]
        public void ThrowExceptionWhenThereIsNoTeamForThisStory()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var command = new string[] { "StoryName123", "There should be a long story here.", "low", "small", "inprogress", "Board123", "RandomName" };

            //Act 
            var sut = new CreateStoryCommand(command, testlists);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.Execute());
        }

        [TestMethod]
        public void ThrowExceptionWhenThereIsNoBoardForThisStory()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var currentTeam = new Team("RandomName");
            testlists.AddTeam(currentTeam);
            var command = new string[] { "StoryName123", "There should be a long story here.", "low", "small", "inprogress", "Board123", "RandomName" };

            //Act 
            var sut = new CreateStoryCommand(command, testlists);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.Execute());
        }

        [TestMethod]
        public void AddStoryToBoardWorkIIemLists()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var currentTeam = new Team("RandomName");
            testlists.AddTeam(currentTeam);
            var currentBoard = new Board("Board123");
            var firstBoard = factory.CreateBoard("Board123");
            currentTeam.TeamBoards.Add(firstBoard);
            var title = "StoryName123";
            var description = "There should be a long story here.";
            Priority priorityLevel = Priority.Low;
            Size storySize = Size.Small;
            StoryStatus statusType = StoryStatus.InProgress;
            var newStory = factory.CreateStory(title, description, priorityLevel, storySize, statusType);

            //Act 
            currentBoard.Add(newStory);

            //Assert
            Assert.IsTrue(currentBoard.BoardWorkItems.Contains(newStory));
        }
        //[TestMethod]
        //public void PrintAfterCreatingBug()
        //{
        //    // Arrange
        //    var testlists = new Lists();
        //    var factory = new WorkItemFactory();
        //    var currentTeam = factory.CreateTeam("RandomName");
        //    testlists.AddTeam(currentTeam);
        //    var currentBoard = factory.CreateBoard("Board345");
        //    currentTeam.TeamBoards.Add(currentBoard);
        //    var command = new string[] { "BugTitle123", "Very, very bad.",
        //    "I cannot log in the site", "medium", "major", "active", "Board345", "RandomName" };
        //    var newBug = new CreateBugCommand(command, testlists);
        //    string commandPrint = $"Created BugTitle123" + Environment.NewLine +
        //        $"Added to Team - RandomName in Board - Board345";

        //    //Act 
        //    var sut = newBug.Execute();

        //    //Assert
        //    Assert.AreEqual(commandPrint, sut);
        //}
    }
}
