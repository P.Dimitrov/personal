﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WorkItemManagement.Core.Factories;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Creating
{
    [TestClass]
    public class CreateFeedbackCommand_Should
    {
        [TestMethod]

        public void ParseValidCommand()
        {
            //Arrange
            var title = "Feedback123";
            var description = "Great program.";
            var rating = 10;
            FeedbackStatus statusType = FeedbackStatus.Scheduled;
            var factory = new WorkItemFactory();

            //Act
            var newFeedback = factory.CreateFeedback(title, description,rating, statusType);

            //Assert
            Assert.IsInstanceOfType(newFeedback, typeof(IFeedback));
        }

        [TestMethod]
        public void ThrowExceptionWhenThereIsNoTeamForThisFeedback()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var command = new string[] { "Feedback123", "Great program.", "10", "scheduled", "Board123", "RandomName" };

            //Act 
            var sut = new CreateFeedbackCommand(command, testlists);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.Execute());
        }

        [TestMethod]
        public void ThrowExceptionWhenThereIsNoBoardForThisFeedback()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var currentTeam = new Team("RandomName");
            testlists.AddTeam(currentTeam);
            var command = new string[] { "Feedback123", "Great program.", "10", "scheduled", "Board123", "RandomName" };

            //Act 
            var sut = new CreateFeedbackCommand(command, testlists);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.Execute());
        }

        [TestMethod]
        public void AddFeedbackToBoardWorkIIemLists()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var currentTeam = new Team("RandomName");
            testlists.AddTeam(currentTeam);
            var currentBoard = new Board("Board123");
            var firstBoard = factory.CreateBoard("Board123");
            currentTeam.TeamBoards.Add(firstBoard);
            var title = "Feedback123";
            var description = "Great program.";
            var rating = 10;
            FeedbackStatus statusType = FeedbackStatus.Scheduled;
            var newFeedback = factory.CreateFeedback(title, description, rating, statusType);

            //Act 
            currentBoard.Add(newFeedback);

            //Assert
            Assert.IsTrue(currentBoard.BoardWorkItems.Contains(newFeedback));
        }
        //[TestMethod]
        //public void PrintAfterCreatingBug()
        //{
        //    // Arrange
        //    var testlists = new Lists();
        //    var factory = new WorkItemFactory();
        //    var currentTeam = factory.CreateTeam("RandomName");
        //    testlists.AddTeam(currentTeam);
        //    var currentBoard = factory.CreateBoard("Board345");
        //    currentTeam.TeamBoards.Add(currentBoard);
        //    var command = new string[] { "BugTitle123", "Very, very bad.",
        //    "I cannot log in the site", "medium", "major", "active", "Board345", "RandomName" };
        //    var newBug = new CreateBugCommand(command, testlists);
        //    string commandPrint = $"Created BugTitle123" + Environment.NewLine +
        //        $"Added to Team - RandomName in Board - Board345";

        //    //Act 
        //    var sut = newBug.Execute();

        //    //Assert
        //    Assert.AreEqual(commandPrint, sut);
        //}
    }
}
