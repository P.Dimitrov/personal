﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Factories;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Contracts;

namespace UnitTestWorkitemManagementSystemTests.Commands.Creating
{
    [TestClass]
    public class CreateTeamCommand_Should
    {
        [TestMethod]

        public void ParseValidCommand()
        {
            //Arrange
            var teamName = "RandomName";
            var factory = new WorkItemFactory();

            //Act
            var newTeam = factory.CreateTeam(teamName);

            //Assert
            Assert.IsInstanceOfType(newTeam, typeof(ITeam));
        }

        [TestMethod]
         public void ThrowExceptionWhenTheSameTeamNameIsUsed()
        {
            // Arrange

            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var firstTeam = factory.CreateTeam("RandomTeam");
            testlists.AddTeam(firstTeam);
            var command = new string[] { "RandomTeam" };

            //Act 
            var sut = new CreateTeamCommand(command, testlists);

            //Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Execute());
        }

        [TestMethod]
        public void AddTeamToTeamLists()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var firstTeam = factory.CreateTeam("RandomTeam");

            //Act 
            testlists.AddTeam(firstTeam);

            //Assert
            Assert.IsTrue(testlists.Teams.Contains(firstTeam));
        }

        [TestMethod]
        public void PrintAfterCreatingTeam()
        {
            // Arrange
            var testlists = new Lists();
            var command = new string[] { "RandomTeam" };
            var newTeam = new CreateTeamCommand(command, testlists);
            string commandPrint = "Created team RandomTeam";

            //Act 
            var sut = newTeam.Execute();

            //Assert
            Assert.AreEqual(commandPrint, sut);
        }
    }

    
}
