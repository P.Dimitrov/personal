﻿using System;
using WorkItemManagement.Core.Factories;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkItemManagement.WorkItemManagement.Classes;

namespace UnitTestWorkitemManagementSystemTests.Commands.Creating
{
    [TestClass]
    public class CreateBoardCommand_Should
    {
        [TestMethod]

        public void ParseValidCommand()
        {
            //Arrange
            var board = "Board123d";
            var factory = new WorkItemFactory();

            //Act
            var newBoard = factory.CreateBoard(board);

            //Assert
            Assert.IsInstanceOfType(newBoard, typeof(IBoard));
        }


        [TestMethod]
        public void ThrowExceptionWhenTheSameBoardNameIsUsed()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var currentTeam = new Team("RandomName");
            testlists.AddTeam(currentTeam);
            var firstBoard = factory.CreateBoard("Board123");
            currentTeam.TeamBoards.Add(firstBoard);
            var command = new string[] { "Board123", "RandomName" };

            //Act 
            var sut = new CreateBoardCommand(command, testlists);

            //Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Execute());
        }

        [TestMethod]
        public void AddBoardToBoardLists()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var currentTeam = new Team("RandomName");
            testlists.AddTeam(currentTeam);
            var firstBoard = factory.CreateBoard("Board123");

            //Act 
            currentTeam.TeamBoards.Add(firstBoard);

            //Assert
            Assert.IsTrue(currentTeam.TeamBoards.Contains(firstBoard));
        }

        [TestMethod]
        public void ThrowExceptionWhenThereIsNoTeamForThisBoard()
        {
            // Arrange
            var testlists = new Lists();
            var factory = new WorkItemFactory();
            var command = new string[] { "Board123", "RandomName" };
            var newBoard = new CreateBoardCommand(command, testlists);

            //Act 
            var sut = new CreateBoardCommand(command, testlists);

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => newBoard.Execute());
        }

        [TestMethod]
        public void PrintAfterCreatingBoard()
        {
            // Arrange
            var testlists = new Lists();
            var currentTeam = new Team("RandomName");
            testlists.AddTeam(currentTeam);
            var command = new string[] { "Board123", "RandomName" };
            var newBoard = new CreateBoardCommand(command, testlists);
            string commandPrint = "Created board Board123 in team RandomName";

            //Act 
            var sut = newBoard.Execute();

            //Assert
            Assert.AreEqual(commandPrint, sut);
        }
    }
}
