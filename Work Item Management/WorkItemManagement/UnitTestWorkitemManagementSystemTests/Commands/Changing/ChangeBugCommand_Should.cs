﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Changing
{
    [TestClass]
    public class ChangeBugCommand_Should
    {
        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void ChangeBugCommand_Throws_Exception_When_Bug_Does_Not_Exist()
        {
            //Arrange
            FakeLists fakeList = new FakeLists();
            var fakeBug = new FakeBug
            {
                Status = BugStatus.Active,
                Id = 1
            };

            fakeList.Bugs.Add(fakeBug);
            var fakeParams = new List<string>() { "5", "fixed" };

            //Act & Assert
            var command = new ChangeBugCommand(fakeParams, fakeList);
            command.Execute();
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void ChangeBugCommand_Throws_Exception_When_Params_are_Invalid()
        {
            FakeLists fakeList = new FakeLists();
            var fakeBug = new FakeBug();
            fakeBug.Status = BugStatus.Active;
            fakeList.Bugs.Add(fakeBug);
            var fakeParams = new List<string>() { "1" };
            //Act 
            var sut = new ChangeBugCommand(fakeParams, fakeList);
            //Assert
            sut.Execute();
        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void ChangeBugCommand_Throws_Exception_When_Change_IS_Invalid()
        {
            FakeLists fakeList = new FakeLists();
            var fakeBug = new FakeBug();
            fakeBug.Status = BugStatus.Active;
            fakeList.Bugs.Add(fakeBug);
            var fakeParams = new List<string>() { "1", "asdasd" };

            //Act & Assert
            var sut = new ChangeBugCommand(fakeParams, fakeList);
            sut.Execute();
        }

        [TestMethod]
        public void ChangeBugCommand_Correctly_Changes_Properties()
        {
            var fakeList = new Lists();
            var steps = new List<string>();
            var fakeBug = new Bug("Bugs Bunny", "This is a very serious bug!", steps, Priority.High,
                Severity.Critical, BugStatus.Active)
            {
                Id = 1
            };
            fakeBug.Status = BugStatus.Active;
            fakeBug.PriorityLevel = Priority.High;
            fakeBug.Id = 1;
            fakeList.Bugs.Add(fakeBug);
            var fakeParams = new List<string>() { "1", "fixed" };
            var fakeParams2 = new List<string>() { "1", "low" };
            //Act
            var sut = new ChangeBugCommand(fakeParams, fakeList);
            sut.Execute();
            var sut2 = new ChangeBugCommand(fakeParams2, fakeList);
            sut2.Execute();
            //Act & Assert
            Assert.AreEqual(BugStatus.Fixed, fakeBug.Status);
            Assert.AreEqual(Priority.Low, fakeBug.PriorityLevel);
        }
    }

    class FakeLists : ILists
    {
        private IList<IBug> bugs = new List<IBug>();
        public IList<IMember> Members => throw new NotImplementedException();

        public IList<ITeam> Teams => throw new NotImplementedException();

        public IList<IBug> Bugs
        {
            get
            {
                return this.bugs;
            }
        }

        public IList<IStory> Story => throw new NotImplementedException();

        public IList<IFeedback> Feedback => throw new NotImplementedException();

        public void AddMember(IMember members)
        {
            throw new NotImplementedException();
        }

        public void AddTeam(ITeam teams)
        {
            throw new NotImplementedException();
        }

        public void AddWorkItem(IBug bug)
        {
            throw new NotImplementedException();
        }

        public void AddWorkItem(IFeedback feedback)
        {
            throw new NotImplementedException();
        }

        public void AddWorkItem(IStory story)
        {
            throw new NotImplementedException();
        }
    }

    class FakeBug : IBug
    {
        public List<string> Steps => throw new NotImplementedException();

        public Priority PriorityLevel { get; set; }
        public Severity SeverityLevel { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public BugStatus Status { get; set; }

        public IMember Assignee => throw new NotImplementedException();

        public long Id { get; set; }

        public string Title
        {
            get; set;
        }
        public string Description
        {
            get;
            set;
        }

        public void Assign(IMember member)
        {
            throw new NotImplementedException();
        }

        public void CommentAdd(IComment comment)
        {
            throw new NotImplementedException();
        }

        public void HistoryAdd(string history)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            throw new NotImplementedException();
        }

        public void Unassign()
        {
            throw new NotImplementedException();
        }
    }
}
