﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Changing
{
    [TestClass]
    public class ChangeStoryCommand_Should
    {
        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void ChangeStoryCommand_Throws_Exception_When_Story_Does_Not_Exist()
        {
            //Arrange
            var fakeList = new Lists();
            var fakeStory = new FakeStory
            {
                Status = StoryStatus.InProgress,
                Id = 1
            };

            fakeList.Story.Add(fakeStory);
            var fakeParams = new List<string>() { "5", "fixed" };

            //Act & Assert
            var command = new ChangeStoryCommand(fakeParams, fakeList);
            command.Execute();
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void ChangeStoryCommand_Throws_Exception_When_Params_are_Invalid()
        {
            var fakeList = new Lists();
            var fakeStory = new FakeStory
            {
                Status = StoryStatus.InProgress,
                Id = 1
            };
            fakeList.Story.Add(fakeStory);
            var fakeParams = new List<string>() { "1" };
            //Act 
            var sut = new ChangeStoryCommand(fakeParams, fakeList);
            //Assert
            sut.Execute();
        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void ChangeStoryCommand_Throws_Exception_When_Change_IS_Invalid()
        {
            var fakeList = new Lists();
            var fakeStory = new FakeStory
            {
                Status = StoryStatus.InProgress,
                Id = 1
            };
            fakeList.Story.Add(fakeStory);
            var fakeParams = new List<string>() { "1", "asdasd" };

            //Act & Assert
            var sut = new ChangeStoryCommand(fakeParams, fakeList);
            sut.Execute();
        }

        [TestMethod]
        public void ChangeStoryCommand_Correctly_Changes_Properties()
        {
            var fakeList = new Lists();
            var fakeStory = new Story("Fake Story", "FNN Fake News Network", Priority.Low, Size.Medium, StoryStatus.InProgress)
            {
                Id = 1
            };
            fakeList.Story.Add(fakeStory);
            var fakeParams = new List<string>() { "1", "done" };
            var fakeParams2 = new List<string>() { "1", "large" };
            //Act
            var sut = new ChangeStoryCommand(fakeParams, fakeList);
            sut.Execute();
            var sut2 = new ChangeStoryCommand(fakeParams2, fakeList);
            sut2.Execute();
            //Act & Assert
            Assert.AreEqual(StoryStatus.Done, fakeStory.Status);
            Assert.AreEqual(Size.Large, fakeStory.Size);
        }
    }
    class FakeStory : IStory
    {
        public Priority Priority { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Size Size { get; set; }
        public StoryStatus Status { get; set; }

        public IMember Assignee => throw new NotImplementedException();

        public long Id { get; set; }

        public string Title { get; set; }

        public string Description => throw new NotImplementedException();

        public void Assign(IMember member)
        {
            throw new NotImplementedException();
        }

        public void CommentAdd(IComment comment)
        {
            throw new NotImplementedException();
        }

        public void HistoryAdd(string history)
        {
            throw new NotImplementedException();
        }

        public void Unassign()
        {
            throw new NotImplementedException();
        }
    }
}
