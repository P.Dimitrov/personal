﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Commands.Creating;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Providers;
using WorkItemManagement.WorkItemManagement.Classes;
using WorkItemManagement.WorkItemManagement.Contracts;
using WorkItemManagement.WorkItemManagement.Enums;

namespace UnitTestWorkitemManagementSystemTests.Commands.Changing
{
    [TestClass]
    public class ChangeFeedbackCommand_Should
    {
        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void ChangeFeedbackCommand_Throws_Exception_When_Feedback_Does_Not_Exist()
        {
            //Arrange
            FakeFeedbackLists fakeList = new FakeFeedbackLists();
            var fakeFeedback = new FakeFeedback
            {
                Status = FeedbackStatus.Scheduled,
                Id = 1
            };

            fakeList.Feedback.Add(fakeFeedback);
            var fakeParams = new List<string>() { "5", "fixed" };

            //Act & Assert
            var command = new ChangeFeedbackCommand(fakeParams, fakeList);
            command.Execute();
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void ChangeFeedbackCommand_Throws_Exception_When_Params_are_Invalid()
        {
            FakeFeedbackLists fakeList = new FakeFeedbackLists();
            var fakeFeedback = new FakeFeedback();
            fakeFeedback.Status = FeedbackStatus.Scheduled;
            fakeList.Feedback.Add(fakeFeedback);
            var fakeParams = new List<string>() { "1" };
            //Act 
            var sut = new ChangeFeedbackCommand(fakeParams, fakeList);
            //Assert
            sut.Execute();
        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void ChangeFeedbackCommand_Throws_Exception_When_Change_IS_Invalid()
        {
            FakeFeedbackLists fakeList = new FakeFeedbackLists();
            var fakeFeedback = new FakeFeedback();
            fakeFeedback.Status = FeedbackStatus.Scheduled;
            fakeList.Feedback.Add(fakeFeedback);
            var fakeParams = new List<string>() { "1", "asdasd" };

            //Act & Assert
            var sut = new ChangeFeedbackCommand(fakeParams, fakeList);
            sut.Execute();
        }

        [TestMethod]
        public void ChangeFeedbackCommand_Correctly_Changes_Properties()
        {
            var fakeList = new Lists();
            var fakeFeedback = new Feedback("Fake Feedback", "Fakest of them all", 666, FeedbackStatus.Scheduled);
            fakeFeedback.Status = FeedbackStatus.Scheduled;
            fakeFeedback.Rating = 10;
            fakeFeedback.Id = 1;
            fakeList.Feedback.Add(fakeFeedback);
            var fakeParams = new List<string>() { "1", "unscheduled" };
            var fakeParams2 = new List<string>() { "1", "5" };
            //Act
            var sut = new ChangeFeedbackCommand(fakeParams, fakeList);
            sut.Execute();
            var sut2 = new ChangeFeedbackCommand(fakeParams2, fakeList);
            sut2.Execute();
            //Act & Assert
            Assert.AreEqual(FeedbackStatus.Unscheduled, fakeFeedback.Status);
            Assert.AreEqual(5, fakeFeedback.Rating);
        }
    }

    class FakeFeedbackLists : ILists
    {
        private IList<IFeedback> feedbacks = new List<IFeedback>();
        public IList<IMember> Members => throw new NotImplementedException();

        public IList<ITeam> Teams => throw new NotImplementedException();

        public IList<IBug> Bugs => throw new NotImplementedException();
        public IList<IStory> Story => throw new NotImplementedException();
        public IList<IFeedback> Feedback
        {
            get => this.feedbacks;
        }

        public void AddMember(IMember members)
        {
            throw new NotImplementedException();
        }

        public void AddTeam(ITeam teams)
        {
            throw new NotImplementedException();
        }

        public void AddWorkItem(IBug bug)
        {
            throw new NotImplementedException();
        }

        public void AddWorkItem(IFeedback feedback)
        {
            throw new NotImplementedException();
        }

        public void AddWorkItem(IStory story)
        {
            throw new NotImplementedException();
        }
    }
    class FakeFeedback : IFeedback
    {
        public int Rating { get; set; }
        public FeedbackStatus Status { get; set; }

        public long Id { get; set; }

        public string Title => throw new NotImplementedException();

        public string Description => throw new NotImplementedException();

        public void Assign(IMember member)
        {
            throw new NotImplementedException();
        }

        public void CommentAdd(IComment comment)
        {
            throw new NotImplementedException();
        }

        public void HistoryAdd(string history)
        {
            throw new NotImplementedException();
        }

        public void Unassign()
        {
            throw new NotImplementedException();
        }
    }
}

