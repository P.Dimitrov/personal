package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	cards := newDeck()
	if len(cards) != 52 {
		t.Errorf("Expected 52 cards, but got %d instead.", len(cards))
	}

	if cards[0] != "Ace of Spades" {
		t.Errorf("Expected First Card to be Ace of Spades, but got %s isntead.", cards[0])
	}

	if cards[len(cards)-1] != "King of Clubs" {
		t.Errorf("Expected First Card to be King of Clubs, but got %s isntead.", cards[len(cards)-1])
	}
}

func TestSaveToDeckAndNewDeckFromFile(t *testing.T) {
	os.Remove("_decktesting")

	deck := newDeck()
	deck.saveToFile("_decktesting")

	loadedDeck := readFromFile("_decktesting")

	if len(loadedDeck) != 52 {
		t.Errorf("Expected 52 cards, but got %d instead.", len(loadedDeck))
	}

	os.Remove("_decktesting")
}
